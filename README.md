# SVMTL

The C++ Scalar, Vector, Matrix and Tensor class portable reference library
was available from http://www.netwood.net/~edwin/svmtl/

The svmtl directory was archived using tar and compressed using gzip.
After you download the directory, just type

```
	gunzip svmtl.tgz
	tar xvf svmtl.tar
	cd svmtl/src
	make
```

to compile the library then type

```
	cd ../example
	make tutorial
	./tutorial
```

to compile and run the tutorial program.
Please read the comments in .../svmtl/examples/tutorial.cc 

The portable reference library is not a high-performance implementation.
It exists primarily to help SVMT class library developers verify
their own high performance implementations of the SVMT library standard.
Eventually, you should be able to purchase a high-performance implementation
from one or more SVMT class library developers.
But, until then, you must make due with the portable reference library.

Currently, only floating-point type double is supported
by the portable reference library and it will probably remain that way
until the SVMT class library standard stabilizes
but provisions have been made to add the other types easily.
Please read .../svmt/bin/genclass if you want to add another type.

The portable reference library is incomplete and virtually untested.
I have compiled it with several different C++ compilers as documented
in .../svmtl/src/Makefile

I would appreciate it very much if anyone would try to compile
the portable reference library the let me know which compiler and
operating system they used and include any warning or error messages.

Thanks in advance, Bob Tisdale <edwin@netwood.net>
