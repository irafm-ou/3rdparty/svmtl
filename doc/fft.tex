\documentclass{article}
\title{Discrete Fourier Transform}
\author{E. Robert Tisdale}
\begin{document}
\bibliographystyle{plain}
\maketitle

\begin{abstract}
A prime factor algorithm is used to compute
the Discrete Fourier Transform (DFT) of a complex vector recursively.
\end{abstract}

\section{Introduction}
A forward (direct) discrete Fourier transform
$y = xW_{n}^{-}$ where ${W_{n}^{-}}_{jk} = e^{-i2\pi jk/n}$ and
a reverse (inverse) discrete Fourier transform
$y = xW_{n}^{+}$ where ${W_{n}^{+}}_{jk} = e^{+i2\pi jk/n}$ and
$0\!<\!n$ is the length of both complex vectors $x$ and $y$.
The computational complexity is of order $n^{2}$ if $n$ is a prime number
but can be reduced if $n$ can be factored into small prime numbers.

Suppose that $n = pq$ where $p$ is the smallest prime number that can be
factored out of $n$.  Then
\begin{eqnarray}
y_{k}	& = &	\sum_{j=0}^{n-1} x_{j}\cdot e^{\mp i2\pi jk/n}\nonumber	\\
	& = &	\sum_{\ell=0}^{p-1}
		\sum_{j=0}^{q-1} x_{pj+\ell}\cdot
		e^{\mp i2\pi(pj+\ell)k/n}\nonumber\\
	& = &	\sum_{\ell=0}^{p-1}e^{\mp i2\pi\ell k/n}\cdot
		\sum_{j=0}^{q-1} x_{pj+\ell}\cdot e^{\mp i2\pi jk/q}
\label{eqn:sft}
\end{eqnarray}
$\forall k \in \{0, 1, \ldots, n-1\}$ and
\begin{eqnarray}
y_{qh+k}& = &	\sum_{\ell=0}^{p-1}e^{\mp i2\pi\ell(qh+k)/n}\cdot
		\sum_{j=0}^{q-1} x_{pj+\ell}\cdot
		e^{\mp i2\pi j(qh+k)/q}\nonumber\\
	& = &	\sum_{\ell=0}^{p-1}e^{\mp i2\pi h\ell/p}\cdot
		e^{\mp i2\pi\ell k/n}\cdot
		\sum_{j=0}^{q-1} x_{pj+\ell}\cdot e^{\mp i2\pi jk/q}
\label{eqn:fft}
\end{eqnarray}
$\forall h \in \{0, 1, \ldots, p-1\}$ and $\forall k \in \{0, 1, \ldots, q-1\}$.

If $Y$ is a $p\!\times\!q$ matrix view of vector $y$ where $Y_{hk} = y_{qh+k}$
and $X$ is a $q\!\times\!p$ matrix view of vector $x$
where $X_{j\ell} = x_{pj+\ell}$,
then the discrete Fourier transform
\begin{equation}
Y = W_{p}^{\mp}\left(M_{n}^{\mp}\!*\!\left(X^{T}W_{q}^{\mp}\right)\right)
\label{eqn:row}
\end{equation}
can be computed recursively by applying the discrete Fourier transform
$W_{q}^{\mp}$ where ${W_{q}^{\mp}}_{jk} = e^{\mp i2\pi jk/q}$
to each of the $p$ rows of matrix $X^{T}$,
multiplying matrix $T = X^{T}W_{q}^{\mp}$ element by element
by the $p\!\times\!q$ matrix $M_{n}^{\mp}$
where ${M_{n}^{\mp}}_{\ell k} = e^{\mp i2\pi \ell k/n}$
and applying the discrete Fourier transform $W_{p}^{\mp}$
where ${W_{p}^{\mp}}_{hl} = e^{\mp i2\pi hl/p}$ to each of the $q$ columns
of matrix $M_{n}^{\mp}\!*\!T$.

The transform may be computed in place
by transposing matrix $X$ in place recursively before all other processing.
If a digit reverse algorithm is used instead of recursive transposition,
no element $x_{j}$ is moved to another location $x_{k}$ in vector $x$
more than once. Offset
\begin{equation}
j = \sum_{i=0}^{\ell-1} d_{\ell-1-i} \prod_{h=0}^{i-1} r_{\ell-1-h}
\end{equation}
is computed by first decomposing offset
\begin{equation}
k = \sum_{i=0}^{\ell-1} d_{i} \prod_{h=0}^{i-1} r_{h}
\end{equation}
into digits $0 \leq d_{i} < r_{i}$ of the mixed radix representation 
where the $\ell$ radices are the factors of $n = \prod_{i=0}^{\ell-1} r_{i}$.
In this case, the radices are the prime factors of $n$ in order
from least to greatest.

The total number of complex floating-point multiplications $C$
can be calculated from the length
\begin{equation}
n = n_{0} = \prod_{j=0}^{\ell-1} p_{j}
\end{equation}
of the initial vector and the length
\begin{equation}
n_{k} = \prod_{j=k}^{\ell-1} p_{j}
\end{equation}
of the vectors after $k$ reductions
where the $p_{j}$ are the $\ell$ prime factors of $n$.

The total number of complex floating-point multiplications
\begin{equation}
C = C_{0} = p_{0}^{2}n_{1} + n_{0} + p_{0}C_{1}
	  = n_{0}\left(p_{0} + 1 + \frac{C_{1}}{n_{1}}\right)
\end{equation}
for the initial function call is calculated by substituting
the recursion equation
\begin{equation}
\frac{C_{k}}{n_{k}} = p_{k} + 1 + \frac{C_{k+1}}{n_{k+1}}
\end{equation}
up through the final equation
\begin{equation}
\frac{C_{\ell-1}}{n_{\ell-1}} = n_{\ell-1} = p_{\ell-1}
\end{equation}
which yields
\begin{equation}
C = n\left(\sum_{k=0}^{\ell-1}\left(p_{k}+1\right) - 1\right).
\end{equation}
If the prime factors $p_{j} = p$ are all identical, then
\begin{equation}
C = n\left(\ell\left(p+1\right) - 1\right).
\end{equation}
The computational complexity is
${\cal O}\left(n\cdot\log_{p}\left(n\right)p\right)$
or ${\cal O}\left(n^{2}\right)$ if $p = n$
or ${\cal O}\left(n^{3/2}\right)$ if $p^{2} = n$
or ${\cal O}\left(n\cdot\lg\left(n\right)\right)$ if $p = 2$.
The computational complexity of a radix~$p > 2$ algorithm is always greater
than a radix~$2$ algorithm by a factor of $p/\lg\left(p\right)$
but a radix~$p > 2$ implementation may actually be faster
than a radix~$2$ implementation.

\section{Real to Complex FFTs}
When the source vector $x$ is real, the destination vector $y$ is complex but
$y_{0}$ is real, $y_{n-k} = y_{k}^{*}$ and $y_{n/2}$ is real if $n$ is even.
The fast Fourier transform can be computed in-place and returned
with the imaginary part of $y_{0}$ set to the real part of $y_{n/2}$
if $n$ is even or to the imaginary part of $y_{(n-1)/2}$ if $n$ is odd.

The discrete Fourier transform $W_{q}^{\mp}$ is applied in-place
to each of the $p$ rows of real matrix $X^{T}$
to form the product $T = X^{T}W_{q}^{\mp}$.
Because $T_{\ell, 0}$ and $T_{\ell, q/2}$ are real,
the first $q/2\!+\!1$ columns of complex matrix $T$ are packed
so that each row of real matrix $X^{T}$ contains
\begin{equation}
\begin{array}{lllcl}
\Re\{T_{\ell, 0}\}, \Re\{T_{\ell, q/2}\} & T_{\ell, 1} & T_{\ell, 2}
 & \ldots & T_{\ell, q/2-1}
\end{array}
\end{equation}
if $q$ is even
but the first $(q\!+\!1)/2$ columns of complex matrix $T$ are packed
so that each row of real matrix $X^{T}$ contains
\begin{equation}
\begin{array}{lllcll}
\Re\{T_{\ell, 0}\}, \Im\{T_{\ell, (q-1)/2}\} & T_{\ell, 1} & T_{\ell, 2}
 & \ldots & T_{\ell, (q-3)/2} & \Re\{T_{\ell, (q-1)/2}\}
\end{array}
\end{equation}
if $q$ is odd.
The transform can be completed
for the missing columns of complex matrix $T$ using
\begin{eqnarray}
y_{qh+q-k} & = & \sum_{\ell=0}^{p-1}e^{\mp i2\pi\ell h/p}\cdot
	e^{\mp i2\pi\ell(q-k)/n}\cdot
	\sum_{j=0}^{q-1} x_{pj+\ell}\cdot e^{\mp i2\pi j(q-k)/q}\nonumber\\
	 & = & \left(\sum_{\ell=0}^{p-1}e^{\mp i2\pi\ell(p-1-h)/p}\cdot
	e^{\mp i2\pi\ell k/n}\cdot
	\sum_{j=0}^{q-1} x_{pj+\ell}\cdot e^{\mp i2\pi jk/q}\right)^{*}
								\nonumber\\
	 & = & \left(\sum_{\ell=0}^{p-1}e^{\mp i2\pi\ell(p-1-h)/p}\cdot
	{M_{n}^{\mp}}_{\ell k}T_{\ell k}\right)^{*} = y_{q(p-1-h) + k}^{*}.
\end{eqnarray}

If both $p$ and $q$ are odd,
each of the first $(q\!+\!1)/2$ columns of matrix $T$ is multiplied
by the corresponding column of matrix $M_{n}^{\mp}$ element by element
then the discrete Fourier transform $W_{p}^{\mp}$ is applied
before the column is stored back into real matrix $X^{T}$
with row $h$ of complex matrix $Y$
stored in rows $2h$ and $2h\!+\!1$ of real matrix $X^{T}$
\begin{equation}
\begin{array}{clcll}
\Re\{Y_{0, 0}\},\Im\{Y_{0, (q-1)/2}\} & Y_{0, 1} & \ldots &
Y_{0, (q-3)/2} & \Re\{Y_{0, (q-1)/2}\}			\\
\Im\{Y_{(p-1)/2, 0}\}, \Re\{Y_{0, (q+1)/2}\}\;\;\;\;\;\;\;\;\: &
Y_{0, q-1} & \ldots &
Y_{0, (q+3)/2} & \Im\{Y_{0, (q+1)/2}\}		\\
\Re\{Y_{1, 0}\},\Im\{Y_{1, (q-1)/2}\} & Y_{1, 1} & \ldots &
Y_{1, (q-3)/2} & \Re\{Y_{1, (q-1)/2}\}			\\
\Im\{Y_{1, 0}\}, \Re\{Y_{1, (q+1)/2}\} & Y_{1, q-1} &
\ldots & Y_{1, (q+3)/2} & \Im\{Y_{1, (q+1)/2}\}		\\
& & \vdots & &	\\
\Re\{Y_{h, 0}\},\Im\{Y_{h, (q-1)/2}\} & Y_{h, 1} & \ldots &
Y_{h, (q-3)/2} & \Re\{Y_{h, (q-1)/2}\}			\\
\Im\{Y_{h, 0}\}, \Re\{Y_{h, (q+1)/2}\} & Y_{h, q-1} &
\ldots & Y_{h, (q+3)/2} & \Im\{Y_{h, (q+1)/2}\}		\\
& & \vdots & &	\\
\Re\{Y_{(p-3)/2, 0}\},\Im\{Y_{(p-3)/2, (q-1)/2}\} & Y_{(p-3)/2, 1} & \ldots &
Y_{(p-3)/2, (q-3)/2} & \Re\{Y_{(p-3)/2, (q-1)/2}\}	\\
\Im\{Y_{(p-3)/2, 0}\}, \Re\{Y_{(p-3)/2, (q+1)/2}\} & Y_{(p-3)/2, q-1} & \ldots &
Y_{(p-3)/2, (q+3)/2} & \Im\{Y_{(p-3)/2, (q+1)/2}\}	\\
\Re\{Y_{(p-1)/2, 0}\},\Im\{Y_{(p-1)/2, (q-1)/2}\} & Y_{(p-1)/2, 1} & \ldots &
Y_{(p-1)/2, (q-3)/2} & \Re\{Y_{(p-1)/2, (q-1)/2}\}
\end{array}
\end{equation}
but if $p$ is even and $q$ is odd,
\begin{equation}
\begin{array}{clcll}
\Re\{Y_{0, 0}\},\Im\{Y_{0, (q-1)/2}\} & Y_{0, 1} & \ldots &
Y_{0, (q-3)/2} & \Re\{Y_{0, (q-1)/2}\}			\\
\Re\{Y_{p/2, 0}\}, \Re\{Y_{0, (q+1)/2}\}\;\;\: & Y_{0, q-1} & \ldots &
Y_{0, (q+3)/2} & \Im\{Y_{0, (q+1)/2}\}		\\
\Re\{Y_{1, 0}\},\Im\{Y_{1, (q-1)/2}\} & Y_{1, 1} & \ldots &
Y_{1, (q-3)/2} & \Re\{Y_{1, (q-1)/2}\}			\\
\Im\{Y_{1, 0}\}, \Re\{Y_{1, (q+1)/2}\} & Y_{1, q-1} &
\ldots & Y_{1, (q+3)/2} & \Im\{Y_{1, (q+1)/2}\}		\\
& & \vdots & &	\\
\Re\{Y_{h, 0}\},\Im\{Y_{h, (q-1)/2}\} & Y_{h, 1} & \ldots &
Y_{h, (q-3)/2} & \Re\{Y_{h, (q-1)/2}\}			\\
\Im\{Y_{h, 0}\}, \Re\{Y_{h, (q+1)/2}\} & Y_{h, q-1} &
\ldots & Y_{h, (q+3)/2} & \Im\{Y_{h, (q+1)/2}\}		\\
& & \vdots & &	\\
\Re\{Y_{p/2-1, 0}\},\Im\{Y_{p/2-1, (q-1)/2}\} & Y_{p/2-1, 1} & \ldots &
Y_{p/2-1, (q-3)/2} & \Re\{Y_{p/2-1, (q-1)/2}\}	\\
\Im\{Y_{p/2-1, 0}\}, \Re\{Y_{p/2-1, (q+1)/2}\} & Y_{p/2-1, q-1} & \ldots &
Y_{p/2-1, (q+3)/2} & \Im\{Y_{p/2-1, (q+1)/2}\}
\end{array}
\end{equation}
and if $p = 2$ and $q$ is odd,
\begin{equation}
\begin{array}{clcll}
\Re\{Y_{0, 0}\},\Im\{Y_{0, (q-1)/2}\} & Y_{0, 1} & \ldots &
Y_{0, (q-3)/2} & \Re\{Y_{0, (q-1)/2}\}			\\
\Re\{Y_{1, 0}\}, \Re\{Y_{0, (q+1)/2}\} & Y_{0, q-1} & \ldots &
Y_{0, (q+3)/2} & \Im\{Y_{0, (q+1)/2}\}.
\end{array}
\end{equation}
The real and imaginary parts
of complex elements $Y_{h, (q+1)/2}$ through $Y_{h, q-1}$
are stored in reverse order so that the elements
in columns $2$ through $q-1$ of rows $2h\!+\!1$ of real matrix $X^{T}$
may simply be reversed.
The real elements in row $2h$ and column $1$ are swapped
with the real elements in row $2h\!+\!1$ and column $0$ of real matrix $X^{T}$
$\forall h \in \{0, 1, \ldots, p/2\!-\!1\}$ if $p$ is even or
$\forall h \in \{0, 1, \ldots, (p\!-\!3)/2\}$ if $p$ is odd.
Then, if $p$ is odd, the element in row $(p\!-\!1)/2$ is swapped
with the element in row $0$ of column $1$ of real matrix $X^{T}$.

\pagebreak
If both $p$ and $q$ are even,
each of the first $q/2\!+\!1$ columns of matrix $T$ is multiplied
by the corresponding column of matrix $M_{n}^{\mp}$ element by element
then the discrete Fourier transform $W_{p}^{\mp}$ is applied
before row $h$ of complex matrix $Y$ is stored
back into rows $2h$ and $2h\!+\!1$ of real matrix $X^{T}$
\begin{equation}
\begin{array}{cllcl}
\Re\{Y_{0, 0}\},\Re\{Y_{0, q/2}\}& Y_{0, 1}& Y_{0, 2}& \ldots&
Y_{0, q/2-1}	\\
\Re\{Y_{p/2, 0}\},\Im\{Y_{0, q/2}\}\ \ & Y_{0, q-1}& Y_{0, q-2}& \ldots
& Y_{0, q/2+1}	\\
\Re\{Y_{1, 0}\},\Re\{Y_{1, q/2}\}& Y_{1, 1}& Y_{1, 2}& \ldots&
Y_{1, q/2-1}	\\
\Im\{Y_{1, 0}\},\Im\{Y_{1, q/2}\}& Y_{1, q-1}& Y_{1, q-2}& \ldots&
Y_{1, q/2+1}	\\
& & & \vdots &	\\
\Re\{Y_{h, 0}\},\Re\{Y_{h, q/2}\}& Y_{h, 1}& Y_{h, 2}& \ldots&
Y_{h, q/2-1}	\\
\Im\{Y_{h, 0}\},\Im\{Y_{h, q/2}\}& Y_{h, q-1}& Y_{h, q-2}& \ldots&
Y_{h, q/2+1}	\\
& & & \vdots &	\\
\Re\{Y_{p/2-1, 0}\},\Re\{Y_{p/2-1, q/2}\}& Y_{p/2-1, 1}&
Y_{p/2-1, 2}& \ldots & Y_{p/2-1, q/2-1}		\\
\Im\{Y_{p/2-1, 0}\},\Im\{Y_{p/2-1, q/2}\}& Y_{p/2-1, q-1}&
Y_{p/2-1, q-2}& \ldots & Y_{p/2-1, q/2+1}
\end{array}
\end{equation}
and if $p = 2$ and $q$ is even
\begin{equation}
\begin{array}{cllcl}
\Re\{Y_{0, 0}\},\Re\{Y_{0, q/2}\}& Y_{0, 1}& Y_{0, 2}& \ldots& Y_{0, q/2-1} \\
\Re\{Y_{1, 0}\},\Im\{Y_{0, q/2}\}& Y_{0, q-1}& Y_{0, q-2}& \ldots& Y_{0, q/2+1}.
\end{array}
\end{equation}
Only the first $p/2$ rows of column $q/2$ are stored back into column $1$
of real matrix $X^{T}$ because $Y_{p-1-h, q/2} = Y_{h, q/2}^{*}$.
The real and imaginary parts
of complex elements $Y_{h, q/2+1}$ through $Y_{h, q-1}$
are stored in reverse order so that the elements
in columns $2$ through $q-1$ of rows $2h\!+\!1$ of real matrix $X^{T}$
may simply be reversed.  The real elements in row $2h$ and column $1$ are
swapped with the elements in row $2h\!+\!1$ and column $0$
of real matrix $X^{T}$ $\forall h \in \{0, 1, \ldots, p/2\!-\!1\}$.

\section{Complex to Real FFTs}
The complex to real discrete Fourier Transform is computed
by reversing the real to complex discrete Fourier Transform.

Equation~\ref{eqn:sft} becomes
\begin{eqnarray}
x_{j}	& = &	\sum_{k=0}^{n-1} y_{k}\cdot e^{\mp i2\pi kj/n}	\nonumber\\
	& = &	\sum_{k=0}^{q-1}
		\sum_{h=0}^{p-1} y_{qh+k}\cdot
		e^{\mp i2\pi(qh+k)j/n}				\nonumber\\
	& = &	\sum_{k=0}^{q-1}\left(
		\sum_{h=0}^{p-1} e^{\mp i2\pi jh/p}\cdot y_{qh+k}\right)
		e^{\mp i2\pi kj/n}
\end{eqnarray}
$\forall j \in \{0, 1, \ldots, n-1\}$.

Equation~\ref{eqn:fft} becomes
\begin{eqnarray}
x_{pj+\ell}& = &\sum_{k=0}^{q-1}\left(
		\sum_{h=0}^{p-1}
		e^{\mp i2\pi (pj+\ell)h/p}\cdot y_{qh+k}\right)
		e^{\mp i2\pi k(pj+\ell)/n}			\nonumber\\
	& = &	\sum_{k=0}^{q-1}\left(\left(
		\sum_{h=0}^{p-1} e^{\mp i2\pi\ell h/p}\cdot y_{qh+k}\right)
		e^{\mp i2\pi\ell k/n}\right) e^{\mp i2\pi kj/q}
\end{eqnarray}
$\forall \ell \in \{0, 1, \ldots, p-1\}$ and
$\forall j \in \{0, 1, \ldots, q-1\}$.

Equation~\ref{eqn:row} becomes
\begin{equation}
X = \left(\left(\left(W_{p}^{\mp}Y\right)\!*\!M_{n}^{\mp}\right)
W_{q}^{\mp}\right)^{T}.
\end{equation}
The transform may be computed in place by transposing matrix $Y$ in place
recursively after all other processing.
If a digit reverse algorithm is used instead of recursive transposition,
no element $x_{k}$ is moved to another location $x_{j}$ in vector $x$
more than once. Offset
\begin{equation}
k = \sum_{i=0}^{\ell-1} d_{i} \prod_{h=0}^{i-1} r_{h}
\end{equation}
is computed by first decomposing offset
\begin{equation}
j = \sum_{i=0}^{\ell-1} d_{\ell-1-i} \prod_{h=0}^{i-1} r_{\ell-1-h}
\end{equation}
into digits $0 \leq d_{i} < r_{i}$ of the mixed radix representation 
where the $\ell$ radices are the factors of $n = \prod_{i=0}^{\ell-1} r_{i}$.
In this case, the radices are the prime factors of $n$ in order
from least to greatest.

% \bibliography{dft}
\end{document}

