TARGET = $$PWD/svmtl

TEMPLATE = lib
CONFIG += staticlib

include($$PWD/../../../config/config_noqt.pri)
include($$PWD/../../../config/config_cpp17.pri)

include($$PWD/../../../config/config_svmtl.pri)
