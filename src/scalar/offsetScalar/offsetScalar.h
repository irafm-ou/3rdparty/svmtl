#ifndef _offsetScalar_h
#define _offsetScalar_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/
#include<offsetHandle.h>
#include<iostream>

class offsetSubScalar {
  private:
  // Representation
  offsetHandle	H;
  Offset	O;
  public:
  // Constructors
		offsetSubScalar(const offsetHandle& h, Offset o);
		offsetSubScalar(const offsetSubScalar& s);
		~offsetSubScalar(void);
  // Functions
  const
  offsetHandle&	handle(void) const;
  Offset	offset(void) const;
  bool		 empty(void) const;
  // Operators
		operator Offset (void) const;
  offsetSubScalar&
		operator  =(Offset j);
  offsetSubScalar&
		operator  =(const offsetSubScalar& s);
  };

// Constructors
inline		offsetSubScalar::offsetSubScalar(
  const offsetHandle& h, Offset o): H(h), O(o) { }
inline		offsetSubScalar::offsetSubScalar(const offsetSubScalar& s):
  H(s.H), O(s.O) { }
inline		offsetSubScalar::~offsetSubScalar(void) { }

// Functions
inline
const
offsetHandle&	offsetSubScalar::handle(void) const { return H; }
inline
Offset		offsetSubScalar::offset(void) const { return O; }
inline
bool		offsetSubScalar::empty(void) const { return handle().empty(); }

// Operators
inline		offsetSubScalar::operator Offset (void) const {
  return H.get(offset()); }
inline
std::istream&	operator >>(std::istream& is,       offsetSubScalar& s) {
  Offset	x;
  if (is >> x) {
    ((offsetHandle)s.handle()).put(s.offset(), x);
    }
  return is;
  }
inline
std::ostream&	operator <<(std::ostream& os, const offsetSubScalar& s) {
  return os << s.handle().get(s.offset());
  }
inline
offsetSubScalar&
		offsetSubScalar::operator  =(Offset j) {
  H.put(offset(), j); return *this; }
inline
offsetSubScalar&
		offsetSubScalar::operator  =(const offsetSubScalar& s) {
  return operator =((Offset)s); }

class offsetSubArray0: public offsetSubScalar {
  public:
  // Constructors
		offsetSubArray0(Offset* p, Offset o);
		offsetSubArray0(const offsetSubArray0& s);
		~offsetSubArray0(void);
  // Operators
  offsetSubArray0&
		operator  =(Offset j);
  offsetSubArray0&
		operator  =(const offsetSubScalar& s);
  };

// Constructors
inline		offsetSubArray0::offsetSubArray0(Offset* p, Offset o):
  offsetSubScalar(offsetHandle(p), o) { }
inline		offsetSubArray0::offsetSubArray0(const offsetSubArray0& s):
  offsetSubScalar(s) { }
inline		offsetSubArray0::~offsetSubArray0(void) { }

// Operators
inline
offsetSubArray0&
		offsetSubArray0::operator  =(Offset j) {
  offsetSubScalar::operator =(j); return *this; }
inline
offsetSubArray0&
		offsetSubArray0::operator  =(const offsetSubScalar& s) {
  offsetSubScalar::operator =(s); return *this; }

#endif /* _offsetScalar_h */

