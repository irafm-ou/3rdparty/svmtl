#ifndef _boolScalar_h
#define _boolScalar_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/
#include<boolHandle.h>
#include<iostream>
class boolSubVector;	class boolSubArray1;	class boolVector;
class boolSubSquare;				class boolSquare;
class boolSubMatrix;	class boolSubArray2;	class boolMatrix;
class boolSubTensor;	class boolSubArray3;	class boolTensor;

class boolSubScalar {
  private:
  // Representation
  boolHandle	H;
  Offset	O;
  public:
  // Constructors
		boolSubScalar(const boolHandle& h, Offset o);
		boolSubScalar(const boolSubScalar& s);
		~boolSubScalar(void);
  // Functions
  const
  boolHandle&
		handle(void) const;
  Offset	offset(void) const;
  bool		 empty(void) const;
  const
  boolSubVector
	     subvector(Extent n = 1) const;
  const
  boolSubMatrix
	     submatrix(Extent n = 1, Extent m = 1) const;
  const
  boolSubTensor
	     subtensor(Extent n = 1, Extent m = 1, Extent l = 1) const;
  // Operators
		operator bool (void) const;
  boolSubScalar&
		operator  =(bool b);
  boolSubScalar&
		operator  =(const boolSubScalar& s);
  };

// Constructors
inline		boolSubScalar::boolSubScalar(const boolHandle& h, Offset o):
  H(h), O(o) { }
inline		boolSubScalar::boolSubScalar(const boolSubScalar& s):
  H(s.H), O(s.O) { }
inline		boolSubScalar::~boolSubScalar(void) { }

// Functions
inline
const
boolHandle&	boolSubScalar::handle(void) const { return H; }
inline
Offset		boolSubScalar::offset(void) const { return O; }
inline
bool		boolSubScalar::empty(void) const { return handle().empty(); }

// Operators
inline		boolSubScalar::operator bool (void) const {
  return H.get(offset()); }
inline
std::istream&	operator >>(std::istream& is,       boolSubScalar& s) {
  bool		x;
  if (is >> x) {
    ((boolHandle)s.handle()).put(s.offset(), x);
    }
  return is;
  }
inline
std::ostream&	operator <<(std::ostream& os, const boolSubScalar& s) {
  return os << s.handle().get(s.offset());
  }
inline
boolSubScalar&	boolSubScalar::operator  =(bool b) {
  H.put(offset(), b); return *this; }
inline
boolSubScalar&	boolSubScalar::operator  =(const boolSubScalar& s) {
  return operator = ((bool)s); }

class boolSubArray0: public boolSubScalar {
  public:
  // Constructors
		boolSubArray0(bool* p, Offset o);
		boolSubArray0(const boolSubArray0& s);
		~boolSubArray0(void);
  // Functions
  const
  boolSubArray1
	     subvector(Extent n = 1) const;
  const
  boolSubArray2
	     submatrix(Extent n = 1, Extent m = 1) const;
  const
  boolSubArray3
	     subtensor(Extent n = 1, Extent m = 1, Extent l = 1) const;
  // Operators
  boolSubArray0&
		operator  =(bool b);
  boolSubArray0&
		operator  =(const boolSubScalar& s);
  };

// Constructors
inline		boolSubArray0::boolSubArray0(bool* p, Offset o):
  boolSubScalar(boolHandle(p), o) { }
inline		boolSubArray0::boolSubArray0(const boolSubArray0& s):
  boolSubScalar(s) { }
inline		boolSubArray0::~boolSubArray0(void) { }

// Operators
inline
boolSubArray0&	boolSubArray0::operator  =(bool b) {
  boolSubScalar::operator =(b); return *this; }
inline
boolSubArray0&	boolSubArray0::operator  =(const boolSubScalar& s) {
  boolSubScalar::operator =(s); return *this; }

#endif /* _boolScalar_h */

