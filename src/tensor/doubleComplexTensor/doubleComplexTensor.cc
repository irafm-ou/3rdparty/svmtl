/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleComplexTensor.h>


// Functions
doubleComplexSubTensor&
  doubleComplexSubTensor::reverse1(void) {
  doubleComplexSubTensor&      T = *this;
  Extent                        l = extent3();
  for (Offset h = 0; h < l; h++)
    T[h].reverse1();
  return *this;
  }
doubleComplexSubTensor&
  doubleComplexSubTensor::reverse2(void) {
  doubleComplexSubTensor&      T = *this;
  Extent                        l = extent3();
  for (Offset h = 0; h < l; h++)
    T[h].reverse2();
  return *this;
  }
doubleComplexSubTensor&
  doubleComplexSubTensor::reverse3(void) {
  Extent                        l = extent3();
  for (Offset h = 0; h < l/2; h++)
    swap(h, l-1 - h);
  return *this;
  }

doubleSubTensor&
  doubleSubTensor::cdft(int sign) {	// real to complex dft
  doubleSubTensor&
		T = *this;
  Extent	n = T.extent1();
  if (1 < n) {
    doubleComplexVector
		w(n);
		w.twiddle(sign);
    Extent	m = T.extent2();
    Extent	l = T.extent3();
    for (Offset h = 0; h < l; h++)
      for (Offset i = 0; i < m; i++)
	T[h][i].transpose(false).rcfdft(w);
    }
  return T;
  }

doubleSubTensor&
  doubleSubTensor::rdft(int sign) {	// complex to real dft
  doubleSubTensor&
		T = *this;
  Extent	n = T.extent1();
  if (1 < n) {
    doubleComplexVector
		w(n);
		w.twiddle(sign);
    Extent	m = T.extent2();
    Extent	l = T.extent3();
    for (Offset h = 0; h < l; h++)
      for (Offset i = 0; i < m; i++)
	T[h][i].crfdft(w).transpose(true);
    }
  return T;
  }

doubleComplexSubTensor&
  doubleComplexSubTensor::dft(int sign) {	// complex to complex dft
  doubleComplexSubTensor&
		T = *this;
  Extent	n = T.extent1();
  if (1 < n) {
    doubleComplexVector
		w(n);
		w.twiddle(sign);
    Extent	m = T.extent2();
    Extent	l = T.extent3();
    for (Offset h = 0; h < l; h++)
      for (Offset i = 0; i < m; i++)
	T[h][i].transpose().fdft(w);
    }
  return T;
  }

doubleComplexSubTensor&
  doubleComplexSubTensor::swap(doubleComplexSubTensor& U) {
  doubleComplexSubTensor	T = (*this);
  Extent			l = extent3();
  for (Offset h = 0; h < l; h++) {
    doubleComplexSubMatrix	M = T[h];
    doubleComplexSubMatrix	N = U[h];
    M.swap(N);
    }
  return *this;
  }

doubleComplexSubTensor&
  doubleComplexSubTensor::rotate(Stride n) {
  doubleComplexSubTensor&	T = *this;
  Extent			l = extent3();
  for (Offset h = 0; h < l; h++) {
    doubleComplexSubMatrix	M = T[h];
    M.rotate(n);
    }
  return *this;
  }

doubleComplexSubTensor&
  doubleComplexSubTensor::shift(Stride n,const doubleComplex& s) {
  doubleComplexSubTensor&	T = *this;
  Extent			l = extent3();
  for (Offset h = 0; h < l; h++) {
    doubleComplexSubMatrix	M = T[h];
    M.shift(n,s);
    }
  return *this;
  }


// T.sum() = T_{h,i,0} + T_{h,i,1} + ... + T_{h,i,n-1}
const
doubleComplexMatrix
  doubleComplexSubTensor::sum(void) const {
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexMatrix		result(extent3(), extent2(), doubleComplex((double)0, (double)0));
  for (Offset h = 0; h < extent3(); h++) {
    result[h] += T[h].sum();
    }
  return result;
  }
const
doubleComplexTensor
  doubleComplexSubTensor::permutation(const offsetSubVector& p) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubTensor::permutation(const offsetSubVector&) const",
    extent1(), p.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor&	T = *this;
  Offset			l = extent3();
  Offset			m = extent2();
  Offset			n = extent1();
  doubleComplexTensor		U(l, m, n);
  for (Offset h = 0; h < l; h++) {
    const
    doubleComplexSubMatrix	M = T[h];
    doubleComplexSubMatrix	N = U[h];
    for (Offset i = 0; i < m; i++) {
      const
      doubleComplexSubVector	v = M[i];
      doubleComplexSubVector	w = N[i];
      for (Offset j = 0; j < n; j++) {
	w[j] = v[p[j]];
	}
      }
    }
  return U;
  }

const
boolTensor
  doubleComplexSubTensor::eq(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::eq(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor&	T = *this;
  boolTensor			result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h]	= T[h].eq(U[h]);
    }
  return result;
  }

const
boolTensor
  doubleComplexSubTensor::ne(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::ne(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor&	T = *this;
  boolTensor			result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].ne(U[h]);
    }
  return result;
  }

const
boolTensor
  doubleComplexSubTensor::eq(const doubleComplexSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::eq(const doubleComplexSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor&	T = *this;
  boolTensor			result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); ++h) {
    result[h] = T[h].eq(U[h]);
    }
  return result;
  }

const
boolTensor
  doubleComplexSubTensor::ne(const doubleComplexSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::ne(const doubleComplexSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor	T = *this;
  boolTensor			result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].ne(U[h]);
    }
  return result;
  }

Extent
  doubleComplexSubTensor::zeros(void) const {
  const
  doubleComplexSubTensor&	T = *this;
  Extent			result = 0;
  for (Offset h = 0; h < extent3(); h++) {
    result += T[h].zeros();
    }
  return result;
  }
/*
const
offsetVector
  doubleComplexSubTensor::index(void) const {
  offsetVector		result(extent3()*extent2()*extent1() - zeros());
  Offset		k = 0;
  for (Offset h = 0; h < extent3(); h++) {
    }
  return result;
  }

const
doubleComplexVector
  doubleComplexSubTensor::gather(const offsetSubVector& x) const {
  doubleComplexVector	result(x.extent());
  for (Offset h = 0; h < extent3(); h++) {
#ifdef SVMT_DEBUG_MODE
    svmt_check_vector_index_range(
      "doubleComplexSubTensor::gather(const offsetSubVector&) const",
      h, extent2());
#endif // SVMT_DEBUG_MODE
    }
  return result;
  }

doubleComplexSubTensor&
  doubleComplexSubTensor::scatter(const offsetSubVector& x,
    const doubleComplexSubVector& v) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubTensor::scatter(\n"
    "const offsetSubVector&, const doubleComplexSubVector&)",
    x.extent(), v.extent());
#endif // SVMT_DEBUG_MODE
  return *this;
  }

const
doubleComplexTensor
  doubleComplexSubTensor::aside(const doubleComplexSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubTensor::aside(const doubleComplexSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2());
#endif // SVMT_DEBUG_MODE
  doubleComplexTensor		result(extent3(), extent2(), extent1()
							 + U.extent1());
  result.sub((Offset)0, extent3(), (Stride)1,
	     (Offset)0, extent2(), (Stride)1,
	     (Offset)0, extent1(), (Stride)1) = *this;
  result.sub((Offset)0, extent3(), (Stride)1,
	     (Offset)0, extent2(), (Stride)1,
	     extent1(), U.extent1(), (Stride)1) = U;
  return result;
  }

const
doubleComplexTensor
  doubleComplexSubTensor::above(const doubleComplexSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubTensor::above(const doubleComplexSubTensor&) const",
    extent1(), U.extent1(), extent3(), U.extent3());
#endif // SVMT_DEBUG_MODE
  doubleComplexTensor		result(extent3(), extent2()
					      + U.extent2(), extent1());
  result.sub((Offset)0, extent3(), (Stride)1,
	     (Offset)0, extent2(), (Stride)1) = *this;
  result.sub((Offset)0, extent3(), (Stride)1,
	     extent2(), U.extent2(), (Stride)1) = U;
  return result;
  }

const
doubleComplexTensor
  doubleComplexSubTensor::afore(const doubleComplexSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubTensor::afore(const doubleComplexSubTensor&) const",
    extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexTensor		result(extent3()
				   + U.extent3(), extent2(), extent1());
  result.sub((Offset)0, extent3(), (Stride)1) = *this;
  result.sub(extent3(), U.extent3(), (Stride)1) = U
  return result;
  }

const
doubleComplexTensor
  doubleComplexSubVector::kron(const doubleComplexSubTensor& T) const {
  const
  doubleComplexSubVector&	v = *this;
  doubleComplexTensor		result(T.extent2(), extent()*T.extent1());

  for (Offset h = 0; h < extent3(); h++) {
    result[h] = v.kron(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
  doubleComplexSubTensor::kron(const doubleComplexSubTensor& U) const {
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexTensor	result(extent2()*U.extent2(), extent1()*U.extent1());
  for (Offset h = 0; h < extent3(); h++) {
    for (Offset j = 0; j < extent1(); j++) {
      result.sub(h*U.extent2(), U.extent2(), (Stride)1,
		 j*U.extent1(), U.extent1(), (Stride)1)
	= T[h][j]*U;
      }
    }
  return result;
  }

const
doubleComplexTensor
  doubleComplexSubTensor::kron(const doubleComplexSubVector& v) const {
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexTensor		result(T.extent2(), T.extent1()*extent());

  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].kron(v);
    }
  return result;
  }


const
doubleComplexTensor
  doubleSubVector::kron(const doubleComplexSubTensor& T) const {
  const
  doubleSubVector&	v = *this;
  doubleComplexTensor	result(T.extent2(), extent()*T.extent1());

  for (Offset h = 0; h < extent3(); h++) {
    result[h] = v.kron(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
  doubleSubTensor::kron(const doubleComplexSubTensor& U) const {
  const
  doubleSubTensor&	T = *this;
  doubleComplexTensor	result(extent2()*U.extent2(), extent1()*U.extent1());
  for (Offset h = 0; h < extent3(); h++) {
    for (Offset j = 0; j < extent1(); j++) {
      result.sub(h*U.extent2(), U.extent2(), (Stride)1,
		 j*U.extent1(), U.extent1(), (Stride)1)
	= T[h][j]*U;
      }
    }
  return result;
  }

const
doubleComplexTensor
  doubleSubTensor::kron(const doubleComplexSubVector& v) const {
  const
  doubleSubTensor&	T = *this;
  doubleComplexTensor	result(T.extent2(), T.extent1()*extent());

  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].kron(v);
    }
  return result;
  }

const
doubleComplexTensor
  doubleComplexSubVector::kron(const doubleSubTensor& T) const {
  const
  doubleComplexSubVector&	v = *this;
  doubleComplexTensor		result(T.extent2(), extent()*T.extent1());

  for (Offset h = 0; h < extent3(); h++) {
    result[h] = v.kron(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
  doubleComplexSubTensor::kron(const doubleSubTensor& U) const {
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexTensor	result(extent2()*U.extent2(), extent1()*U.extent1());
  for (Offset h = 0; h < extent3(); h++) {
    for (Offset j = 0; j < extent1(); j++) {
      result.sub(h*U.extent2(), U.extent2(), (Stride)1,
		 j*U.extent1(), U.extent1(), (Stride)1)
	= T[h][j]*U;
      }
    }
  return result;
  }

const
doubleComplexTensor
  doubleComplexSubTensor::kron(const doubleSubVector& v) const {
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexTensor		result(T.extent2(), T.extent1()*extent());

  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].kron(v);
    }
  return result;
  }
*/

const
doubleComplexTensor
  doubleComplexSubTensor::apply(const doubleComplex (*f)(const doubleComplex&)) const {
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].apply(f);
    }
  return result;
  }

const
doubleComplexTensor
  doubleComplexSubTensor::apply(      doubleComplex (*f)(const doubleComplex&)) const {
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].apply(f);
    }
  return result;
  }

const
doubleComplexTensor
  doubleComplexSubTensor::apply(      doubleComplex (*f)(      doubleComplex )) const {
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].apply(f);
    }
  return result;
  }

const
doubleComplexTensor
    exp(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = exp(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
    log(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = log(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
   sqrt(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = sqrt(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
    cos(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = cos(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
    sin(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = sin(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
    tan(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = tan(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
   acos(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = acos(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
   asin(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = asin(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
   atan(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = atan(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
   cosh(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = cosh(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
   sinh(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = sinh(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
   tanh(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = tanh(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
  acosh(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = acosh(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
  asinh(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = asinh(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
  atanh(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = atanh(T[h]);
    }
  return result;
  }

// Complex Functions
const
doubleComplexTensor
   conj(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = conj(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
  iconj(const doubleComplexSubTensor& T) {
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = iconj(T[h]);
    }
  return result;
  }

const
doubleComplexTensor
  polar(const doubleSubTensor& T, const doubleSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "polar(const doubleSubTensor&, const doubleSubTensor&)",
  T.extent3(), U.extent3(), T.extent2(), U.extent2(), T.extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = polar(T[h], U[h]);
    }
  return result;
  }

const
doubleTensor
   norm(const doubleComplexSubTensor& T) {
  doubleTensor			result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = norm(T[h]);
    }
  return result;
  }

// Operators

// minus
const
doubleComplexTensor
  doubleComplexSubTensor::operator -(void) const {
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = -T[h];
    }
  return result;
  }


// multiply
const
doubleComplexTensor
  doubleComplexSubTensor::operator *(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator *(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h]*U[h];
    }
  return result;
  }

// divide
const
doubleComplexTensor
  doubleComplexSubTensor::operator /(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator /(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h]/U[h];
    }
  return result;
  }

// add
const
doubleComplexTensor
  doubleComplexSubTensor::operator +(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator +(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h] + U[h];
    }
  return result;
  }

// subtract
const
doubleComplexTensor
  doubleComplexSubTensor::operator -(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator -(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h] - U[h];
    }
  return result;
  }

// divide
const
doubleComplexTensor
  doubleSubTensor::operator /(const doubleComplexSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::operator /(const doubleComplexSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubTensor&		T = *this;
  doubleComplexTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h]/U[h];
    }
  return result;
  }

// subtract
const
doubleComplexTensor
  doubleSubTensor::operator -(const doubleComplexSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::operator -(const doubleComplexSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubTensor&		T = *this;
  doubleComplexTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h] - U[h];
    }
  return result;
  }

// multiply
const
doubleComplexTensor
  doubleComplexSubTensor::operator *(const doubleComplexSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator *(const doubleComplexSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h]*U[h];
    }
  return result;
  }

// divide
const
doubleComplexTensor
  doubleComplexSubTensor::operator /(const doubleComplexSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator /(const doubleComplexSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h]/U[h];
    }
  return result;
  }


// add
const
doubleComplexTensor
  doubleComplexSubTensor::operator +(const doubleComplexSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator +(const doubleComplexSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h] + U[h];
    }
  return result;
  }

// subtract
const
doubleComplexTensor
  doubleComplexSubTensor::operator -(const doubleComplexSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator -(const doubleComplexSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor&	T = *this;
  doubleComplexTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h] - U[h];
    }
  return result;
  }


std::istream&	operator >>(std::istream& is,       doubleComplexSubTensor& T) {
  for (Offset h = 0; h < T.extent3(); h++) {
    doubleComplexSubMatrix	M = T[h];
    if (!(is >> M)) break;
    }
  return is;
  }

std::ostream&	operator <<(std::ostream& os, const doubleComplexSubTensor& T) {
  int			width = os.width();
  for (Offset h = 0; h < T.extent3(); h++) {
    os << std::setw(width) << T[h];
    }
  return os;
  }


// equal
bool	doubleComplexSubTensor::operator ==(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator ==(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor&	T = *this;
  Offset			h = (Offset)0;
  while (h < extent3() && T[h] == U[h]) {
   ++h;
   }
  return h == extent3();
  }

// not equal
bool	doubleComplexSubTensor::operator !=(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator !=(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor&	T = *this;
  Offset			h = (Offset)0;
  while (h < extent3() && T[h] != U[h]) {
   ++h;
   }
  return h == extent3();
  }

// equal
bool	doubleComplexSubTensor::operator ==(
  const doubleComplexSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator ==(const doubleComplexSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor&	T = *this;
  Offset			h = (Offset)0;
  while (h < extent3() && T[h] == U[h]) {
   ++h;
   }
  return h == extent3();
  }

// not equal
bool	doubleComplexSubTensor::operator !=(
  const doubleComplexSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator !=(const doubleComplexSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubTensor&	T = *this;
  Offset			h = (Offset)0;
  while (h < extent3() && T[h] != U[h]) {
   ++h;
   }
  return h == extent3();
  }



// simple assignment
doubleComplexSubTensor&
  doubleComplexSubTensor::operator  =(const doubleSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator  =(const doubleSubTensor&)",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubTensor&	T = *this;
  for (Offset h = 0; h < extent3(); h++) {
    T[h] = U[h];
    }
  return *this;
  }

// multiply and assign
doubleComplexSubTensor&
  doubleComplexSubTensor::operator *=(const doubleSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator *=(const doubleSubTensor&)",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubTensor&	T = *this;
  for (Offset h = 0; h < extent3(); h++) {
    T[h] *= U[h];
    }
  return *this;
  }

// divide and assign
doubleComplexSubTensor&
  doubleComplexSubTensor::operator /=(const doubleSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator /=(const doubleSubTensor&)",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubTensor&	T = *this;
  for (Offset h = 0; h < extent3(); h++) {
    T[h] /= U[h];
    }
  return *this;
  }

// add and assign
doubleComplexSubTensor&
  doubleComplexSubTensor::operator +=(const doubleSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator +=(const doubleSubTensor&)",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubTensor&	T = *this;
  for (Offset h = 0; h < extent3(); h++) {
    T[h] += U[h];
    }
  return *this;
  }

// subtract and assign
doubleComplexSubTensor&
  doubleComplexSubTensor::operator -=(const doubleSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator -=(const doubleSubTensor&)",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubTensor&	T = *this;
  for (Offset h = 0; h < extent3(); h++) {
    T[h] -= U[h];
    }
  return *this;
  }

// simple assignment
doubleComplexSubTensor&
  doubleComplexSubTensor::operator  =(const doubleComplexSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator  =(const doubleComplexSubTensor&)",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubTensor&	T = *this;
  for (Offset h = 0; h < extent3(); h++) {
    T[h] = U[h];
    }
  return *this;
  }

// multiply and assign
doubleComplexSubTensor&
  doubleComplexSubTensor::operator *=(const doubleComplexSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator *=(const doubleComplexSubTensor&)",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubTensor&	T = *this;
  for (Offset h = 0; h < extent3(); h++) {
    T[h] *= U[h];
    }
  return *this;
  }

// divide and assign
doubleComplexSubTensor&
  doubleComplexSubTensor::operator /=(const doubleComplexSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator /=(const doubleComplexSubTensor&)",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubTensor&	T = *this;
  for (Offset h = 0; h < extent3(); h++) {
    T[h] /= U[h];
    }
  return *this;
  }


// add and assign
doubleComplexSubTensor&
  doubleComplexSubTensor::operator +=(const doubleComplexSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator +=(const doubleComplexSubTensor&)",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubTensor&	T = *this;
  for (Offset h = 0; h < extent3(); h++) {
    T[h] += U[h];
    }
  return *this;
  }

// subtract and assign
doubleComplexSubTensor&
  doubleComplexSubTensor::operator -=(const doubleComplexSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexSubTensor::operator -=(const doubleComplexSubTensor&)",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubTensor&	T = *this;
  for (Offset h = 0; h < extent3(); h++) {
    T[h] -= U[h];
    }
  return *this;
  }


