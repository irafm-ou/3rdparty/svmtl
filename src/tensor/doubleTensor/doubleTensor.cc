/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleTensor.h>


// Functions
doubleSubTensor&
  doubleSubTensor::reverse1(void) {
  doubleSubTensor&      T = *this;
  Extent                        l = extent3();
  for (Offset h = 0; h < l; h++)
    T[h].reverse1();
  return *this;
  }
doubleSubTensor&
  doubleSubTensor::reverse2(void) {
  doubleSubTensor&      T = *this;
  Extent                        l = extent3();
  for (Offset h = 0; h < l; h++)
    T[h].reverse2();
  return *this;
  }
doubleSubTensor&
  doubleSubTensor::reverse3(void) {
  Extent                        l = extent3();
  for (Offset h = 0; h < l/2; h++)
    swap(h, l-1 - h);
  return *this;
  }


// T_{h,i,j} = j
doubleSubTensor&
  doubleSubTensor::ramp(void) {
  doubleSubTensor&	T = *this;
  Extent			l = extent3();
  for (Offset h = 0; h < l; h++) {
    T[h].ramp();
    }
  return *this;
  }

doubleSubTensor&
  doubleSubTensor::swap(doubleSubTensor& U) {
  doubleSubTensor	T = (*this);
  Extent			l = extent3();
  for (Offset h = 0; h < l; h++) {
    doubleSubMatrix	M = T[h];
    doubleSubMatrix	N = U[h];
    M.swap(N);
    }
  return *this;
  }

doubleSubTensor&
  doubleSubTensor::rotate(Stride n) {
  doubleSubTensor&	T = *this;
  Extent			l = extent3();
  for (Offset h = 0; h < l; h++) {
    doubleSubMatrix	M = T[h];
    M.rotate(n);
    }
  return *this;
  }

doubleSubTensor&
  doubleSubTensor::shift(Stride n,const double& s) {
  doubleSubTensor&	T = *this;
  Extent			l = extent3();
  for (Offset h = 0; h < l; h++) {
    doubleSubMatrix	M = T[h];
    M.shift(n,s);
    }
  return *this;
  }


const
doubleMatrix
  min(const doubleSubTensor& T) {
  doubleMatrix	result(T.extent3(), T.extent2());
  Extent			l = T.extent3();
  for (Offset h = 0; h < l; h++) {
    result[h] = min(T[h]);
    }
  return result;
  }

const
doubleMatrix
  max(const doubleSubTensor& T) {
  doubleMatrix	result(T.extent3(), T.extent2());
  Extent			l = T.extent3();
  for (Offset h = 0; h < l; h++) {
    result[h] = max(T[h]);
    }
  return result;
  }

const
doubleTensor
  min(const doubleSubTensor& T, const doubleSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "min(const doubleSubTensor&, const doubleSubTensor&)",
  T.extent3(), U.extent3(), T.extent2(), U.extent2(), T.extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleTensor	result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = min(T[h], U[h]);
    }
  return result;
  }

const
doubleTensor
  max(const doubleSubTensor& T, const doubleSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "max(const doubleSubTensor&, const doubleSubTensor&)",
  T.extent3(), U.extent3(), T.extent2(), U.extent2(), T.extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleTensor	result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = max(T[h], U[h]);
    }
  return result;
  }

// T.sum() = T_{h,i,0} + T_{h,i,1} + ... + T_{h,i,n-1}
const
doubleMatrix
  doubleSubTensor::sum(void) const {
  const
  doubleSubTensor&	T = *this;
  doubleMatrix		result(extent3(), extent2(), (double)0);
  for (Offset h = 0; h < extent3(); h++) {
    result[h] += T[h].sum();
    }
  return result;
  }
const
doubleTensor
  doubleSubTensor::permutation(const offsetSubVector& p) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubTensor::permutation(const offsetSubVector&) const",
    extent1(), p.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubTensor&	T = *this;
  Offset			l = extent3();
  Offset			m = extent2();
  Offset			n = extent1();
  doubleTensor		U(l, m, n);
  for (Offset h = 0; h < l; h++) {
    const
    doubleSubMatrix	M = T[h];
    doubleSubMatrix	N = U[h];
    for (Offset i = 0; i < m; i++) {
      const
      doubleSubVector	v = M[i];
      doubleSubVector	w = N[i];
      for (Offset j = 0; j < n; j++) {
	w[j] = v[p[j]];
	}
      }
    }
  return U;
  }
const
boolTensor
  doubleSubTensor::lt(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::lt(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubTensor&	T = *this;
  boolTensor			result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].lt(U[h]);
    }
  return result;
  }

const
boolTensor
  doubleSubTensor::le(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::le(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubTensor&	T = *this;
  boolTensor			result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].le(U[h]);
    }
  return result;
  }

const
boolTensor
  doubleSubTensor::eq(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::eq(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubTensor&	T = *this;
  boolTensor			result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); ++h) {
    result[h] = T[h].eq(U[h]);
    }
  return result;
  }

const
boolTensor
  doubleSubTensor::ne(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::ne(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubTensor	T = *this;
  boolTensor			result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].ne(U[h]);
    }
  return result;
  }

Extent
  doubleSubTensor::zeros(void) const {
  const
  doubleSubTensor&	T = *this;
  Extent			result = 0;
  for (Offset h = 0; h < extent3(); h++) {
    result += T[h].zeros();
    }
  return result;
  }
/*
const
offsetVector
  doubleSubTensor::index(void) const {
  offsetVector		result(extent3()*extent2()*extent1() - zeros());
  Offset		k = 0;
  for (Offset h = 0; h < extent3(); h++) {
    }
  return result;
  }

const
doubleVector
  doubleSubTensor::gather(const offsetSubVector& x) const {
  doubleVector	result(x.extent());
  for (Offset h = 0; h < extent3(); h++) {
#ifdef SVMT_DEBUG_MODE
    svmt_check_vector_index_range(
      "doubleSubTensor::gather(const offsetSubVector&) const",
      h, extent2());
#endif // SVMT_DEBUG_MODE
    }
  return result;
  }

doubleSubTensor&
  doubleSubTensor::scatter(const offsetSubVector& x,
    const doubleSubVector& v) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubTensor::scatter(\n"
    "const offsetSubVector&, const doubleSubVector&)",
    x.extent(), v.extent());
#endif // SVMT_DEBUG_MODE
  return *this;
  }

const
doubleTensor
  doubleSubTensor::aside(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubTensor::aside(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2());
#endif // SVMT_DEBUG_MODE
  doubleTensor		result(extent3(), extent2(), extent1()
							 + U.extent1());
  result.sub((Offset)0, extent3(), (Stride)1,
	     (Offset)0, extent2(), (Stride)1,
	     (Offset)0, extent1(), (Stride)1) = *this;
  result.sub((Offset)0, extent3(), (Stride)1,
	     (Offset)0, extent2(), (Stride)1,
	     extent1(), U.extent1(), (Stride)1) = U;
  return result;
  }

const
doubleTensor
  doubleSubTensor::above(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubTensor::above(const doubleSubTensor&) const",
    extent1(), U.extent1(), extent3(), U.extent3());
#endif // SVMT_DEBUG_MODE
  doubleTensor		result(extent3(), extent2()
					      + U.extent2(), extent1());
  result.sub((Offset)0, extent3(), (Stride)1,
	     (Offset)0, extent2(), (Stride)1) = *this;
  result.sub((Offset)0, extent3(), (Stride)1,
	     extent2(), U.extent2(), (Stride)1) = U;
  return result;
  }

const
doubleTensor
  doubleSubTensor::afore(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubTensor::afore(const doubleSubTensor&) const",
    extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleTensor		result(extent3()
				   + U.extent3(), extent2(), extent1());
  result.sub((Offset)0, extent3(), (Stride)1) = *this;
  result.sub(extent3(), U.extent3(), (Stride)1) = U
  return result;
  }

const
doubleTensor
  doubleSubVector::kron(const doubleSubTensor& T) const {
  const
  doubleSubVector&	v = *this;
  doubleTensor		result(T.extent2(), extent()*T.extent1());

  for (Offset h = 0; h < extent3(); h++) {
    result[h] = v.kron(T[h]);
    }
  return result;
  }

const
doubleTensor
  doubleSubTensor::kron(const doubleSubTensor& U) const {
  const
  doubleSubTensor&	T = *this;
  doubleTensor	result(extent2()*U.extent2(), extent1()*U.extent1());
  for (Offset h = 0; h < extent3(); h++) {
    for (Offset j = 0; j < extent1(); j++) {
      result.sub(h*U.extent2(), U.extent2(), (Stride)1,
		 j*U.extent1(), U.extent1(), (Stride)1)
	= T[h][j]*U;
      }
    }
  return result;
  }

const
doubleTensor
  doubleSubTensor::kron(const doubleSubVector& v) const {
  const
  doubleSubTensor&	T = *this;
  doubleTensor		result(T.extent2(), T.extent1()*extent());

  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].kron(v);
    }
  return result;
  }

*/

const
doubleTensor
  doubleSubTensor::apply(const double (*f)(const double&)) const {
  const
  doubleSubTensor&	T = *this;
  doubleTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].apply(f);
    }
  return result;
  }

const
doubleTensor
  doubleSubTensor::apply(      double (*f)(const double&)) const {
  const
  doubleSubTensor&	T = *this;
  doubleTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].apply(f);
    }
  return result;
  }

const
doubleTensor
  doubleSubTensor::apply(      double (*f)(      double )) const {
  const
  doubleSubTensor&	T = *this;
  doubleTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].apply(f);
    }
  return result;
  }

const
doubleTensor
    exp(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = exp(T[h]);
    }
  return result;
  }

const
doubleTensor
    log(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = log(T[h]);
    }
  return result;
  }

const
doubleTensor
   sqrt(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = sqrt(T[h]);
    }
  return result;
  }

const
doubleTensor
    cos(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = cos(T[h]);
    }
  return result;
  }

const
doubleTensor
    sin(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = sin(T[h]);
    }
  return result;
  }

const
doubleTensor
    tan(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = tan(T[h]);
    }
  return result;
  }

const
doubleTensor
   acos(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = acos(T[h]);
    }
  return result;
  }

const
doubleTensor
   asin(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = asin(T[h]);
    }
  return result;
  }

const
doubleTensor
   atan(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = atan(T[h]);
    }
  return result;
  }

const
doubleTensor
   cosh(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = cosh(T[h]);
    }
  return result;
  }

const
doubleTensor
   sinh(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = sinh(T[h]);
    }
  return result;
  }

const
doubleTensor
   tanh(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = tanh(T[h]);
    }
  return result;
  }

const
doubleTensor
  acosh(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = acosh(T[h]);
    }
  return result;
  }

const
doubleTensor
  asinh(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = asinh(T[h]);
    }
  return result;
  }

const
doubleTensor
  atanh(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = atanh(T[h]);
    }
  return result;
  }


const
doubleTensor
    sgn(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = sgn(T[h]);
    }
  return result;
  }

const
doubleTensor
    abs(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = abs(T[h]);
    }
  return result;
  }

const
doubleTensor
  floor(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = floor(T[h]);
    }
  return result;
  }

const
doubleTensor
   ceil(const doubleSubTensor& T) {
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = ceil(T[h]);
    }
  return result;
  }

const
doubleTensor
  hypot(const doubleSubTensor& T, const doubleSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "hypot(const doubleSubTensor&, const doubleSubTensor&)",
  T.extent3(), U.extent3(), T.extent2(), U.extent2(), T.extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleTensor		result(T.extent3(), T.extent2(), T.extent1());
  for (Offset h = 0; h < T.extent3(); h++) {
    result[h] = hypot(T[h], U[h]);
    }
  return result;
  }

const
doubleTensor
  atan2(const doubleSubTensor& U, const doubleSubTensor& T) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "atan2(const doubleSubTensor&, const doubleSubTensor&)",
  U.extent3(), T.extent3(), U.extent2(), T.extent2(), U.extent1(), T.extent1());
#endif // SVMT_DEBUG_MODE
  doubleTensor		result(U.extent3(), U.extent2(), U.extent1());
  for (Offset h = 0; h < U.extent3(); h++) {
    result[h] = atan2(U[h], T[h]);
    }
  return result;
  }

// Operators

// minus
const
doubleTensor
  doubleSubTensor::operator -(void) const {
  const
  doubleSubTensor&	T = *this;
  doubleTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = -T[h];
    }
  return result;
  }


// multiply
const
doubleTensor
  doubleSubTensor::operator *(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::operator *(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubTensor&	T = *this;
  doubleTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h]*U[h];
    }
  return result;
  }

// divide
const
doubleTensor
  doubleSubTensor::operator /(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::operator /(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubTensor&	T = *this;
  doubleTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h]/U[h];
    }
  return result;
  }


// add
const
doubleTensor
  doubleSubTensor::operator +(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::operator +(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubTensor&	T = *this;
  doubleTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h] + U[h];
    }
  return result;
  }

// subtract
const
doubleTensor
  doubleSubTensor::operator -(const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::operator -(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubTensor&	T = *this;
  doubleTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h] - U[h];
    }
  return result;
  }


std::istream&	operator >>(std::istream& is,       doubleSubTensor& T) {
  for (Offset h = 0; h < T.extent3(); h++) {
    doubleSubMatrix	M = T[h];
    if (!(is >> M)) break;
    }
  return is;
  }

std::ostream&	operator <<(std::ostream& os, const doubleSubTensor& T) {
  int			width = os.width();
  for (Offset h = 0; h < T.extent3(); h++) {
    os << std::setw(width) << T[h];
    }
  return os;
  }

// less than
bool	doubleSubTensor::operator < (
  const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::operator < (const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubTensor&	T = *this;
  Offset			h = (Offset)0;
  while (h < extent3() && T[h] <  U[h]) {
   ++h;
   }
  return h == extent3();
  }

// less than or equal
bool	doubleSubTensor::operator <=(
  const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::operator <=(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubTensor&	T = *this;
  Offset			h = (Offset)0;
  while (h < extent3() && T[h] <= U[h]) {
   ++h;
   }
  return h == extent3();
  }

// equal
bool	doubleSubTensor::operator ==(
  const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::operator ==(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubTensor&	T = *this;
  Offset			h = (Offset)0;
  while (h < extent3() && T[h] == U[h]) {
   ++h;
   }
  return h == extent3();
  }

// not equal
bool	doubleSubTensor::operator !=(
  const doubleSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::operator !=(const doubleSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubTensor&	T = *this;
  Offset			h = (Offset)0;
  while (h < extent3() && T[h] != U[h]) {
   ++h;
   }
  return h == extent3();
  }



// simple assignment
doubleSubTensor&
  doubleSubTensor::operator  =(const doubleSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::operator  =(const doubleSubTensor&)",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleSubTensor&	T = *this;
  for (Offset h = 0; h < extent3(); h++) {
    T[h] = U[h];
    }
  return *this;
  }

// multiply and assign
doubleSubTensor&
  doubleSubTensor::operator *=(const doubleSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::operator *=(const doubleSubTensor&)",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleSubTensor&	T = *this;
  for (Offset h = 0; h < extent3(); h++) {
    T[h] *= U[h];
    }
  return *this;
  }

// divide and assign
doubleSubTensor&
  doubleSubTensor::operator /=(const doubleSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::operator /=(const doubleSubTensor&)",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleSubTensor&	T = *this;
  for (Offset h = 0; h < extent3(); h++) {
    T[h] /= U[h];
    }
  return *this;
  }


// add and assign
doubleSubTensor&
  doubleSubTensor::operator +=(const doubleSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::operator +=(const doubleSubTensor&)",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleSubTensor&	T = *this;
  for (Offset h = 0; h < extent3(); h++) {
    T[h] += U[h];
    }
  return *this;
  }

// subtract and assign
doubleSubTensor&
  doubleSubTensor::operator -=(const doubleSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleSubTensor::operator -=(const doubleSubTensor&)",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  doubleSubTensor&	T = *this;
  for (Offset h = 0; h < extent3(); h++) {
    T[h] -= U[h];
    }
  return *this;
  }


