#ifndef _boolTensor_h
#define _boolTensor_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<boolMatrix.h>

class boolSubTensor {
  private:
  // Representation
  boolHandle
		H;
  Offset	O;
  Extent	N1;
  Stride	S1;
  Extent	N2;
  Stride	S2;
  Extent	N3;
  Stride	S3;
  public:
  // Constructors
		boolSubTensor(void);
		boolSubTensor(const boolHandle& h,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
		boolSubTensor(const boolSubTensor& T);
		~boolSubTensor(void);
  // Functions
  const
  boolHandle&	handle(void) const;
  Offset	offset(void) const;
  Extent	extent1(void) const;
  Extent	length1(void) const;
  Stride	stride1(void) const;
  Extent	extent2(void) const;
  Extent	length2(void) const;
  Stride	stride2(void) const;
  Extent	extent3(void) const;
  Extent	length3(void) const;
  Stride	stride3(void) const;
  bool		 empty(void) const;
  static
  boolHandle  allocate(Extent l, Extent m, Extent n);
  static
  boolHandle  allocate_(Extent n, Extent m, Extent l);
  boolSubTensor&
		  free(void);
  boolSubTensor&
		resize(void);
  boolSubTensor&
		resize(const boolHandle& h,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
  boolSubTensor&
		resize(const boolSubTensor& T);
  boolSubTensor&
		resize_(const boolHandle& h,
      Offset o, Extent n1, Stride s1,
		Extent n2, Stride s2,
		Extent n3, Stride s3);

  static
  bool	      contains(Offset h, Extent n3, Stride s3, Extent l3,
		       Offset i, Extent n2, Stride s2, Extent l2,
		       Offset j, Extent n1, Stride s1, Extent l1);
  bool	      contains(Offset h, Extent n3, Stride s3) const;
  bool	      contains(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2) const;
  bool	      contains(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  static
  bool	      contains_(Offset j, Extent n1, Stride s1, Extent l1,
			Offset i, Extent n2, Stride s2, Extent l2,
			Offset h, Extent n3, Stride s3, Extent l3);
  bool	      contains_(Offset h, Extent n3, Stride s3) const;
  bool	      contains_(Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;
  bool	      contains_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;
  boolSubTensor	   sub(Offset h, Extent n3, Stride s3);
  const
  boolSubTensor	   sub(Offset h, Extent n3, Stride s3) const;
  boolSubTensor	   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2);
  const
  boolSubTensor	   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2) const;
  boolSubTensor	   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1);
  const
  boolSubTensor	   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  boolSubTensor	   sub_(Offset h, Extent n3, Stride s3);
  const
  boolSubTensor	   sub_(Offset h, Extent n3, Stride s3) const;
  boolSubTensor	   sub_(Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3);
  const
  boolSubTensor	   sub_(Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;
  boolSubTensor	   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3);
  const
  boolSubTensor	   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;

  boolSubTensor	     r1(void);
  const
  boolSubTensor	     r1(void) const;
  boolSubTensor&
	       reverse1(void);
  boolSubTensor	     r2(void);
  const
  boolSubTensor	     r2(void) const;
  boolSubTensor&
	       reverse2(void);
  boolSubTensor	     r3(void);
  const
  boolSubTensor	     r3(void) const;
  boolSubTensor&
	       reverse3(void);
  boolSubTensor	     r(void);
  const
  boolSubTensor	     r(void) const;
  boolSubTensor&
	       reverse(void);
  boolSubTensor	  even(void);
  const
  boolSubTensor	  even(void) const;
  boolSubTensor	   odd(void);
  const
  boolSubTensor	   odd(void) const;
  boolSubTensor	  even_(void);
  const
  boolSubTensor	  even_(void) const;
  boolSubTensor	   odd_(void);
  const
  boolSubTensor	   odd_(void) const;

  boolSubTensor	     t12(void);
  const
  boolSubTensor	     t12(void) const;
  boolSubTensor	     t23(void);
  const
  boolSubTensor	     t23(void) const;
  boolSubTensor	     t31(void);
  const
  boolSubTensor	     t31(void) const;
  boolSubMatrix	  diag12(void);
  const
  boolSubMatrix	  diag12(void) const;
  boolSubMatrix	  diag23(void);
  const
  boolSubMatrix	  diag23(void) const;
  boolSubMatrix	  diag31(void);
  const
  boolSubMatrix	  diag31(void) const;
  boolSubTensor&  swap(Offset h, Offset k);
  boolSubTensor&  swap(boolSubTensor& U);
  boolSubTensor&  swap_(Offset h, Offset k);
  boolSubTensor&
		rotate(Stride n);
  boolSubTensor&
		 shift(Stride n, bool b = false);
  const
  boolTensor	    eq(bool b) const;
  const
  boolTensor	    ne(bool b) const;
  const
  boolTensor	    eq(const boolSubTensor& U) const;
  const
  boolTensor	    ne(const boolSubTensor& U) const;
  Extent	 zeros(void) const;
  const
  offsetVector	 index(void) const;
  const
  boolTensor	 aside(const boolSubTensor& U) const;
  const
  boolTensor	 above(const boolSubTensor& U) const;
  const
  boolTensor	 afore(const boolSubMatrix& M) const;
  const
  boolTensor	 afore(const boolSubTensor& U) const;
  const
  boolTensor	 above_(const boolSubTensor& U) const;
  const
  boolTensor	 aside_(const boolSubTensor& U) const;
  const
  boolTensor	 apply(const bool (*f)(const bool&)) const;
  const
  boolTensor	 apply(      bool (*f)(const bool&)) const;
  const
  boolTensor	 apply(      bool (*f)(      bool )) const;

  // Operators
  boolSubMatrix	operator [](Offset h);
  const
  boolSubMatrix	operator [](Offset h) const;
  boolSubMatrix	operator ()(Offset h);
  const
  boolSubMatrix	operator ()(Offset h) const;
  boolSubVector	operator ()(Offset i, Offset h);
  const
  boolSubVector	operator ()(Offset i, Offset h) const;
  boolSubScalar	operator ()(Offset j, Offset i, Offset h);
  const
  boolSubScalar	operator ()(Offset j, Offset i, Offset h) const;
  const
  boolTensor	operator !(void) const;
  bool		operator ==(bool b) const;
  bool		operator !=(bool b) const;
  bool		operator ==(const boolSubTensor& U) const;
  bool		operator !=(const boolSubTensor& U) const;
  const
  boolTensor	operator &&(bool b) const;
  const
  boolTensor	operator ||(bool b) const;
  const
  boolTensor	operator &&(const boolSubTensor& U) const;
  const
  boolTensor	operator ||(const boolSubTensor& U) const;
  boolSubTensor&
		operator  =(bool b);
  boolSubTensor&
		operator  =(const boolSubTensor& U);
  friend class boolTensor;
  };

// Constructors
inline		boolSubTensor::boolSubTensor(void):
  H((bool*)0), O((Offset)0),
  N1((Extent)0), S1((Stride)0),
  N2((Extent)0), S2((Stride)0),
  N3((Extent)0), S3((Stride)0) { }
inline		boolSubTensor::boolSubTensor(const boolHandle& h, Offset o,
    Extent n3, Stride s3,
    Extent n2, Stride s2,
    Extent n1, Stride s1):
  H(h), O(o), N1(n1), S1(s1), N2(n2), S2(s2), N3(n3), S3(s3) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment("boolSubTensor::boolSubTensor(\n"
  "const boolHandle&, Offset, Extent, Stride, Extent, Stride, Extent, Stride)",
    o, n3, s3, n2, s2, n1, s1);
#endif // SVMT_DEBUG_MODE
  }
inline		boolSubTensor::boolSubTensor(const boolSubTensor& T):
  H(T.H), O(T.O), N1(T.N1), S1(T.S1), N2(T.N2), S2(T.S2), N3(T.N3), S3(T.S3) { }
inline		boolSubTensor::~boolSubTensor(void) { }

// Functions
inline
const
boolHandle&	boolSubTensor::handle(void) const { return H; }
inline
Offset		boolSubTensor::offset(void) const { return O; }
inline
Extent		boolSubTensor::extent1(void) const { return N1; }
inline
Extent		boolSubTensor::length1(void) const { return N1; }
inline
Stride		boolSubTensor::stride1(void) const { return S1; }
inline
Extent		boolSubTensor::extent2(void) const { return N2; }
inline
Extent		boolSubTensor::length2(void) const { return N2; }
inline
Stride		boolSubTensor::stride2(void) const { return S2; }
inline
Extent		boolSubTensor::extent3(void) const { return N3; }
inline
Extent		boolSubTensor::length3(void) const { return N3; }
inline
Stride		boolSubTensor::stride3(void) const { return S3; }
inline
bool		boolSubTensor::empty(void) const {
  return handle().empty()||0 == extent1()||0 == extent2()||0 == extent3(); }
inline
boolHandle	boolSubTensor::allocate(Extent l, Extent m, Extent n) {
  return boolHandle(new bool[l*m*n]); }
inline
boolHandle	boolSubTensor::allocate_(Extent n, Extent m, Extent l) {
  return allocate(l, m, n); }
inline
boolSubTensor&	boolSubTensor::free(void) {
  delete [] (bool*)H; return *this; }
inline
boolSubTensor&	boolSubTensor::resize(void) {
  H = boolHandle((bool*)0); O = (Offset)0;
  N1 = (Extent)0; S1 = (Stride)0;
  N2 = (Extent)0; S2 = (Stride)0;
  N3 = (Extent)0; S3 = (Stride)0; return *this; }
inline
boolSubTensor&	boolSubTensor::resize(const boolHandle& h, Offset o,
    Extent n3, Stride s3, Extent n2, Stride s2, Extent n1, Stride s1) {
  H = h; O = o; N1 = n1; S1 = s1; N2 = n2; S2 = s2; N3 = n3; S3 = s3;
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment("boolSubTensor::resize(const boolHandle&,\n"
    "Offset, Extent, Stride, Extent, Stride, Extent, Stride)",
    o, n3, s3, n2, s2, n1, s1);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
boolSubTensor&	boolSubTensor::resize(const boolSubTensor& T) {
  return resize(T.handle(), T.offset(),
    T.extent3(), T.stride3(),
    T.extent2(), T.stride2(),
    T.extent1(), T.stride1()); }
inline
boolSubTensor&	boolSubTensor::resize_(const boolHandle& h, Offset o,
    Extent n1, Stride s1, Extent n2, Stride s2, Extent n3, Stride s3) {
  return resize(h, o, n3, s3, n2, s2, n1, s1); }

inline
bool		boolSubTensor::contains(
    Offset h, Extent n3, Stride s3, Extent l3,
    Offset i, Extent n2, Stride s2, Extent l2,
    Offset j, Extent n1, Stride s1, Extent l1) {
  return boolSubVector::contains(h, n3, s3, l3)
      && boolSubVector::contains(i, n2, s2, l2)
      && boolSubVector::contains(j, n1, s1, l1); }
inline
bool		boolSubTensor::contains(
    Offset h, Extent n3, Stride s3) const {
  return boolSubVector::contains(h, n3, s3, extent3()); }
inline
bool		boolSubTensor::contains(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2) const {
  return boolSubMatrix::contains(h, n3, s3, extent3(),
				 i, n2, s2, extent2()); }
inline
bool		boolSubTensor::contains(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) const {
  return boolSubTensor::contains(h, n3, s3, extent3(),
				 i, n2, s2, extent2(),
				 j, n1, s1, extent1()); }
inline
bool		boolSubTensor::contains_(
    Offset h, Extent n3, Stride s3) const {
  return contains(h-1, n3, s3); }
inline
bool		boolSubTensor::contains_(
    Offset i, Extent n2, Stride s2,
    Offset h, Extent n3, Stride s3) const {
  return contains(h-1, n3, s3, i-1, n2, s2); }
inline
bool		boolSubTensor::contains_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2,
    Offset h, Extent n3, Stride s3) const {
  return contains(h-1, n3, s3, i-1, n2, s2, j, n1, s1); }

inline
boolSubTensor	boolSubTensor::sub(Offset h, Extent n3, Stride s3) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "boolSubTensor::sub(Offset, Extent, Stride)",
    h, n3, s3, extent3());
#endif // SVMT_DEBUG_MODE
  return boolSubTensor(handle(),
    offset() + (Stride)h*stride3(),
    n3, s3*stride3(), extent2(), stride2(), extent1(), stride1()); }
inline
const
boolSubTensor	boolSubTensor::sub(Offset h, Extent n3, Stride s3) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "boolSubTensor::sub(Offset, Extent, Stride) const",
    h, n3, s3, extent3());
#endif // SVMT_DEBUG_MODE
  return boolSubTensor(handle(),
    offset() + (Stride)h*stride3(),
    n3, s3*stride3(), extent2(), stride2(), extent1(), stride1()); }
inline
boolSubTensor	boolSubTensor::sub(Offset h, Extent n3, Stride s3,
				   Offset i, Extent n2, Stride s2) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("boolSubTensor::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride)",
    h, n3, s3, extent3(),
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return boolSubTensor(handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2(),
    n3, s3*stride3(), n2, s2*stride2(), extent1(), stride1()); }
inline
const
boolSubTensor	boolSubTensor::sub(Offset h, Extent n3, Stride s3,
				   Offset i, Extent n2, Stride s2) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("boolSubTensor::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride) const",
    h, n3, s3, extent3(),
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return boolSubTensor(handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2(),
    n3, s3*stride3(), n2, s2*stride2(), extent1(), stride1()); }

inline
boolSubTensor	boolSubTensor::sub(Offset h, Extent n3, Stride s3,
				   Offset i, Extent n2, Stride s2,
				   Offset j, Extent n1, Stride s1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment("boolSubTensor::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride, Offset, Extent, Stride)",
    h, n3, s3, extent3(),
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return boolSubTensor(handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2() + (Stride)j*stride1(),
    n3, s3*stride3(), n2, s2*stride2(), n1, s1*stride1()); }
inline
const
boolSubTensor	boolSubTensor::sub(Offset h, Extent n3, Stride s3,
				   Offset i, Extent n2, Stride s2,
				   Offset j, Extent n1, Stride s1) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment("boolSubTensor::sub(\n"
"Offset, Extent, Stride, Offset, Extent, Stride, Offset, Extent, Stride) const",
    h, n3, s3, extent3(),
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return boolSubTensor(handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2() + (Stride)j*stride1(),
    n3, s3*stride3(), n2, s2*stride2(), n1, s1*stride1()); }
inline
boolSubTensor	boolSubTensor::sub_(Offset h, Extent n3, Stride s3) {
  return sub(h-1, n3, s3); }
inline
const
boolSubTensor	boolSubTensor::sub_(Offset h, Extent n3, Stride s3) const {
  return sub(h-1, n3, s3); }
inline
boolSubTensor	boolSubTensor::sub_(Offset i, Extent n2, Stride s2,
				    Offset h, Extent n3, Stride s3) {
  return sub(h-1, n3, s3, i-1, n2, s2); }
inline
const
boolSubTensor	boolSubTensor::sub_(Offset i, Extent n2, Stride s2,
				    Offset h, Extent n3, Stride s3) const {
  return sub(h-1, n3, s3, i-1, n2, s2); }
inline
boolSubTensor	boolSubTensor::sub_(Offset j, Extent n1, Stride s1,
				    Offset i, Extent n2, Stride s2,
				    Offset h, Extent n3, Stride s3) {
  return sub(h-1, n3, s3, i-1, n2, s2, j-1, n1, s1); }
inline
const
boolSubTensor	boolSubTensor::sub_(Offset j, Extent n1, Stride s1,
				    Offset i, Extent n2, Stride s2,
				    Offset h, Extent n3, Stride s3) const {
  return sub(h-1, n3, s3, i-1, n2, s2, j-1, n1, s1); }

inline
const
boolSubTensor	boolSubScalar::subtensor(Extent n, Extent m, Extent l) const {
  return boolSubTensor(handle(),
    offset(), l, (Stride)0, m, (Stride)0, n, (Stride)0); }
inline
const
boolSubTensor	boolSubVector::subtensor(Extent m, Extent l) const {
  return boolSubTensor(handle(),
    offset(), l, (Stride)0, m, (Stride)0, extent(), stride()); }
inline
const
boolSubTensor	boolSubMatrix::subtensor(Extent l) const {
  return boolSubTensor(handle(),
    offset(), l, (Stride)0, extent2(), stride2(), extent1(), stride1()); }
inline
boolSubTensor	boolSubTensor::r1(void) {
  return boolSubTensor(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent3(), +stride3(),
    extent2(), +stride2(),
    extent1(), -stride1()); }

inline
const
boolSubTensor	boolSubTensor::r1(void) const {
  return boolSubTensor(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent3(), +stride3(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
boolSubTensor	boolSubTensor::r2(void) {
  return boolSubTensor(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent3(), +stride3(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
const
boolSubTensor	boolSubTensor::r2(void) const {
  return boolSubTensor(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent3(), +stride3(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
boolSubTensor	boolSubTensor::r3(void) {
  return boolSubTensor(handle(),
    offset() + (Stride)(extent3() - 1)*stride3(),
    extent3(), -stride3(),
    extent2(), +stride2(),
    extent1(), +stride1()); }
inline
const
boolSubTensor	boolSubTensor::r3(void) const {
  return boolSubTensor(handle(),
    offset() + (Stride)(extent3() - 1)*stride3(),
    extent3(), -stride3(),
    extent2(), +stride2(),
    extent1(), +stride1()); }
inline
boolSubTensor	boolSubTensor::r(void) {
  return boolSubTensor(handle(),
    offset() + (Stride)(extent3() - 1)*stride3()
	     + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent3(), -stride3(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
const
boolSubTensor	boolSubTensor::r(void) const {
  return boolSubTensor(handle(),
    offset() + (Stride)(extent3() - 1)*stride3()
	     + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent3(), -stride3(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
boolSubTensor&	boolSubTensor::reverse(void) {
  return reverse1().reverse2().reverse3(); }

inline
boolSubTensor	boolSubTensor::even(void) {
  return boolSubTensor(handle(), offset(), extent3(), stride3(),
    extent2(), stride2(), (extent1() + 1) >> 1, stride1() << 1); }
inline
const
boolSubTensor	boolSubTensor::even(void) const {
  return boolSubTensor(handle(), offset(), extent3(), stride3(),
    extent2(), stride2(), (extent1() + 1) >> 1, stride1() << 1); }
inline
boolSubTensor	boolSubTensor::odd(void) {
  return boolSubTensor(handle(), offset() + stride1(), extent3(), stride3(),
    extent2(), stride2(), extent1() >> 1, stride1() << 1); }
inline
const
boolSubTensor	boolSubTensor::odd(void) const {
  return boolSubTensor(handle(), offset() + stride1(), extent3(), stride3(),
    extent2(), stride2(), extent1() >> 1, stride1() << 1); }
inline
boolSubTensor	boolSubTensor::even_(void) { return odd(); }
inline
const
boolSubTensor	boolSubTensor::even_(void) const { return odd(); }
inline
boolSubTensor	boolSubTensor::odd_(void) { return even(); }
inline
const
boolSubTensor	boolSubTensor::odd_(void) const { return even(); }
inline
boolSubTensor	boolSubTensor::t12(void) {
  return boolSubTensor(handle(), offset(), extent3(), stride3(),
    extent1(), stride1(), extent2(), stride2()); }
inline
const
boolSubTensor	boolSubTensor::t12(void) const {
  return boolSubTensor(handle(), offset(), extent3(), stride3(),
    extent1(), stride1(), extent2(), stride2()); }
inline
boolSubTensor	boolSubTensor::t23(void) {
  return boolSubTensor(handle(), offset(), extent2(), stride2(),
    extent3(), stride3(), extent1(), stride1()); }
inline
const
boolSubTensor	boolSubTensor::t23(void) const {
  return boolSubTensor(handle(), offset(), extent2(), stride2(),
    extent3(), stride3(), extent1(), stride1()); }
inline
boolSubTensor	boolSubTensor::t31(void) {
  return boolSubTensor(handle(), offset(), extent1(), stride1(),
    extent2(), stride2(), extent3(), stride3()); }
inline
const
boolSubTensor	boolSubTensor::t31(void) const {
  return boolSubTensor(handle(), offset(), extent1(), stride1(),
    extent2(), stride2(), extent3(), stride3()); }

inline
boolSubMatrix	boolSubTensor::diag12(void) {
  return boolSubMatrix(handle(), offset(), extent3(), stride3(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
const
boolSubMatrix	boolSubTensor::diag12(void) const {
  return boolSubMatrix(handle(), offset(), extent3(), stride3(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
boolSubMatrix	boolSubTensor::diag23(void) {
  return boolSubMatrix(handle(), offset(), extent1(), stride1(),
    (extent2() < extent3())? extent2(): extent3(), stride3() + stride2()); }
inline
const
boolSubMatrix	boolSubTensor::diag23(void) const {
  return boolSubMatrix(handle(), offset(), extent1(), stride1(),
    (extent2() < extent3())? extent2(): extent3(), stride3() + stride2()); }
inline
boolSubMatrix	boolSubTensor::diag31(void) {
  return boolSubMatrix(handle(), offset(), extent2(), stride2(),
    (extent3() < extent1())? extent3(): extent1(), stride1() + stride3()); }
inline
const
boolSubMatrix	boolSubTensor::diag31(void) const {
  return boolSubMatrix(handle(), offset(), extent2(), stride2(),
    (extent3() < extent1())? extent3(): extent1(), stride1() + stride3()); }

// Operators
inline
boolSubMatrix	boolSubTensor::operator [](Offset h) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "boolSubTensor::operator [](Offset)", h, extent3());
#endif // SVMT_DEBUG_MODE
  return boolSubMatrix(handle(), offset() + (Stride)h*stride3(),
    extent2(), stride2(), extent1(), stride1()); }
inline
const
boolSubMatrix	boolSubTensor::operator [](Offset h) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "boolSubTensor::operator [](Offset) const", h, extent3());
#endif // SVMT_DEBUG_MODE
  return boolSubMatrix(handle(), offset() + (Stride)h*stride3(),
    extent2(), stride2(), extent1(), stride1()); }
inline
boolSubMatrix	boolSubTensor::operator ()(Offset h) {
  return operator [](h-1); }
inline
const
boolSubMatrix	boolSubTensor::operator ()(Offset h) const {
  return operator [](h-1); }
inline
boolSubVector	boolSubTensor::operator ()(Offset i, Offset h) {
  boolSubTensor&	T = *this; return T[h-1][i-1]; }
inline
const
boolSubVector	boolSubTensor::operator ()(Offset i, Offset h) const {
  const
  boolSubTensor&	T = *this; return T[h-1][i-1]; }
inline
boolSubScalar	boolSubTensor::operator ()(Offset j, Offset i, Offset h) {
  boolSubTensor&	T = *this; return T[h-1][i-1][j-1]; }
inline
const
boolSubScalar	boolSubTensor::operator ()(Offset j, Offset i, Offset h) const {
  const
  boolSubTensor&	T = *this; return T[h-1][i-1][j-1]; }

std::istream&	operator >>(std::istream& s,       boolSubTensor& T);
std::ostream&	operator <<(std::ostream& s, const boolSubTensor& T);

inline
boolSubTensor&	boolSubTensor::swap(Offset h, Offset k) {
  boolSubTensor		T = *this;
  boolSubMatrix		M = T[h];
  boolSubMatrix		N = T[k];
  return *this; }
inline
boolSubTensor&	boolSubTensor::swap_(Offset h, Offset k) {
  return swap(h-1, k-1); }

class boolSubArray3: public boolSubTensor {
  public:
  // Constructors
		boolSubArray3(void);
		boolSubArray3(bool* p,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
		boolSubArray3(const boolSubArray3& T);
		~boolSubArray3(void);
  // Functions
  boolSubArray3&
		resize(void);
  boolSubArray3&			// resize from pointer
		resize(bool* p,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
  boolSubArray3&			// resize from SubArray3
		resize(const boolSubArray3& T);
  boolSubArray3&			// resize from pointer
		resize_(bool* p,
      Offset o, Extent n1, Stride s1,
		Extent n2, Stride s2,
		Extent n3, Stride s3);
  private:
  boolSubArray3&			// prevent resize from Handle
		resize(const boolHandle& h,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
  boolSubArray3&			// prevent resize from SubTensor
		resize(const boolSubTensor& T);
  public:
  boolSubArray3	   sub(Offset h, Extent n3, Stride s3);
  const
  boolSubArray3	   sub(Offset h, Extent n3, Stride s3) const;
  boolSubArray3	   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2);
  const
  boolSubArray3	   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2) const;
  boolSubArray3	   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1);
  const
  boolSubArray3	   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  boolSubArray3	   sub_(Offset h, Extent n3, Stride s3);
  const
  boolSubArray3	   sub_(Offset h, Extent n3, Stride s3) const;
  boolSubArray3	   sub_(Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3);
  const
  boolSubArray3	   sub_(Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;
  boolSubArray3	   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3);
  const
  boolSubArray3	   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;

  boolSubArray3	     r1(void);
  const
  boolSubArray3	     r1(void) const;
  boolSubArray3&
	       reverse1(void);
  boolSubArray3	     r2(void);
  const
  boolSubArray3	     r2(void) const;
  boolSubArray3&
	       reverse2(void);
  boolSubArray3	     r3(void);
  const
  boolSubArray3	     r3(void) const;
  boolSubArray3&
	       reverse3(void);
  boolSubArray3	     r(void);
  const
  boolSubArray3	     r(void) const;
  boolSubArray3&
	       reverse(void);
  boolSubArray3	  even(void);
  const
  boolSubArray3	  even(void) const;
  boolSubArray3	   odd(void);
  const
  boolSubArray3	   odd(void) const;
  boolSubArray3	  even_(void);
  const
  boolSubArray3	  even_(void) const;
  boolSubArray3	   odd_(void);
  const
  boolSubArray3	   odd_(void) const;
  boolSubArray3	     t12(void);
  const
  boolSubArray3	     t12(void) const;
  boolSubArray3	     t23(void);
  const
  boolSubArray3	     t23(void) const;
  boolSubArray3	     t31(void);
  const
  boolSubArray3	     t31(void) const;
  boolSubArray2	  diag12(void);
  const
  boolSubArray2	  diag12(void) const;
  boolSubArray2	  diag23(void);
  const
  boolSubArray2	  diag23(void) const;
  boolSubArray2	  diag31(void);
  const
  boolSubArray2	  diag31(void) const;
  boolSubArray3&  swap(Offset h, Offset k);
  boolSubArray3&  swap(boolSubTensor& v);
  boolSubArray3&  swap_(Offset h, Offset k);
  boolSubArray3&
		rotate(Stride n);
  boolSubArray3&
		 shift(Stride n, bool b = false);
  // Operators
  boolSubArray3&
		operator  =(bool b);
  boolSubArray3&
		operator  =(const boolSubTensor& U);
  };

// Constructors
inline		boolSubArray3::boolSubArray3(void): boolSubTensor() { }
inline		boolSubArray3::boolSubArray3(bool* p,
    Offset o, Extent n3, Stride s3,
	      Extent n2, Stride s2,
	      Extent n1, Stride s1):
  boolSubTensor(boolHandle(p), o, n3, s3, n2, s2, n1, s1) { }
inline		boolSubArray3::boolSubArray3(const boolSubArray3& T):
  boolSubTensor(T) { }
inline		boolSubArray3::~boolSubArray3(void) { }

// Functions
inline
boolSubArray3&	boolSubArray3::resize(void) {
  boolSubTensor::resize(); return *this; }
inline
boolSubArray3&	boolSubArray3::resize(bool* p,
   Offset o, Extent n3, Stride s3,
	     Extent n2, Stride s2,
	     Extent n1, Stride s1) {
  boolSubTensor::resize(boolHandle(p), o, n3, s3, n2, s2, n1, s1);
  return *this; }
inline
boolSubArray3&	boolSubArray3::resize(const boolSubArray3& T) {
  boolSubTensor::resize(T); return *this; }
inline
boolSubArray3&	boolSubArray3::resize_(bool* p,
   Offset o, Extent n1, Stride s1,
	     Extent n2, Stride s2,
	     Extent n3, Stride s3) {
  return resize(p, o, n3, s3, n2, s2, n1, s1); }
inline
boolSubArray3	boolSubArray3::sub(Offset h, Extent n3, Stride s3) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "boolSubArray3::sub(Offset, Extent, Stride)",
    h, n3, s3, extent3());
#endif // SVMT_DEBUG_MODE
  return boolSubArray3((bool*)(boolHandle&)handle(),
    offset() + (Stride)h*stride3(),
    n3, s3*stride3(), extent2(), stride2(), extent1(), stride1()); }
inline
const
boolSubArray3	boolSubArray3::sub(Offset h, Extent n3, Stride s3) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "boolSubArray3::sub(Offset, Extent, Stride) const",
    h, n3, s3, extent3());
#endif // SVMT_DEBUG_MODE
  return boolSubArray3((bool*)(boolHandle&)handle(),
    offset() + (Stride)h*stride3(),
    n3, s3*stride3(), extent2(), stride2(), extent1(), stride1()); }

inline
boolSubArray3	boolSubArray3::sub(Offset h, Extent n3, Stride s3,
				   Offset i, Extent n2, Stride s2) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("boolSubArray3::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride)",
    h, n3, s3, extent3(),
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return boolSubArray3((bool*)(boolHandle&)handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2(),
    n3, s3*stride3(), n2, s2*stride2(), extent1(), stride1()); }
inline
const
boolSubArray3	boolSubArray3::sub(Offset h, Extent n3, Stride s3,
				   Offset i, Extent n2, Stride s2) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("boolSubArray3::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride) const",
    h, n3, s3, extent3(),
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return boolSubArray3((bool*)(boolHandle&)handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2(),
    n3, s3*stride3(), n2, s2*stride2(), extent1(), stride1()); }
inline
boolSubArray3	boolSubArray3::sub(Offset h, Extent n3, Stride s3,
				   Offset i, Extent n2, Stride s2,
				   Offset j, Extent n1, Stride s1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment("boolSubArray3::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride, Offset, Extent, Stride)",
    h, n3, s3, extent3(),
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return boolSubArray3((bool*)(boolHandle&)handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2() + (Stride)j*stride1(),
    n3, s3*stride3(),
    n2, s2*stride2(),
    n1, s1*stride1()); }
inline
const
boolSubArray3	boolSubArray3::sub(Offset h, Extent n3, Stride s3,
				   Offset i, Extent n2, Stride s2,
				   Offset j, Extent n1, Stride s1) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment("boolSubArray3::sub(\n"
"Offset, Extent, Stride, Offset, Extent, Stride, Offset, Extent, Stride) const",
    h, n3, s3, extent3(),
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return boolSubArray3((bool*)(boolHandle&)handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2() + (Stride)j*stride1(),
    n3, s3*stride3(),
    n2, s2*stride2(),
    n1, s1*stride1()); }

inline
boolSubArray3	boolSubArray3::sub_(Offset h, Extent n3, Stride s3) {
  return sub(h-1, n3, s3); }
inline
const
boolSubArray3	boolSubArray3::sub_(Offset h, Extent n3, Stride s3) const {
  return sub(h-1, n3, s3); }
inline
boolSubArray3	boolSubArray3::sub_(Offset i, Extent n2, Stride s2,
				    Offset h, Extent n3, Stride s3) {
  return sub(h-1, n3, s3, i-1, n2, s2); }
inline
const
boolSubArray3	boolSubArray3::sub_(Offset i, Extent n2, Stride s2,
				    Offset h, Extent n3, Stride s3) const {
  return sub(h-1, n3, s3, i-1, n2, s2); }
inline
boolSubArray3	boolSubArray3::sub_(Offset j, Extent n1, Stride s1,
				    Offset i, Extent n2, Stride s2,
				    Offset h, Extent n3, Stride s3) {
  return sub(h-1, n3, s3, i-1, n2, s2, j-1, n1, s1); }
inline
const
boolSubArray3	boolSubArray3::sub_(Offset j, Extent n1, Stride s1,
				    Offset i, Extent n2, Stride s2,
				    Offset h, Extent n3, Stride s3) const {
  return sub(h-1, n3, s3, i-1, n2, s2, j-1, n1, s1); }
inline
const
boolSubArray3	boolSubArray0::subtensor(Extent n, Extent m, Extent l) const {
  return boolSubArray3((bool*)(boolHandle&)handle(), offset(),
    l, (Stride)0, m, (Stride)0, n, (Stride)0); }
inline
const
boolSubArray3	boolSubArray1::subtensor(Extent m, Extent l) const {
  return boolSubArray3((bool*)(boolHandle&)handle(), offset(),
    l, (Stride)0, m, (Stride)0, extent(), stride()); }
inline
const
boolSubArray3	boolSubArray2::subtensor(Extent l) const {
  return boolSubArray3((bool*)(boolHandle&)handle(), offset(),
    l, (Stride)0, extent2(), stride2(), extent1(), stride1()); }
inline
boolSubArray3	boolSubArray3::r1(void) {
  return boolSubArray3((bool*)(boolHandle&)handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent3(), +stride3(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
const
boolSubArray3	boolSubArray3::r1(void) const {
  return boolSubArray3((bool*)(boolHandle&)handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent3(), +stride3(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
boolSubArray3&	boolSubArray3::reverse1(void) {
  boolSubTensor::reverse1(); return *this; }

inline
boolSubArray3	boolSubArray3::r2(void) {
  return boolSubArray3((bool*)(boolHandle&)handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent3(), +stride3(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
const
boolSubArray3	boolSubArray3::r2(void) const {
  return boolSubArray3((bool*)(boolHandle&)handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent3(), +stride3(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
boolSubArray3&	boolSubArray3::reverse2(void) {
  boolSubTensor::reverse2(); return *this; }
inline
boolSubArray3	boolSubArray3::r3(void) {
  return boolSubArray3((bool*)(boolHandle&)handle(),
    offset() + (Stride)(extent3() - 1)*stride3(),
    extent3(), +stride3(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
const
boolSubArray3	boolSubArray3::r3(void) const {
  return boolSubArray3((bool*)(boolHandle&)handle(),
    offset() + (Stride)(extent3() - 1)*stride3(),
    extent3(), +stride3(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
boolSubArray3&	boolSubArray3::reverse3(void) {
  boolSubTensor::reverse3(); return *this; }
inline
boolSubArray3	boolSubArray3::r(void) {
  return boolSubArray3((bool*)(boolHandle&)handle(),
    offset() + (Stride)(extent1() - 1)*stride1()
	     + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent3() - 1)*stride3(),
    extent3(), -stride3(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
const
boolSubArray3	boolSubArray3::r(void) const {
  return boolSubArray3((bool*)(boolHandle&)handle(),
    offset() + (Stride)(extent1() - 1)*stride1()
	     + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent3() - 1)*stride3(),
    extent3(), -stride3(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
boolSubArray3&	boolSubArray3::reverse(void) {
  boolSubTensor::reverse(); return *this; }

inline
boolSubArray3	boolSubArray3::even(void) {
  return boolSubArray3((bool*)(boolHandle&)handle(), offset(),
    extent3(), stride3(),
    extent2(), stride2(),
    (extent1() + 1) >> 1, stride1() << 1); }
inline
const
boolSubArray3	boolSubArray3::even(void) const {
  return boolSubArray3((bool*)(boolHandle&)handle(), offset(),
    extent3(), stride3(),
    extent2(), stride2(),
    (extent1() + 1) >> 1, stride1() << 1); }
inline
boolSubArray3	boolSubArray3::odd(void) {
  return boolSubArray3((bool*)(boolHandle&)handle(), offset() + stride1(),
  extent3(), stride3(),
  extent2(), stride2(),
  extent1() >> 1, stride1() << 1); }
inline
const
boolSubArray3	boolSubArray3::odd(void) const {
  return boolSubArray3((bool*)(boolHandle&)handle(), offset() + stride1(),
  extent3(), stride3(),
  extent2(), stride2(),
  extent1() >> 1, stride1() << 1); }
inline
boolSubArray3	boolSubArray3::even_(void) { return odd(); }
inline
const
boolSubArray3	boolSubArray3::even_(void) const { return odd(); }
inline
boolSubArray3	boolSubArray3::odd_(void) { return even(); }
inline
const
boolSubArray3	boolSubArray3::odd_(void) const { return even(); }

inline
boolSubArray3	boolSubArray3::t12(void) {
  return boolSubArray3((bool*)(boolHandle&)handle(), offset(),
    extent3(), stride3(), extent1(), stride1(), extent2(), stride2()); }
inline
const
boolSubArray3	boolSubArray3::t12(void) const {
  return boolSubArray3((bool*)(boolHandle&)handle(), offset(),
    extent3(), stride3(), extent1(), stride1(), extent2(), stride2()); }
inline
boolSubArray3	boolSubArray3::t23(void) {
  return boolSubArray3((bool*)(boolHandle&)handle(), offset(),
    extent2(), stride2(), extent3(), stride3(), extent1(), stride1()); }
inline
const
boolSubArray3	boolSubArray3::t23(void) const {
  return boolSubArray3((bool*)(boolHandle&)handle(), offset(),
    extent2(), stride2(), extent3(), stride3(), extent1(), stride1()); }
inline
boolSubArray3	boolSubArray3::t31(void) {
  return boolSubArray3((bool*)(boolHandle&)handle(), offset(),
    extent1(), stride1(), extent2(), stride2(), extent3(), stride3()); }
inline
const
boolSubArray3	boolSubArray3::t31(void) const {
  return boolSubArray3((bool*)(boolHandle&)handle(), offset(),
    extent1(), stride1(), extent2(), stride2(), extent3(), stride3()); }
inline
boolSubArray2	boolSubArray3::diag12(void) { return boolSubArray2(
    (bool*)(boolHandle&)handle(), offset(), extent3(), stride3(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
const
boolSubArray2	boolSubArray3::diag12(void) const { return boolSubArray2(
    (bool*)(boolHandle&)handle(), offset(), extent3(), stride3(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
boolSubArray2	boolSubArray3::diag23(void) { return boolSubArray2(
    (bool*)(boolHandle&)handle(), offset(), extent1(), stride1(),
   (extent2() < extent3())? extent2(): extent3(), stride3() + stride2()); }
inline
const
boolSubArray2	boolSubArray3::diag23(void) const { return boolSubArray2(
    (bool*)(boolHandle&)handle(), offset(), extent1(), stride1(),
    (extent2() < extent3())? extent2(): extent3(), stride3() + stride2()); }
inline
boolSubArray2	boolSubArray3::diag31(void) { return boolSubArray2(
    (bool*)(boolHandle&)handle(), offset(), extent2(), stride2(),
    (extent3() < extent1())? extent3(): extent1(), stride1() + stride3()); }
inline
const
boolSubArray2	boolSubArray3::diag31(void) const { return boolSubArray2(
    (bool*)(boolHandle&)handle(), offset(), extent2(), stride2(),
    (extent3() < extent1())? extent3(): extent1(), stride1() + stride3()); }

inline
boolSubArray3&	boolSubArray3::swap(Offset h, Offset k) {
  boolSubTensor::swap(h, k); return *this; }
inline
boolSubArray3&	boolSubArray3::swap(boolSubTensor& U) {
  boolSubTensor::swap(U); return *this; }
inline
boolSubArray3&	boolSubArray3::swap_(Offset h, Offset k) {
  return swap(h-1, k-1); }
inline
boolSubArray3&  boolSubArray3::rotate(Stride n) {
  boolSubTensor::rotate(n); return *this; }
inline
boolSubArray3&  boolSubArray3::shift(Stride n, bool b) {
  boolSubTensor::shift(n, b); return *this; }

// Operators
inline
boolSubArray3&	boolSubArray3::operator  =(bool b) {
  boolSubTensor::operator  =(boolSubArray3((bool*)&b, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
boolSubArray3&	boolSubArray3::operator  =(const boolSubTensor& U) {
  boolSubTensor::operator  =(U); return *this; }

// boolSubTensor operator definitions which use boolSubArray3
inline
bool		boolSubTensor::operator ==(bool b) const {
  return operator ==(boolSubArray3((bool*)&b, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		boolSubTensor::operator !=(bool b) const {
  return operator !=(boolSubArray3((bool*)&b, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		operator ==(bool b, const boolSubTensor& T) { return T == b; }
inline
bool		operator !=(bool b, const boolSubTensor& T) { return T != b; }
boolMatrix	any(const boolSubTensor& T);
boolMatrix	all(const boolSubTensor& T);
inline
boolSubTensor&	boolSubTensor::operator  =(bool b) {
  return operator  =(boolSubArray3((bool*)&b, (Offset)0,
      extent3(), (Stride)0,
      extent2(), (Stride)0,
      extent1(), (Stride)0)); }

class boolTensor: public boolSubTensor {
  public:
  // Constructors
  		boolTensor(void);
  explicit	boolTensor(Extent l, Extent m = 1, Extent n = 1);
		boolTensor(Extent l, Extent m, Extent n, bool b);
		boolTensor(const boolSubTensor& T);
		boolTensor(const boolTensor& T);
		~boolTensor(void);
  // Functions
  boolTensor&	resize(void);
  boolTensor&	resize(Extent l, Extent m = 1, Extent n = 1);
  boolTensor&	resize(Extent l, Extent m, Extent n, bool b);
  boolTensor&	resize(const boolSubTensor& U);
  boolTensor&	resize_(Extent m, Extent l);
  boolTensor&	resize_(Extent n, Extent m, Extent l);
  boolTensor&	resize_(Extent n, Extent m, Extent l, bool b);
private:
  boolTensor&
		resize(const boolHandle& h,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
  boolTensor&
		free(void) { boolSubTensor::free(); return *this; }
public:
  // Operators
  boolTensor&	operator  =(bool b);
  boolTensor&	operator  =(const boolSubTensor& U);
  boolTensor&	operator  =(const boolTensor& U);
  };

// Constructors
inline		boolTensor::boolTensor(void): boolSubTensor() { }
inline		boolTensor::boolTensor(Extent l, Extent m, Extent n):
  boolSubTensor(allocate(l, m, n), (Offset)0,
      l, (Stride)n*(Stride)m, m, (Stride)n, n, (Stride)1) {
#ifdef SVMT_DEBUG_MODE
  boolSubTensor::operator =(true);
#endif // SVMT_DEBUG_MODE
  }
inline		boolTensor::boolTensor(Extent l, Extent m, Extent n, bool b):
  boolSubTensor(allocate(l, m, n), (Offset)0, 
      l, (Stride)n*(Stride)m, m, (Stride)n, n, (Stride)1) {
  boolSubTensor::operator =(b); }
inline		boolTensor::boolTensor(const boolSubTensor& T):
  boolSubTensor(allocate(T.extent3(), T.extent2(), T.extent1()), (Offset)0,
      T.extent3(), (Stride)T.extent1()*(Stride)T.extent2(),
      T.extent2(), (Stride)T.extent1(), T.extent1(), (Stride)1) {
  boolSubTensor::operator =(T); }
inline		boolTensor::boolTensor(const boolTensor& T):
  boolSubTensor(allocate(T.extent3(), T.extent2(), T.extent1()), (Offset)0,
      T.extent3(), (Stride)T.extent1()*(Stride)T.extent2(),
      T.extent2(), (Stride)T.extent1(), T.extent1(), (Stride)1) {
  boolSubTensor::operator =(T); }
inline		boolTensor::~boolTensor(void) { free(); }

// Assignment Operators
inline
boolTensor&	boolTensor::operator  =(bool b) {
  boolSubTensor::operator =(b); return *this; }
inline
boolTensor&	boolTensor::operator  =(const boolSubTensor& U) {
  boolSubTensor::operator =(U); return *this; }
inline
boolTensor&	boolTensor::operator  =(const boolTensor& U) {
  boolSubTensor::operator =(U); return *this; }

// Functions
inline
boolTensor&	boolTensor::resize(void) { free();
  boolSubTensor::resize(); return *this; }
inline
boolTensor&	boolTensor::resize(Extent l, Extent m, Extent n) { free();
  boolSubTensor::resize(allocate(l, m, n), (Offset)0,
    l, (Stride)n*(Stride)m, m, (Stride)n, n, (Stride)1);
#ifdef SVMT_DEBUG_MODE
  boolSubTensor::operator =(true);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
boolTensor&	boolTensor::resize(Extent l, Extent m, Extent n, bool b) {
  return resize(l, m, n) = b; }
inline
boolTensor&	boolTensor::resize(const boolSubTensor& U) {
  return resize(U.extent3(), U.extent2(), U.extent1()) = U; }
inline
boolTensor&	boolTensor::resize_(Extent m, Extent l) {
  return resize(l, m); }
inline
boolTensor&	boolTensor::resize_(Extent n, Extent m, Extent l) {
  return resize(l, m, n); }
inline
boolTensor&	boolTensor::resize_(Extent n, Extent m, Extent l, bool b) {
  return resize(l, m, n, b); }

// boolSubTensor function definitions which return boolTensor
inline
const
boolTensor	boolSubTensor::eq(bool b) const {
  return eq(boolSubArray3(&b, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolTensor	boolSubTensor::ne(bool b) const {
  return ne(boolSubArray3(&b, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolTensor	boolSubMatrix::afore(const boolSubTensor& T) const {
  return subtensor().afore(T); }
inline
const
boolTensor	boolSubTensor::afore(const boolSubMatrix& M) const {
  return afore(M.subtensor()); }
inline
const
boolTensor	boolSubTensor::above_(const boolSubTensor& T) const {
  return aside(T); }
inline
const
boolTensor	boolSubTensor::aside_(const boolSubTensor& T) const {
  return above(T); }

// boolSubTensor operator definitions which return boolTensor
inline
const
boolTensor	boolSubTensor::operator &&(bool b) const {
  return operator &&(boolSubArray3((bool*)&b, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolTensor	boolSubTensor::operator ||(bool b) const {
  return operator ||(boolSubArray3((bool*)&b, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolTensor	operator &&(bool b, const boolSubTensor& T) { return T && b; }
inline
const
boolTensor	operator ||(bool b, const boolSubTensor& T) { return T || b; }

#endif /* _boolTensor_h */

