/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<boolTensor.h>

// Functions
boolSubTensor&
  boolSubTensor::reverse1(void) {
  boolSubTensor&	T = *this;
  Extent		l = extent3();
  for (Offset h = 0; h < l; h++)
    T[h].reverse1();
  return *this;
  }
boolSubTensor&
  boolSubTensor::reverse2(void) {
  boolSubTensor&	T = *this;
  Extent		l = extent3();
  for (Offset h = 0; h < l; h++)
    T[h].reverse2();
  return *this;
  }
boolSubTensor&
  boolSubTensor::reverse3(void) {
  Extent		l = extent3();
  for (Offset h = 0; h < l/2; h++)
    swap(h, l-1 - h);
  return *this;
  }
boolSubTensor&
  boolSubTensor::swap(boolSubTensor& U) {
  boolSubTensor	T = (*this);
  for (Offset h = 0; h < extent3(); h++) {
    boolSubMatrix	M = T[h];
    boolSubMatrix	N = U[h];
    M.swap(N);
    }
  return *this;
  }
boolSubTensor&
  boolSubTensor::rotate(Stride n) {
  boolSubTensor&	T = *this;
  Extent		l = extent3();
  for (Offset h = 0; h < l; h++) {
    T[h].rotate(n);
    }
  return *this;
  }

boolSubTensor&
  boolSubTensor::shift(Stride n, bool b) {
  boolSubTensor&	T = *this;
  Extent		l = extent3();
  for (Offset h = 0; h < l; h++) {
    T[h].shift(n, b);
    }
  return *this;
  }

// xnor
const
boolTensor
  boolSubTensor::eq(const boolSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "boolSubTensor::eq(const boolSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  boolSubTensor&	T = *this;
  boolTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].eq(U[h]);
    }
  return result;
  }

// xor
const
boolTensor
  boolSubTensor::ne(const boolSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "boolSubTensor::ne(const boolSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  boolSubTensor&	T = *this;
  boolTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].ne(U[h]);
    }
  return result;
  }
boolMatrix	any(const boolSubTensor& T) {
  Extent	l = T.extent3();
  Extent	m = T.extent2();
  boolMatrix	result(l, m);
  for (Offset h = 0; h < l; h++)
    for (Offset i = 0; i < m; i++)
      result[h][i] = any(T[h][i]);
  return result; }
boolMatrix	all(const boolSubTensor& T) {
  Extent	l = T.extent3();
  Extent	m = T.extent2();
  boolMatrix	result(l, m);
  for (Offset h = 0; h < l; h++)
    for (Offset i = 0; i < m; i++)
      result[h][i] = all(T[h][i]);
  return result; }

Extent
  boolSubTensor::zeros(void) const {
  const
  boolSubTensor&	T = *this;
  Extent		result = 0;
  for (Offset h = 0; h < extent3(); h++) {
    result += T[h].zeros();
    }
  return result;
  }

// const
// offsetVector
//   boolSubTensor::index(void) const {
//   const
//   boolSubTensor&	T = *this;
//   Extent		n = extent3()*extent2()*extent1() - zeros();
//   offsetVector	result(n);
//   Offset		k = 0;
//   for (Offset h = 0; h < extent3(); h++) {
//     Extent		n = extent1() - T[h].zeros();
//     result
//     }
//   return result;
//   }

const
boolTensor
  boolSubTensor::aside(const boolSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "boolSubTensor::aside(const boolSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2());
#endif // SVMT_DEBUG_MODE
  const
  boolSubTensor&	T = *this;
  boolTensor		result(extent3(), extent2(), extent1() + U.extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].aside(U[h]);
    }
  return result;
  }

const
boolTensor
  boolSubTensor::above(const boolSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "boolSubTensor::above(const boolSubTensor&) const",
    extent1(), U.extent1(), extent3(), U.extent3());
#endif // SVMT_DEBUG_MODE
  boolTensor		result(extent3(), extent2() + U.extent2(), extent1());
  result.sub((Offset)0, extent3(), (Stride)1,
	     (Offset)0, extent2(), (Stride)1) = *this;
  result.sub((Offset)0, extent3(), (Stride)1,
	     extent2(), U.extent2(), (Stride)1) = U;
  return result;
  }

const
boolTensor
  boolSubTensor::afore(const boolSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "boolSubTensor::above(const boolSubTensor&) const",
    extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  boolTensor		result(extent3() + U.extent3(), extent2(), extent1());
  result.sub((Offset)0, extent3(), (Stride)1) = *this;
  result.sub(extent3(), U.extent3(), (Stride)1) = U;
  return result;
  }

const
boolTensor
  boolSubTensor::apply(const bool (*f)(const bool&)) const {
  const
  boolSubTensor&	T = *this;
  boolTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].apply(f);
    }
  return result;
  }

const
boolTensor
  boolSubTensor::apply(      bool (*f)(const bool&)) const {
  const
  boolSubTensor&	T = *this;
  boolTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].apply(f);
    }
  return result;
  }

const
boolTensor
  boolSubTensor::apply(      bool (*f)(      bool )) const {
  const
  boolSubTensor&	T = *this;
  boolTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h].apply(f);
    }
  return result;
  }

// Operators
// not
const
boolTensor
  boolSubTensor::operator !(void) const {
  const
  boolSubTensor&	T = *this;
  boolTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = !T[h];
    }
  return result;
  }

// and
const
boolTensor
  boolSubTensor::operator &&(const boolSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "boolSubTensor::operator &&(const boolSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  boolSubTensor&	T = *this;
  boolTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h] && U[h];
    }
  return result;
  }

// or
const
boolTensor
  boolSubTensor::operator ||(const boolSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "boolSubTensor::operator &&(const boolSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  boolSubTensor&	T = *this;
  boolTensor		result(extent3(), extent2(), extent1());
  for (Offset h = 0; h < extent3(); h++) {
    result[h] = T[h] || U[h];
    }
  return result;
  }
std::istream&	operator >>(std::istream& is, boolSubTensor& T) {
  for (Offset h = 0; h < T.extent3(); h++) {
    boolSubMatrix	M = T[h];
    if (!(is >> M)) break;
    }
  return is;
  }

std::ostream&	operator <<(std::ostream& os, const boolSubTensor& T) {
  int			width = os.width();
  Offset		h = 0;
  while (h < T.extent3() && os << std::setw(width) << T[h]) {
    ++h;
    }
  return os;
  }

// equal
bool	boolSubTensor::operator ==(const boolSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "boolSubTensor::operator ==(const boolSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  boolSubTensor&	T = *this;
  Offset		h = 0;
  while (h < extent3() && T[h] == U[h]) {
   ++h;
   }
  return h == extent3();
  }

// not equal
bool	boolSubTensor::operator !=(const boolSubTensor& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "boolSubTensor::operator !=(const boolSubTensor&) const",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  boolSubTensor&	T = *this;
  Offset		h = 0;
  while (h < extent3() &&  T[h] != U[h]) {
   ++h;
   }
  return h == extent3();
  }
// simple assignment
boolSubTensor&
  boolSubTensor::operator  =(const boolSubTensor& U) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "boolSubTensor::operator  =(const boolSubTensor&)",
    extent3(), U.extent3(), extent2(), U.extent2(), extent1(), U.extent1());
#endif // SVMT_DEBUG_MODE
  boolSubTensor&	T = *this;
  for (Offset h = 0; h < extent3(); h++) {
    T[h] = U[h];
    }
  return *this;
  }

