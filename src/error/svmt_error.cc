/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<svmt_error.h>

int	svmt_report_error(svmt_error_code, const char* format, ...) {
  using namespace std;
  va_list ap;
  va_start(ap, format);
  vfprintf(stderr, format, ap);		/* Print to standard error.	*/
  va_end(ap);
  return fflush(stderr);
  }
 
void	svmt_check_vector_containment(const char* name,
  Offset o, Extent n, Stride s) {
  using namespace std;
  if (0 < n && s < 0 && o < -s*(n - 1)) {
    svmt_report_error(svmt_containment_error, "In function %s,\n"
      "(offset = %lu) < -(stride = %ld)*((extent = %lu) - 1).\n", name,
      (unsigned long int)o, (  signed long int)s, (unsigned long int)n);
    exit(EXIT_FAILURE);
    }
  }

void	svmt_check_vector_containment(const char* name,
  Offset o, Extent n, Stride s, Extent l) {
  using namespace std;
  if (0 < n) {
    if (l <= o) {
      svmt_report_error(svmt_containment_error,
	"In function %s,\n(extent = %lu) <= (%lu = offset).\n",
	name, (unsigned long int)l, (unsigned long int)o);
      exit(EXIT_FAILURE);
      }
    if (s < 0) {
      if (o < -s*(n - 1)) {
        svmt_report_error(svmt_containment_error, "In function %s,\n"
	  "(offset = %lu) < -(stride = %ld)*((extent = %lu) - 1).\n", name,
	  (unsigned long int)o, (  signed long int)s, (unsigned long int)n);
	exit(EXIT_FAILURE);
	}
      }
    else {				// 0 <= s
      if (l <= o + s*(n - 1)) {
        svmt_report_error(svmt_containment_error, "In function %s,\n"
"(extent = %lu) <= (offset = %lu) + (stride = %ld)*((extent = %lu) - 1).\n",
	   name, (unsigned long int)l, (unsigned long int)o,
	   (  signed long int)s, (unsigned long int)n);
	exit(EXIT_FAILURE);
	}
      }
    }
  }

void	svmt_check_vector_index_range(const char* name,
  Offset j, Extent n) {
  using namespace std;
  if (n <= j) {
    svmt_report_error(svmt_index_range_error,
      "In function %s,\n(extent = %lu) <= (%ld = offset).\n",
      name, (unsigned long int)n, (unsigned long int)j);
    exit(EXIT_FAILURE);
    }
  }

void	svmt_check_vector_conformance(const char* name,
  Extent n1, Extent n2) {
  using namespace std;
  if (n1 != n2) {
    svmt_report_error(svmt_conformance_error,
      "In function %s,\n(extent_1 = %lu) != (%ld = extent_2).\n",
      name, (unsigned long int)n1, (unsigned long int)n2);
    exit(EXIT_FAILURE);
    }
  }
 
void	svmt_check_matrix_containment(const char* name,
  Offset o, Extent n2, Stride s2, Extent n1, Stride s1) {
  using namespace std;
  if (0 < n2 && 0 < n1) {
    svmt_check_vector_containment(name, o, n2, s2);
    svmt_check_vector_containment(name, o, n1, s1);
    if (s2 < 0 && s1 < 0 && o < -s2*(n2 - 1) + -s1*(n1 - 1)) {
      svmt_report_error(svmt_containment_error, "In function %s,\n"
	"(offset = %lu)\n"
	" < -(stride = %ld)*((extent = %lu) - 1)\n"
	" + -(stride = %ld)*((extent = %lu) - 1).\n",
	name, (unsigned long int)o,
	(  signed long int)s2, (unsigned long int)n2,
	(  signed long int)s1, (unsigned long int)n1);
      exit(EXIT_FAILURE);
      }
    }
  }

void	svmt_check_matrix_containment(const char* name,
  Offset o, Extent n2, Stride s2, Extent n1, Stride s1, Extent l) {
  using namespace std;
  if (0 < n2 && 0 < n1) {
    if (o < l) {
      svmt_check_vector_containment(name, o, n2, s2, l);
      svmt_check_vector_containment(name, o, n1, s1, l);
      if (0 < s2 && 0 < s1 && l <= o + s2*(n2 - 1) + s1*(n1 - 1)) {
	svmt_report_error(svmt_containment_error, "In function %s,\n"
	  "(extent = %lu) <= (offset = %lu)\n"
	  " + (stride = %ld)*((extent = %lu) - 1)\n"
	  " + (stride = %ld)*((extent = %lu) - 1).\n",
	  name, (unsigned long int)l, (unsigned long int)o,
	  (  signed long int)s2, (unsigned long int)n2,
	  (  signed long int)s1, (unsigned long int)n1);
	exit(EXIT_FAILURE);
	}
      else
      if (s2 < 0 && s1 < 0 && o < -s2*(n2 - 1) + -s1*(n1 - 1)) {
	svmt_report_error(svmt_containment_error, "In function %s,\n"
	  "(offset = %lu)\n"
	  " < -(stride = %ld)*((extent = %lu) - 1)\n"
	  " + -(stride = %ld)*((extent = %lu) - 1).\n",
	  name, (unsigned long int)o,
	  (  signed long int)s2, (unsigned long int)n2,
	  (  signed long int)s1, (unsigned long int)n1);
	exit(EXIT_FAILURE);
	}
      }
    else {				// l <= j
      svmt_report_error(svmt_containment_error,
	"In function %s,\n(extent = %lu) <= (%lu = offset).\n",
	name, (unsigned long int)l, (unsigned long int)o);
      exit(EXIT_FAILURE);
      }
    }
  }

void	svmt_check_matrix_containment(const char* name,
  Offset i, Extent n2, Stride s2, Extent l2,
  Offset j, Extent n1, Stride s1, Extent l1) {
  svmt_check_vector_containment(name, i, n2, s2, l2);
  svmt_check_vector_containment(name, j, n1, s1, l1);
  }

void	svmt_check_matrix_index_range(const char* name,
  Offset i, Extent n2, Offset j, Extent n1) {
  svmt_check_vector_index_range(name, i, n2);
  svmt_check_vector_index_range(name, j, n1);
  }

void	svmt_check_matrix_conformance(const char* name,
  Extent m1, Extent m2, Extent n1, Extent n2) {
  svmt_check_vector_conformance(name, m1, m2);
  svmt_check_vector_conformance(name, n1, n2);
  }

 
void	svmt_check_tensor_containment(const char* name, Offset o,
    Extent n3, Stride s3, Extent n2, Stride s2, Extent n1, Stride s1) {
  if (0 < n3 && 0 < n2 && 0 < n1) {
    svmt_check_matrix_containment(name, o, n1, s1, n3, s3);
    svmt_check_matrix_containment(name, o, n3, s3, n2, s2);
    svmt_check_matrix_containment(name, o, n2, s2, n1, s1);
    if (s3 < 0 && s2 < 0 && s1 < 0
     && o < -s3*(n3 - 1) + -s2*(n2 - 1) + -s1*(n1 - 1)) {
      svmt_report_error(svmt_containment_error, "In function %s,\n"
	"(offset = %lu)\n"
	" < -(stride = %ld)*((extent = %lu) - 1)\n"
	" + -(stride = %ld)*((extent = %lu) - 1)\n",
	" + -(stride = %ld)*((extent = %lu) - 1).\n",
	name, (unsigned long int)o,
	(  signed long int)s3, (unsigned long int)n3,
	(  signed long int)s2, (unsigned long int)n2,
	(  signed long int)s1, (unsigned long int)n1);
      }
    }
  }

void	svmt_check_tensor_containment(const char* name, Offset o,
  Extent n3, Stride s3, Extent n2, Stride s2, Extent n1, Stride s1, Extent l) {
  using namespace std;
  if (0 < n3 && 0 < n2 && 0 < n1) {
    if (o < l) {
      svmt_check_matrix_containment(name, o, n1, s1, n3, s3, l);
      svmt_check_matrix_containment(name, o, n3, s3, n2, s2, l);
      svmt_check_matrix_containment(name, o, n2, s2, n1, s1, l);
      if (0 < s3 && 0 < s2 && 0 < s1
       && l <= o + s3*(n3 - 1) + s2*(n2 - 1) + s1*(n1 - 1)) {
	svmt_report_error(svmt_containment_error, "In function %s,\n"
	  "(extent = %lu) <= (offset = %lu)\n"
	  " + (stride = %ld)*((extent = %lu) - 1)\n"
	  " + (stride = %ld)*((extent = %lu) - 1)\n"
	  " + (stride = %ld)*((extent = %lu) - 1).\n",
	  name, (unsigned long int)l, (unsigned long int)o,
	  (  signed long int)s3, (unsigned long int)n3,
	  (  signed long int)s2, (unsigned long int)n2,
	  (  signed long int)s1, (unsigned long int)n1);
	}
      else
      if (s3 < 0 && s2 < 0 && s1 < 0
       && o < -s3*(n3 - 1) + -s2*(n2 - 1) + -s1*(n1 - 1)) {
	svmt_report_error(svmt_containment_error, "In function %s,\n"
	  "(offset = %lu)\n"
	  " < -(stride = %ld)*((extent = %lu) - 1)\n"
	  " + -(stride = %ld)*((extent = %lu) - 1)\n",
	  " + -(stride = %ld)*((extent = %lu) - 1).\n",
	  name, (unsigned long int)o,
	  (  signed long int)s3, (unsigned long int)n3,
	  (  signed long int)s2, (unsigned long int)n2,
	  (  signed long int)s1, (unsigned long int)n1);
	}
      }
    else {				// l <= o
      svmt_report_error(svmt_containment_error,
	"In function %s,\n(extent = %lu) <= (%lu = offset).\n",
	name, (unsigned long int)l, (unsigned long int)o);
      exit(EXIT_FAILURE);
      }
    }
  }

void	svmt_check_tensor_containment(const char* name,
  Offset h, Extent n3, Stride s3, Extent l3,
  Offset i, Extent n2, Stride s2, Extent l2,
  Offset j, Extent n1, Stride s1, Extent l1) {
  svmt_check_vector_containment(name, h, n3, s3, l3);
  svmt_check_vector_containment(name, i, n2, s2, l2);
  svmt_check_vector_containment(name, j, n1, s1, l1);
  }

void	svmt_check_tensor_index_range(const char* name,
  Offset h, Extent n3, Offset i, Extent n2, Offset j, Extent n1) {
  svmt_check_vector_index_range(name, h, n3);
  svmt_check_vector_index_range(name, i, n2);
  svmt_check_vector_index_range(name, j, n1);
  }

void	svmt_check_tensor_conformance(const char* name,
  Extent l1, Extent l2, Extent m1, Extent m2, Extent n1, Extent n2) {
  svmt_check_vector_conformance(name, l1, l2);
  svmt_check_vector_conformance(name, m1, m2);
  svmt_check_vector_conformance(name, n1, n2);
  }

