#ifndef _svmt_error_h
#define _svmt_error_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<cfloat>
#include<cstdio>
#include<cstdarg>
#include<cstdlib>
#include<svmt_typedefs.h>

#define	      floatBad  FLT_MAX
#define	     doubleBad  DBL_MAX
#define	long_doubleBad LDBL_MAX

typedef enum {
  svmt_index_range_error,
  svmt_containment_error,
  svmt_conformance_error
  } svmt_error_code;

extern
int	svmt_report_error(svmt_error_code, const char*, ...);

extern
void	svmt_check_vector_containment(const char*,
  Offset, Extent, Stride);
extern
void	svmt_check_vector_containment(const char*,
  Offset, Extent, Stride, Extent);
extern
void	svmt_check_vector_index_range(const char*,
  Offset, Extent);
extern
void	svmt_check_vector_conformance(const char*,
  Extent, Extent);

extern
void	svmt_check_matrix_containment(const char*,
  Offset, Extent, Stride,
	  Extent, Stride);
extern
void	svmt_check_matrix_containment(const char*,
  Offset, Extent, Stride,
          Extent, Stride, Extent);
extern
void	svmt_check_matrix_containment(const char*,
  Offset, Extent, Stride, Extent,
  Offset, Extent, Stride, Extent);
extern
void	svmt_check_matrix_index_range(const char*,
  Offset, Extent, Offset, Extent);
extern
void	svmt_check_matrix_conformance(const char*,
  Extent, Extent, Extent, Extent);

extern
void	svmt_check_tensor_containment(const char*,
  Offset, Extent, Stride,
	  Extent, Stride,
	  Extent, Stride);
extern
void	svmt_check_tensor_containment(const char*,
  Offset, Extent, Stride,
	  Extent, Stride,
	  Extent, Stride, Extent);
extern
void	svmt_check_tensor_containment(const char*,
  Offset, Extent, Stride, Extent,
  Offset, Extent, Stride, Extent,
  Offset, Extent, Stride, Extent);
extern
void	svmt_check_tensor_index_range(const char*,
  Offset, Extent, Offset, Extent, Offset, Extent);
extern
void	svmt_check_tensor_conformance(const char*,
  Extent, Extent, Extent, Extent, Extent, Extent);

#endif /* _svmt_error_h */
