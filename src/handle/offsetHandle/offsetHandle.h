#ifndef _offsetHandle_h
#define _offsetHandle_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<svmt_typedefs.h>

class offsetVector;
class offsetSubArray1;
class offsetSubArray0;

class offsetHandle {
  private:
  // Representation
  Offset*	P;
  // Constructors
		offsetHandle(Offset* p);
  public:	offsetHandle(const offsetHandle& h);
		~offsetHandle(void);
  // Operators
		operator Offset* (void);
  offsetHandle&	operator  =(const offsetHandle& h);
  // Functions
  bool		empty(void) const;
  Offset	get(Offset o) const;
  offsetHandle&	put(Offset o, Offset j);
  friend class offsetSubArray0;
  friend class offsetSubVector;	friend class offsetVector;
  friend class offsetSubArray1;
  };

// Constructors
inline		offsetHandle::offsetHandle(Offset* p): P(p) { }
inline		offsetHandle::offsetHandle(const offsetHandle& h): P(h.P) { }
inline		offsetHandle::~offsetHandle(void) { }

// Operators
inline		offsetHandle::operator Offset* (void) { return P; }
inline
offsetHandle&	offsetHandle::operator  =(const offsetHandle& h) {
  P = h.P; return *this; }

// Functions
inline
bool		offsetHandle::empty(void) const { return 0 == P; }
inline
Offset		offsetHandle::get(Offset o) const { return P[o]; }

inline
offsetHandle&	offsetHandle::put(Offset o, Offset j) {
  P[o] = j; return *this; }

#endif /* _offsetHandle_h */

