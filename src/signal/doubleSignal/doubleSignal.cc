/*
The C++ Digital Signal Processing classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleSignal.h>


const
doubleVector
	doubleBlackman(Extent n) {
  doubleVector
		w(n);
  if (0 < n) {
    if (1 < n) {
      w.ramp() *= M_PI/(n-1);		// w_j = j*pi/(n-1)
      w  = sin(w);			// w_j = sin(j*pi/(n-1))
      w *= w;				// w_j = sin^2(j*pi/(n-1))
      w *= ((double)16/(double)25)*(((double)9/(double)16) + w);
		// w_j = (16/25)*(9/16 + sin^2(j*pi/(n-1)))*sin^2(j*pi/(n-1))
      }
    else {
      w[0] = (double)1;
      }
    }
  return w;
  }
const
doubleVector
	doubleChebyshev(Extent n, double ripple) {
  doubleVector
		w(n);
  if (0 < n) {
    if (1 < n) {
      double	delta = exp((-log((double)10)/(double)20)*ripple);
      double	tau   = ((double)1 + delta)/delta;
      double	sigma = cosh(acosh(tau)/(n-1));
      doubleSubVector		// real and imaginary parts of vector w
		r = w.even(), i = w.odd();
		r.ramp() *= M_PI/n;	// r_j = pi*j/n
		r = sigma*cos(r);	// r_j = sigma*cos(pi*j/n)
		r = 2*r*r - 1;		// r_j = 2*(sigma*cos(pi*j/n))^{2} - 1
      for (Offset j = 0; 2*j < n; j++) {
	if (1 < r[j])
	  r[j] = cosh(0.5*(n-1)*acosh(r[j]));
	else
	  r[j] = cos(0.5*(n-1)*acos(r[j]));
	}
      if (1&n)				// n is odd
	i = (double)0;
      else {				// n is even
	i.ramp() *= M_PI/n;
	for (Offset j = 0; (j << 1) < n; j++) {
	  double	x = r[j];
	  double	y = i[j];
		r[j] = x*cos(y);
		i[j] = x*sin(y);
	  }
	}
      w.rdft(+1);
      w /= w[0];
      w.rotate((int)((n+1)/2));
      }
    else {
      w[0] = (double)1;
      }
    }
  return w;
  }
const
doubleVector
	doubleHamming(Extent n) {
  doubleVector
		w(n);
  if (0 < n) {
    if (1 < n) {
      w.ramp() *= M_PI/(n-1);		// w_j = j*pi/(n-1)
      w  = sin(w);			// w_j = sin(j*pi/(n-1))
      w *= (double)0.92*w;		// w_j = 0.92*sin^2(j*pi/(n-1))
      w += (double)0.08;			// w_j = 0.08 + 0.92*sin^2(j*pi/(n-1))
      }
    else {
      w[0] = (double)1;
      }
    }
  return w;
  }
const
doubleVector
	doubleHanning(Extent n) {
  doubleVector
		w(n);
  if (0 < n) {
    if (1 < n) {
      w.ramp() += (double)1;		// w_j = j+1
      w *= M_PI/(n+1);			// w_j = (j+1)*pi/(n+1)
      w  = sin(w);			// w_j = sin((j+1)*pi/(n+1))
      w *= w;				// w_j = sin^2((j+1)*pi/(n+1))
      }
    else {
      w[0] = (double)1;
      }
    }
  return w;
  }
inline
double I_0(double x) {			// Modified Bessel Function
  // This approximation was lifted from Numerical Recipes in C page 237.
  double		ans = 0.0;
  double		ax = fabs((double)x);
  if (ax < 3.75) {
    double	y = (double)x/3.75;
    y *= y;
    ans = 1.0 + y*(+3.5156229 + y*(+3.0899424 + y*(+1.2067492
	      + y*(+0.2659732 + y*(+0.0360768 + y*(+0.0045813))))));
    }
  else {
    double	y = 3.75/ax;
    ans = (exp(ax)/sqrt(ax))*(+0.39894228 + y*(+0.01328592 + y*(+0.00225319
	+ y*(-0.00157565 + y*(+0.00916281 + y*(-0.02057706 + y*(+0.02635537
	+ y*(-0.01647633 + y*(+0.00392377)))))))));
    }
  return ans;
  }
inline
const
doubleVector
   I_0(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,  I_0(h.get(o)));
    o += s;
    }
  return result;
  }
const
doubleVector
	doubleKaiser(Extent n, double beta) {
  doubleVector
		w(n);
  if (0 < n) {
    if (1 < n) {
      w.ramp();				// w_j = j
      w *= (double)(n-1) - w;		// w_j = j*(n-1-j)
      w  = I_0((2*beta/(n-1))*sqrt(w))/I_0(beta);
      }
    else {
      w[0] = (double)1;
      }
    }
  return w;
  }

