/*
The C++ Digital Signal Processing classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleComplexSignal.h>


// 1 Dimensional Complex to Complex Direct Discrete Fourier Transform
// Functions
double1DCCDDFT&	double1DCCDDFT::resize(Extent n, Extent invocations) {
  resize(n).twiddle(-1);
  return *this; }

// Operators
doubleComplexSubVector&
		double1DCCDDFT::operator ()(doubleComplexSubVector& v) const {
  v.transpose().fdft(*this);
  return v; }
doubleComplexSubMatrix&
		double1DCCDDFT::operator ()(doubleComplexSubMatrix& M) const {
  Extent	m = M.extent2();
  for (Offset i = 0; i < m; i++)
    M[i].transpose().fdft(*this);
  return M; }
doubleComplexSubTensor&
		double1DCCDDFT::operator ()(doubleComplexSubTensor& T) const {
  Extent	m = T.extent2();
  Extent	l = T.extent3();
  for (Offset h = 0; h < l; h++)
    for (Offset i = 0; i < m; i++)
      T[h][i].transpose().fdft(*this);
  return T; }

// 1 Dimensional Complex Direct Discrete Fourier Transform from Complex
// Functions
double1DCDDFTC&	double1DCDDFTC::resize(Extent n, Extent invocations) {
  resize(n).twiddle(-1);
  return *this; }

// Operators
doubleComplexVector
	double1DCDDFTC::operator ()(const doubleComplexSubVector& v) const {
  doubleComplexVector
		u = v;
  u.transpose().fdft(*this);
  return u; }
doubleComplexMatrix
	double1DCDDFTC::operator ()(const doubleComplexSubMatrix& M) const {
  doubleComplexMatrix
		L = M;
  Extent	m = L.extent2();
  for (Offset i = 0; i < m; i++)
    L[i].transpose().fdft(*this);
  return L; }
doubleComplexTensor
	double1DCDDFTC::operator ()(const doubleComplexSubTensor& T) const {
  doubleComplexTensor
		S = T;
  Extent	m = S.extent2();
  Extent	l = S.extent3();
  for (Offset h = 0; h < l; h++)
    for (Offset i = 0; i < m; i++)
      S[h][i].transpose().fdft(*this);
  return S; }

// 1 Dimensional Complex to Complex Inverse Discrete Fourier Transform
// Functions
double1DCCIDFT&	double1DCCIDFT::resize(Extent n, Extent invocations) {
  resize(n).twiddle(+1);
  return *this; }

// Operators
doubleComplexSubVector&
		double1DCCIDFT::operator ()(doubleComplexSubVector& v) const {
  v.transpose().fdft(*this);
  return v; }
doubleComplexSubMatrix&
		double1DCCIDFT::operator ()(doubleComplexSubMatrix& M) const {
  Extent	m = M.extent2();
  for (Offset i = 0; i < m; i++)
    M[i].transpose().fdft(*this);
  return M; }
doubleComplexSubTensor&
		double1DCCIDFT::operator ()(doubleComplexSubTensor& T) const {
  Extent	m = T.extent2();
  Extent	l = T.extent3();
  for (Offset h = 0; h < l; h++)
    for (Offset i = 0; i < m; i++)
      T[h][i].transpose().fdft(*this);
  return T; }

// 1 Dimensional Complex Inverse Discrete Fourier Transform from Complex
// Functions
double1DCIDFTC&	double1DCIDFTC::resize(Extent n, Extent invocations) {
  resize(n).twiddle(+1);
  return *this; }

// Operators
doubleComplexVector
	double1DCIDFTC::operator ()(const doubleComplexSubVector& v) const {
  doubleComplexVector
		u = v;
  u.transpose().fdft(*this);
  return u; }
doubleComplexMatrix
	double1DCIDFTC::operator ()(const doubleComplexSubMatrix& M) const {
  doubleComplexMatrix
		L = M;
  Extent	m = L.extent2();
  for (Offset i = 0; i < m; i++)
    L[i].transpose().fdft(*this);
  return L; }
doubleComplexTensor
	double1DCIDFTC::operator ()(const doubleComplexSubTensor& T) const {
  doubleComplexTensor
		S = T;
  Extent	m = S.extent2();
  Extent	l = S.extent3();
  for (Offset h = 0; h < l; h++)
    for (Offset i = 0; i < m; i++)
      S[h][i].transpose().fdft(*this);
  return S; }

// 1 Dimensional Real to Complex Direct Discrete Fourier Transform
// Functions
double1DRCDDFT&	double1DRCDDFT::resize(Extent n, Extent invocations) {
  resize(n).twiddle(-1);
  return *this; }

// Operators
doubleSubVector&
		double1DRCDDFT::operator ()(doubleSubVector& v) const {
  v.transpose(false).rcfdft(*this);
  return v; }
doubleSubMatrix&
		double1DRCDDFT::operator ()(doubleSubMatrix& M) const {
  Extent	m = M.extent2();
  for (Offset i = 0; i < m; i++)
    M[i].transpose(false).rcfdft(*this);
  return M; }
doubleSubTensor&
		double1DRCDDFT::operator ()(doubleSubTensor& T) const {
  Extent	m = T.extent2();
  Extent	l = T.extent3();
  for (Offset h = 0; h < l; h++)
    for (Offset i = 0; i < m; i++)
      T[h][i].transpose(false).rcfdft(*this);
  return T; }

// 1 Dimensional Complex Direct Discrete Fourier Transform from Real
// Functions
double1DCDDFTR&	double1DCDDFTR::resize(Extent n, Extent invocations) {
  resize(n).twiddle(-1);
  return *this; }

// Operators
doubleVector
	double1DCDDFTR::operator ()(const doubleSubVector& v) const {
  doubleVector	u = v;
  u.transpose(false).rcfdft(*this);
  return u; }
doubleMatrix
	double1DCDDFTR::operator ()(const doubleSubMatrix& M) const {
  doubleMatrix	L = M;
  Extent	m = L.extent2();
  for (Offset i = 0; i < m; i++)
    L[i].transpose(false).rcfdft(*this);
  return L; }
doubleTensor
	double1DCDDFTR::operator ()(const doubleSubTensor& T) const {
  doubleTensor	S = T;
  Extent	m = S.extent2();
  Extent	l = S.extent3();
  for (Offset h = 0; h < l; h++)
    for (Offset i = 0; i < m; i++)
      S[h][i].transpose(false).rcfdft(*this);
  return S; }

// 1 Dimensional Real to Complex Inverse Discrete Fourier Transform
// Functions
double1DRCIDFT&	double1DRCIDFT::resize(Extent n, Extent invocations) {
  resize(n).twiddle(+1);
  return *this; }

// Operators
doubleSubVector&
		double1DRCIDFT::operator ()(doubleSubVector& v) const {
  v.transpose(false).rcfdft(*this);
  return v; }
doubleSubMatrix&
		double1DRCIDFT::operator ()(doubleSubMatrix& M) const {
  Extent	m = M.extent2();
  for (Offset i = 0; i < m; i++)
    M[i].transpose(false).rcfdft(*this);
  return M; }
doubleSubTensor&
		double1DRCIDFT::operator ()(doubleSubTensor& T) const {
  Extent	m = T.extent2();
  Extent	l = T.extent3();
  for (Offset h = 0; h < l; h++)
    for (Offset i = 0; i < m; i++)
      T[h][i].transpose(false).rcfdft(*this);
  return T; }

// 1 Dimensional Complex Inverse Discrete Fourier Transform from Real
// Functions
double1DCIDFTR&	double1DCIDFTR::resize(Extent n, Extent invocations) {
  resize(n).twiddle(+1);
  return *this; }

// Operators
doubleVector
	double1DCIDFTR::operator ()(const doubleSubVector& v) const {
  doubleVector	u = v;
  u.transpose(false).rcfdft(*this);
  return u; }
doubleMatrix
	double1DCIDFTR::operator ()(const doubleSubMatrix& M) const {
  doubleMatrix	L = M;
  Extent	m = L.extent2();
  for (Offset i = 0; i < m; i++)
    L[i].transpose(false).rcfdft(*this);
  return L; }
doubleTensor
	double1DCIDFTR::operator ()(const doubleSubTensor& T) const {
  doubleTensor	S = T;
  Extent	m = S.extent2();
  Extent	l = S.extent3();
  for (Offset h = 0; h < l; h++)
    for (Offset i = 0; i < m; i++)
      S[h][i].transpose(false).rcfdft(*this);
  return S; }

// 1 Dimensional Complex to Real Direct Discrete Fourier Transform
// Functions
double1DCRDDFT&	double1DCRDDFT::resize(Extent n, Extent invocations) {
  resize(n).twiddle(-1);
  return *this; }

// Operators
doubleSubVector&
		double1DCRDDFT::operator ()(doubleSubVector& v) const {
  v.crfdft(*this).transpose(true);
  return v; }
doubleSubMatrix&
		double1DCRDDFT::operator ()(doubleSubMatrix& M) const {
  Extent	m = M.extent2();
  for (Offset i = 0; i < m; i++)
    M[i].crfdft(*this).transpose(true);
  return M; }
doubleSubTensor&
		double1DCRDDFT::operator ()(doubleSubTensor& T) const {
  Extent	m = T.extent2();
  Extent	l = T.extent3();
  for (Offset h = 0; h < l; h++)
    for (Offset i = 0; i < m; i++)
      T[h][i].crfdft(*this).transpose(true);
  return T; }

// 1 Dimensional Real Direct Discrete Fourier Transform from Complex
// Functions
double1DRDDFTC&	double1DRDDFTC::resize(Extent n, Extent invocations) {
  resize(n).twiddle(-1);
  return *this; }

// Operators
doubleVector
	double1DRDDFTC::operator ()(const doubleSubVector& v) const {
  doubleVector	u = v;
  u.crfdft(*this).transpose(true);
  return u; }
doubleMatrix
	double1DRDDFTC::operator ()(const doubleSubMatrix& M) const {
  doubleMatrix	L = M;
  Extent	m = L.extent2();
  for (Offset i = 0; i < m; i++)
    L[i].crfdft(*this).transpose(true);
  return L; }
doubleTensor
	double1DRDDFTC::operator ()(const doubleSubTensor& T) const {
  doubleTensor	S = T;
  Extent	m = S.extent2();
  Extent	l = S.extent3();
  for (Offset h = 0; h < l; h++)
    for (Offset i = 0; i < m; i++)
      S[h][i].crfdft(*this).transpose(true);
  return S; }

// 1 Dimensional Complex to Real Inverse Discrete Fourier Transform
// Functions
double1DCRIDFT&	double1DCRIDFT::resize(Extent n, Extent invocations) {
  resize(n).twiddle(+1);
  return *this; }

// Operators
doubleSubVector&
		double1DCRIDFT::operator ()(doubleSubVector& v) const {
  v.crfdft(*this).transpose(true);
  return v; }
doubleSubMatrix&
		double1DCRIDFT::operator ()(doubleSubMatrix& M) const {
  Extent	m = M.extent2();
  for (Offset i = 0; i < m; i++)
    M[i].crfdft(*this).transpose(true);
  return M; }
doubleSubTensor&
		double1DCRIDFT::operator ()(doubleSubTensor& T) const {
  Extent	m = T.extent2();
  Extent	l = T.extent3();
  for (Offset h = 0; h < l; h++)
    for (Offset i = 0; i < m; i++)
      T[h][i].crfdft(*this).transpose(true);
  return T; }

// 1 Dimensional Real Inverse Discrete Fourier Transform from Complex
// Functions
double1DRIDFTC&	double1DRIDFTC::resize(Extent n, Extent invocations) {
  resize(n).twiddle(+1);
  return *this; }

// Operators
doubleVector
	double1DRIDFTC::operator ()(const doubleSubVector& v) const {
  doubleVector	u = v;
  u.crfdft(*this).transpose(true);
  return u; }
doubleMatrix
	double1DRIDFTC::operator ()(const doubleSubMatrix& M) const {
  doubleMatrix	L = M;
  Extent	m = L.extent2();
  for (Offset i = 0; i < m; i++)
    L[i].crfdft(*this).transpose(true);
  return L; }
doubleTensor
	double1DRIDFTC::operator ()(const doubleSubTensor& T) const {
  doubleTensor	S = T;
  Extent	m = S.extent2();
  Extent	l = S.extent3();
  for (Offset h = 0; h < l; h++)
    for (Offset i = 0; i < m; i++)
      S[h][i].crfdft(*this).transpose(true);
  return S; }

