# The C++ Scalar, Vector, Matrix and Tensor classes.
# Copyright (C) 1998 E. Robert Tisdale
# 
# This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
# This library is free software which you can redistribute and/or modify
# under the terms of the GNU Library General Public License
# as published by the Free Software Foundation;
# either version 2, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU Library General Public License
# along with this library.  If not, write to the Free Software Foundation,
# Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# 
# Written by E. Robert Tisdale

CC=g++
INCLUDE=-I../../../include
DEFINES=-DSVMT_DEBUG_MODE
OPTIONS=-O2 -Wall -ansi -pedantic
AR=ar
AR_OPTIONS=rcvs
ARCHIVE=../../../lib/libdoubleComplexSquare.a
HEADER=../../../include/doubleComplexSquare.h
COMPILE=$(CC) $(INCLUDE) $(DEFINES) $(OPTIONS)
GENCLASS=../../../bin/genclass

SOURCES=doubleComplexSquare.cc

OBJECTS=doubleComplexSquare.o

INSTANT=doubleComplexSquare.ti

all:
	(cd ../doubleSquare; make \
		CC=$(CC) DEFINES='$(DEFINES)' OPTIONS='$(OPTIONS)' \
		AR=$(AR) AR_OPTIONS='$(AR_OPTIONS)')
	(cd ../../matrix/doubleComplexMatrix; make \
		CC=$(CC) DEFINES='$(DEFINES)' OPTIONS='$(OPTIONS)' \
		AR=$(AR) AR_OPTIONS='$(AR_OPTIONS)')
	(make $(ARCHIVE) \
		CC=$(CC) DEFINES='$(DEFINES)' OPTIONS='$(OPTIONS)' \
		AR=$(AR) AR_OPTIONS='$(AR_OPTIONS)')

$(ARCHIVE):		$(OBJECTS)
	$(AR) $(AR_OPTIONS) $(ARCHIVE) $(OBJECTS)

doubleComplexSquare.o:	$(HEADER) doubleComplexSquare.cc
	$(COMPILE) -c doubleComplexSquare.cc

$(HEADER):		../Square.hP
	$(GENCLASS) double double complex Complex Square
	mv doubleComplexSquare.h $(HEADER)

doubleComplexSquare.cc:	../Square.ccP
	$(GENCLASS) double double complex Complex Square
	mv doubleComplexSquare.h $(HEADER)

clean:
	rm -f $(OBJECTS) $(INSTANT)

sterile:
	rm -f $(ARCHIVE) $(OBJECTS) $(SOURCES) $(HEADER) $(INSTANT)

