/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleComplexSquare.h>

// Functions
doubleComplexSubSquare&
doubleComplexSubSquare::transpose(void) {
  doubleComplexSubSquare&	M = *this;
  doubleComplexSubSquare	T = M.t();
  Extent			m = extent2();
  for (Offset i = 1; i < m; i++) {
    doubleComplexSubVector	u = M[i].sub(0, i, 1);
    doubleComplexSubVector	v = T[i].sub(0, i, 1);
    u.swap(v);
    }
  return *this;
  }
const
doubleComplexSquare
doubleComplexSubMatrix::dot(void) const {
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexSquare		S(extent2());
  for (Offset i = 0; i < extent2(); i++) {
    for (Offset j = 0; j < i; j++)
      S[j][i] = S[i][j] = M[i].dot(M[j]);
    S[i][i] = M[i].dot(M[i]);
    }
  return S;
  }

// Cholesky decomposition with pivoting.
const
offsetVector
  doubleComplexSubSquare::lld(void) {
  Extent			m = extent2();
  offsetVector			p(m);	// permutation vector
  if (0 < m) {				// Ignore superdiagonal elements.
    doubleComplexSubSquare&	L = *this;	// lower triangular view
    doubleComplexSubSquare	U = L.t();	// upper triangular view
    doubleComplexSubVector	d = L.diag();	//	   diagonal view
    p.ramp();
  
    for (Offset i = 0; i < m-1; i++) {	// all except the last column
      Offset	k = i + norm(d.sub(i, m-i, 1)).max();	// pivot row and column
      if (i < k) {
	// Swap row i for row k and column i for column k
 	// without disturbing any superdiagonal elements.
	if (0 < i) {
	  doubleComplexSubVector	l = L[k].sub(0, i, 1);
	  L[i].sub(0, i, 1).swap(l);
	  }
	if (k+1 < m) {
	  doubleComplexSubVector	u = U[k].sub(k+1, m-k-1, 1);
	  U[i].sub(k+1, m-k-1, 1).swap(u);
          }
        if (i+1 < k) {
	  doubleComplexSubVector	u = U[i].sub(i+1, k-i-1, 1);
	  L[k].sub(i+1, k-i-1, 1).swap(u);
	  }
	d.swap(i, k);			// Swap L[i][i] and L[k][k].
	p.swap(i, k);			// Update the permutation vector.
	}
      if (0 < norm(L[i][i])) {		// still nonsingular
	L[i][i] = sqrt(L[i][i]);
	doubleComplexSubVector	u = U[i].sub(i+1, m-i-1, 1);
	if (0 < i)
	  u -= L[i].sub(0, i, 1).dot(L.sub(i+1, m-i-1, 1, 0, i, 1));
	u /= L[i][i];
	d.sub(i+1, m-i-1, 1) -= u*u;
	}
      else
	break;
      }
    if (0 < norm(L[m-1][m-1])) {	// still nonsingular
      L[m-1][m-1] = sqrt(L[m-1][m-1]);	// last row and column
      }
    }
  return p;
  }

// Operators

