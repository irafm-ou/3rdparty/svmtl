/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleComplexVector.h>


// Functions
// v_{j} = v_{n-1-j}
doubleComplexSubVector&
  doubleComplexSubVector::reverse(void) {
  const
  Extent        n = extent();
  for (Offset j = 0; j < n/2; j++)
    swap(j, n-1 - j);
  return *this;
  }
doubleSubVector&
  doubleSubVector::pack(const doubleComplexSubVector& y) {
  doubleSubVector&
		x = *this;
  Extent	n = x.extent();
  if (0 < n) {
    x.even() = y.real().sub(0, (n+1)/2, 1);	// (n+1)/2 = n/2 if n is even
    x.odd()  = y.imag().sub(0,     n/2, 1);	// (n-1)/2 = n/2 if n is odd
    }
  return x;
  }

doubleComplexSubVector&
  doubleComplexSubVector::pack(const doubleSubVector& x) {
  doubleComplexSubVector&
		y = *this;
  Extent	n = x.extent();
  if (0 < n) {
    y.real().sub(0, (n+1)/2, 1) = x.even();	// (n+1)/2 = n/2 if n is even
    y.imag().sub(0,     n/2, 1) = x.odd();	// (n-1)/2 = n/2 if n is odd
    }
  return y;
  }

doubleComplexSubVector&
	doubleComplexSubVector::transpose(bool b) {
  // This function transposes the elements of a vector v of extent n
  // in-place using a mixed radix digit-reverse algorithm
  // where the radix vector r contains the prime factors of n
  // sorted in order from least to greatest.  No element of the vector
  // is moved to another location in the vector more than once.
  doubleComplexSubVector&
		v = *this;
  Extent	n = v.extent();
  offsetVector	r = offsetPrimeFactor(n);
  if (1 < r.extent()) {			// n is not prime
    offsetMixedRadixDigitReverse
		t(r);
    // The algorithm searches for every sequence k_s of S indices
    // such that a circular shift of elements v_{k_s} <-- v_{k_{s+1}}
    // and v_{k_{S-1}} <-- v_{k_0} effects an in-place transpose.
    // The first and last elements of the vector never move
    // so the transpose is complete after no more than n-2 moves.
    Extent	m = 0;			// count up to n-2 moves
    Offset	l = 0;			// 1 <= l <= n-2
    while (++l < n-1 && m < n-2) {
      Offset	k = l;
      Offset	j = k;
      while (l < (k = (b? t.next(j): t.last(j)))) {
	j = k;				// Search backward for k < l.
	}
      // If a sequence of indices beginning with l has any index k < l,
      // it has already been transposed.  The sequence length S = 1
      // and diagonal element v_k is its own transpose if k = j.
      // Skip every index sequence that has already been transposed.
      if (k == l) {			// a new sequence
	if (k < j) {			// with 1 < S
	  doubleComplex	x = v[k];		// save v_{k_0}
	  do {
	    v[k] = v[j];		// v_{k_{s}} <-- v_{k_{s+1}}
	    k = j;
	    ++m;
	    } while (l < (j = (b? t.last(k): t.next(k))));
	  v[k] = x;			// v_{k_{S-1}} <-- v_{k_0}
	  }
	++m;
	}
      }
    }
  return v;
  }

doubleComplexSubVector&
	doubleComplexSubVector::transpose(Extent p, Extent q) {
  doubleComplexSubVector&
		v = *this;
  if (1 < p && 1 < q) {
    // A vector v of extent n = qp is viewed as a q by p matrix U and
    // a p by q matrix V where U_{ij} = v_{p*i+j} and V_{ij} = v_{q*i+j}.
    // The vector v is modified in-place so that V is the transpose of U.
    // The algorithm searches for every sequence k_s of S indices
    // such that a circular shift of elements v_{k_s} <-- v_{k_{s+1}}
    // and v_{k_{S-1}} <-- v_{k_0} effects an in-place transpose.
    Extent	n = q*p;
    Extent	m = 0;			// count up to n-2
    Offset	l = 0;			// 1 <= l <= n-2
    while (++l < n-1 && m < n-2) {
      Offset	k = l;
      Offset	j = k;
      while (l < (k = (j%p)*q + j/p)) {	// Search backward for k < l.
	j = k;
	}
      // If a sequence of indices beginning with l has any index k < l,
      // it has already been transposed.  The sequence length S = 1
      // and diagonal element v_k is its own transpose if k = j.
      // Skip every index sequence that has already been transposed.
      if (k == l) {			// a new sequence
	if (k < j) {			// with 1 < S
	  doubleComplex	x = v[k];		// save v_{k_0}
	  do {
	    v[k] = v[j];		// v_{k_{s}} <-- v_{k_{s+1}}
	    k = j;
	    ++m;
	    } while (l < (j = (k%q)*p + k/q));
	  v[k] = x;			// v_{k_{S-1}} <-- v_{k_0}
	  }
	++m;
	}
      }
    }
  return v;
  }
doubleComplexSubVector&
  doubleComplexSubVector::twiddle(int sign) {
  doubleComplexSubVector&
		w = *this;
  Extent	n = w.extent();
  if (0 < n) {
    sign = (sign < 0)? -1: +1;
    w.imag().ramp() *= sign*2*M_PI/n;
    w.real() = cos(w.imag());
    w.imag() = sin(w.imag());
    }
  return w;
  }

doubleVector
  doubleComplexSubVector::sdftcr(const doubleSubVector& x) const {
  // Slow Discrete Fourier Transform Complex from Real  y = w.sdftcr(x)
  // (y_{2k}, y_{2k+1}) = x_0*w_0^k + x_1*w_1^k + ... + x_{n-1}*w_{n-1}^k
  // where w_j = e^{-i*2*pi*j/n} for forward Discrete Fourier Transforms
  //   and w_j = e^{+i*2*pi*j/n} for reverse Discrete Fourier Transforms.
  // Because y_{2(n-k)} = +y_{2k} and y_{2(n-k)+1} = -y_{2k+1},
  // the complex result is packed into a real vector y of length n with
  // the real and imaginary parts in the even and odd elements respectively.
  // Since the result is real for k = 0, y_{n} is stored in y_{1}.
  // The computational complexity of the algorithm is order n^2.

  const
  doubleComplexSubVector&
		w = *this;
  Extent	n = w.extent();
  doubleVector	y(n);			// return vector
  if (0 < n) {
    y[0] = x.sum();			// w_{j}^0 = 1
    for (Offset m = 2; m < n; m += 2) {
      Offset	k = m >> 1;		// m = 2k
      Offset	l = (m+1 < n)? m+1: 1;	// l = 2k+1 or 1 if 2k+1 = n
      doubleComplex
		t = doubleComplex(x[0], (double)0);	// w_{0}^k = 1
      for (Offset j = 1; j < n; j++)
	t += x[j]*w[j*k%n];
      y[m] = t.real();			// real and
      y[l] = t.imag();			// imaginary parts
      }
    if (0 == (1&n)) {			// n is even and y_{1} <-- y_{n}
      y[1] = x.even().sum()		// w_{n/2}^{2k}   = +1
  	   - x.odd().sum();		// w_{n/2}^{2k+1} = -1
      }
    }
  return y;
  }

doubleSubVector&
  doubleSubVector::rcfdft(const doubleComplexSubVector& w) {
  // (in-place) Real to Complex Fast Discrete Fourier Transform  x.rcfdft(w)
  // (y_{2k}, y_{2k+1}) = x_0*w_0^k + x_1*w_1^k + ... + x_{n-1}*w_{n-1}^k
  // where w_j = e^{-i*2*pi*j/n} for forward Fast Fourier Transforms
  //   and w_j = e^{+i*2*pi*j/n} for reverse Fast Fourier Transforms
  // Because y_{2(n-k)} = +y_{2k} and y_{2(n-k)+1} = -y_{2k+1},
  // the complex result vector y is packed back into real vector x with
  // the real and imaginary parts in the even and odd elements respectively.
  // Since the result is real for k = 0, y_{n} is stored in y_{1}.

  doubleSubVector&
		x = *this;
  Extent	n = x.extent();
  if (1 < n) {
    Extent	p = lpf(n);		// least prime factor of n = qp
    if (p == n) {			// n is prime
      x.rcsdft(w);// (in-place) Real to Complex Slow Discrete Fourier Transform
      }

    else {				// n is not prime
      // computational complexity order n*log(n) recursive algorithm
      // The recursive in-place algorithm has four parts.
      // 1.) transpose matrix X (vector x viewed as a q by p matrix) in-place
      //     to get matrix Y = X^{T} (vector x viewed as a p by q matrix),
      // 2.) apply the fast Fourier transform to each row of matrix Y,
      // 3.) multiply each column of matrix Y by the corresponding column
      //     of matrix M where M_{lk} = w_{l*k} = e^{-/+i*2*pi*l*k/n} then
      //     apply the discrete Fourier transform to each column of Y and
      // 4.) reverse elements Y_{2h+1, 2} through Y_{2h+1, p-1} in place,
      //     swap element Y_{2h, 1} with Y_{2h+1, 0} for 0 < 2h+1 < p and
      //     swap element Y_{0, 1} with Y_{p-1, 1} if p is odd.

      Extent	q = n/p;
      doubleSubVector&			// part 1
//		y = x.transpose(p, q);
		y = x;			// Call x.transpose(false) before entry.
      doubleComplexSubVector		// part 2
		w_q = w.sub(0, q, p);
      for (Offset h = 0; h < p; h++)
	y.sub(q*h, q, 1).rcfdft(w_q);
      doubleComplexSubVector		// part 3
		w_p = w.sub(0, p, q);
      y.sub(0, p, q).rcsdft(w_p);		// column 0
      for (Offset m = 2; m < q; m += 2) {	// column m and column l
	Offset	k = m >> 1;		// m = 2k
	Offset	l = (m+1 < q)? m+1: 1;	// l = 2k+1 or 1 if 2k+1 = q
	doubleSubVector
		r_k = y.sub(m, p, q);	// real and
	doubleSubVector
		i_k = y.sub(l, p, q);	// imaginary parts
	doubleComplexVector		// column k
		t = doubleComplexVector(r_k, i_k);
		t *= w.sub(0, p, k);	// multiply by column k of matrix M
		t.sdft(w_p);		// apply DFT in-place
	doubleComplexSubVector		// t = [u, v.r()]
		u = t.sub(0, (p+1)/2, +1);	// (p+1)/2 = p/2 if p is even
	doubleComplexSubVector		// t = [u, v.r()]
		v = t.sub(p-1,   p/2, -1);	// (p-1)/2 = p/2 if p is odd
		v = conj(v);	// y_{2(qh + q-k)}   = +y_{2(q(p-1-h) + k)} and
				// y_{2(qh + q-k)+1} = -y_{2(q(p-1-h) + k)+1}
	r_k.even() = u.real(); i_k.even() = u.imag();	// standard order
	r_k.odd()  = v.imag(); i_k.odd()  = v.real();	// reversed order
	}
      if (0 == (1&q)) {			// q is even
	doubleSubVector
		y_1 = y.sub(1, p, q);	// Y_{h, 1} = Y_{h, q}
	doubleComplexVector
		t = y_1*w.sub(0, p, q/2);
		t.sdft(w_p);
		y_1.pack(t);
	}
					// part 4
      for (Offset l = 1; l < p; l += 2) {
	Offset	m = l-1;
	y.sub(q*l+2, q-2, 1).reverse();
	double	t = y[q*m+1]; y[q*m+1] = y[q*l]; y[q*l] = t;
	}
      if (1&p) {			// p is odd
	double	t = y[1]; y[1] = y[n-q+1]; y[n-q+1] = t;
	}
      }
    }
  return x;
  }

doubleSubVector&
  doubleSubVector::cdft(int sign) {	// real to complex dft
  Extent	n = length();
  if (1 < n) {
    doubleComplexVector
		w(n);
		w.twiddle(sign);
    transpose(false).rcfdft(w);
    }
  return *this;
  }

doubleVector
  doubleComplexSubVector::sdftrc(const doubleSubVector& y) const {
  // Slow Discrete Fourier Transform Real from Complex  x = w.sdftrc(y)
  // x_j = (y_0,y_1)*w_0^j + (y_2,y_3)*w_1^j +...+ (y_{2n-2},y_{2n-1})*w_{n-1}^j
  // where w_k = e^{-i*2*pi*k/n} for forward Discrete Fourier Transforms
  //   and w_k = e^{+i*2*pi*k/n} for reverse Discrete Fourier Transforms
  // Because y_{2(n-k)} = +y_{2k} and y_{2(n-k)+1} = -y_{2k+1},
  // the complex source is packed into a real vector y of length n with
  // the real and imaginary parts in the even and odd elements respectively.
  // Since the source is real for k = 0, y_{n} is stored in y_{1}.
  // The computational complexity of the algorithm is order n^2.

  const
  doubleComplexSubVector&
		w = *this;
  Extent	n = w.extent();
  doubleVector	x(n);			// result vector
  if (0 < n) {
    x[0] = 2*y.even().sum() - y[0];	// w_{k}^0 = 1
    if (0 == (1&n))			// n is even
      x[0] += y[1];			// y_{1} = y{n}

    for (Offset j = 1; j < n; j++) {
      double	t = 0.5*y[0];		// w_{0}^j = 1
      for (Offset m = 2; m < n; m += 2) {
	Offset	k = m >> 1;		// m = 2k
	Offset	l = (m+1 < n)? m+1: 1;	// l = 2k+1 or 1 if 2k+1 = n
	t += y[m]*w[k*j%n].real() - y[l]*w[k*j%n].imag();
	}
      t *= 2.0;
      if (0 == (1&n))			// n is even and y_{n} = y_{1}
	if (1&j)			// j is odd	
	  t -= y[1];			// w_{n/2}^j = -1
	else				// j is even
	  t += y[1];			// w_{n/2}^j = +1
      x[j] = t;
      }
    }
  return x;
  }

doubleSubVector&
  doubleSubVector::crfdft(const doubleComplexSubVector& w) {
  // (in-place) Complex to Real Fast Discrete Fourier Transform  y.crfdft(w)
  // x_j = (y_0,y_1)*w_0^j + (y_2,y_3)*w_1^j +...+ (y_{2n-2},y_{2n-1})*w_{n-1}^j
  // where w_k = e^{-i*2*pi*k/n} for forward Discrete Fourier Transforms
  //   and w_k = e^{+i*2*pi*k/n} for reverse Discrete Fourier Transforms
  // Because y_{2(n-k)} = +y_{2k} and y_{2(n-k)+1} = -y_{2k+1},
  // the complex source is packed into a real vector y of length n with
  // the real and imaginary parts in the even and odd elements respectively.
  // Since the source is real for k = 0, y_{n} is stored in y_{1}.

  doubleSubVector&
		y = *this;
  Extent	n = y.extent();
  if (1 < n) {
    Extent	p = lpf(n);		// least prime factor of n = qp
    if (p == n) {			// n is prime
      y.crsdft(w);// (in-place) Complex to Real Slow Discrete Fourier Transform
      }

    else {				// n is not prime
      // computational complexity order n*log(n) recursive algorithm
      // The recursive in-place algorithm has four parts.
      // 1.) swap element Y_{0, 1} with Y_{p-1, 1} if p is odd then
      //     swap element Y_{2h, 1} with Y_{2h+1, 0} for 0 < 2h+1 < p and
      //     reverse elements Y_{2h+1, 2} through Y_{2h+1, p-1} in place,
      // 2.) apply the discrete Fourier transform to each column of Y then
      //     multiply each column of matrix Y by the corresponding column
      //     of matrix M where M_{lk} = w_{l*k} = e^{-/+i*2*pi*l*k/n},
      // 3.) apply the fast Fourier transform to each row of matrix Y,
      // 4.) transpose matrix Y (vector x viewed as a p by q matrix) in-place
      //     to get matrix X = Y^{T} (vector x viewed as a q by p matrix).

      Extent	q = n/p;		// part 1
      if (1&p) {			// p is odd
	double	t = y[1]; y[1] = y[n-q+1]; y[n-q+1] = t;
	}
      for (Offset l = 1; l < p; l += 2) {
	Offset	m = l-1;
	double	t = y[q*m+1]; y[q*m+1] = y[q*l]; y[q*l] = t;
	y.sub(q*l+2, q-2, 1).reverse();
	}
      doubleComplexSubVector		// part 2
		w_p = w.sub(0, p, q);
      y.sub(0, p, q).crsdft(w_p);		// column 0
      for (Offset m = 2; m < q; m += 2) {	// column m and column l
	Offset	k = m >> 1;		// m = 2k
	Offset	l = (m+1 < q)? m+1: 1;	// l = 2k+1 or 1 if 2k+1 = q
	doubleSubVector
		r_k = y.sub(m, p, q);	// real and
	doubleSubVector
		i_k = y.sub(l, p, q);	// imaginary parts
	doubleComplexVector		// column k
		t = doubleComplexVector(r_k.even().aside( i_k.odd().r()),
					 i_k.even().aside(-r_k.odd().r()));
		t.sdft(w_p) *= w.sub(0, p, k);
	r_k = t.real(); i_k = t.imag();
	}
      if (0 == (1&q)) {			// q is even
	doubleSubVector
		y_1 = y.sub(1, p, q);	// Y_{h, 1} = Y_{h, q}
        doubleComplexVector
		t(p);
		t.pack(y_1);
	if (1&p)			// p is odd
	  t[(p-1)/2].imag() = (double)0;
		t.sub(p-1, p/2, -1) = conj(t.sub(0, p/2, +1));
        	t.sdft(w_p) *= w.sub(0, p, q/2);
		y_1 = t.real();
	}
      doubleComplexSubVector		// part 3
		w_q = w.sub(0, q, p);
      for (Offset h = 0; h < p; h++)
	y.sub(q*h, q, 1).crfdft(w_q);
//    doubleSubVector&			// part 4
//		x = y.transpose(q, p);	// Call y.transpose(true) after return.
      }
    }
  return y;
  }

doubleSubVector&
  doubleSubVector::rdft(int sign) {	// complex to real dft
  Extent	n = length();
  if (1 < n) {
    doubleComplexVector
		w(n);
		w.twiddle(sign);
    crfdft(w).transpose(true);
    }
  return *this;
  }

doubleComplexSubVector&		// Slow Discrete Fourier Transform
  doubleComplexSubVector::sdft(const doubleComplexSubVector& w) {
  // v <-- dft(v) (in-place)
  // v_k <-- v_0*w_0^k + v_1*w_1^k + v_2*w_2^k + ... + v_{n-1}*w_{n-1}^k
  // where w_j = e^{-i*2*pi*j/n} for forward Discrete Fourier Transforms
  //   and w_j = e^{+i*2*pi*j/n} for inverse Discrete Fourier Transforms
  // computational complexity order n^2	algorithm
  Extent	n = extent();
  doubleComplexVector
		t(n);			// temporary workspace
  doubleComplexSubVector&
		v = *this;
  for (Offset k = 0; k < n; k++) {
    doubleComplex	z = v[0];
    for (Offset j = 1; j < n; j++) {
      z += v[j]*w[j*k%n];
      }
    t[k] = z;
    }
  return v = t;
  }

doubleComplexSubVector&		// Fast Discrete Fourier Transform
  doubleComplexSubVector::fdft(const doubleComplexSubVector& w) {
  // v <-- dft(v) (in-place)
  // v_k = v_0*w_0^k + v_1*w_1^k + v_2*w_2^k + ... + v_{n-1}*w_{n-1}^k
  // where w_j = e^{-i*2*pi*j/n} for forward Fast Fourier Transforms
  //   and w_j = e^{+i*2*pi*j/n} for inverse Fast Fourier Transforms
  doubleComplexSubVector&
		v = *this;
  Extent	n = v.extent();
  Extent	p = lpf(n);		// least prime factor of n = qp
  if (p == n) {				// n is prime
    v.sdft(w);				// Slow Discrete Fourier Transform
    }
  else {				// n is not prime
    // computational complexity order n*log(n) recursive algorithm
    // The recursive in-place algorithm has three parts.
    // 1.) transpose matrix U (vector v viewed as a q by p matrix)
    //     in-place to get matrix V (vector v viewed as a p by q matrix),
    // 2.) apply the fast Fourier transform to each row of V
    //     and multiply by the corresponding row of matrix M
    //     where M_{lk} = w_{l*k} = e^{-/+i*2*pi*l*k/n} then
    // 3.) apply the discrete Fourier transform to each column of V.
    Extent	q = n/p;
//  v.transpose(p, q);			// part 1
    doubleComplexSubVector
		w_q = w.sub(0, q, p);
    for (Offset l = 0; l < p; l++) {	// part 2
      v.sub(q*l, q, 1).fdft(w_q) *= w.sub(0, q, l);
      }
    doubleComplexSubVector
		w_p = w.sub(0, p, q);
    for (Offset k = 0; k < q; k++) {	// part 3
      v.sub(k, p, q).sdft(w_p);
      }
    }
  return v;
  }

doubleComplexSubVector&
  doubleComplexSubVector::dft(int sign) {	// complex to complex dft
  Extent	n = length();
  if (1 < n) {
    doubleComplexVector
		w(n);
		w.twiddle(sign);
    transpose().fdft(w);
    }
  return *this;
  }

doubleComplexSubVector&
  doubleComplexSubVector::swap(doubleComplexSubVector& w) {
  doubleComplexHandle&	h  = (doubleComplexHandle&)handle();
  Offset		o  = offset();
  Extent		n  = extent();
  Stride		s  = stride();
  doubleComplexHandle&	hw = (doubleComplexHandle&)w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();

  for (Offset j = 0; j < n; j++) {
    doubleComplex t = h.get(o); h.put(o, hw.get(ow)); hw.put(ow, t);
    o += s; ow += sw;
    }
  return *this;
  }

doubleComplexSubVector&
  doubleComplexSubVector::rotate(Stride n) {
  if (n < 0) {
    reverse().rotate(-n);
    }
  else {
    if (1 < extent()) {
      doubleComplexHandle&
			h = (doubleComplexHandle&)handle();
      Offset		o = offset();
      Stride		s = stride();

      n %= extent();
      for (Offset i = 0; i < gcd((Extent)n, extent()); i++) {
	doubleComplex		x = h.get(o + (Stride)i*s);
	Offset		j = i;
	Offset		k = 0;
	while (i != (k = (j+n)%extent())) {
	  h.put(o + (Stride)j*s, h.get(o + (Stride)k*s));
	  j = k;
	  }
	h.put(o + (Stride)j*s, x);
	}
      }
    }
  return *this;
  }

doubleComplexSubVector&
  doubleComplexSubVector::shift(Stride n,const doubleComplex& x) {
  if (n < 0) {
    reverse().shift(-n,x);
    }
  else {
    doubleComplexHandle&
			h = (doubleComplexHandle&)handle();
    Offset		o = offset();
    Stride		s = stride();
    for (Offset j = 0; n+j < extent(); j++) {
      h.put(o + (Stride)j*s, h.get(o + (Stride)(j+n)*s));
      }
    Offset		k = ((Offset)n < extent())? extent() - n: 0;
    for (Offset j = k;   j < extent(); j++) {
      h.put(o + (Stride)j*s, x);
      }
    }
  return *this;
  }


// v.sum() = v_{0} + v_{1} + ... + v_{n-1}
doubleComplex
  doubleComplexSubVector::sum(void) const {
  doubleComplex			result = doubleComplex((double)0, (double)0);
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();

  for (Offset j = 0; j < n; j++) {
    result += h.get(o);
    o += s;
    }
  return result;
  }


// v.dot(w) = v_{0}*w_{0} + v_{1}*w_{1} + ... + v_{n-1}*w_{n-1}
doubleComplex
  doubleComplexSubVector::dot(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::dot(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleComplex			result = doubleComplex((double)0, (double)0);
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&		hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();

  for (Offset j = 0; j < n; j++) {
    result += h.get(o)*hw.get(ow);
    o += s; ow += sw;
    }
  return result;
  }

// v.dot(w) = v_{0}*w_{0} + v_{1}*w_{1} + ... + v_{n-1}*w_{n-1}
doubleComplex
  doubleComplexSubVector::dot(const doubleComplexSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::dot(const doubleComplexSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleComplex			result = doubleComplex((double)0, (double)0);
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleComplexHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();

  for (Offset j = 0; j < n; j++) {
    result += h.get(o)*hw.get(ow);
    o += s; ow += sw;
    }
  return result;
  }

const
doubleComplexVector
  doubleComplexSubVector::permutation(const offsetSubVector& p) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::permutation(const offsetSubVector&) const",
    extent(), p.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubVector&	v = *this;
  Offset			n = extent();
  doubleComplexVector		w(n);
  for (Offset j = 0; j < n; j++) {
    w[j] = v[p[j]];
    }
  return w;
  }


const
boolVector
  doubleComplexSubVector::eq(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::eq(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&		hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  boolVector		result(extent());
  boolHandle&		p = (boolHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o) == hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }

const
boolVector
  doubleComplexSubVector::ne(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::ne(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&		hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  boolVector		result(extent());
  boolHandle&		p = (boolHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o) != hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }

const
boolVector
  doubleComplexSubVector::eq(const doubleComplexSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::eq(const doubleComplexSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleComplexHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  boolVector		result(extent());
  boolHandle&		p = (boolHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o) == hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }

const
boolVector
  doubleComplexSubVector::ne(const doubleComplexSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::ne(const doubleComplexSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleComplexHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  boolVector		result(extent());
  boolHandle&		p = (boolHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o) != hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }

Extent
  doubleComplexSubVector::zeros(void) const {
  doubleComplex			zero = doubleComplex((double)0, (double)0);
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();

  Extent		result = 0;
  for (Offset j = 0; j < n; j++) {
    if (zero == h.get(o)) ++result;
    o += s;
    }
  return result;
  }

const
offsetVector
  doubleComplexSubVector::index(void) const {
  doubleComplex			zero = doubleComplex((double)0, (double)0);
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  offsetVector		result(n - zeros());
  offsetHandle&		p = (offsetHandle&)result.handle();

  Offset		k = 0;
  for (Offset j = 0; j < n; j++) {
    if (zero != h.get(o))
      p.put(k++, j);
    o += s;
    }
  return result;
  }

const
doubleComplexVector
  doubleComplexSubVector::gather(const offsetSubVector& x) const {
  const
  doubleComplexHandle&	h  =   handle();
  Offset		o  =   offset();
  Extent		n  =   extent();
  Stride		s  =   stride();
  const
  offsetHandle&		hx = x.handle();
  Offset		ox = x.offset();
  Extent		nx = x.extent();
  Stride		sx = x.stride();
  doubleComplexVector	result(nx);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < nx; j++) {
    Offset		k = hx.get(ox);
#ifdef SVMT_DEBUG_MODE
    svmt_check_vector_index_range(
      "doubleComplexSubVector::gather(const offsetSubVector&) const", k, n);
#endif // SVMT_DEBUG_MODE
    p.put(j, h.get(o + (Stride)k*s));
    ox += sx;
    }
  return result;
  }

doubleComplexSubVector&
  doubleComplexSubVector::scatter(const offsetSubVector& x,
    const doubleComplexSubVector& t) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::scatter(\n"
    "const offsetSubVector&, const doubleComplexSubVector&)",
    x.extent(), t.extent());
#endif // SVMT_DEBUG_MODE
  doubleComplexHandle&	h  =   (doubleComplexHandle&)handle();
  Offset		o  =   offset();
  Extent		n  =   extent();
  Stride		s  =   stride();
  const
  offsetHandle&		hx = x.handle();
  Offset		ox = x.offset();
  Extent		nx = x.extent();
  Stride		sx = x.stride();
  const
  doubleComplexHandle&	ht = t.handle();
  Offset		ot = t.offset();
  Stride		st = t.stride();

  for (Offset j = 0; j < nx; j++) {
    Offset		k = hx.get(ox);
#ifdef SVMT_DEBUG_MODE
    svmt_check_vector_index_range(
      "doubleComplexSubVector::scatter(\n"
      "const offsetSubVector&, const doubleComplexSubVector&)", k, n);
#endif // SVMT_DEBUG_MODE
    h.put(o + (Stride)k*s, ht.get(ot));
    ox += sx;
    ot += st;
    }
  return *this;
  }

const
doubleComplexVector
  doubleComplexSubVector::aside(const doubleComplexSubVector& w) const {
  doubleComplexVector	result(extent() + w.extent());
  result.sub((Offset)0, extent(), (Stride)1) = *this;
  result.sub(extent(), w.extent(), (Stride)1) = w;
  return result;
  }
const
doubleComplexVector
  doubleComplexSubVector::kron(const doubleComplexSubVector& w) const {
  const
  doubleComplexSubVector&
			v = *this;
  Extent		nv = v.extent();
  Extent		nw = w.extent();
  doubleComplexVector	result(nv*nw);
  doubleComplexVector&	shadow = result;

  for (Offset j = 0; j < nv; j++) {
    shadow.sub((Stride)j*nw, nw, (Stride)1) = v[j]*w;
    }
  return result;
  }

const
doubleComplexVector
  doubleComplexSubVector::kron(const doubleSubVector& w) const {
  const
  doubleComplexSubVector&
			v = *this;
  Extent		nv = v.extent();
  Extent		nw = w.extent();
  doubleComplexVector	result(nv*nw);
  doubleComplexVector&	shadow = result;

  for (Offset j = 0; j < nv; j++) {
    shadow.sub(j*nw, nw, (Stride)1) = v[j]*w;
    }
  return result;
  }

const
doubleComplexVector
  doubleSubVector::kron(const doubleComplexSubVector& w) const {
  const
  doubleSubVector&	v = *this;
  Extent		nv = v.extent();
  Extent		nw = w.extent();
  doubleComplexVector	result(nv*nw);
  doubleComplexVector&	shadow = result;

  for (Offset j = 0; j < nv; j++) {
    shadow.sub(j*nw, nw, (Stride)1) = v[j]*w;
    }
  return result;
  }

const
doubleComplexVector
  doubleComplexSubVector::apply(const doubleComplex (*f)(const doubleComplex&)) const {
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, f(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
  doubleComplexSubVector::apply(      doubleComplex (*f)(const doubleComplex&)) const {
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, f(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
  doubleComplexSubVector::apply(      doubleComplex (*f)(      doubleComplex )) const {
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, f(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
    exp(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,   exp(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
    log(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,   log(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
   sqrt(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,  sqrt(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
    cos(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,   cos(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
    sin(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,   sin(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
    tan(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,   tan(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
   acos(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,  acos(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
   asin(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,  asin(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
   atan(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,  atan(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
   cosh(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,  cosh(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
   sinh(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,  sinh(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
   tanh(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,  tanh(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
  acosh(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, acosh(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
  asinh(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, asinh(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleComplexVector
  atanh(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, atanh(h.get(o)));
    o += s;
    }
  return result;
  }


// Complex Functions
const
doubleComplexVector
   conj(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	hv = v.handle();
  Offset		ov = v.offset();
  Extent		nv = v.extent();
  Stride		sv = v.stride();
  doubleComplexVector	result(nv);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < nv; j++) {
    p.put(j,  conj(hv.get(ov)));
    ov += sv;
    }
  return result;
  }

const
doubleComplexVector
  iconj(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	hv = v.handle();
  Offset		ov = v.offset();
  Extent		nv = v.extent();
  Stride		sv = v.stride();
  doubleComplexVector	result(nv);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < nv; j++) {
    p.put(j, iconj(hv.get(ov)));
    ov += sv;
    }
  return result;
  }

const
doubleComplexVector
  polar(const doubleSubVector& v, const doubleSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "polar(const doubleSubVector&, const doubleSubVector&)",
    v.extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&		hv = v.handle();
  Offset		ov = v.offset();
  Extent		nv = v.extent();
  Stride		sv = v.stride();
  const
  doubleHandle&		hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleComplexVector	result(nv);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < nv; j++) {
    p.put(j, polar(hv.get(ov), hw.get(ow)));
    ov += sv; ow += sw;
    }
  return result;
  }

const
doubleVector
   norm(const doubleComplexSubVector& v) {
  const
  doubleComplexHandle&	hv = v.handle();
  Offset		ov = v.offset();
  Extent		nv = v.extent();
  Stride		sv = v.stride();
  doubleVector		result(nv);
  doubleHandle&		p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < nv; j++) {
    p.put(j,  norm(hv.get(ov)));
    ov += sv;
    }
  return result;
  }

// Operators

// minus
const
doubleComplexVector
  doubleComplexSubVector::operator -(void) const {
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, -h.get(o));
    o += s;
    }
  return result;
  }


// multiply
const
doubleComplexVector
  doubleComplexSubVector::operator *(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator *(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&		hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o)*hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }

// divide
const
doubleComplexVector
  doubleComplexSubVector::operator /(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator /(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&		hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o)/hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }

// add
const
doubleComplexVector
  doubleComplexSubVector::operator +(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator +(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&		hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o) + hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }

// subtract
const
doubleComplexVector
  doubleComplexSubVector::operator -(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator -(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&		hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o) - hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }

// divide
const
doubleComplexVector
  doubleSubVector::operator /(const doubleComplexSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::operator /(const doubleComplexSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubVector&	v = *this;
  const
  doubleHandle&		hv = v.handle();
  Offset		ov = v.offset();
  Extent		nv = v.extent();
  Stride		sv = v.stride();
  const
  doubleComplexHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleComplexVector	result(nv);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < nv; j++) {
    p.put(j, hv.get(ov)/hw.get(ow));
    ov += sv; ow += sw;
    }
  return result;
  }

// subtract
const
doubleComplexVector
  doubleSubVector::operator -(const doubleComplexSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::operator -(const doubleComplexSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubVector&	v = *this;
  const
  doubleHandle&		hv = v.handle();
  Offset		ov = v.offset();
  Extent		nv = v.extent();
  Stride		sv = v.stride();
  const
  doubleComplexHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleComplexVector	result(nv);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < nv; j++) {
    p.put(j, hv.get(ov) - hw.get(ow));
    ov += sv; ow += sw;
    }
  return result;
  }

// multiply
const
doubleComplexVector
  doubleComplexSubVector::operator *(const doubleComplexSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator *(const doubleComplexSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleComplexHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o)*hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }

// divide
const
doubleComplexVector
  doubleComplexSubVector::operator /(const doubleComplexSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator /(const doubleComplexSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleComplexHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o)/hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }


// add
const
doubleComplexVector
  doubleComplexSubVector::operator +(const doubleComplexSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator +(const doubleComplexSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleComplexHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o) + hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }

// subtract
const
doubleComplexVector
  doubleComplexSubVector::operator -(const doubleComplexSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator -(const doubleComplexSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleComplexHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleComplexVector	result(n);
  doubleComplexHandle&	p = (doubleComplexHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o) - hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }


std::istream&	operator >>(std::istream& is,       doubleComplexSubVector& v) {
  doubleComplexHandle&	h = (doubleComplexHandle&)v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleComplex			x;

  for (Offset j = 0; j < n && is >> x; j++) {
    h.put(o, x);
    o += s;
    }
  return is;
  }

Extent		doubleComplexSubVector::C = 0;
std::ostream&	operator <<(std::ostream& os, const doubleComplexSubVector& v) {
  doubleComplexHandle&	h = (doubleComplexHandle&)v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  int			width = os.width()%65536;
  int			columns = os.width()/65536;
  if (columns <= 0) {
    columns = doubleComplexSubVector::columns();
    if (columns <= 0) {
      columns = 2;
      }
    }
  for (Offset j = 1; j <= n; j++) {
    if (os << std::setw(width) << h.get(o))
      if (j < n && j%columns)
	os << ' ';
      else
	os << '\n';
    o += s;
    }
  return os;
  }


// equal
bool	doubleComplexSubVector::operator ==(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator ==(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&		hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  Offset		j = 0;
  while (j < n && h.get(o) == hw.get(ow)) {
   ++j; o += s; ow += sw;
   }
  return j == n;
  }

// not equal
bool	doubleComplexSubVector::operator !=(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator !=(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&		hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  Offset		j = 0;
  while (j < n && h.get(o) != hw.get(ow)) {
   ++j; o += s; ow += sw;
   }
  return j == n;
  }

// equal
bool	doubleComplexSubVector::operator ==(
  const doubleComplexSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator ==(const doubleComplexSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleComplexHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  Offset		j = 0;
  while (j < n && h.get(o) == hw.get(ow)) {
   ++j; o += s; ow += sw;
   }
  return j == n;
  }

// not equal
bool	doubleComplexSubVector::operator !=(
  const doubleComplexSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator !=(const doubleComplexSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleComplexHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  Offset		j = 0;
  while (j < n && h.get(o) != hw.get(ow)) {
   ++j; o += s; ow += sw;
   }
  return j == n;
  }



// simple assignment
doubleComplexSubVector&
  doubleComplexSubVector::operator  =(const doubleSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator  =(const doubleSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleComplexHandle&	h = (doubleComplexHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&		hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  for (Offset j = 0; j < n; j++) {
    h.put(o, hw.get(ow));
    o += s; ow += sw;
    }
  return *this;
  }

// multiply and assign
doubleComplexSubVector&
  doubleComplexSubVector::operator *=(const doubleSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator *=(const doubleSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleComplexHandle&	h = (doubleComplexHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&		hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  for (Offset j = 0; j < n; j++) {
    h.put(o, h.get(o)*hw.get(ow));
    o += s; ow += sw;
    }
  return *this;
  }

// divide and assign
doubleComplexSubVector&
  doubleComplexSubVector::operator /=(const doubleSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator /=(const doubleSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleComplexHandle&	h = (doubleComplexHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&		hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  for (Offset j = 0; j < n; j++) {
    h.put(o, h.get(o)/hw.get(ow));
    o += s; ow += sw;
    }
  return *this;
  }

// add and assign
doubleComplexSubVector&
  doubleComplexSubVector::operator +=(const doubleSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator +=(const doubleSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleComplexHandle&	h = (doubleComplexHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&		hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  for (Offset j = 0; j < n; j++) {
    h.put(o, h.get(o) + hw.get(ow));
    o += s; ow += sw;
    }
  return *this;
  }

// subtract and assign
doubleComplexSubVector&
  doubleComplexSubVector::operator -=(const doubleSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator -=(const doubleSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleComplexHandle&	h = (doubleComplexHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&		hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  for (Offset j = 0; j < n; j++) {
    h.put(o, h.get(o) - hw.get(ow));
    o += s; ow += sw;
    }
  return *this;
  }

// simple assignment
doubleComplexSubVector&
  doubleComplexSubVector::operator  =(const doubleComplexSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator  =(const doubleComplexSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleComplexHandle&	h = (doubleComplexHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleComplexHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  for (Offset j = 0; j < n; j++) {
    h.put(o, hw.get(ow));
    o += s; ow += sw;
    }
  return *this;
  }

// multiply and assign
doubleComplexSubVector&
  doubleComplexSubVector::operator *=(const doubleComplexSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator *=(const doubleComplexSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleComplexHandle&	h = (doubleComplexHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleComplexHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  for (Offset j = 0; j < n; j++) {
    h.put(o, h.get(o)*hw.get(ow));
    o += s; ow += sw;
    }
  return *this;
  }

// divide and assign
doubleComplexSubVector&
  doubleComplexSubVector::operator /=(const doubleComplexSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator /=(const doubleComplexSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleComplexHandle&	h = (doubleComplexHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleComplexHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  for (Offset j = 0; j < n; j++) {
    h.put(o, h.get(o)/hw.get(ow));
    o += s; ow += sw;
    }
  return *this;
  }


// add and assign
doubleComplexSubVector&
  doubleComplexSubVector::operator +=(const doubleComplexSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator +=(const doubleComplexSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleComplexHandle&	h = (doubleComplexHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleComplexHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  for (Offset j = 0; j < n; j++) {
    h.put(o, h.get(o) + hw.get(ow));
    o += s; ow += sw;
    }
  return *this;
  }

// subtract and assign
doubleComplexSubVector&
  doubleComplexSubVector::operator -=(const doubleComplexSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::operator -=(const doubleComplexSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleComplexHandle&	h = (doubleComplexHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleComplexHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  for (Offset j = 0; j < n; j++) {
    h.put(o, h.get(o) - hw.get(ow));
    o += s; ow += sw;
    }
  return *this;
  }


