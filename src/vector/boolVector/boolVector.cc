/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<boolVector.h>

// Functions
// v_{j} = v_{n-1-j}
boolSubVector&
  boolSubVector::reverse(void) {
  const
  Extent        n = extent();
  for (Offset j = 0; j < n/2; j++)
    swap(j, n-1 - j);
  return *this;
  }
boolSubVector&
  boolSubVector::swap(boolSubVector& w) {
  boolHandle&	h = (boolHandle&)handle();
  Offset	o  = offset();
  Extent	n  = extent();
  Stride	s  = stride();
  boolHandle&	hw = (boolHandle&)w.handle();
  Offset	ow = w.offset();
  Stride	sw = w.stride();

  for (Offset j = 0; j < n; j++) {
    bool t = h.get(o); h.put(o, hw.get(ow)); hw.put(ow, t);
    o += s; ow += sw;
    }
  return *this;
  }

boolSubVector&
  boolSubVector::rotate(Stride n) {
  if (n < 0) {
    r().rotate(-n);
    }
  else {
    if (1 < extent()) {
      boolHandle&	h = (boolHandle&)handle();
      Offset		o = offset();
      Stride		s = stride();

      n %= extent();
      for (Offset i = 0; i < gcd(n, extent()); i++) {
	bool		b = h.get(o + (Stride)i*s);
	Offset		j = i;
	Offset		k = 0;
	while (i != (k = (j+n)%extent())) {
	  h.put(o + (Stride)j*s, h.get(o + (Stride)k*s));
	  j = k;
	  }
	h.put(o + (Stride)j*s, b);
	}
      }
    }
  return *this;
  }

boolSubVector&
  boolSubVector::shift(Stride n, bool b) {
  if (n < 0) {
    r().shift(-n, b);
    }
  else {
    boolHandle&	h = (boolHandle&)handle();
    Offset		o = offset();
    Stride		s = stride();
    for (Offset j = 0; n+j < extent(); j++) {
      h.put(o + (Stride)j*s, h.get(o + (Stride)(j+n)*s));
      }
    Offset		k = ((Offset)n < extent())? extent() - n: 0;
    for (Offset j = k;   j < extent(); j++) {
      h.put(o + (Stride)j*s, b);
      }
    }
  return *this;
  }

// xnor
const
boolVector
  boolSubVector::eq(const boolSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "boolSubVector::eq(const boolSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  { const
    boolHandle&		h = handle();
    Offset		o = offset();
    Extent		n = extent();
    Stride		s = stride();
    const
    boolHandle&		hw = w.handle();
    Offset		ow = w.offset();
    Stride		sw = w.stride();
    boolVector		result(n);
    boolHandle&		p = (boolHandle&)result.handle();

    for (Offset j = 0; j < n; j++) {
      p.put(j, h.get(o) == hw.get(ow));
      o += s; ow += sw;
      }
    return result;
    }
  }

// xor
const
boolVector
  boolSubVector::ne(const boolSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "boolSubVector::ne(const boolSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  { const
    boolHandle&		h = handle();
    Offset		o = offset();
    Extent		n = extent();
    Stride		s = stride();
    const
    boolHandle&		hw = w.handle();
    Offset		ow = w.offset();
    Stride		sw = w.stride();
    boolVector		result(n);
    boolHandle&		p = (boolHandle&)result.handle();

    for (Offset j = 0; j < n; j++) {
      p.put(j, h.get(o) != hw.get(ow));
      o += s; ow += sw;
      }
    return result;
    }
  }

Extent
  boolSubVector::zeros(void) const {
  const
  boolHandle&		h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();

  Extent		result = 0;
  for (Offset j = 0; j < n; j++) {
    if (!h.get(o)) ++result;
    o += s;
    }
  return result;
  }

const
offsetVector
  boolSubVector::index(void) const {
  const
  boolHandle&		h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  offsetVector		result(n - zeros());
  offsetHandle&		p = (offsetHandle&)result.handle();

  Offset		k = 0;
  for (Offset j = 0; j < n; j++) {
    if (h.get(o))
      p.put(k++, j);
    o += s;
    }
  return result;
  }

const
boolVector
  boolSubVector::aside(const boolSubVector& w) const {
  const
  boolHandle&		h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  boolHandle&		hw = w.handle();
  Offset		ow = w.offset();
  Extent		nw = w.extent();
  Stride		sw = w.stride();
  boolVector		result(n + nw);
  boolHandle&		p = (boolHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o));
    o += s;
    }
  for (Offset j = n; j < n+nw; j++) {
    p.put(j, hw.get(ow));
    ow += sw;
    }
  return result;
  }

const
boolVector
  boolSubVector::apply(const bool (*f)(const bool&)) const {
  const
  boolHandle&		h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  boolVector		result(n);
  boolHandle&		p = (boolHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, f(h.get(o)));
    o += s;
    }
  return result;
  }

const
boolVector
  boolSubVector::apply(      bool (*f)(const bool&)) const {
  const
  boolHandle&		h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  boolVector		result(n);
  boolHandle&		p = (boolHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, f(h.get(o)));
    o += s;
    }
  return result;
  }

const
boolVector
  boolSubVector::apply(      bool (*f)(      bool )) const {
  const
  boolHandle&		h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  boolVector		result(n);
  boolHandle&		p = (boolHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, f(h.get(o)));
    o += s;
    }
  return result;
  }

// Operators
// not
const
boolVector
  boolSubVector::operator !(void) const {
  const
  boolHandle&		h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  boolVector		result(n);
  boolHandle&		p = (boolHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, !h.get(o));
    o += s;
    }
  return result;
  }

// and
const
boolVector
  boolSubVector::operator &&(const boolSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "boolSubVector::operator &&(const boolSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  { const
    boolHandle&		h = handle();
    Offset		o = offset();
    Extent		n = extent();
    Stride		s = stride();
    const
    boolHandle&		hw = w.handle();
    Offset		ow = w.offset();
    Stride		sw = w.stride();
    boolVector		result(n);
    boolHandle&		p = (boolHandle&)result.handle();

    for (Offset j = 0; j < n; j++) {
      p.put(j, h.get(o) && hw.get(ow));
      o += s; ow += sw;
      }
    return result;
    }
  }

// or
const
boolVector
  boolSubVector::operator ||(const boolSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "boolSubVector::operator &&(const boolSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  { const
    boolHandle&		h = handle();
    Offset		o = offset();
    Extent		n = extent();
    Stride		s = stride();
    const
    boolHandle&		hw = w.handle();
    Offset		ow = w.offset();
    Stride		sw = w.stride();
    boolVector		result(n);
    boolHandle&		p = (boolHandle&)result.handle();

    for (Offset j = 0; j < n; j++) {
      p.put(j, h.get(o) || hw.get(ow));
      o += s; ow += sw;
      }
    return result;
    }
  }

std::istream&	operator >>(std::istream& is, boolSubVector& v) {
  boolHandle&		h = (boolHandle&)v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  bool			b;

  for (Offset j = 0; j < n && is >> b; j++) {
    h.put(o, b);
    o += s;
    }
  return is;
  }

Extent		boolSubVector::C = 0;
std::ostream&	operator <<(std::ostream& os, const boolSubVector& v) {
  boolHandle&		h = (boolHandle&)v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  int			width = os.width()%65536;
  int			columns = os.width()/65536;
  if (columns <= 0) {
    columns = boolSubVector::columns();
    if (columns <= 0) {
      columns = 4;
      }
    }
  for (Offset j = 1; j <= n; j++) {
    if (os << std::setw(width) << h.get(o))
      if (j < n && j%columns)
	os << ' ';
      else
	os << '\n';
    o += s;
    }
  return os;
  }

// equal
bool	boolSubVector::operator ==(const boolSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "boolSubVector::operator ==(const boolSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  { const
    boolHandle&		h = handle();
    Offset		o = offset();
    Extent		n = extent();
    Stride		s = stride();
    const
    boolHandle&		hw = w.handle();
    Offset		ow = w.offset();
    Stride		sw = w.stride();
    Offset		j = 0;
    while (j < n && h.get(o) == hw.get(ow)) {
     ++j; o += s; ow += sw;
     }
    return j == n;
    }
  }

// not equal
bool	boolSubVector::operator !=(const boolSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "boolSubVector::operator !=(const boolSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  { const
    boolHandle&		h = handle();
    Offset		o = offset();
    Extent		n = extent();
    Stride		s = stride();
    const
    boolHandle&		hw = w.handle();
    Offset		ow = w.offset();
    Stride		sw = w.stride();
    Offset		j = 0;
    while (j < n && h.get(o) != hw.get(ow)) {
     ++j; o += s; ow += sw;
     }
    return j == n;
    }
  }

// simple assignment
boolSubVector&
  boolSubVector::operator  =(const boolSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "boolSubVector::operator  =(const boolSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  { boolHandle&		h = (boolHandle&)handle();
    Offset		o = offset();
    Extent		n = extent();
    Stride		s = stride();
    const
    boolHandle&		hw = w.handle();
    Offset		ow = w.offset();
    Stride		sw = w.stride();
    for (Offset j = 0; j < n; j++) {
      h.put(o, hw.get(ow));
      o += s; ow += sw;
      }
    return *this;
    }
  }
