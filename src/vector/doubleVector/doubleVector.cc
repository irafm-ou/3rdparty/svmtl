/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleVector.h>


// Functions
// v_{j} = v_{n-1-j}
doubleSubVector&
  doubleSubVector::reverse(void) {
  const
  Extent        n = extent();
  for (Offset j = 0; j < n/2; j++)
    swap(j, n-1 - j);
  return *this;
  }

doubleSubVector&
	doubleSubVector::transpose(bool b) {
  // This function transposes the elements of a vector v of extent n
  // in-place using a mixed radix digit-reverse algorithm
  // where the radix vector r contains the prime factors of n
  // sorted in order from least to greatest.  No element of the vector
  // is moved to another location in the vector more than once.
  doubleSubVector&
		v = *this;
  Extent	n = v.extent();
  offsetVector	r = offsetPrimeFactor(n);
  if (1 < r.extent()) {			// n is not prime
    offsetMixedRadixDigitReverse
		t(r);
    // The algorithm searches for every sequence k_s of S indices
    // such that a circular shift of elements v_{k_s} <-- v_{k_{s+1}}
    // and v_{k_{S-1}} <-- v_{k_0} effects an in-place transpose.
    // The first and last elements of the vector never move
    // so the transpose is complete after no more than n-2 moves.
    Extent	m = 0;			// count up to n-2 moves
    Offset	l = 0;			// 1 <= l <= n-2
    while (++l < n-1 && m < n-2) {
      Offset	k = l;
      Offset	j = k;
      while (l < (k = (b? t.next(j): t.last(j)))) {
	j = k;				// Search backward for k < l.
	}
      // If a sequence of indices beginning with l has any index k < l,
      // it has already been transposed.  The sequence length S = 1
      // and diagonal element v_k is its own transpose if k = j.
      // Skip every index sequence that has already been transposed.
      if (k == l) {			// a new sequence
	if (k < j) {			// with 1 < S
	  double	x = v[k];		// save v_{k_0}
	  do {
	    v[k] = v[j];		// v_{k_{s}} <-- v_{k_{s+1}}
	    k = j;
	    ++m;
	    } while (l < (j = (b? t.last(k): t.next(k))));
	  v[k] = x;			// v_{k_{S-1}} <-- v_{k_0}
	  }
	++m;
	}
      }
    }
  return v;
  }

doubleSubVector&
	doubleSubVector::transpose(Extent p, Extent q) {
  doubleSubVector&
		v = *this;
  if (1 < p && 1 < q) {
    // A vector v of extent n = qp is viewed as a q by p matrix U and
    // a p by q matrix V where U_{ij} = v_{p*i+j} and V_{ij} = v_{q*i+j}.
    // The vector v is modified in-place so that V is the transpose of U.
    // The algorithm searches for every sequence k_s of S indices
    // such that a circular shift of elements v_{k_s} <-- v_{k_{s+1}}
    // and v_{k_{S-1}} <-- v_{k_0} effects an in-place transpose.
    Extent	n = q*p;
    Extent	m = 0;			// count up to n-2
    Offset	l = 0;			// 1 <= l <= n-2
    while (++l < n-1 && m < n-2) {
      Offset	k = l;
      Offset	j = k;
      while (l < (k = (j%p)*q + j/p)) {	// Search backward for k < l.
	j = k;
	}
      // If a sequence of indices beginning with l has any index k < l,
      // it has already been transposed.  The sequence length S = 1
      // and diagonal element v_k is its own transpose if k = j.
      // Skip every index sequence that has already been transposed.
      if (k == l) {			// a new sequence
	if (k < j) {			// with 1 < S
	  double	x = v[k];		// save v_{k_0}
	  do {
	    v[k] = v[j];		// v_{k_{s}} <-- v_{k_{s+1}}
	    k = j;
	    ++m;
	    } while (l < (j = (k%q)*p + k/q));
	  v[k] = x;			// v_{k_{S-1}} <-- v_{k_0}
	  }
	++m;
	}
      }
    }
  return v;
  }

// v_{j} = j
doubleSubVector&
  doubleSubVector::ramp(void) {
  doubleHandle&	h = (doubleHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();

  for (Offset j = 0; j < n; j++) {
    h.put(o, (double)j);
    o += s;
    }
  return *this;
  }

doubleSubVector&
  doubleSubVector::swap(doubleSubVector& w) {
  doubleHandle&	h  = (doubleHandle&)handle();
  Offset		o  = offset();
  Extent		n  = extent();
  Stride		s  = stride();
  doubleHandle&	hw = (doubleHandle&)w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();

  for (Offset j = 0; j < n; j++) {
    double t = h.get(o); h.put(o, hw.get(ow)); hw.put(ow, t);
    o += s; ow += sw;
    }
  return *this;
  }

doubleSubVector&
  doubleSubVector::rotate(Stride n) {
  if (n < 0) {
    reverse().rotate(-n);
    }
  else {
    if (1 < extent()) {
      doubleHandle&
			h = (doubleHandle&)handle();
      Offset		o = offset();
      Stride		s = stride();

      n %= extent();
      for (Offset i = 0; i < gcd((Extent)n, extent()); i++) {
	double		x = h.get(o + (Stride)i*s);
	Offset		j = i;
	Offset		k = 0;
	while (i != (k = (j+n)%extent())) {
	  h.put(o + (Stride)j*s, h.get(o + (Stride)k*s));
	  j = k;
	  }
	h.put(o + (Stride)j*s, x);
	}
      }
    }
  return *this;
  }

doubleSubVector&
  doubleSubVector::shift(Stride n,const double& x) {
  if (n < 0) {
    reverse().shift(-n,x);
    }
  else {
    doubleHandle&
			h = (doubleHandle&)handle();
    Offset		o = offset();
    Stride		s = stride();
    for (Offset j = 0; n+j < extent(); j++) {
      h.put(o + (Stride)j*s, h.get(o + (Stride)(j+n)*s));
      }
    Offset		k = ((Offset)n < extent())? extent() - n: 0;
    for (Offset j = k;   j < extent(); j++) {
      h.put(o + (Stride)j*s, x);
      }
    }
  return *this;
  }


Offset
  doubleSubVector::min(void) const {
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();

  double		m = h.get(o);
  Offset		k = 0;
  for (Offset j = 1; j < n; j++) {
    o += s;
    if (h.get(o) < m) {
      m = h.get(o);
      k = j;
      }
    }
  return k;
  }

Offset
  doubleSubVector::max(void) const {
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();

  double		m = h.get(o);
  Offset		k = 0;
  for (Offset j = 1; j < n; j++) {
    o += s;
    if (h.get(o) > m) {
      m = h.get(o);
      k = j;
      }
    }
  return k;
  }

const
doubleVector
  min(const doubleSubVector& v, const doubleSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "min(const doubleSubVector&, const doubleSubVector&)",
    v.extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&	hv = v.handle();
  Offset		ov = v.offset();
  Extent		nv = v.extent();
  Stride		sv = v.stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleVector	result(nv);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < nv; j++) {
    p.put(j, (hv.get(ov) < hw.get(ow))? hv.get(ov): hw.get(ow));
    ov += sv; ow += sw;
    }
  return result;
  }

const
doubleVector
  max(const doubleSubVector& v, const doubleSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "max(const doubleSubVector&, const doubleSubVector&)",
    v.extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&	hv = v.handle();
  Offset		ov = v.offset();
  Extent		nv = v.extent();
  Stride		sv = v.stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleVector	result(nv);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < nv; j++) {
    p.put(j, (hv.get(ov) > hw.get(ow))? hv.get(ov): hw.get(ow));
    ov += sv; ow += sw;
    }
  return result;
  }

// v.sum() = v_{0} + v_{1} + ... + v_{n-1}
double
  doubleSubVector::sum(void) const {
  double			result = (double)0;
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();

  for (Offset j = 0; j < n; j++) {
    result += h.get(o);
    o += s;
    }
  return result;
  }


// v.dot(w) = v_{0}*w_{0} + v_{1}*w_{1} + ... + v_{n-1}*w_{n-1}
double
  doubleSubVector::dot(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::dot(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  double			result = (double)0;
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();

  for (Offset j = 0; j < n; j++) {
    result += h.get(o)*hw.get(ow);
    o += s; ow += sw;
    }
  return result;
  }

const
doubleVector
  doubleSubVector::permutation(const offsetSubVector& p) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::permutation(const offsetSubVector&) const",
    extent(), p.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubVector&	v = *this;
  Offset			n = extent();
  doubleVector		w(n);
  for (Offset j = 0; j < n; j++) {
    w[j] = v[p[j]];
    }
  return w;
  }

const
boolVector
  doubleSubVector::lt(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::lt(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  boolVector		result(extent());
  boolHandle&		p = (boolHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o) <  hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }

const
boolVector
  doubleSubVector::le(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::le(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  boolVector		result(extent());
  boolHandle&		p = (boolHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o) <= hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }

const
boolVector
  doubleSubVector::eq(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::eq(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  boolVector		result(extent());
  boolHandle&		p = (boolHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o) == hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }

const
boolVector
  doubleSubVector::ne(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::ne(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  boolVector		result(extent());
  boolHandle&		p = (boolHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o) != hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }

Extent
  doubleSubVector::zeros(void) const {
  double			zero = (double)0;
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();

  Extent		result = 0;
  for (Offset j = 0; j < n; j++) {
    if (zero == h.get(o)) ++result;
    o += s;
    }
  return result;
  }

const
offsetVector
  doubleSubVector::index(void) const {
  double			zero = (double)0;
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  offsetVector		result(n - zeros());
  offsetHandle&		p = (offsetHandle&)result.handle();

  Offset		k = 0;
  for (Offset j = 0; j < n; j++) {
    if (zero != h.get(o))
      p.put(k++, j);
    o += s;
    }
  return result;
  }

const
doubleVector
  doubleSubVector::gather(const offsetSubVector& x) const {
  const
  doubleHandle&	h  =   handle();
  Offset		o  =   offset();
  Extent		n  =   extent();
  Stride		s  =   stride();
  const
  offsetHandle&		hx = x.handle();
  Offset		ox = x.offset();
  Extent		nx = x.extent();
  Stride		sx = x.stride();
  doubleVector	result(nx);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < nx; j++) {
    Offset		k = hx.get(ox);
#ifdef SVMT_DEBUG_MODE
    svmt_check_vector_index_range(
      "doubleSubVector::gather(const offsetSubVector&) const", k, n);
#endif // SVMT_DEBUG_MODE
    p.put(j, h.get(o + (Stride)k*s));
    ox += sx;
    }
  return result;
  }

doubleSubVector&
  doubleSubVector::scatter(const offsetSubVector& x,
    const doubleSubVector& t) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::scatter(\n"
    "const offsetSubVector&, const doubleSubVector&)",
    x.extent(), t.extent());
#endif // SVMT_DEBUG_MODE
  doubleHandle&	h  =   (doubleHandle&)handle();
  Offset		o  =   offset();
  Extent		n  =   extent();
  Stride		s  =   stride();
  const
  offsetHandle&		hx = x.handle();
  Offset		ox = x.offset();
  Extent		nx = x.extent();
  Stride		sx = x.stride();
  const
  doubleHandle&	ht = t.handle();
  Offset		ot = t.offset();
  Stride		st = t.stride();

  for (Offset j = 0; j < nx; j++) {
    Offset		k = hx.get(ox);
#ifdef SVMT_DEBUG_MODE
    svmt_check_vector_index_range(
      "doubleSubVector::scatter(\n"
      "const offsetSubVector&, const doubleSubVector&)", k, n);
#endif // SVMT_DEBUG_MODE
    h.put(o + (Stride)k*s, ht.get(ot));
    ox += sx;
    ot += st;
    }
  return *this;
  }

const
doubleVector
  doubleSubVector::aside(const doubleSubVector& w) const {
  doubleVector	result(extent() + w.extent());
  result.sub((Offset)0, extent(), (Stride)1) = *this;
  result.sub(extent(), w.extent(), (Stride)1) = w;
  return result;
  }
const
doubleVector
  doubleSubVector::kron(const doubleSubVector& w) const {
  const
  doubleSubVector&
			v = *this;
  Extent		nv = v.extent();
  Extent		nw = w.extent();
  doubleVector	result(nv*nw);
  doubleVector&	shadow = result;

  for (Offset j = 0; j < nv; j++) {
    shadow.sub((Stride)j*nw, nw, (Stride)1) = v[j]*w;
    }
  return result;
  }

const
doubleVector
  doubleSubVector::apply(const double (*f)(const double&)) const {
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, f(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
  doubleSubVector::apply(      double (*f)(const double&)) const {
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, f(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
  doubleSubVector::apply(      double (*f)(      double )) const {
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, f(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
    exp(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,   exp(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
    log(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,   log(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
   sqrt(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,  sqrt(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
    cos(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,   cos(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
    sin(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,   sin(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
    tan(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,   tan(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
   acos(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,  acos(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
   asin(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,  asin(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
   atan(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,  atan(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
   cosh(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,  cosh(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
   sinh(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,  sinh(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
   tanh(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,  tanh(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
  acosh(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, acosh(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
  asinh(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, asinh(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
  atanh(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, atanh(h.get(o)));
    o += s;
    }
  return result;
  }


const
doubleVector
    sgn(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,   sgn(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
    abs(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  using std::abs;
  for (Offset j = 0; j < n; j++) {
    p.put(j,   abs(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
  floor(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, floor(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
   ceil(const doubleSubVector& v) {
  const
  doubleHandle&	h = v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j,  ceil(h.get(o)));
    o += s;
    }
  return result;
  }

const
doubleVector
  hypot(const doubleSubVector& v, const doubleSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "hypot(const doubleSubVector&, const doubleSubVector&)",
    v.extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&	hv = v.handle();
  Offset		ov = v.offset();
  Extent		nv = v.extent();
  Stride		sv = v.stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleVector	result(nv);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < nv; j++) {
    p.put(j, hypot(hv.get(ov), hw.get(ow)));
    ov += sv; ow += sw;
    }
  return result;
  }

const
doubleVector
  atan2(const doubleSubVector& w, const doubleSubVector& v) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "atan2(const doubleSubVector&, const doubleSubVector&)",
    w.extent(), v.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Extent		nw = w.extent();
  Stride		sw = w.stride();
  const
  doubleHandle&	hv = v.handle();
  Offset		ov = v.offset();
  Stride		sv = v.stride();
  doubleVector	result(nw);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < nw; j++) {
    p.put(j, atan2(hw.get(ow), hv.get(ov)));
    ow += sw; ov += sv;
    }
  return result;
  }

// Operators

// minus
const
doubleVector
  doubleSubVector::operator -(void) const {
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, -h.get(o));
    o += s;
    }
  return result;
  }


// multiply
const
doubleVector
  doubleSubVector::operator *(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::operator *(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o)*hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }

// divide
const
doubleVector
  doubleSubVector::operator /(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::operator /(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o)/hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }


// add
const
doubleVector
  doubleSubVector::operator +(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::operator +(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o) + hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }

// subtract
const
doubleVector
  doubleSubVector::operator -(const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::operator -(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  doubleVector	result(n);
  doubleHandle&	p = (doubleHandle&)result.handle();

  for (Offset j = 0; j < n; j++) {
    p.put(j, h.get(o) - hw.get(ow));
    o += s; ow += sw;
    }
  return result;
  }


std::istream&	operator >>(std::istream& is,       doubleSubVector& v) {
  doubleHandle&	h = (doubleHandle&)v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  double			x;

  for (Offset j = 0; j < n && is >> x; j++) {
    h.put(o, x);
    o += s;
    }
  return is;
  }

Extent		doubleSubVector::C = 0;
std::ostream&	operator <<(std::ostream& os, const doubleSubVector& v) {
  doubleHandle&	h = (doubleHandle&)v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  int			width = os.width()%65536;
  int			columns = os.width()/65536;
  if (columns <= 0) {
    columns = doubleSubVector::columns();
    if (columns <= 0) {
      columns = 4;
      }
    }
  for (Offset j = 1; j <= n; j++) {
    if (os << std::setw(width) << h.get(o))
      if (j < n && j%columns)
	os << ' ';
      else
	os << '\n';
    o += s;
    }
  return os;
  }

// less than
bool	doubleSubVector::operator < (
  const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::operator < (const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  Offset		j = 0;
  while (j < n && h.get(o) <  hw.get(ow)) {
   ++j; o += s; ow += sw;
   }
  return j == n;
  }

// less than or equal
bool	doubleSubVector::operator <=(
  const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::operator <=(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  Offset		j = 0;
  while (j < n && h.get(o) <= hw.get(ow)) {
   ++j; o += s; ow += sw;
   }
  return j == n;
  }

// equal
bool	doubleSubVector::operator ==(
  const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::operator ==(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  Offset		j = 0;
  while (j < n && h.get(o) == hw.get(ow)) {
   ++j; o += s; ow += sw;
   }
  return j == n;
  }

// not equal
bool	doubleSubVector::operator !=(
  const doubleSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::operator !=(const doubleSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleHandle&	h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  Offset		j = 0;
  while (j < n && h.get(o) != hw.get(ow)) {
   ++j; o += s; ow += sw;
   }
  return j == n;
  }



// simple assignment
doubleSubVector&
  doubleSubVector::operator  =(const doubleSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::operator  =(const doubleSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleHandle&	h = (doubleHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  for (Offset j = 0; j < n; j++) {
    h.put(o, hw.get(ow));
    o += s; ow += sw;
    }
  return *this;
  }

// multiply and assign
doubleSubVector&
  doubleSubVector::operator *=(const doubleSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::operator *=(const doubleSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleHandle&	h = (doubleHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  for (Offset j = 0; j < n; j++) {
    h.put(o, h.get(o)*hw.get(ow));
    o += s; ow += sw;
    }
  return *this;
  }

// divide and assign
doubleSubVector&
  doubleSubVector::operator /=(const doubleSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::operator /=(const doubleSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleHandle&	h = (doubleHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  for (Offset j = 0; j < n; j++) {
    h.put(o, h.get(o)/hw.get(ow));
    o += s; ow += sw;
    }
  return *this;
  }


// add and assign
doubleSubVector&
  doubleSubVector::operator +=(const doubleSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::operator +=(const doubleSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleHandle&	h = (doubleHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  for (Offset j = 0; j < n; j++) {
    h.put(o, h.get(o) + hw.get(ow));
    o += s; ow += sw;
    }
  return *this;
  }

// subtract and assign
doubleSubVector&
  doubleSubVector::operator -=(const doubleSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::operator -=(const doubleSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  doubleHandle&	h = (doubleHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();
  const
  doubleHandle&	hw = w.handle();
  Offset		ow = w.offset();
  Stride		sw = w.stride();
  for (Offset j = 0; j < n; j++) {
    h.put(o, h.get(o) - hw.get(ow));
    o += s; ow += sw;
    }
  return *this;
  }


