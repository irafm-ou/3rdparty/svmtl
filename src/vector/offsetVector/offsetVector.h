#ifndef _offsetVector_h
#define _offsetVector_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<offsetScalar.h>
#include<svmt_error.h>
#include<iostream>
#include<iomanip>

#ifdef	   __FCC_VERSION
#define	SMANIP(T) std::smanip<std::ios_base::fmtflags>
#else	// __FCC_VERSION
#ifdef	   __KCC
#define SMANIP(T) __kai::smanip_gen<streamsize>
#else	// __KCC
#ifdef	   __SUNPRO_CC
#define SMANIP(T) std::smanip<T>
#else	// __SUNPRO_CC
#ifdef	   __GNUG__
#if	   __GNUG__ < 3
#define SMANIP(T) smanip<T>
#else	// __GNUG__ < 3
#define SMANIP(T) std::_Setw
#endif	// __GNUG__ < 3
#endif	// __GNUG__
#endif	// __SUNPRO_CC
#endif	// __KCC
#endif	// __FCC_VERSION
inline SMANIP(int)
  setw(int width, int columns) { return std::setw(65536*columns + width); }
#undef SMANIP

class offsetSubVector {
  private:
  static
  Extent	C;
  // Representation
  offsetHandle	H;
  Offset	O;
  Extent	N;
  Stride	S;
  public:
  // Constructors
		offsetSubVector(void);
		offsetSubVector(const offsetHandle& h,
      Offset o, Extent n, Stride s);
		offsetSubVector(const offsetSubVector& v);
		~offsetSubVector(void);
  // Functions
  static
  Extent&	columns(void);
  const
  offsetHandle&	handle(void) const;
  Offset	offset(void) const;
  Extent	extent(void) const;
  Extent	length(void) const;
  Stride	stride(void) const;
  bool		 empty(void) const;
  static
  offsetHandle
	      allocate(Extent n);
  offsetSubVector&
		  free(void);
  offsetSubVector&
		resize(void);
  offsetSubVector&
		resize(const offsetHandle& h,
      Offset o, Extent n, Stride s);
  offsetSubVector&
		resize(const offsetSubVector& v);
  offsetSubVector    r(void);
  const
  offsetSubVector    r(void) const;
  offsetSubVector&
	       reverse(void);
  offsetSubVector&
		  ramp(void);
  offsetSubVector&
		  swap(Offset j, Offset k);
  offsetSubVector&
		  swap_(Offset j, Offset k);
  offsetSubVector&
		  swap(const offsetSubVector& w);
  Offset	   min(void) const;
  Offset	   max(void) const;
  Offset	   min_(void) const;
  Offset	   max_(void) const;

  // Operators
  offsetSubScalar
		operator [](Offset j);
  const
  offsetSubScalar
		operator [](Offset j) const;
  offsetSubScalar
		operator ()(Offset j);
  const
  offsetSubScalar
		operator ()(Offset j) const;
  bool		operator < (Offset j) const;
  bool		operator <=(Offset j) const;
  bool		operator > (Offset j) const;
  bool		operator >=(Offset j) const;
  bool		operator < (const offsetSubVector& w) const;
  bool		operator <=(const offsetSubVector& w) const;
  bool		operator > (const offsetSubVector& w) const;
  bool		operator >=(const offsetSubVector& w) const;
  bool		operator ==(Offset j) const;
  bool		operator !=(Offset j) const;
  bool		operator ==(const offsetSubVector& w) const;
  bool		operator !=(const offsetSubVector& w) const;
  offsetSubVector&
		operator  =(Offset j);
  offsetSubVector&
		operator  =(const offsetSubVector& w);
  friend class offsetVector;
  };

// Constructors
inline		offsetSubVector::offsetSubVector(void):
  H((Offset*)0), O((Offset)0), N((Extent)0), S((Stride)0) { }
inline		offsetSubVector::offsetSubVector(
  const offsetHandle& h, Offset o, Extent n, Stride s):
  H(h), O(o), N(n), S(s) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment("offsetSubVector::offsetSubVector(\n"
    "const offsetHandle&, Offset, Extent, Stride)", o, n, s);
#endif // SVMT_DEBUG_MODE
  }
inline		offsetSubVector::offsetSubVector(const offsetSubVector& v):
  H(v.H), O(v.O), N(v.N), S(v.S) { }
inline		offsetSubVector::~offsetSubVector(void) { }

// Functions
inline
Extent&		offsetSubVector::columns(void) { return C; }
inline
const
offsetHandle&	offsetSubVector::handle(void) const { return H; }
inline
Offset		offsetSubVector::offset(void) const { return O; }
inline
Extent		offsetSubVector::extent(void) const { return N; }
inline
Extent		offsetSubVector::length(void) const { return N; }
inline
Stride		offsetSubVector::stride(void) const { return S; }
inline
bool		offsetSubVector::empty(void) const {
  return handle().empty() || 0 == extent(); }
inline
offsetHandle	offsetSubVector::allocate(Extent n) {
  return offsetHandle(new Offset[n]); }
inline
offsetSubVector&
		offsetSubVector::free(void) {
  delete [] (Offset*)H; return *this; }
inline
offsetSubVector&
		offsetSubVector::resize(void) {
  H = offsetHandle((Offset*)0); O = (Offset)0;
  N = (Extent)0; S = (Stride)0; return *this; }
inline
offsetSubVector&
		offsetSubVector::resize(const offsetHandle& h, Offset o,
    Extent n, Stride s) { H = h; O = o; N = n; S = s;
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment("offsetSubVector::resize(const offsetHandle&,\n"
    "Offset, Extent, Stride)",
    o, n, s);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
offsetSubVector&
		offsetSubVector::resize(const offsetSubVector& v) {
  return resize(v.handle(), v.offset(),
    v.extent(), v.stride()); }
inline
offsetSubVector	offsetSubVector::r(void) { return offsetSubVector(
  handle(), offset() + (Stride)(extent() - 1)*stride(), extent(), -stride()); }
inline
const
offsetSubVector	offsetSubVector::r(void) const { return offsetSubVector(
  handle(), offset() + (Stride)(extent() - 1)*stride(), extent(), -stride()); }
inline
Offset		min(const offsetSubVector& v) {
  return v.handle().get(v.offset() + (Stride)v.min()*v.stride()); }
inline
Offset		max(const offsetSubVector& v) {
  return v.handle().get(v.offset() + (Stride)v.max()*v.stride()); }
offsetVector    offsetPrimeFactor(Offset n);

// Operators
inline
offsetSubScalar	offsetSubVector::operator [](Offset j) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "offsetSubVector::operator [](Offset)", j, extent());
#endif // SVMT_DEBUG_MODE
  return offsetSubScalar(handle(), offset() + (Stride)j*stride()); }
inline
const
offsetSubScalar	offsetSubVector::operator [](Offset j) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "offsetSubVector::operator [](Offset) const", j, extent());
#endif // SVMT_DEBUG_MODE
  return offsetSubScalar(handle(), offset() + (Stride)j*stride()); }
inline
offsetSubScalar	offsetSubVector::operator ()(Offset j) {
  return operator [](j-1); }
inline
const
offsetSubScalar	offsetSubVector::operator ()(Offset j) const {
  return operator [](j-1); }

std::istream&	operator >>(std::istream& s,       offsetSubVector& v);
std::ostream&	operator <<(std::ostream& s, const offsetSubVector& v);

inline
offsetSubVector&
		offsetSubVector::swap(Offset j, Offset k) {
  offsetSubVector&
		v = *this;
  Offset	t = v[j]; v[j] = v[k]; v[k] = t; return v; }
inline
offsetSubVector&
		offsetSubVector::swap_(Offset j, Offset k) {
  return swap(j-1, k-1); }

class offsetSubArray1: public offsetSubVector {
  public:
  // Constructors
		offsetSubArray1(void);
		offsetSubArray1(Offset* p, Offset o, Extent n, Stride s);
		offsetSubArray1(const offsetSubArray1& v);
		~offsetSubArray1(void);
  // Functions
  offsetSubArray1&
		  ramp(void);
  offsetSubArray1&
		  swap(Offset j, Offset k);
  offsetSubArray1&
		  swap_(Offset j, Offset k);
  offsetSubArray1&
		  swap(const offsetSubVector& w);
  // Operators
  offsetSubArray0
		operator [](Offset j);
  const
  offsetSubArray0
		operator [](Offset j) const;
  offsetSubArray0
		operator ()(Offset j);
  const
  offsetSubArray0
		operator ()(Offset j) const;
  offsetSubArray1&
		operator  =(Offset j); offsetSubArray1&
		operator  =(const offsetSubVector& w);
  };

// Constructors
inline		offsetSubArray1::offsetSubArray1(void): offsetSubVector() { }
inline		offsetSubArray1::offsetSubArray1(Offset* p,
    Offset o, Extent n, Stride s):
  offsetSubVector(offsetHandle(p), o, n, s) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment("offsetSubVector::offsetSubArray1(\n"
    "Offset*, Offset, Extent, Stride)", o, n, s);
#endif // SVMT_DEBUG_MODE
  }
inline		offsetSubArray1::offsetSubArray1(
    const offsetSubArray1& v): offsetSubVector(v) { }
inline		offsetSubArray1::~offsetSubArray1(void) { }

// Functions
inline
offsetSubArray1&
		offsetSubArray1::ramp(void) {
  offsetSubVector::ramp(); return *this; }
inline
offsetSubArray1&
		offsetSubArray1::swap(Offset j, Offset k) {
  offsetSubVector::swap(j, k); return *this; }
inline
offsetSubArray1&
		offsetSubArray1::swap_(Offset j, Offset k) {
  return swap(j-1, k-1); }
inline
offsetSubArray1&
		offsetSubArray1::swap(const offsetSubVector& w) {
  offsetSubVector::swap(w); return *this; }

// Operators
inline
offsetSubArray0	offsetSubArray1::operator [](Offset j) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "offsetSubArray1::operator [](Offset)", j, extent());
#endif // SVMT_DEBUG_MODE
  return offsetSubArray0(
  (Offset*)(offsetHandle&)handle(), offset() + (Stride)j*stride()); }
inline
const
offsetSubArray0	offsetSubArray1::operator [](Offset j) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "offsetSubArray1::operator [](Offset) const", j, extent());
#endif // SVMT_DEBUG_MODE
  return offsetSubArray0(
  (Offset*)(offsetHandle&)handle(), offset() + (Stride)j*stride()); }
inline
offsetSubArray0	offsetSubArray1::operator ()(Offset j) {
  return operator [](j-1); }
inline
const
offsetSubArray0	offsetSubArray1::operator ()(Offset j) const {
  return operator [](j-1); }

// offsetSubVector operators which use offsetSubArray1
inline
bool		offsetSubVector::operator > (const offsetSubVector& w) const {
  return w <  *this; }
inline
bool		offsetSubVector::operator >=(const offsetSubVector& w) const {
  return w <= *this; }
inline
bool		offsetSubVector::operator < (Offset j) const {
  return operator < (offsetSubArray1(
  (Offset*)&j, (Offset)0, extent(), (Stride)0)); }
inline
bool		offsetSubVector::operator <=(Offset j) const {
  return operator <=(offsetSubArray1(
  (Offset*)&j, (Offset)0, extent(), (Stride)0)); }
inline
bool		offsetSubVector::operator > (Offset j) const {
  return operator > (offsetSubArray1(
  (Offset*)&j, (Offset)0, extent(), (Stride)0)); }
inline
bool		offsetSubVector::operator >=(Offset j) const {
  return operator >=(offsetSubArray1(
  (Offset*)&j, (Offset)0, extent(), (Stride)0)); }
inline
bool		operator < (Offset j, const offsetSubVector& v) {
  return v >  j; }
inline
bool		operator <=(Offset j, const offsetSubVector& v) {
  return v >= j; }
inline
bool		operator > (Offset j, const offsetSubVector& v) {
  return v <  j; }
inline
bool		operator >=(Offset j, const offsetSubVector& v) {
  return v <= j; }
inline
bool		offsetSubVector::operator ==(Offset j) const {
  return operator ==(offsetSubArray1(
    (Offset*)&j, (Offset)0, extent(), (Stride)0)); }
inline
bool		offsetSubVector::operator !=(Offset j) const {
  return operator !=(offsetSubArray1(
    (Offset*)&j, (Offset)0, extent(), (Stride)0)); }
inline
bool		operator ==(Offset j, const offsetSubVector& v) {
  return v == j; }
inline
bool		operator !=(Offset j, const offsetSubVector& v) {
  return v != j; }

inline
offsetSubVector&
		offsetSubVector::operator  =(Offset j) {
  return operator = (offsetSubArray1(
  (Offset*)&j, (Offset)0, extent(), (Stride)0)); }
inline
offsetSubArray1&
		offsetSubArray1::operator  =(const offsetSubVector& w) {
  offsetSubVector::operator  =(w); return *this; }
inline
offsetSubArray1&
		offsetSubArray1::operator  =(Offset j) {
  offsetSubVector::operator = (j); return *this; }

class offsetVector: public offsetSubVector {
  public:
  // Constructors
		offsetVector(void);
  explicit	offsetVector(Extent n);
		offsetVector(Extent n, Offset o);
		offsetVector(const offsetSubVector& v);
		offsetVector(const offsetVector& v);
		~offsetVector(void);
  // Functions
  offsetVector&	resize(void);
  offsetVector&	resize(Extent n);
  offsetVector&	resize(const offsetSubVector& v);
private:
  offsetVector& resize(const offsetHandle& h, Offset o, Extent n, Stride s);
  offsetVector&	free(void) { offsetSubVector::free(); return *this; }
public:
  // Operators
  offsetVector&	operator  =(const offsetSubVector& w);
  offsetVector&	operator  =(const offsetVector& w);
  };

// Constructors
inline		offsetVector::offsetVector(void):
  offsetSubVector(offsetHandle((Offset*)0), (Offset)0, (Extent)0, (Stride)0) { }
inline		offsetVector::offsetVector(Extent n):
  offsetSubVector(allocate(n), (Offset)0, n, (Stride)1) { }
inline		offsetVector::offsetVector(Extent n, Offset o):
  offsetSubVector(allocate(n), (Offset)0, n, (Stride)1) {
  offsetSubVector::operator = (o); }
inline		offsetVector::offsetVector(const offsetSubVector& v):
  offsetSubVector(allocate(v.extent()), (Offset)0,
    v.extent(), (Stride)1) { offsetSubVector::operator = (v); }
inline		offsetVector::offsetVector(const offsetVector& v):
  offsetSubVector(allocate(v.extent()), (Offset)0,
    v.extent(), (Stride)1) { offsetSubVector::operator = (v); }
inline		offsetVector::~offsetVector(void) { free(); }

// Operators
inline
offsetVector&	offsetVector::operator  =(const offsetSubVector& w) {
  offsetSubVector::operator =(w); return *this; }
inline
offsetVector&	offsetVector::operator  =(const offsetVector& w) {
  offsetSubVector::operator =(w); return *this; }

// Functions
inline
offsetVector&	offsetVector::resize(void) { free();
  offsetSubVector::resize(); return *this; }
inline
offsetVector&	offsetVector::resize(Extent n) { free();
  offsetSubVector::resize(allocate(n), (Offset)0, n, (Stride)1);
  return *this; }
inline
offsetVector&	offsetVector::resize(const offsetSubVector& v) {
  return resize(v.extent()) = v; }

class	offsetMixedRadixDigitReverse {
  // Representation
  offsetVector	p;
  offsetVector	q;

  // Constructors
		offsetMixedRadixDigitReverse(void);
public:
  explicit	offsetMixedRadixDigitReverse(const offsetSubVector& r);
    // Offset i = d_{n-1}*p_{n-1} + ... + d_j*p_j + ... + d_0*p_0
    // where 0 <= d_j < r_j for all j such that 0 <= j < n
    // and p_{j+1} = r_j*p_j where p_0 = 1
		offsetMixedRadixDigitReverse(
    const offsetMixedRadixDigitReverse& t);
		~offsetMixedRadixDigitReverse(void);

  // Functions
  Extent	 places(void) const;
  const
  offsetVector	  radix(void) const;
  const
  offsetVector	  digit(Offset i) const;
  Offset	   next(Offset i) const;
  Offset	   last(Offset i) const;

  // Operators
  offsetMixedRadixDigitReverse&
		operator  =(const offsetMixedRadixDigitReverse& t);
  };

// Constructors
inline		offsetMixedRadixDigitReverse::offsetMixedRadixDigitReverse(
    const offsetMixedRadixDigitReverse& t): p(t.p), q(t.q) { }
inline		offsetMixedRadixDigitReverse::~offsetMixedRadixDigitReverse(
    void) { }

// Functions
inline
Extent	offsetMixedRadixDigitReverse::places(void) const {
  return q.extent()-1; }

// Operators
inline
offsetMixedRadixDigitReverse&
		offsetMixedRadixDigitReverse::operator  =(
    const offsetMixedRadixDigitReverse& t) { p = t.p; q = t.q; return *this; }

#endif /* _offsetVector_h */

