/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<offsetVector.h>

// Functions
// v_{j} = v_{n-1-j}
offsetSubVector&
  offsetSubVector::reverse(void) {
  const
  Extent	n = extent();
  for (Offset j = 0; j < n/2; j++)
    swap(j, n-1 - j);
  return *this;
  }
// v_{j} = j
offsetSubVector&
  offsetSubVector::ramp(void) {
  offsetHandle&		h = (offsetHandle&)handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();

  for (Offset j = 0; j < n; j++) {
    h.put(o, (Offset)j);
    o += s;
    }
  return *this;
  }

Offset
  offsetSubVector::min(void) const {
  const
  offsetHandle&		h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();

  Offset		m = h.get(o);
  Offset		k = 0;
  for (Offset j = 1; j < n; j++) {
    o += s;
    if (h.get(o) < m) {
      m = h.get(o);
      k = j;
      }
    }
  return k;
  }

Offset
  offsetSubVector::max(void) const {
  const
  offsetHandle&		h = handle();
  Offset		o = offset();
  Extent		n = extent();
  Stride		s = stride();

  Offset		m = h.get(o);
  Offset		k = 0;
  for (Offset j = 1; j < n; j++) {
    o += s;
    if (h.get(o) > m) {
      m = h.get(o);
      k = j;
      }
    }
  return k;
  }

offsetVector	offsetPrimeFactor(Offset n) {
  // This function returns the prime factors of Offset n
  // in order from least to greatest.
  const
  Extent	bits = CHAR_BIT*sizeof(Offset);
  Offset	p[bits];
  Extent	factors = 0;
  while (1 < n) {
    p[factors] = lpf(n);		// least prime factor of n
    n /= p[factors];
    ++factors;
    }
  offsetVector	v(factors);
  for (Offset factor = 0; factor < factors; factor++) {
    v[factor] = p[factor];
    }
  return v;
  }

// Operators
std::istream&	operator >>(std::istream& is,       offsetSubVector& v) {
  offsetHandle&		h = (offsetHandle&)v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  Offset		x;

  for (Offset j = 0; j < n && is >> x; j++) {
    h.put(o, x);
    o += s;
    }
  return is;
  }

Extent		offsetSubVector::C = 0;
std::ostream&	operator <<(std::ostream& os, const offsetSubVector& v) {
  offsetHandle&		h = (offsetHandle&)v.handle();
  Offset		o = v.offset();
  Extent		n = v.extent();
  Stride		s = v.stride();
  int			width = os.width()%65536;
  int			columns = os.width()/65536;
  if (columns <= 0) {
    columns = offsetSubVector::columns();
    if (columns <= 0) {
      columns = 4;
      }
    }
  for (Offset j = 1; j <= n; j++) {
    if (os << std::setw(width) << h.get(o))
      if (j < n && j%columns)
	os << ' ';
      else
	os << '\n';
    o += s;
    }
  return os;
  }

// less than
bool	offsetSubVector::operator < (
  const offsetSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "offsetSubVector::operator < (const offsetSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  { const
    offsetHandle&	h = handle();
    Offset		o = offset();
    Extent		n = extent();
    Stride		s = stride();
    const
    offsetHandle&	hw = w.handle();
    Offset		ow = w.offset();
    Stride		sw = w.stride();
    Offset		j = 0;
    while (j < n && h.get(o) <  hw.get(ow)) {
     ++j; o += s; ow += sw;
     }
    return j == n;
    }
  }

// less than or equal
bool	offsetSubVector::operator <=(
  const offsetSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "offsetSubVector::operator <=(const offsetSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  { const
    offsetHandle&	h = handle();
    Offset		o = offset();
    Extent		n = extent();
    Stride		s = stride();
    const
    offsetHandle&	hw = w.handle();
    Offset		ow = w.offset();
    Stride		sw = w.stride();
    Offset		j = 0;
    while (j < n && h.get(o) <= hw.get(ow)) {
     ++j; o += s; ow += sw;
     }
    return j == n;
    }
  }

// equal
bool	offsetSubVector::operator ==(
  const offsetSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "offsetSubVector::operator ==(const offsetSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  { const
    offsetHandle&	h = handle();
    Offset		o = offset();
    Extent		n = extent();
    Stride		s = stride();
    const
    offsetHandle&	hw = w.handle();
    Offset		ow = w.offset();
    Stride		sw = w.stride();
    Offset		j = 0;
    while (j < n && h.get(o) == hw.get(ow)) {
     ++j; o += s; ow += sw;
     }
    return j == n;
    }
  }

// not equal
bool	offsetSubVector::operator !=(
  const offsetSubVector& w) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "offsetSubVector::operator !=(const offsetSubVector&) const",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  { const
    offsetHandle&	h = handle();
    Offset		o = offset();
    Extent		n = extent();
    Stride		s = stride();
    const
    offsetHandle&	hw = w.handle();
    Offset		ow = w.offset();
    Stride		sw = w.stride();
    Offset		j = 0;
    while (j < n && h.get(o) != hw.get(ow)) {
     ++j; o += s; ow += sw;
     }
    return j == n;
    }
  }

// simple assignment
offsetSubVector&
  offsetSubVector::operator  =(const offsetSubVector& w) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "offsetSubVector::operator  =(const offsetSubVector&)",
    extent(), w.extent());
#endif // SVMT_DEBUG_MODE
  { offsetHandle&	h = (offsetHandle&)handle();
    Offset		o = offset();
    Extent		n = extent();
    Stride		s = stride();
    const
    offsetHandle&	hw = w.handle();
    Offset		ow = w.offset();
    Stride		sw = w.stride();
    for (Offset j = 0; j < n; j++) {
      h.put(o, hw.get(ow));
      o += s; ow += sw;
      }
    return *this;
    }
  }

// Constructors
	offsetMixedRadixDigitReverse::offsetMixedRadixDigitReverse(
    const offsetSubVector& r): p(r.extent()+1), q(r.extent()+1) {
  p[0] = 1;
  for (Offset place = 0; place < places(); place++)
    p[place+1] = r[place]*p[place];
  q[places()] = 1;
  for (Offset place = places(); 0 < place; place--)
    q[place-1] = r[place-1]*q[place];
  }

// Functions
const
offsetVector
	offsetMixedRadixDigitReverse::radix(void) const {
  offsetVector r(places());
  for (Offset place = 0; place < places(); place++)
    r[place] = p[place+1]/p[place];
  return r;
  }

const
offsetVector
	offsetMixedRadixDigitReverse::digit(Offset i) const {
  offsetVector d(places());
  for (Offset place = places(); 0 < place; place--) {
    i %= p[place];
    d[place-1] = i/p[place-1];
    }
  return d;
  }

Offset	offsetMixedRadixDigitReverse::next(Offset k) const {
  Offset		j = 0;
  for (Offset place = 0; place < places(); place++) {
    k %= q[place];
    j += k/q[place+1]*p[place];
    }
  return j;
  }

Offset	offsetMixedRadixDigitReverse::last(Offset j) const {
  Offset		k = 0;
  for (Offset place = places(); 0 < place; place--) {
    j %= p[place];
    k += j/p[place-1]*q[place];
    }
  return k;
  }

