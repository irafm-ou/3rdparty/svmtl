/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleMatrix.h>


// Functions
doubleSubMatrix&
  doubleSubMatrix::reverse1(void) {
  doubleSubMatrix&	M = *this;
  Extent			m = extent2();
  for (Offset i = 0; i < m; i++)
    M[i].reverse();
  return *this;
  }
doubleSubMatrix&
  doubleSubMatrix::reverse2(void) {
  Extent			m = extent2();
  for (Offset i = 0; i < m/2; i++)
    swap(i, m-1 - i);
  return *this;
  }


// M_{i,j} = j
doubleSubMatrix&
  doubleSubMatrix::ramp(void) {
  doubleSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i].ramp();
    }
  return *this;
  }

doubleSubMatrix&
  doubleSubMatrix::swap(doubleSubMatrix& N) {
  doubleSubMatrix	M = (*this);
  for (Offset i = 0; i < extent2(); i++) {
    doubleSubVector	v = M[i];
    doubleSubVector	w = N[i];
    v.swap(w);
    }
  return *this;
  }

doubleSubMatrix&
  doubleSubMatrix::rotate(Stride n) {
  doubleSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    doubleSubVector	u = M[i];
    u.rotate(n);
    }
  return *this;
  }

doubleSubMatrix&
  doubleSubMatrix::shift(Stride n,const double& s) {
  doubleSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    doubleSubVector	u = M[i];
    u.shift(n,s);
    }
  return *this;
  }


const
doubleVector
  min(const doubleSubMatrix& M) {
  doubleVector	result(M.extent2());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = min(M[i]);
    }
  return result;
  }

const
doubleVector
  max(const doubleSubMatrix& M) {
  doubleVector	result(M.extent2());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = max(M[i]);
    }
  return result;
  }

const
doubleMatrix
  min(const doubleSubMatrix& M, const doubleSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "min(const doubleSubMatrix&, const doubleSubMatrix&)",
    M.extent2(), N.extent2(), M.extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleMatrix	result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = min(M[i], N[i]);
    }
  return result;
  }

const
doubleMatrix
  max(const doubleSubMatrix& M, const doubleSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "max(const doubleSubMatrix&, const doubleSubMatrix&)",
    M.extent2(), N.extent2(), M.extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleMatrix	result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = max(M[i], N[i]);
    }
  return result;
  }

// M.sum() = M_{i,0} + M_{i,1} + ... + M_{i,n-1}
const
doubleVector
  doubleSubMatrix::sum(void) const {
  const
  doubleSubMatrix&	M = *this;
  doubleVector		result(extent2(), (double)0);
  for (Offset i = 0; i < extent2(); i++) {
    result[i] += M[i].sum();
    }
  return result;
  }


// v.dot(M) = v_{0}*M_{j,0} + v_{1}*M_{j,1} + ... + v_{n-1}*M_{j,n-1}
const
doubleVector
  doubleSubVector::dot(const doubleSubMatrix& M) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::dot(const doubleSubMatrix&) const",
    extent(), M.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubVector&	v = *this;
  doubleVector		result(M.extent2());
  for (Offset j = 0; j < M.extent2(); j++) {
    result[j] = v.dot(M[j]);
    }
  return result;
  }

// M.dot(N) = M_{i,0}*N_{j,0} + M_{i,1}*N_{j,1} + ... + M_{i,n-1}*N_{j,n-1}
const
doubleMatrix
  doubleSubMatrix::dot(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubMatrix::dot(const doubleSubMatrix&) const",
    extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&	M = *this;
  doubleMatrix		result(extent2(), N.extent2());
  for (Offset i = 0; i < extent2(); i++) {
    for (Offset j = 0; j < N.extent2(); j++) {
      result[i][j] = M[i].dot(N[j]);
      }
    }
  return result;
  }

// M.dot(v) = M_{i,0}*v_{0} + M_{i,1}*v_{1} + ... + M_{i,n-1}*v_{n-1}
const
doubleMatrix
  doubleSubMatrix::dot(const doubleSubVector& v) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubMatrix::dot(const doubleSubVector&) const",
    extent1(), v.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&	M = *this;
  doubleMatrix		result(extent2());
  for (Offset i = 0; i < extent2(); i++) {
    result[i][0] = M[i].dot(v);
    }
  return result;
  }

// pack the diagonal and subdiagonal elements with scalar s.
doubleSubMatrix&
  doubleSubMatrix::pack(const double& s) {
  doubleSubMatrix&	M = *this;
  Extent		m = extent2();
  Extent		n = extent1();
  if (n < m) {
    for (Offset i = 0; i < n; i++)
      M[i].sub(0, 1+i, 1) = s;
    M.sub(n, m-n, 1) = s;
    }
  else {
    for (Offset i = 0; i < m; i++)
      M[i].sub(0, 1+i, 1) = s;
    }
  return M; }
// pack the diagonal and subdiagonal elements
// with the corresponding elements of submatrix N.
doubleSubMatrix&
  doubleSubMatrix::pack(const doubleSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::pack(const doubleSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleSubMatrix&	M = *this;
  Extent		m = extent2();
  Extent		n = extent1();
  if (n < m) {
    for (Offset i = 0; i < n; i++)
      M[i].sub(0, 1+i, 1) = N[i].sub(0, i+1, 1);
    M.sub(n, m-n, 1) = N.sub(n, m-n, 1);
    }
  else {
    for (Offset i = 0; i < m; i++)
      M[i].sub(0, 1+i, 1) = N[i].sub(0, i+1, 1);
    }
  return M; }

// LU decomposition using Crout's method with partial pivoting.
const
offsetVector
  doubleSubMatrix::lud(void) {
  doubleSubMatrix&	A = *this;
  doubleSubMatrix	U = A.t();
  Extent			m = A.extent2();
  Extent			n = A.extent1();
  offsetVector			p(m);
  p.ramp();				// Initialize the permutation vector.
  
  for (Offset j = 0; j < n; j++) {	// Crout's method.
    doubleSubVector	u = U[j];
    if (0 < j) {
      for (Offset i = 1; i < j && i < m; i++) {
	u[i] -= u.sub(0, i, 1).dot(A[i].sub(0, i, 1));
	}
      if (j < m) {
	u.sub(j, m-j, 1) -= u.sub(0, j, 1).dot(A.sub(j, m-j, 1, 0, j, 1));
	}
      }

    if (j+1 < m) {			// Find the pivot element.
      double			x = (double)0;
      Offset			k = m;
      for (Offset i = j; i < m; i++) {
	double			s = abs(u[i]);
	if (x < s) {
	  x = s;
	  k = i;
	  }
	}
      if (j < k && k < m) {		// There is a non-zero pivot element.
        p.swap(j, k);
	A.swap(j, k);
	}
      if ((double)0 != u[j]) {
	u.sub(j+1, m-(j+1), 1) /= u[j];
	}
      }
    }
  return p;
  }

inline
doubleMatrix
  outer(const doubleSubVector& v, const doubleSubVector& w) {
  return v.submatrix().t().dot(w.submatrix().t()); }

// Householder transformation AQ -- post multiplication update.
doubleSubMatrix&
  doubleSubMatrix::hht(const doubleSubVector& v) {
  doubleSubMatrix&	A = *this;
  double				beta = 2/v.dot(v);
  doubleVector		w = beta*v.dot(A);
  A -= outer(w, v);
  return A;
  }

// QR decomposition using Householder transformations and partial pivoting.
const
offsetVector
  doubleSubMatrix::qrd(void) {
  doubleSubMatrix&	A = *this;
  doubleSubMatrix	T = A.t();
  Extent			m = A.extent2();
  Extent			n = A.extent1();
  offsetVector			p(m);
  p.ramp();				// Initialize the permutation vector.
  
  for (Offset j = 0; j < m && j < n; j++) {	// Householders method.
    doubleSubVector	x = T[j].sub(j, m-j, 1);
    if (j < m-1) {			// Find the pivot element.
      Offset	k = j + abs(x).max();
      if (j < k) {			// There is a non-zero pivot element.
        p.swap(j, k);
	A.swap(j, k);
	}
      }
    double			mu = sqrt(x.dot(x));
    if (0 < mu) {
      double			beta = x[0] + ((x[0] < 0)? -mu: +mu);
      doubleVector	v = (1/beta)*x;
      v[0] = 1;				// Householder vector v
      T.sub(j, n-j, 1, j, m-j, 1).hht(v);
      if (j+1 < m) {
	// Pack the essential part of each Householder vector
	// into the subdiagonal elements of matrix A.
	x.sub(1, m-j-1, 1) = v.sub(1, m-j-1, 1);
	}
      }
    else
      break;
    }
  return p;
  }

// solve v.dot(P) = w.dot(L)
const
doubleVector
  doubleSubVector::pl(const offsetSubVector& p,
			      const doubleSubMatrix& L) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::pl(const doubleSubMatrix&) const",
    extent(), L.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubVector&	v = *this;
  doubleVector		w(L.extent1(), (double)0);
  Extent	n = (L.extent1() < L.extent2())? L.extent1(): L.extent2();
  for (Offset i = 0; i < n; i++) {
    w[i] = v[p[i]] - w.dot(L[i]);
    }
  return w;
  }

// solve v = w.dot(L)
const
doubleVector
  doubleSubVector::l(const doubleSubMatrix& L) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::l(const doubleSubMatrix&) const",
    extent(), L.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubVector&	v = *this;
  doubleVector		w(L.extent1(), (double)0);
  Extent	n = (L.extent1() < L.extent2())? L.extent1(): L.extent2();
  for (Offset i = 0; i < n; i++) {
    w[i] = v[i] - w.dot(L[i]);
    }
  return w;
  }

// solve v = w.dot(LD)
const
doubleVector
  doubleSubVector::pld(const offsetSubVector& p,
			       const doubleSubMatrix& LD) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::pld(const doubleSubMatrix&) const",
    extent(), LD.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubVector&	v = *this;
  doubleVector		w(LD.extent1(), (double)0);
  Extent	n = (LD.extent1() < LD.extent2())? LD.extent1(): LD.extent2();
  for (Offset i = 0; i < n; i++) {
    w[i] = (v[p[i]] - w.dot(LD[i]))/LD[i][i];
    }
  return w;
  }

// solve v = w.dot(LD)
const
doubleVector
  doubleSubVector::ld(const doubleSubMatrix& LD) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::ld(const doubleSubMatrix&) const",
    extent(), LD.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubVector&	v = *this;
  doubleVector		w(LD.extent1(), (double)0);
  Extent	n = (LD.extent1() < LD.extent2())? LD.extent1(): LD.extent2();
  for (Offset i = 0; i < n; i++) {
    w[i] = (v[i] - w.dot(LD[i]))/LD[i][i];
    }
  return w;
  }

// solve v = w.dot(U)
const
doubleVector
  doubleSubVector::u(const doubleSubMatrix& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::u(const doubleSubMatrix&) const",
    extent(), U.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubVector&	v = *this;
  doubleVector		w(U.extent1(), (double)0);
  Extent	n = (U.extent1() < U.extent2())? U.extent1(): U.extent2();
  for (Offset j = 0; j < n; j++) {
    Offset	i = n-1 - j;
    w[i] = v[i] - w.dot(U[i]);
    }
  return w;
  }

// solve v = w.dot(U)
const
doubleVector
  doubleSubVector::up(const doubleSubMatrix& U,
			      const offsetSubVector& p) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::up(const doubleSubMatrix&) const",
    extent(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubVector&	v = *this;
  doubleVector		w(U.extent1(), (double)0);
  Extent	n = (U.extent1() < U.extent2())? U.extent1(): U.extent2();
  for (Offset j = 0; j < n; j++) {
    Offset	i = n-1 - j;
    w[i] = v[i] - w.dot(U[i]);
    }
  return w.permutation(p);
  }

// solve v = w.dot(DU)
const
doubleVector
  doubleSubVector::du(const doubleSubMatrix& DU) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::du(const doubleSubMatrix&) const",
    extent(), DU.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubVector&	v = *this;
  doubleVector		w(DU.extent1(), (double)0);
  Extent	n = (DU.extent1() < DU.extent2())? DU.extent1(): DU.extent2();
  for (Offset j = 0; j < n; j++) {
    Offset	i = n-1 - j;
    w[i] = (v[i] - w.dot(DU[i]))/DU[i][i];
    }
  return w;
  }

// solve v = w.dot(DU)
const
doubleVector
  doubleSubVector::dup(const doubleSubMatrix& DU,
			      const offsetSubVector& p) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::dup(const doubleSubMatrix&) const",
    extent(), DU.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubVector&	v = *this;
  doubleVector		w(DU.extent1(), (double)0);
  Extent	n = (DU.extent1() < DU.extent2())? DU.extent1(): DU.extent2();
  for (Offset j = 0; j < n; j++) {
    Offset	i = n-1 - j;
    w[i] = (v[i] - w.dot(DU[i]))/DU[i][i];
    }
  return w.permutation(p);
  }

// solve B = Y.dot(Q)
const
doubleMatrix
  doubleSubMatrix::q(const doubleSubMatrix& L) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubMatrix::q(const doubleSubMatrix&) const",
    extent1(), L.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix	V = L.t();
  const
  doubleSubMatrix&	B = *this;
  doubleMatrix		Y = B;
  Extent			l = B.extent2();
  Extent			m = L.extent2();
  Extent			n = L.extent1();
  for (Offset i = 0; i < m && i < n; i++) {
    doubleVector	v = V[i].sub(i, m-i, 1);
    v[0] = 1;
    Y.sub(0, l, 1, i, m-i, 1).hht(v);
    }
  return Y;
  }

// solve B.dot(P) = Y.dot(Q)
const
doubleMatrix
  doubleSubMatrix::pq(const offsetSubVector& p,
			      const doubleSubMatrix& L) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubMatrix::pq(const doubleSubMatrix&) const",
    extent1(), L.extent2());
#endif // SVMT_DEBUG_MODE
  return this->permutation(p).q(L);
  }

// solve b = y.dot(Q)
const
doubleVector
  doubleSubVector::q(const doubleSubMatrix& L) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::q(const doubleSubMatrix&) const",
    extent(), L.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix	B = submatrix();
  doubleMatrix		Y = B.q(L);
  return Y[0];
  }

// solve b.dot(P) = y.dot(Q)
const
doubleVector
  doubleSubVector::pq(const offsetSubVector& p,
			      const doubleSubMatrix& L) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::pq(const doubleSubMatrix&) const",
    extent(), L.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix	B = submatrix();
  doubleMatrix		Y = B.pq(p, L);
  return Y[0];
  }
const
doubleMatrix
  doubleSubMatrix::permutation(const offsetSubVector& p) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubMatrix::permutation(const offsetSubVector&) const",
    extent1(), p.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&	M = *this;
  Offset			m = extent2();
  Offset			n = extent1();
  doubleMatrix		N(m, n);
  for (Offset i = 0; i < m; i++) {
    const
    doubleSubVector	v = M[i];
    doubleSubVector	w = N[i];
    for (Offset j = 0; j < n; j++) {
      w[j] = v[p[j]];
      }
    }
  return N;
  }

const
boolMatrix
  doubleSubMatrix::lt(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::lt(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&	M = *this;
  boolMatrix			result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].lt(N[i]);
    }
  return result;
  }

const
boolMatrix
  doubleSubMatrix::le(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::le(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&	M = *this;
  boolMatrix			result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].le(N[i]);
    }
  return result;
  }

const
boolMatrix
  doubleSubMatrix::eq(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::eq(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&	M = *this;
  boolMatrix			result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); ++i) {
    result[i] = M[i].eq(N[i]);
    }
  return result;
  }

const
boolMatrix
  doubleSubMatrix::ne(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::ne(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix	M = *this;
  boolMatrix			result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].ne(N[i]);
    }
  return result;
  }

Extent
  doubleSubMatrix::zeros(void) const {
  const
  doubleSubMatrix&	M = *this;
  Extent			result = 0;
  for (Offset i = 0; i < extent2(); i++) {
    result += M[i].zeros();
    }
  return result;
  }

const
offsetVector
  doubleSubMatrix::index(void) const {
  const
  doubleSubMatrix&	M = *this;
  offsetVector		result(extent2()*extent1() - zeros());
  Offset		k = 0;
  for (Offset i = 0; i < M.extent2(); i++) {
    doubleSubVector	v = M[i];
    for (Offset j = 0; j < M.extent1(); j++)
      if (v[j] != (double)0)
	result[k++] = i*extent1() + j;
    }
  return result;
  }

const
doubleVector
  doubleSubMatrix::gather(const offsetSubVector& x) const {
  doubleVector	result(x.extent());
  for (Offset i = 0; i < extent2(); i++) {
#ifdef SVMT_DEBUG_MODE
    svmt_check_vector_index_range(
      "doubleSubMatrix::gather(const offsetSubVector&) const",
      i, extent2());
#endif // SVMT_DEBUG_MODE
    }
  return result;
  }

doubleSubMatrix&
  doubleSubMatrix::scatter(const offsetSubVector& x,
    const doubleSubVector& v) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubMatrix::scatter(\n"
    "const offsetSubVector&, const doubleSubVector&)",
    x.extent(), v.extent());
#endif // SVMT_DEBUG_MODE
  return *this;
  }

const
doubleMatrix
  doubleSubMatrix::aside(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubMatrix::aside(const doubleSubMatrix&) const",
    extent2(), N.extent2());
#endif // SVMT_DEBUG_MODE
  doubleMatrix		result(extent2(), extent1()
					      + N.extent1());
  result.sub((Offset)0, extent2(), (Stride)1,
	     (Offset)0, extent1(), (Stride)1) = *this;
  result.sub((Offset)0, extent2(), (Stride)1,
	     extent1(), N.extent1(), (Stride)1) = N;
  return result;
  }

const
doubleMatrix
  doubleSubMatrix::above(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubMatrix::above(const doubleSubMatrix&) const",
    extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleMatrix		result(extent2()
				   + N.extent2(), extent1());
  result.sub((Offset)0, extent2(), (Stride)1) = *this;
  result.sub(extent2(), N.extent2(), (Stride)1) = N;
  return result;
  }

const
doubleMatrix
  doubleSubVector::kron(const doubleSubMatrix& M) const {
  const
  doubleSubVector&	v = *this;
  doubleMatrix		result(M.extent2(), extent()*M.extent1());

  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = v.kron(M[i]);
    }
  return result;
  }

const
doubleMatrix
  doubleSubMatrix::kron(const doubleSubMatrix& N) const {
  const
  doubleSubMatrix&	M = *this;
  doubleMatrix	result(extent2()*N.extent2(), extent1()*N.extent1());
  for (Offset i = 0; i < extent2(); i++) {
    for (Offset j = 0; j < extent1(); j++) {
      result.sub(i*N.extent2(), N.extent2(), (Stride)1,
		 j*N.extent1(), N.extent1(), (Stride)1)
	= M[i][j]*N;
      }
    }
  return result;
  }

const
doubleMatrix
  doubleSubMatrix::kron(const doubleSubVector& v) const {
  const
  doubleSubMatrix&	M = *this;
  doubleMatrix		result(extent2(), extent1()*v.extent());

  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].kron(v);
    }
  return result;
  }



const
doubleMatrix
  doubleSubMatrix::apply(const double (*f)(const double&)) const {
  const
  doubleSubMatrix&	M = *this;
  doubleMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].apply(f);
    }
  return result;
  }

const
doubleMatrix
  doubleSubMatrix::apply(      double (*f)(const double&)) const {
  const
  doubleSubMatrix&	M = *this;
  doubleMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].apply(f);
    }
  return result;
  }

const
doubleMatrix
  doubleSubMatrix::apply(      double (*f)(      double )) const {
  const
  doubleSubMatrix&	M = *this;
  doubleMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].apply(f);
    }
  return result;
  }

const
doubleMatrix
    exp(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = exp(M[i]);
    }
  return result;
  }

const
doubleMatrix
    log(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = log(M[i]);
    }
  return result;
  }

const
doubleMatrix
   sqrt(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = sqrt(M[i]);
    }
  return result;
  }

const
doubleMatrix
    cos(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = cos(M[i]);
    }
  return result;
  }

const
doubleMatrix
    sin(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = sin(M[i]);
    }
  return result;
  }

const
doubleMatrix
    tan(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = tan(M[i]);
    }
  return result;
  }

const
doubleMatrix
   acos(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = acos(M[i]);
    }
  return result;
  }

const
doubleMatrix
   asin(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = asin(M[i]);
    }
  return result;
  }

const
doubleMatrix
   atan(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = atan(M[i]);
    }
  return result;
  }

const
doubleMatrix
   cosh(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = cosh(M[i]);
    }
  return result;
  }

const
doubleMatrix
   sinh(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = sinh(M[i]);
    }
  return result;
  }

const
doubleMatrix
   tanh(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = tanh(M[i]);
    }
  return result;
  }

const
doubleMatrix
  acosh(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = acosh(M[i]);
    }
  return result;
  }

const
doubleMatrix
  asinh(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = asinh(M[i]);
    }
  return result;
  }

const
doubleMatrix
  atanh(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = atanh(M[i]);
    }
  return result;
  }


const
doubleMatrix
    sgn(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = sgn(M[i]);
    }
  return result;
  }

const
doubleMatrix
    abs(const doubleSubMatrix& M) {
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = abs(M[i]);
    }
  return result;
  }

const
doubleMatrix
  floor(const doubleSubMatrix& M) {
  doubleMatrix	result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = floor(M[i]);
    }
  return result;
  }

const
doubleMatrix
   ceil(const doubleSubMatrix& M) {
  doubleMatrix	result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = ceil(M[i]);
    }
  return result;
  }

const
doubleMatrix
  hypot(const doubleSubMatrix& M, const doubleSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "hypot(const doubleSubMatrix&, const doubleSubMatrix&)",
    M.extent2(), N.extent2(), M.extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = hypot(M[i], N[i]);
    }
  return result;
  }

const
doubleMatrix
  atan2(const doubleSubMatrix& N, const doubleSubMatrix& M) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "atan2(const doubleSubMatrix&, const doubleSubMatrix&)",
    N.extent2(), M.extent2(), N.extent1(), M.extent1());
#endif // SVMT_DEBUG_MODE
  doubleMatrix		result(N.extent2(), N.extent1());
  for (Offset i = 0; i < N.extent2(); i++) {
    result[i] = atan2(N[i], M[i]);
    }
  return result;
  }

// Operators

// minus
const
doubleMatrix
  doubleSubMatrix::operator -(void) const {
  const
  doubleSubMatrix&	M = *this;
  doubleMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = -M[i];
    }
  return result;
  }


// multiply
const
doubleMatrix
  doubleSubMatrix::operator *(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::operator *(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&	M = *this;
  doubleMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i]*N[i];
    }
  return result;
  }

// divide
const
doubleMatrix
  doubleSubMatrix::operator /(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::operator /(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&	M = *this;
  doubleMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i]/N[i];
    }
  return result;
  }


// add
const
doubleMatrix
  doubleSubMatrix::operator +(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::operator +(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&	M = *this;
  doubleMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i] + N[i];
    }
  return result;
  }

// subtract
const
doubleMatrix
  doubleSubMatrix::operator -(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::operator -(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&	M = *this;
  doubleMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i] - N[i];
    }
  return result;
  }


std::istream&	operator >>(std::istream& is,       doubleSubMatrix& M) {
  for (Offset i = 0; i < M.extent2(); i++) {
    doubleSubVector	v = M[i];
    if (!(is >> v)) break;
    }
  return is;
  }

std::ostream&	operator <<(std::ostream& os, const doubleSubMatrix& M) {
  int			width = os.width();
  for (Offset i = 0; i < M.extent2(); i++) {
    os << std::setw(width) << M[i];
    }
  return os;
  }

// less than
bool	doubleSubMatrix::operator < (
  const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::operator < (const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&	M = *this;
  Offset			i = (Offset)0;
  while (i < extent2() && M[i] <  N[i]) {
   ++i;
   }
  return i == extent2();
  }

// less than or equal
bool	doubleSubMatrix::operator <=(
  const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::operator <=(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&	M = *this;
  Offset			i = (Offset)0;
  while (i < extent2() && M[i] <= N[i]) {
   ++i;
   }
  return i == extent2();
  }

// equal
bool	doubleSubMatrix::operator ==(
  const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::operator ==(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&	M = *this;
  Offset			i = (Offset)0;
  while (i < extent2() && M[i] == N[i]) {
   ++i;
   }
  return i == extent2();
  }

// not equal
bool	doubleSubMatrix::operator !=(
  const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::operator !=(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&	M = *this;
  Offset			i = (Offset)0;
  while (i < extent2() && M[i] != N[i]) {
   ++i;
   }
  return i == extent2();
  }



// simple assignment
doubleSubMatrix&
  doubleSubMatrix::operator  =(const doubleSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::operator  =(const doubleSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i] = N[i];
    }
  return *this;
  }

// multiply and assign
doubleSubMatrix&
  doubleSubMatrix::operator *=(const doubleSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::operator *=(const doubleSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i] *= N[i];
    }
  return *this;
  }

// divide and assign
doubleSubMatrix&
  doubleSubMatrix::operator /=(const doubleSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::operator /=(const doubleSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i] /= N[i];
    }
  return *this;
  }


// add and assign
doubleSubMatrix&
  doubleSubMatrix::operator +=(const doubleSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::operator +=(const doubleSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i] += N[i];
    }
  return *this;
  }

// subtract and assign
doubleSubMatrix&
  doubleSubMatrix::operator -=(const doubleSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::operator -=(const doubleSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i] -= N[i];
    }
  return *this;
  }



/*
// LU decomposition with partial pivoting
const
offsetVector
  doubleSubMatrix::lud(void) {
  const
  doubleSubMatrix&	M = *this;
  Extent			m = M.extent2();
  Extent			n = M.extent1();
  Extent			l = (m < n)? m: n;
  offsetVector			p(m);
  p.ramp();			// Initialize the permutation vector.
  
  for (Offset h = 0; h+1 < l; h++) {
    // First, find the pivot element.
    Offset			k = m;
    double			x = (double)0;
    for (Offset i = h; i < m; i++) {
      double			s = abs(M[p[i]][h]);
      if (x < s) {
	x = s;
	k = i;
	}
      }

    if (k < m) {		// There is a non-zero pivot element.
      if (h < k) {
        p.swap(h, k);
	}
      doubleSubVector	v = M[p[h]];
      for (Offset i = h+1; i < m; i++) {
	doubleSubVector	w = M[p[i]];
	double			t = -w[h]/v[h];
	w += t*v;
	w[h] = t;
	}
      }
    }
  return p;
  }
*/
