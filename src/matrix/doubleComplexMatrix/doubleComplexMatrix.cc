/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleComplexMatrix.h>


// Functions
doubleComplexSubMatrix&
  doubleComplexSubMatrix::reverse1(void) {
  doubleComplexSubMatrix&	M = *this;
  Extent			m = extent2();
  for (Offset i = 0; i < m; i++)
    M[i].reverse();
  return *this;
  }
doubleComplexSubMatrix&
  doubleComplexSubMatrix::reverse2(void) {
  Extent			m = extent2();
  for (Offset i = 0; i < m/2; i++)
    swap(i, m-1 - i);
  return *this;
  }

doubleSubMatrix&
  doubleSubMatrix::cdft(int sign) {	// real to complex dft
  doubleSubMatrix&
		M = *this;
  Extent	n = M.extent1();
  if (1 < n) {
    doubleComplexVector
		w(n);
		w.twiddle(sign);
    Extent	m = M.extent2();
    for (Offset i = 0; i < m; i++)
      M[i].transpose(false).rcfdft(w);
    }
  return M;
  }

doubleSubMatrix&
  doubleSubMatrix::rdft(int sign) {	// complex to real dft
  doubleSubMatrix&
		M = *this;
  Extent	n = M.extent1();
  if (1 < n) {
    doubleComplexVector
		w(n);
		w.twiddle(sign);
    Extent	m = M.extent2();
    for (Offset i = 0; i < m; i++)
      M[i].crfdft(w).transpose(true);
    }
  return M;
  }

doubleComplexSubMatrix&
  doubleComplexSubMatrix::dft(int sign) {	// complex to complex dft
  doubleComplexSubMatrix&
		M = *this;
  Extent	n = M.extent1();
  if (1 < n) {
    doubleComplexVector
		w(n);
		w.twiddle(sign);
    Extent	m = M.extent2();
    for (Offset i = 0; i < m; i++)
      M[i].transpose().fdft(w);
    }
  return M;
  }


doubleComplexSubMatrix&
  doubleComplexSubMatrix::swap(doubleComplexSubMatrix& N) {
  doubleComplexSubMatrix	M = (*this);
  for (Offset i = 0; i < extent2(); i++) {
    doubleComplexSubVector	v = M[i];
    doubleComplexSubVector	w = N[i];
    v.swap(w);
    }
  return *this;
  }

doubleComplexSubMatrix&
  doubleComplexSubMatrix::rotate(Stride n) {
  doubleComplexSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    doubleComplexSubVector	u = M[i];
    u.rotate(n);
    }
  return *this;
  }

doubleComplexSubMatrix&
  doubleComplexSubMatrix::shift(Stride n,const doubleComplex& s) {
  doubleComplexSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    doubleComplexSubVector	u = M[i];
    u.shift(n,s);
    }
  return *this;
  }


// M.sum() = M_{i,0} + M_{i,1} + ... + M_{i,n-1}
const
doubleComplexVector
  doubleComplexSubMatrix::sum(void) const {
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexVector		result(extent2(), doubleComplex((double)0, (double)0));
  for (Offset i = 0; i < extent2(); i++) {
    result[i] += M[i].sum();
    }
  return result;
  }


// v.dot(M) = v_{0}*M_{j,0} + v_{1}*M_{j,1} + ... + v_{n-1}*M_{j,n-1}
const
doubleComplexVector
  doubleSubVector::dot(const doubleComplexSubMatrix& M) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubVector::dot(const doubleComplexSubMatrix&) const",
    extent(), M.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubVector&		v = *this;
  doubleComplexVector		result(M.extent2());
  for (Offset j = 0; j < M.extent2(); j++) {
    result[j] = v.dot(M[j]);
    }
  return result;
  }

// v.dot(M) = v_{0}*M_{j,0} + v_{1}*M_{j,1} + ... + v_{n-1}*M_{j,n-1}
const
doubleComplexVector
  doubleComplexSubVector::dot(const doubleSubMatrix& M) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::dot(const doubleSubMatrix&) const",
    extent(), M.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubVector&	v = *this;
  doubleComplexVector		result(M.extent2());
  for (Offset j = 0; j < M.extent2(); j++) {
    result[j] = v.dot(M[j]);
    }
  return result;
  }

// M.dot(N) = M_{i,0}*N_{j,0} + M_{i,1}*N_{j,1} + ... + M_{i,n-1}*N_{j,n-1}
const
doubleComplexMatrix
  doubleSubMatrix::dot(const doubleComplexSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubMatrix::dot(const doubleComplexSubMatrix&) const",
    extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&		M = *this;
  doubleComplexMatrix		result(extent2(), N.extent2());
  for (Offset i = 0; i < extent2(); i++) {
    for (Offset j = 0; j < N.extent2(); j++) {
      result[i][j] = M[i].dot(N[j]);
      }
    }
  return result;
  }

// M.dot(N) = M_{i,0}*N_{j,0} + M_{i,1}*N_{j,1} + ... + M_{i,n-1}*N_{j,n-1}
const
doubleComplexMatrix
  doubleComplexSubMatrix::dot(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubMatrix::dot(const doubleSubMatrix&) const",
    extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2(), N.extent2());
  for (Offset i = 0; i < extent2(); i++) {
    for (Offset j = 0; j < N.extent2(); j++) {
      result[i][j] = M[i].dot(N[j]);
      }
    }
  return result;
  }

// M.dot(v) = M_{i,0}*v_{0} + M_{i,1}*v_{1} + ... + M_{i,n-1}*v_{n-1}
const
doubleComplexMatrix
  doubleSubMatrix::dot(const doubleComplexSubVector& v) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleSubMatrix::dot(const doubleComplexSubVector&) const",
    extent1(), v.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&		M = *this;
  doubleComplexMatrix		result(extent2());
  for (Offset i = 0; i < extent2(); i++) {
    result[i][0] = M[i].dot(v);
    }
  return result;
  }

// M.dot(v) = M_{i,0}*v_{0} + M_{i,1}*v_{1} + ... + M_{i,n-1}*v_{n-1}
const
doubleComplexMatrix
  doubleComplexSubMatrix::dot(const doubleSubVector& v) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubMatrix::dot(const doubleSubVector&) const",
    extent1(), v.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2());
  for (Offset i = 0; i < extent2(); i++) {
    result[i][0] = M[i].dot(v);
    }
  return result;
  }

// v.dot(M) = v_{0}*M_{j,0} + v_{1}*M_{j,1} + ... + v_{n-1}*M_{j,n-1}
const
doubleComplexVector
  doubleComplexSubVector::dot(const doubleComplexSubMatrix& M) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::dot(const doubleComplexSubMatrix&) const",
    extent(), M.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubVector&	v = *this;
  doubleComplexVector		result(M.extent2());
  for (Offset j = 0; j < M.extent2(); j++) {
    result[j] = v.dot(M[j]);
    }
  return result;
  }

// M.dot(N) = M_{i,0}*N_{j,0} + M_{i,1}*N_{j,1} + ... + M_{i,n-1}*N_{j,n-1}
const
doubleComplexMatrix
  doubleComplexSubMatrix::dot(const doubleComplexSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubMatrix::dot(const doubleComplexSubMatrix&) const",
    extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2(), N.extent2());
  for (Offset i = 0; i < extent2(); i++) {
    for (Offset j = 0; j < N.extent2(); j++) {
      result[i][j] = M[i].dot(N[j]);
      }
    }
  return result;
  }

// M.dot(v) = M_{i,0}*v_{0} + M_{i,1}*v_{1} + ... + M_{i,n-1}*v_{n-1}
const
doubleComplexMatrix
  doubleComplexSubMatrix::dot(const doubleComplexSubVector& v) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubMatrix::dot(const doubleComplexSubVector&) const",
    extent1(), v.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2());
  for (Offset i = 0; i < extent2(); i++) {
    result[i][0] = M[i].dot(v);
    }
  return result;
  }

// pack the diagonal and subdiagonal elements with scalar s.
doubleComplexSubMatrix&
  doubleComplexSubMatrix::pack(const doubleComplex& s) {
  doubleComplexSubMatrix&	M = *this;
  Extent		m = extent2();
  Extent		n = extent1();
  if (n < m) {
    for (Offset i = 0; i < n; i++)
      M[i].sub(0, 1+i, 1) = s;
    M.sub(n, m-n, 1) = s;
    }
  else {
    for (Offset i = 0; i < m; i++)
      M[i].sub(0, 1+i, 1) = s;
    }
  return M; }
// pack the diagonal and subdiagonal elements
// with the corresponding elements of submatrix N.
doubleComplexSubMatrix&
  doubleComplexSubMatrix::pack(const doubleComplexSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::pack(const doubleComplexSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubMatrix&	M = *this;
  Extent		m = extent2();
  Extent		n = extent1();
  if (n < m) {
    for (Offset i = 0; i < n; i++)
      M[i].sub(0, 1+i, 1) = N[i].sub(0, i+1, 1);
    M.sub(n, m-n, 1) = N.sub(n, m-n, 1);
    }
  else {
    for (Offset i = 0; i < m; i++)
      M[i].sub(0, 1+i, 1) = N[i].sub(0, i+1, 1);
    }
  return M; }

// LU decomposition using Crout's method with partial pivoting.
const
offsetVector
  doubleComplexSubMatrix::lud(void) {
  doubleComplexSubMatrix&	A = *this;
  doubleComplexSubMatrix	U = A.t();
  Extent			m = A.extent2();
  Extent			n = A.extent1();
  offsetVector			p(m);
  p.ramp();				// Initialize the permutation vector.
  
  for (Offset j = 0; j < n; j++) {	// Crout's method.
    doubleComplexSubVector	u = U[j];
    if (0 < j) {
      for (Offset i = 1; i < j && i < m; i++) {
	u[i] -= u.sub(0, i, 1).dot(A[i].sub(0, i, 1));
	}
      if (j < m) {
	u.sub(j, m-j, 1) -= u.sub(0, j, 1).dot(A.sub(j, m-j, 1, 0, j, 1));
	}
      }

    if (j+1 < m) {			// Find the pivot element.
      double			x = (double)0;
      Offset			k = m;
      for (Offset i = j; i < m; i++) {
	double			s = abs(u[i]);
	if (x < s) {
	  x = s;
	  k = i;
	  }
	}
      if (j < k && k < m) {		// There is a non-zero pivot element.
        p.swap(j, k);
	A.swap(j, k);
	}
      if (doubleComplex((double)0, (double)0) != u[j]) {
	u.sub(j+1, m-(j+1), 1) /= u[j];
	}
      }
    }
  return p;
  }

inline
doubleComplexMatrix
  outer(const doubleComplexSubVector& v, const doubleComplexSubVector& w) {
  return v.submatrix().t().dot(w.submatrix().t()); }

// Householder transformation AQ -- post multiplication update.
doubleComplexSubMatrix&
  doubleComplexSubMatrix::hht(const doubleComplexSubVector& v) {
  doubleComplexSubMatrix&	A = *this;
  doubleComplex				beta = 2/v.dot(v);
  doubleComplexVector		w = beta*v.dot(A);
  A -= outer(w, v);
  return A;
  }


// solve v.dot(P) = w.dot(L)
const
doubleComplexVector
  doubleComplexSubVector::pl(const offsetSubVector& p,
			      const doubleComplexSubMatrix& L) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::pl(const doubleComplexSubMatrix&) const",
    extent(), L.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubVector&	v = *this;
  doubleComplexVector		w(L.extent1(), doubleComplex((double)0, (double)0));
  Extent	n = (L.extent1() < L.extent2())? L.extent1(): L.extent2();
  for (Offset i = 0; i < n; i++) {
    w[i] = v[p[i]] - w.dot(L[i]);
    }
  return w;
  }

// solve v = w.dot(L)
const
doubleComplexVector
  doubleComplexSubVector::l(const doubleComplexSubMatrix& L) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::l(const doubleComplexSubMatrix&) const",
    extent(), L.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubVector&	v = *this;
  doubleComplexVector		w(L.extent1(), doubleComplex((double)0, (double)0));
  Extent	n = (L.extent1() < L.extent2())? L.extent1(): L.extent2();
  for (Offset i = 0; i < n; i++) {
    w[i] = v[i] - w.dot(L[i]);
    }
  return w;
  }

// solve v = w.dot(LD)
const
doubleComplexVector
  doubleComplexSubVector::pld(const offsetSubVector& p,
			       const doubleComplexSubMatrix& LD) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::pld(const doubleComplexSubMatrix&) const",
    extent(), LD.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubVector&	v = *this;
  doubleComplexVector		w(LD.extent1(), doubleComplex((double)0, (double)0));
  Extent	n = (LD.extent1() < LD.extent2())? LD.extent1(): LD.extent2();
  for (Offset i = 0; i < n; i++) {
    w[i] = (v[p[i]] - w.dot(LD[i]))/LD[i][i];
    }
  return w;
  }

// solve v = w.dot(LD)
const
doubleComplexVector
  doubleComplexSubVector::ld(const doubleComplexSubMatrix& LD) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::ld(const doubleComplexSubMatrix&) const",
    extent(), LD.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubVector&	v = *this;
  doubleComplexVector		w(LD.extent1(), doubleComplex((double)0, (double)0));
  Extent	n = (LD.extent1() < LD.extent2())? LD.extent1(): LD.extent2();
  for (Offset i = 0; i < n; i++) {
    w[i] = (v[i] - w.dot(LD[i]))/LD[i][i];
    }
  return w;
  }

// solve v = w.dot(U)
const
doubleComplexVector
  doubleComplexSubVector::u(const doubleComplexSubMatrix& U) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::u(const doubleComplexSubMatrix&) const",
    extent(), U.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubVector&	v = *this;
  doubleComplexVector		w(U.extent1(), doubleComplex((double)0, (double)0));
  Extent	n = (U.extent1() < U.extent2())? U.extent1(): U.extent2();
  for (Offset j = 0; j < n; j++) {
    Offset	i = n-1 - j;
    w[i] = v[i] - w.dot(U[i]);
    }
  return w;
  }

// solve v = w.dot(U)
const
doubleComplexVector
  doubleComplexSubVector::up(const doubleComplexSubMatrix& U,
			      const offsetSubVector& p) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::up(const doubleComplexSubMatrix&) const",
    extent(), U.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubVector&	v = *this;
  doubleComplexVector		w(U.extent1(), doubleComplex((double)0, (double)0));
  Extent	n = (U.extent1() < U.extent2())? U.extent1(): U.extent2();
  for (Offset j = 0; j < n; j++) {
    Offset	i = n-1 - j;
    w[i] = v[i] - w.dot(U[i]);
    }
  return w.permutation(p);
  }

// solve v = w.dot(DU)
const
doubleComplexVector
  doubleComplexSubVector::du(const doubleComplexSubMatrix& DU) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::du(const doubleComplexSubMatrix&) const",
    extent(), DU.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubVector&	v = *this;
  doubleComplexVector		w(DU.extent1(), doubleComplex((double)0, (double)0));
  Extent	n = (DU.extent1() < DU.extent2())? DU.extent1(): DU.extent2();
  for (Offset j = 0; j < n; j++) {
    Offset	i = n-1 - j;
    w[i] = (v[i] - w.dot(DU[i]))/DU[i][i];
    }
  return w;
  }

// solve v = w.dot(DU)
const
doubleComplexVector
  doubleComplexSubVector::dup(const doubleComplexSubMatrix& DU,
			      const offsetSubVector& p) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::dup(const doubleComplexSubMatrix&) const",
    extent(), DU.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubVector&	v = *this;
  doubleComplexVector		w(DU.extent1(), doubleComplex((double)0, (double)0));
  Extent	n = (DU.extent1() < DU.extent2())? DU.extent1(): DU.extent2();
  for (Offset j = 0; j < n; j++) {
    Offset	i = n-1 - j;
    w[i] = (v[i] - w.dot(DU[i]))/DU[i][i];
    }
  return w.permutation(p);
  }

// solve B = Y.dot(Q)
const
doubleComplexMatrix
  doubleComplexSubMatrix::q(const doubleComplexSubMatrix& L) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubMatrix::q(const doubleComplexSubMatrix&) const",
    extent1(), L.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix	V = L.t();
  const
  doubleComplexSubMatrix&	B = *this;
  doubleComplexMatrix		Y = B;
  Extent			l = B.extent2();
  Extent			m = L.extent2();
  Extent			n = L.extent1();
  for (Offset i = 0; i < m && i < n; i++) {
    doubleComplexVector	v = V[i].sub(i, m-i, 1);
    v[0] = 1;
    Y.sub(0, l, 1, i, m-i, 1).hht(v);
    }
  return Y;
  }

// solve B.dot(P) = Y.dot(Q)
const
doubleComplexMatrix
  doubleComplexSubMatrix::pq(const offsetSubVector& p,
			      const doubleComplexSubMatrix& L) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubMatrix::pq(const doubleComplexSubMatrix&) const",
    extent1(), L.extent2());
#endif // SVMT_DEBUG_MODE
  return this->permutation(p).q(L);
  }

// solve b = y.dot(Q)
const
doubleComplexVector
  doubleComplexSubVector::q(const doubleComplexSubMatrix& L) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::q(const doubleComplexSubMatrix&) const",
    extent(), L.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix	B = submatrix();
  doubleComplexMatrix		Y = B.q(L);
  return Y[0];
  }

// solve b.dot(P) = y.dot(Q)
const
doubleComplexVector
  doubleComplexSubVector::pq(const offsetSubVector& p,
			      const doubleComplexSubMatrix& L) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubVector::pq(const doubleComplexSubMatrix&) const",
    extent(), L.extent2());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix	B = submatrix();
  doubleComplexMatrix		Y = B.pq(p, L);
  return Y[0];
  }
const
doubleComplexMatrix
  doubleComplexSubMatrix::permutation(const offsetSubVector& p) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubMatrix::permutation(const offsetSubVector&) const",
    extent1(), p.extent());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  Offset			m = extent2();
  Offset			n = extent1();
  doubleComplexMatrix		N(m, n);
  for (Offset i = 0; i < m; i++) {
    const
    doubleComplexSubVector	v = M[i];
    doubleComplexSubVector	w = N[i];
    for (Offset j = 0; j < n; j++) {
      w[j] = v[p[j]];
      }
    }
  return N;
  }


const
boolMatrix
  doubleComplexSubMatrix::eq(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::eq(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  boolMatrix			result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i]	= M[i].eq(N[i]);
    }
  return result;
  }

const
boolMatrix
  doubleComplexSubMatrix::ne(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::ne(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  boolMatrix			result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].ne(N[i]);
    }
  return result;
  }

const
boolMatrix
  doubleComplexSubMatrix::eq(const doubleComplexSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::eq(const doubleComplexSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  boolMatrix			result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); ++i) {
    result[i] = M[i].eq(N[i]);
    }
  return result;
  }

const
boolMatrix
  doubleComplexSubMatrix::ne(const doubleComplexSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::ne(const doubleComplexSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix	M = *this;
  boolMatrix			result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].ne(N[i]);
    }
  return result;
  }

Extent
  doubleComplexSubMatrix::zeros(void) const {
  const
  doubleComplexSubMatrix&	M = *this;
  Extent			result = 0;
  for (Offset i = 0; i < extent2(); i++) {
    result += M[i].zeros();
    }
  return result;
  }

const
offsetVector
  doubleComplexSubMatrix::index(void) const {
  const
  doubleComplexSubMatrix&	M = *this;
  offsetVector		result(extent2()*extent1() - zeros());
  Offset		k = 0;
  for (Offset i = 0; i < M.extent2(); i++) {
    doubleComplexSubVector	v = M[i];
    for (Offset j = 0; j < M.extent1(); j++)
      if (v[j] != doubleComplex((double)0, (double)0))
	result[k++] = i*extent1() + j;
    }
  return result;
  }

const
doubleComplexVector
  doubleComplexSubMatrix::gather(const offsetSubVector& x) const {
  doubleComplexVector	result(x.extent());
  for (Offset i = 0; i < extent2(); i++) {
#ifdef SVMT_DEBUG_MODE
    svmt_check_vector_index_range(
      "doubleComplexSubMatrix::gather(const offsetSubVector&) const",
      i, extent2());
#endif // SVMT_DEBUG_MODE
    }
  return result;
  }

doubleComplexSubMatrix&
  doubleComplexSubMatrix::scatter(const offsetSubVector& x,
    const doubleComplexSubVector& v) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubMatrix::scatter(\n"
    "const offsetSubVector&, const doubleComplexSubVector&)",
    x.extent(), v.extent());
#endif // SVMT_DEBUG_MODE
  return *this;
  }

const
doubleComplexMatrix
  doubleComplexSubMatrix::aside(const doubleComplexSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubMatrix::aside(const doubleComplexSubMatrix&) const",
    extent2(), N.extent2());
#endif // SVMT_DEBUG_MODE
  doubleComplexMatrix		result(extent2(), extent1()
					      + N.extent1());
  result.sub((Offset)0, extent2(), (Stride)1,
	     (Offset)0, extent1(), (Stride)1) = *this;
  result.sub((Offset)0, extent2(), (Stride)1,
	     extent1(), N.extent1(), (Stride)1) = N;
  return result;
  }

const
doubleComplexMatrix
  doubleComplexSubMatrix::above(const doubleComplexSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexSubMatrix::above(const doubleComplexSubMatrix&) const",
    extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexMatrix		result(extent2()
				   + N.extent2(), extent1());
  result.sub((Offset)0, extent2(), (Stride)1) = *this;
  result.sub(extent2(), N.extent2(), (Stride)1) = N;
  return result;
  }

const
doubleComplexMatrix
  doubleComplexSubVector::kron(const doubleComplexSubMatrix& M) const {
  const
  doubleComplexSubVector&	v = *this;
  doubleComplexMatrix		result(M.extent2(), extent()*M.extent1());

  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = v.kron(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
  doubleComplexSubMatrix::kron(const doubleComplexSubMatrix& N) const {
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix	result(extent2()*N.extent2(), extent1()*N.extent1());
  for (Offset i = 0; i < extent2(); i++) {
    for (Offset j = 0; j < extent1(); j++) {
      result.sub(i*N.extent2(), N.extent2(), (Stride)1,
		 j*N.extent1(), N.extent1(), (Stride)1)
	= M[i][j]*N;
      }
    }
  return result;
  }

const
doubleComplexMatrix
  doubleComplexSubMatrix::kron(const doubleComplexSubVector& v) const {
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2(), extent1()*v.extent());

  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].kron(v);
    }
  return result;
  }


const
doubleComplexMatrix
  doubleSubVector::kron(const doubleComplexSubMatrix& M) const {
  const
  doubleSubVector&	v = *this;
  doubleComplexMatrix	result(M.extent2(), extent()*M.extent1());

  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = v.kron(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
  doubleSubMatrix::kron(const doubleComplexSubMatrix& N) const {
  const
  doubleSubMatrix&	M = *this;
  doubleComplexMatrix	result(extent2()*N.extent2(), extent1()*N.extent1());
  for (Offset i = 0; i < extent2(); i++) {
    for (Offset j = 0; j < extent1(); j++) {
      result.sub(i*N.extent2(), N.extent2(), (Stride)1,
		 j*N.extent1(), N.extent1(), (Stride)1)
	= M[i][j]*N;
      }
    }
  return result;
  }

const
doubleComplexMatrix
  doubleSubMatrix::kron(const doubleComplexSubVector& v) const {
  const
  doubleSubMatrix&	M = *this;
  doubleComplexMatrix	result(extent2(), extent1()*v.extent());

  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].kron(v);
    }
  return result;
  }

const
doubleComplexMatrix
  doubleComplexSubVector::kron(const doubleSubMatrix& M) const {
  const
  doubleComplexSubVector&	v = *this;
  doubleComplexMatrix		result(M.extent2(), extent()*M.extent1());

  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = v.kron(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
  doubleComplexSubMatrix::kron(const doubleSubMatrix& N) const {
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix	result(extent2()*N.extent2(), extent1()*N.extent1());
  for (Offset i = 0; i < extent2(); i++) {
    for (Offset j = 0; j < extent1(); j++) {
      result.sub(i*N.extent2(), N.extent2(), (Stride)1,
		 j*N.extent1(), N.extent1(), (Stride)1)
	= M[i][j]*N;
      }
    }
  return result;
  }

const
doubleComplexMatrix
  doubleComplexSubMatrix::kron(const doubleSubVector& v) const {
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2(), extent1()*v.extent());

  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].kron(v);
    }
  return result;
  }


const
doubleComplexMatrix
  doubleComplexSubMatrix::apply(const doubleComplex (*f)(const doubleComplex&)) const {
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].apply(f);
    }
  return result;
  }

const
doubleComplexMatrix
  doubleComplexSubMatrix::apply(      doubleComplex (*f)(const doubleComplex&)) const {
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].apply(f);
    }
  return result;
  }

const
doubleComplexMatrix
  doubleComplexSubMatrix::apply(      doubleComplex (*f)(      doubleComplex )) const {
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].apply(f);
    }
  return result;
  }

const
doubleComplexMatrix
    exp(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = exp(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
    log(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = log(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
   sqrt(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = sqrt(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
    cos(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = cos(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
    sin(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = sin(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
    tan(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = tan(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
   acos(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = acos(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
   asin(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = asin(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
   atan(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = atan(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
   cosh(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = cosh(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
   sinh(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = sinh(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
   tanh(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = tanh(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
  acosh(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = acosh(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
  asinh(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = asinh(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
  atanh(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = atanh(M[i]);
    }
  return result;
  }

// Complex Functions
const
doubleComplexMatrix
   conj(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = conj(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
  iconj(const doubleComplexSubMatrix& M) {
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = iconj(M[i]);
    }
  return result;
  }

const
doubleComplexMatrix
  polar(const doubleSubMatrix& M, const doubleSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "polar(const doubleSubMatrix&, const doubleSubMatrix&)",
    M.extent2(), N.extent2(), M.extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexMatrix		result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = polar(M[i], N[i]);
    }
  return result;
  }

const
doubleMatrix
   norm(const doubleComplexSubMatrix& M) {
  doubleMatrix			result(M.extent2(), M.extent1());
  for (Offset i = 0; i < M.extent2(); i++) {
    result[i] = norm(M[i]);
    }
  return result;
  }

// Operators

// minus
const
doubleComplexMatrix
  doubleComplexSubMatrix::operator -(void) const {
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = -M[i];
    }
  return result;
  }


// multiply
const
doubleComplexMatrix
  doubleComplexSubMatrix::operator *(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator *(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i]*N[i];
    }
  return result;
  }

// divide
const
doubleComplexMatrix
  doubleComplexSubMatrix::operator /(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator /(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i]/N[i];
    }
  return result;
  }

// add
const
doubleComplexMatrix
  doubleComplexSubMatrix::operator +(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator +(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i] + N[i];
    }
  return result;
  }

// subtract
const
doubleComplexMatrix
  doubleComplexSubMatrix::operator -(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator -(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i] - N[i];
    }
  return result;
  }

// divide
const
doubleComplexMatrix
  doubleSubMatrix::operator /(const doubleComplexSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::operator /(const doubleComplexSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&		M = *this;
  doubleComplexMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i]/N[i];
    }
  return result;
  }

// subtract
const
doubleComplexMatrix
  doubleSubMatrix::operator -(const doubleComplexSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleSubMatrix::operator -(const doubleComplexSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleSubMatrix&		M = *this;
  doubleComplexMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i] - N[i];
    }
  return result;
  }

// multiply
const
doubleComplexMatrix
  doubleComplexSubMatrix::operator *(const doubleComplexSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator *(const doubleComplexSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i]*N[i];
    }
  return result;
  }

// divide
const
doubleComplexMatrix
  doubleComplexSubMatrix::operator /(const doubleComplexSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator /(const doubleComplexSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i]/N[i];
    }
  return result;
  }


// add
const
doubleComplexMatrix
  doubleComplexSubMatrix::operator +(const doubleComplexSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator +(const doubleComplexSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i] + N[i];
    }
  return result;
  }

// subtract
const
doubleComplexMatrix
  doubleComplexSubMatrix::operator -(const doubleComplexSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator -(const doubleComplexSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  doubleComplexMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i] - N[i];
    }
  return result;
  }


std::istream&	operator >>(std::istream& is,       doubleComplexSubMatrix& M) {
  for (Offset i = 0; i < M.extent2(); i++) {
    doubleComplexSubVector	v = M[i];
    if (!(is >> v)) break;
    }
  return is;
  }

std::ostream&	operator <<(std::ostream& os, const doubleComplexSubMatrix& M) {
  int			width = os.width();
  for (Offset i = 0; i < M.extent2(); i++) {
    os << std::setw(width) << M[i];
    }
  return os;
  }


// equal
bool	doubleComplexSubMatrix::operator ==(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator ==(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  Offset			i = (Offset)0;
  while (i < extent2() && M[i] == N[i]) {
   ++i;
   }
  return i == extent2();
  }

// not equal
bool	doubleComplexSubMatrix::operator !=(const doubleSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator !=(const doubleSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  Offset			i = (Offset)0;
  while (i < extent2() && M[i] != N[i]) {
   ++i;
   }
  return i == extent2();
  }

// equal
bool	doubleComplexSubMatrix::operator ==(
  const doubleComplexSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator ==(const doubleComplexSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  Offset			i = (Offset)0;
  while (i < extent2() && M[i] == N[i]) {
   ++i;
   }
  return i == extent2();
  }

// not equal
bool	doubleComplexSubMatrix::operator !=(
  const doubleComplexSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator !=(const doubleComplexSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  doubleComplexSubMatrix&	M = *this;
  Offset			i = (Offset)0;
  while (i < extent2() && M[i] != N[i]) {
   ++i;
   }
  return i == extent2();
  }



// simple assignment
doubleComplexSubMatrix&
  doubleComplexSubMatrix::operator  =(const doubleSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator  =(const doubleSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i] = N[i];
    }
  return *this;
  }

// multiply and assign
doubleComplexSubMatrix&
  doubleComplexSubMatrix::operator *=(const doubleSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator *=(const doubleSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i] *= N[i];
    }
  return *this;
  }

// divide and assign
doubleComplexSubMatrix&
  doubleComplexSubMatrix::operator /=(const doubleSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator /=(const doubleSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i] /= N[i];
    }
  return *this;
  }

// add and assign
doubleComplexSubMatrix&
  doubleComplexSubMatrix::operator +=(const doubleSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator +=(const doubleSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i] += N[i];
    }
  return *this;
  }

// subtract and assign
doubleComplexSubMatrix&
  doubleComplexSubMatrix::operator -=(const doubleSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator -=(const doubleSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i] -= N[i];
    }
  return *this;
  }

// simple assignment
doubleComplexSubMatrix&
  doubleComplexSubMatrix::operator  =(const doubleComplexSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator  =(const doubleComplexSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i] = N[i];
    }
  return *this;
  }

// multiply and assign
doubleComplexSubMatrix&
  doubleComplexSubMatrix::operator *=(const doubleComplexSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator *=(const doubleComplexSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i] *= N[i];
    }
  return *this;
  }

// divide and assign
doubleComplexSubMatrix&
  doubleComplexSubMatrix::operator /=(const doubleComplexSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator /=(const doubleComplexSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i] /= N[i];
    }
  return *this;
  }


// add and assign
doubleComplexSubMatrix&
  doubleComplexSubMatrix::operator +=(const doubleComplexSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator +=(const doubleComplexSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i] += N[i];
    }
  return *this;
  }

// subtract and assign
doubleComplexSubMatrix&
  doubleComplexSubMatrix::operator -=(const doubleComplexSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSubMatrix::operator -=(const doubleComplexSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  doubleComplexSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i] -= N[i];
    }
  return *this;
  }



/*
// LU decomposition with partial pivoting
const
offsetVector
  doubleComplexSubMatrix::lud(void) {
  const
  doubleComplexSubMatrix&	M = *this;
  Extent			m = M.extent2();
  Extent			n = M.extent1();
  Extent			l = (m < n)? m: n;
  offsetVector			p(m);
  p.ramp();			// Initialize the permutation vector.
  
  for (Offset h = 0; h+1 < l; h++) {
    // First, find the pivot element.
    Offset			k = m;
    double			x = (double)0;
    for (Offset i = h; i < m; i++) {
      double			s = abs(M[p[i]][h]);
      if (x < s) {
	x = s;
	k = i;
	}
      }

    if (k < m) {		// There is a non-zero pivot element.
      if (h < k) {
        p.swap(h, k);
	}
      doubleComplexSubVector	v = M[p[h]];
      for (Offset i = h+1; i < m; i++) {
	doubleComplexSubVector	w = M[p[i]];
	doubleComplex			t = -w[h]/v[h];
	w += t*v;
	w[h] = t;
	}
      }
    }
  return p;
  }
*/
