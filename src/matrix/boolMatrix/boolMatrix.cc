/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<boolMatrix.h>

// Functions
boolSubMatrix&
  boolSubMatrix::reverse1(void) {
  boolSubMatrix&	M = *this;
  Extent		m = extent2();
  for (Offset i = 0; i < m; i++)
    M[i].reverse();
  return *this;
  }
boolSubMatrix&
  boolSubMatrix::reverse2(void) {
  Extent		m = extent2();
  for (Offset i = 0; i < m/2; i++)
    swap(i, m-1 - i);
  return *this;
  }
boolSubMatrix&
  boolSubMatrix::swap(boolSubMatrix& N) {
  boolSubMatrix	M = (*this);
  for (Offset i = 0; i < extent2(); i++) {
    boolSubVector	v = M[i];
    boolSubVector	w = N[i];
    v.swap(w);
    }
  return *this;
  }
boolSubMatrix&
  boolSubMatrix::rotate(Stride n) {
  boolSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i].rotate(n);
    }
  return *this;
  }

boolSubMatrix&
  boolSubMatrix::shift(Stride n, bool b) {
  boolSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i].shift(n, b);
    }
  return *this;
  }

// xnor
const
boolMatrix
  boolSubMatrix::eq(const boolSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "boolSubMatrix::eq(const boolSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  boolSubMatrix&	M = *this;
  boolMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].eq(N[i]);
    }
  return result;
  }

// xor
const
boolMatrix
  boolSubMatrix::ne(const boolSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "boolSubMatrix::ne(const boolSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  boolSubMatrix&	M = *this;
  boolMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].ne(N[i]);
    }
  return result;
  }

boolVector      any(const boolSubMatrix& M) {
  Extent	m = M.extent2();
  boolVector	result(m);
  for (Offset i = 0; i < m; i++)
    result[i] = any(M[i]);
  return result; }
boolVector      all(const boolSubMatrix& M) {
  Extent	m = M.extent2();
  boolVector	result(m);
  for (Offset i = 0; i < m; i++)
    result[i] = all(M[i]);
  return result; }

Extent
  boolSubMatrix::zeros(void) const {
  const
  boolSubMatrix&	M = *this;
  Extent		result = 0;
  for (Offset i = 0; i < extent2(); i++) {
    result += M[i].zeros();
    }
  return result;
  }

// const
// offsetVector
//   boolSubMatrix::index(void) const {
//   const
//   boolSubMatrix&	M = *this;
//   Extent		n = extent2()*extent1() - zeros();
//   offsetVector		result(n);
//   Offset		k = 0;
//   for (Offset i = 0; i < extent2(); i++) {
//     Extent		n = extent1() - M[i].zeros();
//     result
//     }
//   return result;
//   }

const
boolMatrix
  boolSubMatrix::aside(const boolSubMatrix& N) const {
  boolMatrix		result(extent2(), extent1() + N.extent1());
  result.sub((Offset)0, extent2(), (Stride)1,
	     (Offset)0, extent1(), (Stride)1
) = *this;
  result.sub((Offset)0, extent2(), (Stride)1,
	     extent1(), N.extent1(), (Stride)1) = N;
  return result;
  }

const
boolMatrix
  boolSubMatrix::above(const boolSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "boolSubMatrix::above(const boolSubMatrix&) const",
    extent2(), N.extent2());
#endif // SVMT_DEBUG_MODE
  boolMatrix		result(extent2() + N.extent2(), extent1());
  result.sub((Offset)0, extent2(), (Stride)1) = *this;
  result.sub(extent2(), N.extent2(), (Stride)1) = N;
  return result;
  }

const
boolMatrix
  boolSubMatrix::apply(const bool (*f)(const bool&)) const {
  const
  boolSubMatrix&	M = *this;
  boolMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].apply(f);
    }
  return result;
  }

const
boolMatrix
  boolSubMatrix::apply(      bool (*f)(const bool&)) const {
  const
  boolSubMatrix&	M = *this;
  boolMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].apply(f);
    }
  return result;
  }

const
boolMatrix
  boolSubMatrix::apply(      bool (*f)(      bool )) const {
  const
  boolSubMatrix&	M = *this;
  boolMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i].apply(f);
    }
  return result;
  }

// pack the diagonal and subdiagonal elements with scalar b.
boolSubMatrix&
  boolSubMatrix::pack(bool b) {
  boolSubMatrix&	M = *this;
  Extent		m = extent2();
  Extent		n = extent1();
  if (n < m) {
    for (Offset i = 0; i < n; i++)
      M[i].sub(0, 1+i, 1) = b;
    M.sub(n, m-n, 1) = b;
    }
  else {
    for (Offset i = 0; i < m; i++)
      M[i].sub(0, 1+i, 1) = b;
    }
  return M; }
// pack the diagonal and subdiagonal elements
// with the corresponding elements of submatrix N.
boolSubMatrix&
  boolSubMatrix::pack(const boolSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "boolSubMatrix::pack(const boolSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  boolSubMatrix&	M = *this;
  Extent		m = extent2();
  Extent		n = extent1();
  if (n < m) {
    for (Offset i = 0; i < n; i++)
      M[i].sub(0, 1+i, 1) = N[i].sub(0, i+1, 1);
    M.sub(n, m-n, 1) = N.sub(n, m-n, 1);
    }
  else {
    for (Offset i = 0; i < m; i++)
      M[i].sub(0, 1+i, 1) = N[i].sub(0, i+1, 1);
    }
  return M; }
// Operators
// not
const
boolMatrix
  boolSubMatrix::operator !(void) const {
  const
  boolSubMatrix&	M = *this;
  boolMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = !M[i];
    }
  return result;
  }

// and
const
boolMatrix
  boolSubMatrix::operator &&(const boolSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "boolSubMatrix::operator &&(const boolSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  boolSubMatrix&	M = *this;
  boolMatrix		result(extent2(), extent1());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i] && N[i];
    }
  return result;
  }

// or
const
boolMatrix
  boolSubMatrix::operator ||(const boolSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "boolSubMatrix::operator &&(const boolSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  boolSubMatrix&	M = *this;
  boolMatrix		result(extent2(), extent2());
  for (Offset i = 0; i < extent2(); i++) {
    result[i] = M[i] || N[i];
    }
  return result;
  }

std::istream&	operator >>(std::istream& is, boolSubMatrix& M) {
  for (Offset i = 0; i < M.extent2(); i++) {
    boolSubVector	v = M[i];
    if (!(is >> v)) break;
    }
  return is;
  }

std::ostream&	operator <<(std::ostream& os, const boolSubMatrix& M) {
  int			width = os.width();
  Offset		i = 0;
  while (i < M.extent2() && os << std::setw(width) << M[i]) {
    ++i;
    }
  return os;
  }

// equal
bool	boolSubMatrix::operator ==(const boolSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "boolSubMatrix::operator ==(const boolSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  boolSubMatrix&	M = *this;
  Offset		i = 0;
  while (i < extent2() && M[i] == N[i]) {
   ++i;
   }
  return i == extent2();
  }

// not equal
bool	boolSubMatrix::operator !=(const boolSubMatrix& N) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "boolSubMatrix::operator !=(const boolSubMatrix&) const",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  const
  boolSubMatrix&	M = *this;
  Offset		i = 0;
  while (i < extent2() &&  M[i] != N[i]) {
   ++i;
   }
  return i == extent2();
  }

// simple assignment
boolSubMatrix&
  boolSubMatrix::operator  =(const boolSubMatrix& N) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "boolSubMatrix::operator  =(const boolSubMatrix&)",
    extent2(), N.extent2(), extent1(), N.extent1());
#endif // SVMT_DEBUG_MODE
  boolSubMatrix&	M = *this;
  for (Offset i = 0; i < extent2(); i++) {
    M[i] = N[i];
    }
  return *this;
  }

