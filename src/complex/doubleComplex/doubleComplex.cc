/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleComplex.h>

// Functions
doubleComplex	sqrt(const doubleComplex& c) {
  // Solve c = z*z where z = x_z + i*y_z, c = x_c + i*y_c,
  // x_c = x_z^2 - y_z^2, y_c = 2*x_z*y_z and |c| = |z|^2 = x_z^2 + y_z^2.
  // if x_c < 0 then |c| + |x_c| = x_z^2 + y_z^2 - (x_z^2 - y_z^2) = 2*y_z^2.
  // if x_c > 0 then |c| + |x_c| = x_z^2 + y_z^2 + (x_z^2 - y_z^2) = 2*x_z^2.
  double	x = (double)0;
  double	y = (double)0;
  if ((double)0 == c.imag()) {		// pure real
    if (c.real() < (double)0) {
      x = (double)0; y = sqrt(-c.real()); }
    else {
      y = (double)0; x = sqrt( c.real()); }
    }
  else
  if ((double)0 == c.real()) {		// pure imaginary
    if (c.imag() < (double)0) {
      x = sqrt(-c.imag()*(double)(0.5)); y = -x; }
    else {
      x = sqrt( c.imag()*(double)(0.5)); y =  x; }
    }
  else {				// non-zero real & imaginary parts
    x = sqrt(0.5*(abs(c) + std::abs(c.real())));
    y = 0.5*c.imag()/x;
    if (c.real() < (double)0) {
      double	t = x;
      if (c.imag() < (double)0) {
	x = -y; y = -t; }
      else {
	x =  y; y =  t; }
      }
    }
  return doubleComplex(x, y);
  }

// Operators
std::istream&	operator >>(std::istream& s, doubleComplex& c) {
  double	r = (double)0;		// real      part
  double	i = (double)0;		// imaginary part
  char		p = 0;			// punctuation
  // A complex number with  r, (r) or (r, i) is expected.
  if (s >> p) {
    if ('(' == p) {			// (
      if (s >> r && s >> p) {		// (r
	if (',' == p) {			// (r,
	  if (s >> i && s >> p) {	// (r, i
	    if (')' == p)		// (r, i)
	      c = doubleComplex(r, i);
	    else {			// (r, i?
	      s.putback(p);
	      s.clear(std::ios::badbit);
	      }
	    }
	  }
	else
	if (')' == p)			// (r)
	  c = doubleComplex(r, i);
	else {				// (r?
	  s.putback(p);
	  s.clear(std::ios::badbit);
	  }
	}
      }
    else {
      s.putback(p);
      if (s >> r)			// r
	c = doubleComplex(r, i);
      }
    }
  return s;
  }

std::ostream&	operator <<(std::ostream& s, const doubleComplex& c) {
  int		width = s.width();
  return s << '(' << std::setw(width) << c.real() << ','
	   << ' ' << std::setw(width) << c.imag() << ')';
  }

