#ifndef _doubleComplexScalar_h
#define _doubleComplexScalar_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleComplexHandle.h>
#include<doubleScalar.h>

class doubleComplexSubScalar {
  private:
  // Representation
  doubleComplexHandle
		H;
  Offset	O;
  public:
  // Constructors
		doubleComplexSubScalar(
    const doubleComplexHandle& h, Offset o);
		doubleComplexSubScalar(
    const doubleComplexSubScalar& s);
		~doubleComplexSubScalar(void);
  // Functions
  const
  doubleComplexHandle&
		handle(void) const;
  Offset	offset(void) const;
  bool		 empty(void) const;
  const
  doubleComplexSubVector
	     subvector(Extent n = 1) const;
  const
  doubleComplexSubMatrix
	     submatrix(Extent n = 1, Extent m = 1) const;
  const
  doubleComplexSubTensor
	     subtensor(Extent n = 1, Extent m = 1, Extent l = 1) const;
  doubleSubScalar
		  real(void);
  double	  real(void) const;
  doubleSubScalar
		  imag(void);
  double	  imag(void) const;

  // Operators
  doubleComplex		operator -(void) const;
		operator doubleComplex (void) const;
  doubleComplexSubScalar&
		operator  =(const double& s);
  doubleComplexSubScalar&
		operator *=(const double& s);
  doubleComplexSubScalar&
		operator /=(const double& s);
  doubleComplexSubScalar&
		operator +=(const double& s);
  doubleComplexSubScalar&
		operator -=(const double& s);
  doubleComplexSubScalar&
		operator  =(const doubleComplex& s);
  doubleComplexSubScalar&
		operator *=(const doubleComplex& s);
  doubleComplexSubScalar&
		operator /=(const doubleComplex& s);
  doubleComplexSubScalar&
		operator +=(const doubleComplex& s);
  doubleComplexSubScalar&
		operator -=(const doubleComplex& s);
  doubleComplexSubScalar&
		operator  =(const doubleComplexSubScalar& s);
  };

// Constructors
inline		doubleComplexSubScalar::doubleComplexSubScalar(
  const doubleComplexHandle& h, Offset o): H(h), O(o) { }
inline		doubleComplexSubScalar::doubleComplexSubScalar(
  const doubleComplexSubScalar& s): H(s.H), O(s.O) { }
inline		doubleComplexSubScalar::~doubleComplexSubScalar(void) { }

// Functions
inline
const
doubleComplexHandle&
		doubleComplexSubScalar::handle(void) const { return H; }
inline
Offset		doubleComplexSubScalar::offset(void) const { return O; }
inline
bool		doubleComplexSubScalar::empty(void) const {
  return handle().empty(); }
inline
doubleSubScalar	doubleComplexSubScalar::real(void) {
  return doubleSubScalar(handle(), (offset() << 1)); }
inline
double		doubleComplexSubScalar::real(void) const {
  return handle().get(offset()).real(); }
inline
doubleSubScalar	doubleComplexSubScalar::imag(void) {
  return doubleSubScalar(handle(), (offset() << 1) + 1); }
inline
double		doubleComplexSubScalar::imag(void) const {
  return handle().get(offset()).imag(); }

// Operators
inline
doubleComplex		doubleComplexSubScalar::operator -(void) const {
  return -H.get(offset()); }
inline		doubleComplexSubScalar::operator doubleComplex (void) const {
  return H.get(offset()); }
inline
std::istream&	operator >>(std::istream& is,       doubleComplexSubScalar& s) {
  doubleComplex		x;
  if (is >> x)
    ((doubleComplexHandle)s.handle()).put(s.offset(), x);
  return is; }
inline
std::ostream&	operator <<(std::ostream& os, const doubleComplexSubScalar& s) {
  return os << s.handle().get(s.offset()); }

inline
doubleComplexSubScalar&
		doubleComplexSubScalar::operator  =(const double& s) {
  H.put(offset(), s); return *this; }
inline
doubleComplexSubScalar&
		doubleComplexSubScalar::operator *=(const double& s) {
  H.put(offset(), H.get(offset())*s); return *this; }
inline
doubleComplexSubScalar&
		doubleComplexSubScalar::operator /=(const double& s) {
  H.put(offset(), H.get(offset())/s); return *this; }
inline
doubleComplexSubScalar&
		doubleComplexSubScalar::operator +=(const double& s) {
  H.put(offset(), H.get(offset()) + s); return *this; }
inline
doubleComplexSubScalar&
		doubleComplexSubScalar::operator -=(const double& s) {
  H.put(offset(), H.get(offset()) - s); return *this; }
inline
doubleComplexSubScalar&
		doubleComplexSubScalar::operator  =(const doubleComplex& s) {
  H.put(offset(), s); return *this; }
inline
doubleComplexSubScalar&
		doubleComplexSubScalar::operator *=(const doubleComplex& s) {
  H.put(offset(), H.get(offset())*s); return *this; }
inline
doubleComplexSubScalar&
		doubleComplexSubScalar::operator /=(const doubleComplex& s) {
  H.put(offset(), H.get(offset())/s); return *this; }
inline
doubleComplexSubScalar&
		doubleComplexSubScalar::operator +=(const doubleComplex& s) {
  H.put(offset(), H.get(offset()) + s); return *this; }
inline
doubleComplexSubScalar&
		doubleComplexSubScalar::operator -=(const doubleComplex& s) {
  H.put(offset(), H.get(offset()) - s); return *this; }
inline
doubleComplexSubScalar&
		doubleComplexSubScalar::operator  =(
  const doubleComplexSubScalar& s) { return operator =((doubleComplex)s); }

class doubleComplexSubArray0: public doubleComplexSubScalar {
  public:
  // Constructors
		doubleComplexSubArray0(double* p, Offset o);
		doubleComplexSubArray0(const doubleComplexSubArray0& s);
		~doubleComplexSubArray0(void);
  // Functions
  const
  doubleComplexSubArray1
	     subvector(Extent n = 1) const;
  const
  doubleComplexSubArray2
	     submatrix(Extent n = 1, Extent m = 1) const;
  const
  doubleComplexSubArray3
	     subtensor(Extent n = 1, Extent m = 1, Extent l = 1) const;
  // Operators
  doubleComplexSubArray0&
		operator  =(const double& s);
  doubleComplexSubArray0&
		operator *=(const double& s);
  doubleComplexSubArray0&
		operator /=(const double& s);
  doubleComplexSubArray0&
		operator +=(const double& s);
  doubleComplexSubArray0&
		operator -=(const double& s);
  doubleComplexSubArray0&
		operator  =(const doubleComplex& s);
  doubleComplexSubArray0&
		operator *=(const doubleComplex& s);
  doubleComplexSubArray0&
		operator /=(const doubleComplex& s);
  doubleComplexSubArray0&
		operator +=(const doubleComplex& s);
  doubleComplexSubArray0&
		operator -=(const doubleComplex& s);
  doubleComplexSubArray0&
		operator  =(const doubleComplexSubScalar& s);
  };

// Constructors
inline		doubleComplexSubArray0::doubleComplexSubArray0(
  double* p, Offset o): doubleComplexSubScalar(doubleComplexHandle(p), o) { }
inline		doubleComplexSubArray0::doubleComplexSubArray0(
  const doubleComplexSubArray0& s): doubleComplexSubScalar(s) { }
inline		doubleComplexSubArray0::~doubleComplexSubArray0(void) { }

// Operators
inline
doubleComplexSubArray0&
		doubleComplexSubArray0::operator  =(const double& s) {
  doubleComplexSubScalar::operator  =(s); return *this; }
inline
doubleComplexSubArray0&
		doubleComplexSubArray0::operator *=(const double& s) {
  doubleComplexSubScalar::operator *=(s); return *this; }
inline
doubleComplexSubArray0&
		doubleComplexSubArray0::operator /=(const double& s) {
  doubleComplexSubScalar::operator /=(s); return *this; }
inline
doubleComplexSubArray0&
		doubleComplexSubArray0::operator +=(const double& s) {
  doubleComplexSubScalar::operator +=(s); return *this; }
inline
doubleComplexSubArray0&
		doubleComplexSubArray0::operator -=(const double& s) {
  doubleComplexSubScalar::operator -=(s); return *this; }
inline
doubleComplexSubArray0&
		doubleComplexSubArray0::operator  =(const doubleComplex& s) {
  doubleComplexSubScalar::operator  =(s); return *this; }
inline
doubleComplexSubArray0&
		doubleComplexSubArray0::operator *=(const doubleComplex& s) {
  doubleComplexSubScalar::operator *=(s); return *this; }
inline
doubleComplexSubArray0&
		doubleComplexSubArray0::operator /=(const doubleComplex& s) {
  doubleComplexSubScalar::operator /=(s); return *this; }
inline
doubleComplexSubArray0&
		doubleComplexSubArray0::operator +=(const doubleComplex& s) {
  doubleComplexSubScalar::operator +=(s); return *this; }
inline
doubleComplexSubArray0&
		doubleComplexSubArray0::operator -=(const doubleComplex& s) {
  doubleComplexSubScalar::operator -=(s); return *this; }
inline
doubleComplexSubArray0&
		doubleComplexSubArray0::operator  =(
  const doubleComplexSubScalar& s) {
  doubleComplexSubScalar::operator =(s); return *this; }

#endif /* _doubleComplexScalar_h */

