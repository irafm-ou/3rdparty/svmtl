#ifndef _offsetArray1_h
#define _offsetArray1_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<offsetVector.h>


template<Extent n>
class offsetArray1: public offsetSubArray1 {
private:
  // Representation
  offset	A[n];
  // Functions			// prevent offsetSubArray1 resize
  offsetArray1<n>&
		resize(void);
  offsetArray1<n>&
		resize(offset*, Offset, Extent, Stride);
  offsetArray1<n>&
		resize(const offsetSubArray1&);
public:
  // Constructors
		offsetArray1(void):
    offsetSubArray1(A, 0, n, 1) { }
		offsetArray1(const offset& s):
    offsetSubArray1(A, 0, n, 1) {
    offsetSubArray1::operator =(s); }
		offsetArray1(const offsetArray1<n>& v):
    offsetSubArray1(A, 0, n, 1) {
    offsetSubArray1::operator =(v); }
	       ~offsetArray1(void) { }
  // Operators
  offsetArray1<n>&
  operator =(const offsetArray1<n>& v) {
    offsetSubArray1::operator =(v);
    return *this; }
  };

#endif /* _offsetArray1_h */

