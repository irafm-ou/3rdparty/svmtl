#ifndef _doubleComplexMatrix_h
#define _doubleComplexMatrix_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleMatrix.h>
#include<doubleComplexVector.h>

class doubleComplexSubMatrix {
  private:
  // Representation
  doubleComplexHandle
		H;
  Offset	O;
  Extent	N1;
  Stride	S1;
  Extent	N2;
  Stride	S2;
  public:
  // Constructors
		doubleComplexSubMatrix(void);
		doubleComplexSubMatrix(const doubleComplexHandle& h,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
		doubleComplexSubMatrix(const doubleComplexSubMatrix& M);
		~doubleComplexSubMatrix(void);
  // Functions
  const
  doubleComplexHandle&
		handle(void) const;
  Offset	offset(void) const;
  Extent	extent1(void) const;
  Extent	length1(void) const;	// deprecated
  Stride	stride1(void) const;
  Extent	extent2(void) const;
  Extent	length2(void) const;	// deprecated
  Stride	stride2(void) const;
  bool		 empty(void) const;
  static
  doubleComplexHandle
	      allocate(Extent m, Extent n);
  static
  doubleComplexHandle
	      allocate_(Extent n, Extent m);
  doubleComplexSubMatrix&
		  free(void);
  doubleComplexSubMatrix&
		resize(void);
  doubleComplexSubMatrix&
		resize(const doubleComplexHandle& h,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleComplexSubMatrix&
		resize(const doubleComplexSubMatrix& M);
  doubleComplexSubMatrix&
		resize_(const doubleComplexHandle& h,
      Offset o, Extent n1, Stride s1,
		Extent n2, Stride s2);
  bool	      contains(Offset i, Extent n2, Stride s2) const;
  bool	      contains(Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  bool	      contains_(Offset i, Extent n2, Stride s2) const;
  bool	      contains_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2) const;

  doubleComplexSubMatrix
		   sub(Offset i, Extent n2, Stride s2);
  const
  doubleComplexSubMatrix
		   sub(Offset i, Extent n2, Stride s2) const;
  doubleComplexSubMatrix
		   sub(Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1);
  const
  doubleComplexSubMatrix
		   sub(Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  doubleComplexSubMatrix
		   sub_(Offset i, Extent n2, Stride s2);
  const
  doubleComplexSubMatrix
		   sub_(Offset i, Extent n2, Stride s2) const;
  doubleComplexSubMatrix
		   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2);
  const
  doubleComplexSubMatrix
		   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2) const;
  doubleComplexSubSquare
	     subsquare(Offset i, Extent m, Stride s2,
		       Offset j = (Offset)0, Stride s1 = (Stride)1);
  const
  doubleComplexSubSquare
	     subsquare(Offset i, Extent m, Stride s2,
		       Offset j = (Offset)0, Stride s1 = (Stride)1) const;
  doubleComplexSubSquare
	     subsquare_(Offset i, Extent m, Stride s2);
  const
  doubleComplexSubSquare
	     subsquare_(Offset i, Extent m, Stride s2) const;
  doubleComplexSubSquare
	     subsquare_(Offset j, Stride s1,
			Offset i, Extent m, Stride s2);
  const
  doubleComplexSubSquare
	     subsquare_(Offset j, Stride s1,
			Offset i, Extent m, Stride s2) const;
  const
  doubleComplexSubTensor
	     subtensor(Extent l = 1) const;
  doubleComplexSubMatrix
		     t(void);
  const
  doubleComplexSubMatrix
		     t(void) const;
  doubleComplexSubVector
		  diag(void);
  const
  doubleComplexSubVector
		  diag(void) const;

  doubleComplexSubMatrix
		     r1(void);
  const
  doubleComplexSubMatrix
		     r1(void) const;
  doubleComplexSubMatrix&
	       reverse1(void);
  doubleComplexSubMatrix
		     r2(void);
  const
  doubleComplexSubMatrix
		     r2(void) const;
  doubleComplexSubMatrix&
	       reverse2(void);
  doubleComplexSubMatrix
		     r(void);
  const
  doubleComplexSubMatrix
		     r(void) const;
  doubleComplexSubMatrix&
	       reverse(void);
  doubleComplexSubMatrix
		  even(void);
  const
  doubleComplexSubMatrix
		  even(void) const;
  doubleComplexSubMatrix
		   odd(void);
  const
  doubleComplexSubMatrix
		   odd(void) const;
  doubleComplexSubMatrix
		  even_(void);
  const
  doubleComplexSubMatrix
		  even_(void) const;
  doubleComplexSubMatrix
		   odd_(void);
  const
  doubleComplexSubMatrix
		   odd_(void) const;

  doubleSubMatrix
		  real(void);
  const
  doubleSubMatrix
		  real(void) const;
  doubleSubMatrix
		  imag(void);
  const
  doubleSubMatrix
		  imag(void) const;
  doubleComplexSubMatrix&
		   dft(int sign = -1);	// complex to complex dft
  doubleComplexSubMatrix&
		  swap(Offset i, Offset k);
  doubleComplexSubMatrix&
		  swap_(Offset i, Offset k);
  doubleComplexSubMatrix&
		  swap(doubleComplexSubMatrix& N);
  doubleComplexSubMatrix&
		rotate(Stride n);
  doubleComplexSubMatrix&
		 shift(Stride n,const doubleComplex& s
		  = doubleComplex((double)0, (double)0));

  const
  doubleComplexVector
		   sum(void) const;
  const
  doubleComplexMatrix
		   dot(const doubleSubVector& v) const;
  const
  doubleComplexMatrix
		   dot(const doubleSubMatrix& N) const;
  const
  doubleComplexMatrix
		   dot(const doubleComplexSubVector& v) const;
  const
  doubleComplexMatrix
		   dot(const doubleComplexSubMatrix& N) const;
  const
  doubleComplexVector
		   dot_(const doubleSubVector& v) const;
  const
  doubleComplexMatrix
		   dot_(const doubleSubMatrix& N) const;
  const
  doubleComplexVector
		   dot_(const doubleComplexSubVector& v) const;
  const
  doubleComplexMatrix
		   dot_(const doubleComplexSubMatrix& N) const;
  const
  doubleComplexSquare
		   dot(void) const;
  const
  doubleComplexSquare
		square(void) const;
  doubleComplexSubMatrix&
		  pack(const doubleComplex& s = doubleComplex((double)0, (double)0));
  doubleComplexSubMatrix&
		  pack(const doubleComplexSubMatrix& N);

  const
  doubleComplexMatrix
		   svd(void);
  const
  offsetVector	   lud(void);
  const
  offsetVector	   qrd(void);
private:
  doubleComplexSubMatrix&
		   hht(const doubleComplexSubVector& v);
public:
  const
  doubleComplexMatrix
	   permutation(const offsetSubVector& p) const;
  const
  doubleComplexMatrix
		    pl(const offsetSubVector& p,
		       const doubleComplexSubMatrix& L) const;
  const
  doubleComplexMatrix
		     l(const doubleComplexSubMatrix& L) const;
  const
  doubleComplexMatrix
		   pld(const offsetSubVector& p,
		       const doubleComplexSubMatrix& LD) const;
  const
  doubleComplexMatrix
		    ld(const doubleComplexSubMatrix& LD) const;
  const
  doubleComplexMatrix
		     u(const doubleComplexSubMatrix& U) const;
  const
  doubleComplexMatrix
		    up(const doubleComplexSubMatrix& U,
		       const offsetSubVector& p) const;
  const
  doubleComplexMatrix
		    du(const doubleComplexSubMatrix& DU) const;
  const
  doubleComplexMatrix
		   dup(const doubleComplexSubMatrix& DU,
		       const offsetSubVector& p) const;
  const
  doubleComplexMatrix
		    pl_(const offsetSubVector& p,
		       const doubleComplexSubMatrix& L) const;
  const
  doubleComplexMatrix
		     l_(const doubleComplexSubMatrix& L) const;
  const
  doubleComplexMatrix
		   pld_(const offsetSubVector& p,
		       const doubleComplexSubMatrix& LD) const;
  const
  doubleComplexMatrix
		    ld_(const doubleComplexSubMatrix& LD) const;
  const
  doubleComplexMatrix
		     u_(const doubleComplexSubMatrix& U) const;
  const
  doubleComplexMatrix
		    up_(const doubleComplexSubMatrix& U,
		       const offsetSubVector& p) const;
  const
  doubleComplexMatrix
		    du_(const doubleComplexSubMatrix& DU) const;
  const
  doubleComplexMatrix
		   dup_(const doubleComplexSubMatrix& DU,
		       const offsetSubVector& p) const;
  const
  doubleComplexMatrix
		    pq(const offsetSubVector& p,
		       const doubleComplexSubMatrix& L) const;
  const
  doubleComplexMatrix
		     q(const doubleComplexSubMatrix& L) const;

  const
  boolMatrix
		    eq(const double& s) const;
  const
  boolMatrix
		    ne(const double& s) const;
  const
  boolMatrix
		    eq(const doubleComplex& s) const;
  const
  boolMatrix
		    ne(const doubleComplex& s) const;
  const
  boolMatrix
		    eq(const doubleSubMatrix& N) const;
  const
  boolMatrix
		    ne(const doubleSubMatrix& N) const;
  const
  boolMatrix
		    eq(const doubleComplexSubMatrix& N) const;
  const
  boolMatrix
		    ne(const doubleComplexSubMatrix& N) const;

  Extent	 zeros(void) const;
  const
  offsetVector
		 index(void) const;
  const
  doubleComplexVector
		gather(const offsetSubVector& x) const;
  doubleComplexSubMatrix&
	       scatter(const offsetSubVector& x,
		       const doubleComplexSubVector& t);
  const
  doubleComplexMatrix
		 aside(const doubleComplexSubMatrix& N) const;
  const
  doubleComplexMatrix
		 above(const doubleComplexSubMatrix& N) const;
  const
  doubleComplexMatrix
		 above(const doubleComplexSubVector& v) const;
  const
  doubleComplexTensor
		 afore(const doubleComplexSubMatrix& N) const;
  const
  doubleComplexTensor
		 afore(const doubleComplexSubTensor& T) const;
  const
  doubleComplexMatrix
		 above_(const doubleComplexSubMatrix& N) const;
  const
  doubleComplexMatrix
		 aside_(const doubleComplexSubMatrix& N) const;
  const
  doubleComplexMatrix
		 aside_(const doubleComplexSubVector& v) const;
  const
  doubleComplexMatrix
		  kron(const doubleComplexSubVector& v) const;
  const
  doubleComplexMatrix
		  kron(const doubleComplexSubMatrix& N) const;
  const
  doubleComplexTensor
		  kron(const doubleComplexSubTensor& T) const;
  const
  doubleComplexMatrix
		  kron(const doubleSubVector& v) const;
  const
  doubleComplexMatrix
		  kron(const doubleSubMatrix& N) const;
  const
  doubleComplexTensor
		  kron(const doubleSubTensor& T) const;

  const
  doubleComplexMatrix
		 apply(const doubleComplex (*f)(const doubleComplex&)) const;
  const
  doubleComplexMatrix
		 apply(      doubleComplex (*f)(const doubleComplex&)) const;
  const
  doubleComplexMatrix
		 apply(      doubleComplex (*f)(      doubleComplex )) const;

  // Operators
  doubleComplexSubVector
		operator [](Offset i);
  const
  doubleComplexSubVector
		operator [](Offset i) const;
  doubleComplexSubVector
		operator ()(Offset i);
  const
  doubleComplexSubVector
		operator ()(Offset i) const;
  doubleComplexSubScalar
		operator ()(Offset j, Offset i);
  const
  doubleComplexSubScalar
		operator ()(Offset j, Offset i) const;
  const
  doubleComplexMatrix
		operator -(void) const;
//  const
//  doubleComplexSubMatrix
//		operator +(void) const;
  const
  doubleComplexMatrix
		operator *(const double& s) const;
  const
  doubleComplexMatrix
		operator /(const double& s) const;
  const
  doubleComplexMatrix
		operator +(const double& s) const;
  const
  doubleComplexMatrix
		operator -(const double& s) const;

  const
  doubleComplexMatrix
		operator *(const doubleComplex& s) const;
  const
  doubleComplexMatrix
		operator /(const doubleComplex& s) const;
  const
  doubleComplexMatrix
		operator +(const doubleComplex& s) const;
  const
  doubleComplexMatrix
		operator -(const doubleComplex& s) const;
  const
  doubleComplexMatrix
		operator *(const doubleSubMatrix& N) const;
  const
  doubleComplexMatrix
		operator /(const doubleSubMatrix& N) const;
  const
  doubleComplexMatrix
		operator +(const doubleSubMatrix& N) const;
  const
  doubleComplexMatrix
		operator -(const doubleSubMatrix& N) const;

  const
  doubleComplexMatrix
		operator *(const doubleComplexSubMatrix& N) const;
  const
  doubleComplexMatrix
		operator /(const doubleComplexSubMatrix& N) const;
  const
  doubleComplexMatrix
		operator +(const doubleComplexSubMatrix& N) const;
  const
  doubleComplexMatrix
		operator -(const doubleComplexSubMatrix& N) const;
  bool		operator ==(const double& s) const;
  bool		operator !=(const double& s) const;
  bool		operator ==(const doubleComplex& s) const;
  bool		operator !=(const doubleComplex& s) const;
  bool		operator ==(const doubleSubMatrix& N) const;
  bool		operator !=(const doubleSubMatrix& N) const;
  bool		operator ==(const doubleComplexSubMatrix& N) const;
  bool		operator !=(const doubleComplexSubMatrix& N) const;

  doubleComplexSubMatrix&
		operator  =(const double& s);
  doubleComplexSubMatrix&
		operator *=(const double& s);
  doubleComplexSubMatrix&
		operator /=(const double& s);
  doubleComplexSubMatrix&
		operator +=(const double& s);
  doubleComplexSubMatrix&
		operator -=(const double& s);

  doubleComplexSubMatrix&
		operator  =(const doubleComplex& s);
  doubleComplexSubMatrix&
		operator *=(const doubleComplex& s);
  doubleComplexSubMatrix&
		operator /=(const doubleComplex& s);
  doubleComplexSubMatrix&
		operator +=(const doubleComplex& s);
  doubleComplexSubMatrix&
		operator -=(const doubleComplex& s);
  doubleComplexSubMatrix&
		operator  =(const doubleSubMatrix& N);
  doubleComplexSubMatrix&
		operator *=(const doubleSubMatrix& N);
  doubleComplexSubMatrix&
		operator /=(const doubleSubMatrix& N);
  doubleComplexSubMatrix&
		operator +=(const doubleSubMatrix& N);
  doubleComplexSubMatrix&
		operator -=(const doubleSubMatrix& N);
  doubleComplexSubMatrix&
		operator  =(const doubleComplexSubMatrix& N);
  doubleComplexSubMatrix&
		operator *=(const doubleComplexSubMatrix& N);
  doubleComplexSubMatrix&
		operator /=(const doubleComplexSubMatrix& N);
  doubleComplexSubMatrix&
		operator +=(const doubleComplexSubMatrix& N);
  doubleComplexSubMatrix&
		operator -=(const doubleComplexSubMatrix& N);
  friend class doubleComplexMatrix;
  };

// Constructors
inline		doubleComplexSubMatrix::doubleComplexSubMatrix(void):
  H((double*)0), O((Offset)0), N1((Extent)0), S1((Stride)0),
    N2((Extent)0), S2((Stride)0) { }
inline		doubleComplexSubMatrix::doubleComplexSubMatrix(
    const doubleComplexHandle& h, Offset o,
    Extent n2, Stride s2,
    Extent n1, Stride s1):
  H(h), O(o), N1(n1), S1(s1), N2(n2), S2(s2) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment(
    "doubleComplexSubMatrix::doubleComplexSubMatrix(\n"
    "const doubleComplexHandle&, Offset, Extent, Stride, Extent, Stride)",
    o, n2, s2, n1, s1);
#endif // SVMT_DEBUG_MODE
  }
inline		doubleComplexSubMatrix::doubleComplexSubMatrix(
    const doubleComplexSubMatrix& M): H(M.H), O(M.O),
    N1(M.N1), S1(M.S1), N2(M.N2), S2(M.S2) { }
inline		doubleComplexSubMatrix::~doubleComplexSubMatrix(void) { }

// Functions
inline
const
doubleComplexHandle&
		doubleComplexSubMatrix::handle(void) const { return H; }
inline
Offset		doubleComplexSubMatrix::offset(void) const { return O; }
inline
Extent		doubleComplexSubMatrix::extent1(void) const { return N1; }
inline
Extent		doubleComplexSubMatrix::length1(void) const { return N1; }
inline
Stride		doubleComplexSubMatrix::stride1(void) const { return S1; }
inline
Extent		doubleComplexSubMatrix::extent2(void) const { return N2; }
inline
Extent		doubleComplexSubMatrix::length2(void) const { return N2; }
inline
Stride		doubleComplexSubMatrix::stride2(void) const { return S2; }
inline
bool		doubleComplexSubMatrix::empty(void) const {
  return handle().empty()||0 == extent1()||0 == extent2(); }
inline
doubleComplexHandle
		doubleComplexSubMatrix::allocate(Extent m, Extent n) {
  return doubleComplexHandle(new double[m*n << 1]); }
inline
doubleComplexHandle
		doubleComplexSubMatrix::allocate_(Extent n, Extent m) {
  return allocate(m, n); }
inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::free(void) {
  delete [] (double*)H; return *this; }

inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::resize(void) {
  H = doubleComplexHandle((double*)0); O = (Offset)0;
  N1 = (Extent)0; S1 = (Stride)0;
  N2 = (Extent)0; S2 = (Stride)0; return *this; }
inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::resize(const doubleComplexHandle& h,
    Offset o, Extent n2, Stride s2,
	      Extent n1, Stride s1) {
  H = h; O = o; N1 = n1; S1 = s1; N2 = n2; S2 = s2;
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment(
    "doubleComplexSubMatrix::resize(const doubleComplexHandle&,\n"
    "Offset, Extent, Stride, Extent, Stride)",
    o, n2, s2, n1, s1);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::resize(
    const doubleComplexSubMatrix& M) { return resize(M.handle(),
    M.offset(), M.extent2(), M.stride2(),
		M.extent1(), M.stride1()); }
inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::resize_(const doubleComplexHandle& h,
    Offset o, Extent n1, Stride s1,
	      Extent n2, Stride s2) { return resize(h, o, n2, s2, n1, s1); }
inline
bool		doubleComplexSubMatrix::contains(
    Offset i, Extent n2, Stride s2) const {
  return boolSubVector::contains(
    i, n2, s2, extent2()); }
inline
bool		doubleComplexSubMatrix::contains(
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) const {
  return boolSubMatrix::contains(
    i, n2, s2, extent2(),
    j, n1, s1, extent1()); }
inline
bool		doubleComplexSubMatrix::contains_(
    Offset i, Extent n2, Stride s2) const {
  return contains(i-1, n2, s2); }
inline
bool		doubleComplexSubMatrix::contains_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2) const {
  return contains(i-1, n2, s2, j-1, n1, s1); }

inline
doubleComplexSubMatrix
		doubleComplexSubMatrix::sub(
    Offset i, Extent n2, Stride s2) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleComplexSubMatrix::sub(Offset, Extent, Stride)",
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubMatrix(handle(), offset() + (Stride)i*stride2(),
    n2, s2*stride2(), extent1(), stride1()); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubMatrix::sub(
    Offset i, Extent n2, Stride s2) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleComplexSubMatrix::sub(Offset, Extent, Stride) const",
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubMatrix(handle(), offset() + (Stride)i*stride2(),
    n2, s2*stride2(), extent1(), stride1()); }
inline
doubleComplexSubMatrix
		doubleComplexSubMatrix::sub(
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleComplexSubMatrix::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride)",
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubMatrix(handle(),
    offset() + (Stride)i*stride2() + (Stride)j*stride1(),
    n2, s2*stride2(), n1, s1*stride1()); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubMatrix::sub(
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleComplexSubMatrix::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride) const",
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubMatrix(handle(),
    offset() + (Stride)i*stride2() + (Stride)j*stride1(),
    n2, s2*stride2(), n1, s1*stride1()); }

inline
doubleComplexSubMatrix
		doubleComplexSubMatrix::sub_(
    Offset i, Extent n2, Stride s2) { return sub(i-1, n2, s2); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubMatrix::sub_(
    Offset i, Extent n2, Stride s2) const { return sub(i-1, n2, s2); }
inline
doubleComplexSubMatrix
		doubleComplexSubMatrix::sub_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2) {
  return sub(i-1, n2, s2, j-1, n1, s1); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubMatrix::sub_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2) const {
  return sub(i-1, n2, s2, j-1, n1, s1); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubScalar::submatrix(Extent n, Extent m) const {
  return doubleComplexSubMatrix(handle(), offset(),
    m, (Stride)0, n, (Stride)0); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubVector::submatrix(Extent m) const {
  return doubleComplexSubMatrix(handle(), offset(),
    m, (Stride)0, extent(), stride()); }
inline
doubleComplexSubMatrix
		doubleComplexSubMatrix::t(void) {
  return doubleComplexSubMatrix(handle(), offset(),
    extent1(), stride1(), extent2(), stride2()); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubMatrix::t(void) const {
  return doubleComplexSubMatrix(handle(), offset(),
    extent1(), stride1(), extent2(), stride2()); }
inline
doubleComplexSubVector
		doubleComplexSubMatrix::diag(void) {
  return doubleComplexSubVector(handle(), offset(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
const
doubleComplexSubVector
		doubleComplexSubMatrix::diag(void) const {
  return doubleComplexSubVector(handle(), offset(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }

inline
doubleComplexSubMatrix
		doubleComplexSubMatrix::r1(void) {
  return doubleComplexSubMatrix(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubMatrix::r1(void) const {
  return doubleComplexSubMatrix(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
doubleComplexSubMatrix
		doubleComplexSubMatrix::r2(void) {
  return doubleComplexSubMatrix(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubMatrix::r2(void) const {
  return doubleComplexSubMatrix(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
doubleComplexSubMatrix
		doubleComplexSubMatrix::r(void) {
  return doubleComplexSubMatrix(handle(),
    offset() + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubMatrix::r(void) const {
  return doubleComplexSubMatrix(handle(),
    offset() + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::reverse(void) {
  return reverse1().reverse2(); }

inline
doubleComplexSubMatrix
		doubleComplexSubMatrix::even(void) {
  return doubleComplexSubMatrix(handle(), offset(),
    extent2(), stride2(), (extent1() + 1) >> 1, stride1() << 1); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubMatrix::even(void) const {
  return doubleComplexSubMatrix(handle(), offset(),
    extent2(), stride2(), (extent1() + 1) >> 1, stride1() << 1); }
inline
doubleComplexSubMatrix
		doubleComplexSubMatrix::odd(void) {
  return doubleComplexSubMatrix(handle(), offset() + stride1(),
    extent2(), stride2(), extent1() >> 1, stride1() << 1); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubMatrix::odd(void) const {
  return doubleComplexSubMatrix(handle(), offset() + stride1(),
    extent2(), stride2(), extent1() >> 1, stride1() << 1); }
inline
doubleComplexSubMatrix
		doubleComplexSubMatrix::even_(void) { return odd(); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubMatrix::even_(void) const { return odd(); }
inline
doubleComplexSubMatrix
		doubleComplexSubMatrix::odd_(void) { return even(); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubMatrix::odd_(void) const { return even(); }
inline
doubleSubMatrix
		doubleComplexSubMatrix::real(void) {
  return doubleSubMatrix(handle(),     (offset() << 1),
    extent2(), stride2(), extent1(), stride1() << 1); }
inline
const
doubleSubMatrix
		doubleComplexSubMatrix::real(void) const {
  return doubleSubMatrix(handle(),     (offset() << 1),
    extent2(), stride2(), extent1(), stride1() << 1); }
inline
doubleSubMatrix
		doubleComplexSubMatrix::imag(void) {
  return doubleSubMatrix(handle(), 1 + (offset() << 1),
    extent2(), stride2(), extent1(), stride1() << 1); }
inline
const
doubleSubMatrix
		doubleComplexSubMatrix::imag(void) const {
  return doubleSubMatrix(handle(), 1 + (offset() << 1),
    extent2(), stride2(), extent1(), stride1() << 1); }

// Global function declarations
const
doubleComplexMatrix
		  conj(const doubleComplexSubMatrix& M);
const
doubleComplexMatrix
		 iconj(const doubleComplexSubMatrix& M);
const
doubleComplexMatrix
		 polar(const doubleSubMatrix& M,
		       const doubleSubMatrix& N);
const
doubleMatrix
		  norm(const doubleComplexSubMatrix& M);
inline
const
doubleMatrix
		   abs(const doubleComplexSubMatrix& M) {
  return hypot(M.real(), M.imag()); }
inline
const
doubleMatrix
		   arg(const doubleComplexSubMatrix& M) {
  return atan2(M.imag(), M.real()); }

const
doubleComplexMatrix
		   log(const doubleComplexSubMatrix& M);
const
doubleComplexMatrix
		   exp(const doubleComplexSubMatrix& M);
const
doubleComplexMatrix
		  sqrt(const doubleComplexSubMatrix& M);
const
doubleComplexMatrix
		   cos(const doubleComplexSubMatrix& M);
const
doubleComplexMatrix
		   sin(const doubleComplexSubMatrix& M);
const
doubleComplexMatrix
		   tan(const doubleComplexSubMatrix& M);
const
doubleComplexMatrix
		  acos(const doubleComplexSubMatrix& M);
const
doubleComplexMatrix
		  asin(const doubleComplexSubMatrix& M);
const
doubleComplexMatrix
		  atan(const doubleComplexSubMatrix& M);
const
doubleComplexMatrix
		  cosh(const doubleComplexSubMatrix& M);
const
doubleComplexMatrix
		  sinh(const doubleComplexSubMatrix& M);
const
doubleComplexMatrix
		  tanh(const doubleComplexSubMatrix& M);
const
doubleComplexMatrix
		 acosh(const doubleComplexSubMatrix& M);
const
doubleComplexMatrix
		 asinh(const doubleComplexSubMatrix& M);
const
doubleComplexMatrix
		 atanh(const doubleComplexSubMatrix& M);

// Operators
inline
doubleComplexSubVector
		doubleComplexSubMatrix::operator [](Offset i) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleComplexSubMatrix::operator [](Offset)", i, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubVector(handle(), offset() + (Stride)i*stride2(),
    extent1(), stride1()); }
inline
const
doubleComplexSubVector
		doubleComplexSubMatrix::operator [](Offset i) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleComplexSubMatrix::operator [](Offset) const", i, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubVector(handle(), offset() + (Stride)i*stride2(),
    extent1(), stride1()); }
inline
doubleComplexSubVector
		doubleComplexSubMatrix::operator ()(Offset i) {
  return operator [](i-1); }
inline
const
doubleComplexSubVector
		doubleComplexSubMatrix::operator ()(Offset i) const {
  return operator [](i-1); }
inline
doubleComplexSubScalar
		doubleComplexSubMatrix::operator ()(Offset j, Offset i) {
  doubleComplexSubMatrix&	M = *this; return M[i-1][j-1]; }
inline
const
doubleComplexSubScalar
		doubleComplexSubMatrix::operator ()(Offset j, Offset i) const {
  const
  doubleComplexSubMatrix&	M = *this; return M[i-1][j-1]; }
inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::swap(Offset i, Offset k) {
  doubleComplexSubMatrix	M = *this;
  doubleComplexSubVector	v = M[i];
  doubleComplexSubVector	w = M[k];
  v.swap(w); return *this; }
inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::swap_(Offset i, Offset k) {
  return swap(i-1, k-1); }
//inline
//const
//doubleComplexSubMatrix
//		doubleComplexSubMatrix::operator +(void) const {
//  return *this; }
std::istream&	operator >>(std::istream& s,       doubleComplexSubMatrix& M);
std::ostream&	operator <<(std::ostream& s, const doubleComplexSubMatrix& M);

class doubleComplexSubArray2: public doubleComplexSubMatrix {
  public:
  // Constructors
		doubleComplexSubArray2(void);
		doubleComplexSubArray2(double* p,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
		doubleComplexSubArray2(const doubleComplexSubArray2& M);
		~doubleComplexSubArray2(void);
  // Functions
  doubleComplexSubArray2&
		resize(void);
  doubleComplexSubArray2&		// resize from pointer
		resize(double* p,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleComplexSubArray2&		// resize from SubArray2
		resize(const doubleComplexSubArray2& M);
  doubleComplexSubArray2&		// resize from pointer
		resize_(double* p,
      Offset o, Extent n1, Stride s1,
		Extent n2, Stride s2);
  private:
  doubleComplexSubArray2&		// prevent resize from Handle
		resize(const doubleComplexHandle& h,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleComplexSubArray2&		// prevent resize from SubMatrix
		resize(const doubleComplexSubMatrix& M);
  public:
  doubleComplexSubArray2
		   sub(Offset i, Extent n2, Stride s2);
  const
  doubleComplexSubArray2
		   sub(Offset i, Extent n2, Stride s2) const;
  doubleComplexSubArray2
		   sub(Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1);
  const
  doubleComplexSubArray2
		   sub(Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  doubleComplexSubArray2
		   sub_(Offset i, Extent n2, Stride s2);
  const
  doubleComplexSubArray2
		   sub_(Offset i, Extent n2, Stride s2) const;
  doubleComplexSubArray2
		   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2);
  const
  doubleComplexSubArray2
		   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2) const;
  const
  doubleComplexSubArray3
	     subtensor(Extent l = 1) const;
  doubleComplexSubArray2
		     t(void);
  const
  doubleComplexSubArray2
		     t(void) const;

  doubleComplexSubArray1
		  diag(void);
  const
  doubleComplexSubArray1
		  diag(void) const;
  doubleComplexSubArray2
		     r1(void);
  const
  doubleComplexSubArray2
		     r1(void) const;
  doubleComplexSubArray2&
	       reverse1(void);
  doubleComplexSubArray2
		     r2(void);
  const
  doubleComplexSubArray2
		     r2(void) const;
  doubleComplexSubArray2&
	       reverse2(void);
  doubleComplexSubArray2
		     r(void);
  const
  doubleComplexSubArray2
		     r(void) const;
  doubleComplexSubArray2&
	       reverse(void);
  doubleComplexSubArray2
		  even(void);
  const
  doubleComplexSubArray2
		  even(void) const;
  doubleComplexSubArray2
		   odd(void);
  const
  doubleComplexSubArray2
		   odd(void) const;
  doubleComplexSubArray2
		  even_(void);
  const
  doubleComplexSubArray2
		  even_(void) const;
  doubleComplexSubArray2
		   odd_(void);
  const
  doubleComplexSubArray2
		   odd_(void) const;

  doubleSubArray2
		  real(void);
  const
  doubleSubArray2
		  real(void) const;
  doubleSubArray2
		  imag(void);
  const
  doubleSubArray2
		  imag(void) const;
  doubleComplexSubArray2&
		   dft(int sign = -1);	// complex to complex dft
  doubleComplexSubArray2&
		  swap(Offset i, Offset k);
  doubleComplexSubArray2&
		  swap_(Offset i, Offset k);
  doubleComplexSubArray2&
		  swap(doubleComplexSubMatrix& N);
  doubleComplexSubArray2&
		rotate(Stride n);
  doubleComplexSubArray2&
		 shift(Stride n,const doubleComplex& s
		  = doubleComplex((double)0, (double)0));
  doubleComplexSubArray2&
		  pack(const doubleComplex& s = doubleComplex((double)0, (double)0));
  doubleComplexSubArray2&
		  pack(const doubleComplexSubMatrix& N);
  doubleComplexSubArray2&
	       scatter(const offsetSubVector& x,
		       const doubleComplexSubVector& t);

  // Operators
  doubleComplexSubArray1
		operator [](Offset i);
  const
  doubleComplexSubArray1
		operator [](Offset i) const;
  doubleComplexSubArray1
		operator ()(Offset i);
  const
  doubleComplexSubArray1
		operator ()(Offset i) const;
  doubleComplexSubArray0
		operator ()(Offset j, Offset i);
  const
  doubleComplexSubArray0
		operator ()(Offset j, Offset i) const;

//  const
//  doubleComplexSubArray2
//		operator +(void) const;
  doubleComplexSubArray2&
		operator  =(const double& s);
  doubleComplexSubArray2&
		operator *=(const double& s);
  doubleComplexSubArray2&
		operator /=(const double& s);
  doubleComplexSubArray2&
		operator +=(const double& s);
  doubleComplexSubArray2&
		operator -=(const double& s);
  doubleComplexSubArray2&
		operator  =(const doubleComplex& s);
  doubleComplexSubArray2&
		operator *=(const doubleComplex& s);
  doubleComplexSubArray2&
		operator /=(const doubleComplex& s);
  doubleComplexSubArray2&
		operator +=(const doubleComplex& s);
  doubleComplexSubArray2&
		operator -=(const doubleComplex& s);
  doubleComplexSubArray2&
		operator  =(const doubleSubMatrix& N);
  doubleComplexSubArray2&
		operator *=(const doubleSubMatrix& N);
  doubleComplexSubArray2&
		operator /=(const doubleSubMatrix& N);
  doubleComplexSubArray2&
		operator +=(const doubleSubMatrix& N);
  doubleComplexSubArray2&
		operator -=(const doubleSubMatrix& N);
  doubleComplexSubArray2&
		operator  =(const doubleComplexSubMatrix& N);
  doubleComplexSubArray2&
		operator *=(const doubleComplexSubMatrix& N);
  doubleComplexSubArray2&
		operator /=(const doubleComplexSubMatrix& N);
  doubleComplexSubArray2&
		operator +=(const doubleComplexSubMatrix& N);
  doubleComplexSubArray2&
		operator -=(const doubleComplexSubMatrix& N);
  };

// Constructors
inline		doubleComplexSubArray2::doubleComplexSubArray2(void):
  doubleComplexSubMatrix() { }
inline		doubleComplexSubArray2::doubleComplexSubArray2(double* p,
    Offset o, Extent n2, Stride s2, Extent n1, Stride s1):
  doubleComplexSubMatrix(doubleComplexHandle(p), o, n2, s2, n1, s1) { }
inline		doubleComplexSubArray2::doubleComplexSubArray2(
    const doubleComplexSubArray2& M): doubleComplexSubMatrix(M) { }
inline		doubleComplexSubArray2::~doubleComplexSubArray2(void) { }

// Functions
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::resize(void) {
  doubleComplexSubMatrix::resize(); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::resize(double* p,
    Offset o, Extent n2, Stride s2,
	      Extent n1, Stride s1) {
  doubleComplexSubMatrix::resize(doubleComplexHandle(p),
    o, n2, s2, n1, s1); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::resize(
    const doubleComplexSubArray2& M) {
  doubleComplexSubMatrix::resize(M); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::resize_(double* p,
    Offset o, Extent n1, Stride s1,
	      Extent n2, Stride s2) { return resize(p, o, n2, s2, n1, s1); }

inline
doubleComplexSubArray2
		doubleComplexSubArray2::sub(
    Offset i, Extent n2, Stride s2) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleComplexSubArray2::sub(Offset, Extent, Stride)",
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)i*stride2(), n2, s2*stride2(), extent1(), stride1()); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray2::sub(
    Offset i, Extent n2, Stride s2) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleComplexSubArray2::sub(Offset, Extent, Stride) const",
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)i*stride2(), n2, s2*stride2(), extent1(), stride1()); }
inline
doubleComplexSubArray2
		doubleComplexSubArray2::sub(
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleComplexSubArray2::sub(\n"
    "Offset, Extent, Stride , Offset, Extent, Stride)",
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)i*stride2() + (Stride)j*stride1(),
    n2, s2*stride2(), n1, s1*stride1()); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray2::sub(
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleComplexSubArray2::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride) const",
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)i*stride2() + (Stride)j*stride1(),
    n2, s2*stride2(), n1, s1*stride1()); }

inline
doubleComplexSubArray2
		doubleComplexSubArray2::sub_(
    Offset i, Extent n2, Stride s2) { return sub(i-1, n2, s2); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray2::sub_(
    Offset i, Extent n2, Stride s2) const { return sub(i-1, n2, s2); }
inline
doubleComplexSubArray2
		doubleComplexSubArray2::sub_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2) {
  return sub(i-1, n2, s2, j-1, n1, s1); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray2::sub_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2) const {
  return sub(i-1, n2, s2, j-1, n1, s1); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray0::submatrix(Extent n, Extent m) const {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset(), m, (Stride)0, n, (Stride)0); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray1::submatrix(Extent m) const {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset(), m, (Stride)0, extent(), stride()); }
inline
doubleComplexSubArray2
		doubleComplexSubArray2::t(void) {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset(), extent1(), stride1(), extent2(), stride2()); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray2::t(void) const {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset(), extent1(), stride1(), extent2(), stride2()); }
inline
doubleComplexSubArray1
		doubleComplexSubArray2::diag(void) {
  return doubleComplexSubArray1((double*)(doubleComplexHandle&)handle(),
    offset(), (extent1() < extent2())? extent1(): extent2(),
    stride2() + stride1()); }
inline
const
doubleComplexSubArray1
		doubleComplexSubArray2::diag(void) const {
  return doubleComplexSubArray1((double*)(doubleComplexHandle&)handle(),
    offset(), (extent1() < extent2())? extent1(): extent2(),
    stride2() + stride1()
); }

inline
doubleComplexSubArray2
		doubleComplexSubArray2::r1(void) {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray2::r1(void) const {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
     extent2(), +stride2(),
     extent1(), -stride1()) ; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::reverse1(void) {
  doubleComplexSubMatrix::reverse1(); return *this; }
inline
doubleComplexSubArray2
		doubleComplexSubArray2::r2(void) {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray2::r2(void) const {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(),
    extent1(), +stride1()) ; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::reverse2(void) {
  doubleComplexSubMatrix::reverse2(); return *this; }
inline
doubleComplexSubArray2
		doubleComplexSubArray2::r(void) {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray2::r(void) const {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
     extent2(), -stride2(),
     extent1(), -stride1()) ; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::reverse(void) {
  doubleComplexSubMatrix::reverse(); return *this; }

inline
doubleComplexSubArray2
		doubleComplexSubArray2::even(void) {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset(), extent2(), stride2(), (extent1() + 1) >> 1, stride1() << 1); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray2::even(void) const {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset(), extent2(), stride2(), (extent1() + 1) >> 1, stride1() << 1); }
inline
doubleComplexSubArray2
		doubleComplexSubArray2::odd(void) {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
  offset() + stride1(), extent2(), stride2(), extent1() >> 1, stride1() << 1); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray2::odd(void) const {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
  offset() + stride1(), extent2(), stride2(), extent1() >> 1, stride1() << 1); }
inline
doubleComplexSubArray2
		doubleComplexSubArray2::even_(void) { return odd(); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray2::even_(void) const { return odd(); }
inline
doubleComplexSubArray2
		doubleComplexSubArray2::odd_(void) { return even(); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray2::odd_(void) const { return even(); }
inline
doubleSubArray2
		doubleComplexSubArray2::real(void) {
  return doubleSubArray2((double*)(doubleComplexHandle&)handle(),
    offset() << 1, extent2(), stride2(), extent1(), stride1() << 1); }
inline
const
doubleSubArray2
		doubleComplexSubArray2::real(void) const {
  return doubleSubArray2((double*)(doubleComplexHandle&)handle(),
    offset() << 1, extent2(), stride2(), extent1(), stride1() << 1); }
inline
doubleSubArray2
		doubleComplexSubArray2::imag(void) {
  return doubleSubArray2((double*)(doubleComplexHandle&)handle(),
    1 + (offset() << 1), extent2(), stride2(), extent1(), stride1() << 1); }
inline
const
doubleSubArray2
		doubleComplexSubArray2::imag(void) const {
  return doubleSubArray2((double*)(doubleComplexHandle&)handle(),
    1 + (offset() << 1), extent2(), stride2(), extent1(), stride1() << 1); }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::dft(int sign) {
  doubleComplexSubMatrix::dft(sign); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::swap(Offset i, Offset k) {
  doubleComplexSubMatrix::swap(i, k); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::swap(doubleComplexSubMatrix& N) {
  doubleComplexSubMatrix::swap(N); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::swap_(Offset i, Offset k) {
  return swap(i-1, k-1); }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::rotate(Stride n) {
  doubleComplexSubMatrix::rotate(n); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::shift(Stride n,const doubleComplex& s) {
  doubleComplexSubMatrix::shift(n,s); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::pack(const doubleComplex& s) {
  doubleComplexSubMatrix::pack(s); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::pack(
    const doubleComplexSubMatrix& N) {
  doubleComplexSubMatrix::pack(N); return *this; }

// doubleComplexSubMatrix function definitions
// which use doubleComplexSubArray2

inline
const
boolMatrix
		doubleComplexSubMatrix::eq(const double& s) const {
  return eq(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolMatrix
		doubleComplexSubMatrix::ne(const double& s) const {
  return ne(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolMatrix
		doubleComplexSubMatrix::eq(const doubleComplex& s) const {
  return eq(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolMatrix
		doubleComplexSubMatrix::ne(const doubleComplex& s) const {
  return ne(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolMatrix
		doubleSubMatrix::eq(const doubleComplexSubMatrix& N) const {
  return N.eq(*this); }
inline
const
boolMatrix
		doubleSubMatrix::ne(const doubleComplexSubMatrix& N) const {
  return N.ne(*this); }
inline
doubleComplexSubArray2&
	       doubleComplexSubArray2::scatter(
    const offsetSubVector& x, const doubleComplexSubVector& t) {
  doubleComplexSubMatrix::scatter(x, t); return *this; }

// Operators
inline
doubleComplexSubArray1
		doubleComplexSubArray2::operator [](Offset i) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleComplexSubArray2::operator [](Offset)", i, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray1((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)i*stride2(), extent1(), stride1()); }
inline
const
doubleComplexSubArray1
		doubleComplexSubArray2::operator [](Offset i) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleComplexSubArray2::operator [](Offset) const", i, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray1((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)i*stride2(), extent1(), stride1()); }
inline
doubleComplexSubArray1
		doubleComplexSubArray2::operator ()(Offset i) {
  return operator [](i-1); }
inline
const
doubleComplexSubArray1
		doubleComplexSubArray2::operator ()(Offset i) const {
  return operator [](i-1); }
inline
doubleComplexSubArray0
		doubleComplexSubArray2::operator ()(Offset j, Offset i) {
  doubleComplexSubArray2&	M = *this; return M[i-1][j-1]; }
inline
const
doubleComplexSubArray0
		doubleComplexSubArray2::operator ()(Offset j, Offset i) const {
  const
  doubleComplexSubArray2&	M = *this; return M[i-1][j-1]; }

//inline
//const
//doubleComplexSubArray2
//		doubleComplexSubArray2::operator +(void) const {
//  return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator  =(const double& s) {
  doubleComplexSubMatrix::operator  =(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator *=(const double& s) {
  doubleComplexSubMatrix::operator *=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator /=(const double& s) {
  doubleComplexSubMatrix::operator /=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator +=(const double& s) {
  doubleComplexSubMatrix::operator +=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator -=(const double& s) {
  doubleComplexSubMatrix::operator -=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); return *this; }


inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator  =(const doubleComplex& s) {
  doubleComplexSubMatrix::operator  =(doubleComplexSubArray2(
    (double*)&s.real(), (Offset)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator *=(const doubleComplex& s) {
  doubleComplexSubMatrix::operator *=(doubleComplexSubArray2(
    (double*)&s.real(), (Offset)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator /=(const doubleComplex& s) {
  doubleComplexSubMatrix::operator /=(doubleComplexSubArray2(
    (double*)&s.real(), (Offset)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator +=(const doubleComplex& s) {
  doubleComplexSubMatrix::operator +=(doubleComplexSubArray2(
    (double*)&s.real(), (Offset)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator -=(const doubleComplex& s) {
  doubleComplexSubMatrix::operator -=(doubleComplexSubArray2(
    (double*)&s.real(), (Offset)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator  =(
    const doubleSubMatrix& N) {
  doubleComplexSubMatrix::operator  =(N); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator *=(
    const doubleSubMatrix& N) {
  doubleComplexSubMatrix::operator *=(N); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator /=(
    const doubleSubMatrix& N) {
  doubleComplexSubMatrix::operator /=(N); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator +=(
    const doubleSubMatrix& N) {
  doubleComplexSubMatrix::operator +=(N); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator -=(
    const doubleSubMatrix& N) {
  doubleComplexSubMatrix::operator -=(N); return *this; }

inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator  =(
    const doubleComplexSubMatrix& N) {
  doubleComplexSubMatrix::operator  =(N); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator *=(
    const doubleComplexSubMatrix& N) {
  doubleComplexSubMatrix::operator *=(N); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator /=(
    const doubleComplexSubMatrix& N) {
  doubleComplexSubMatrix::operator /=(N); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator +=(
    const doubleComplexSubMatrix& N) {
  doubleComplexSubMatrix::operator +=(N); return *this; }
inline
doubleComplexSubArray2&
		doubleComplexSubArray2::operator -=(
    const doubleComplexSubMatrix& N) {
  doubleComplexSubMatrix::operator -=(N); return *this; }

inline
bool		doubleComplexSubMatrix::operator ==(const double& s) const {
  return operator ==(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		doubleComplexSubMatrix::operator !=(const double& s) const {
  return operator !=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		operator ==(
    const double& s, const doubleComplexSubMatrix& M) { return M == s; }
inline
bool		operator !=(
    const double& s, const doubleComplexSubMatrix& M) { return M != s; }
inline
bool		doubleComplexSubMatrix::operator ==(
    const doubleComplex& s) const {
  return operator ==(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		doubleComplexSubMatrix::operator !=(
    const doubleComplex& s) const {
  return operator !=(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1() , (Stride)0)); }
inline
bool		operator ==(const doubleComplex& s,
    const doubleComplexSubMatrix& M) { return M == s; }
inline
bool		operator !=(const doubleComplex& s,
    const doubleComplexSubMatrix& M) { return M != s; }
inline
bool		doubleSubMatrix::operator ==(
    const doubleComplexSubMatrix& N) const { return N == *this; }
inline
bool		doubleSubMatrix::operator !=(
    const doubleComplexSubMatrix& N) const { return N != *this; }
inline
bool		doubleSubMatrix::operator ==(const doubleComplex& s) const {
  return operator ==(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		doubleSubMatrix::operator !=(const doubleComplex& s) const {
  return operator !=(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }

inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::operator  =(const double& s) {
  return operator  =(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::operator *=(const double& s) {
  return operator *=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::operator /=(const double& s) {
  return operator /=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::operator +=(const double& s) {
  return operator +=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::operator -=(const double& s) {
  return operator -=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }


inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::operator  =(const doubleComplex& s) {
  return operator  =(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::operator *=(const doubleComplex& s) {
  return operator *=(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::operator /=(const doubleComplex& s) {
  return operator /=(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::operator +=(const doubleComplex& s) {
  return operator +=(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleComplexSubMatrix&
		doubleComplexSubMatrix::operator -=(const doubleComplex& s) {
  return operator -=(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }

class doubleComplexMatrix: public doubleComplexSubMatrix {
  public:
  // Constructors
		doubleComplexMatrix(void);
  explicit	doubleComplexMatrix(Extent m, Extent n = 1);
		doubleComplexMatrix(Extent m, Extent n,
      const doubleComplex& s);
  explicit	doubleComplexMatrix(const doubleSubMatrix& M);
		doubleComplexMatrix(const doubleSubMatrix& M,
				     const doubleSubMatrix& L);
		doubleComplexMatrix(const doubleComplexSubMatrix& M);
		doubleComplexMatrix(const doubleComplexMatrix& M);
		~doubleComplexMatrix(void);
  // Functions
  doubleComplexMatrix&
		resize(void);
  doubleComplexMatrix&
		resize(Extent m, Extent n = 1);
  doubleComplexMatrix&
		resize_(Extent n, Extent m);
  doubleComplexMatrix&
		resize(Extent m, Extent n,
      const doubleComplex& s);
  doubleComplexMatrix&
		resize_(Extent n, Extent m,
      const doubleComplex& s);
  doubleComplexMatrix&
		resize(const doubleSubMatrix& M);
  doubleComplexMatrix&
		resize(const doubleSubMatrix& M,
		       const doubleSubMatrix& L);
  doubleComplexMatrix&
		resize(const doubleComplexSubMatrix& M);
private:
  doubleComplexMatrix&
		resize(const doubleComplexHandle& h,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleComplexMatrix&
		free(void) { doubleComplexSubMatrix::free(); return *this; }
public:
  // Operators
  doubleComplexMatrix&
		operator  =(const double& s);
  doubleComplexMatrix&
		operator  =(const doubleComplex& s);
  doubleComplexMatrix&
		operator  =(const doubleSubMatrix& N);
  doubleComplexMatrix&
		operator  =(const doubleComplexSubMatrix& N);
  doubleComplexMatrix&
		operator  =(const doubleComplexMatrix& N);
  };

// Constructors
inline		doubleComplexMatrix::doubleComplexMatrix(void):
  doubleComplexSubMatrix() { }
inline		doubleComplexMatrix::doubleComplexMatrix(
    Extent m, Extent n): doubleComplexSubMatrix(
    allocate(m, n), (Offset)0, m, (Stride)n, n,  (Stride)1) {
#ifdef SVMT_DEBUG_MODE
  doubleComplexSubMatrix::operator =(doubleComplex(doubleBad, doubleBad));
#endif // SVMT_DEBUG_MODE
  }
inline		doubleComplexMatrix::doubleComplexMatrix(
    Extent m, Extent n, const doubleComplex& s): doubleComplexSubMatrix(
    allocate(m, n), (Offset)0, m, (Stride)n, n, (Stride)1) {
  doubleComplexSubMatrix::operator  =(s); }
inline		doubleComplexMatrix::doubleComplexMatrix(
    const doubleSubMatrix& M): doubleComplexSubMatrix(
    allocate(M.extent2(), M.extent1()), (Offset)0,
    M.extent2(), (Stride)M.extent1(), M.extent1(), (Stride)1) {
  doubleComplexSubMatrix::operator  =(M); }
inline		doubleComplexMatrix::doubleComplexMatrix(
  const doubleSubMatrix& M, const doubleSubMatrix& L):
  doubleComplexSubMatrix(allocate(M.extent2(), M.extent1()), (Offset)0,
    M.extent2(), (Stride)M.extent1(), M.extent1(), (Stride)1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexMatrix::doubleComplexMatrix(\n"
    "const doubleSubMatrix&, const doubleSubMatrix&)",
    M.extent2(), L.extent2(),
    M.extent1(), L.extent1());
#endif // SVMT_DEBUG_MODE
  real() = M; imag() = L; }
inline		doubleComplexMatrix::doubleComplexMatrix(
    const doubleComplexSubMatrix& M):
  doubleComplexSubMatrix(allocate(M.extent2(), M.extent1()), (Offset)0,
    M.extent2(), (Stride)M.extent1(), M.extent1(), (Stride)1) {
  doubleComplexSubMatrix::operator  =(M); }
inline		doubleComplexMatrix::doubleComplexMatrix(
    const doubleComplexMatrix& M):
  doubleComplexSubMatrix(allocate(M.extent2(), M.extent1()), (Offset)0,
    M.extent2(), (Stride)M.extent1(), M.extent1(), (Stride)1) {
  doubleComplexSubMatrix::operator  =(M); }
inline		doubleComplexMatrix::~doubleComplexMatrix(void) { free(); }

// Assignment Operators
inline
doubleComplexMatrix&	doubleComplexMatrix::operator  =(const double& s) {
  doubleComplexSubMatrix::operator =(s); return *this; }
inline
doubleComplexMatrix&	doubleComplexMatrix::operator  =(
    const doubleComplex& s) {
  doubleComplexSubMatrix::operator =(s); return *this; }
inline
doubleComplexMatrix&	doubleComplexMatrix::operator  =(
    const doubleSubMatrix& N) {
  doubleComplexSubMatrix::operator =(N); return *this; }
inline
doubleComplexMatrix&	doubleComplexMatrix::operator  =(
    const doubleComplexSubMatrix& N) {
  doubleComplexSubMatrix::operator =(N); return *this; }
inline
doubleComplexMatrix&	doubleComplexMatrix::operator  =(
    const doubleComplexMatrix& N) {
  doubleComplexSubMatrix::operator =(N); return *this; }

// Functions
inline
doubleComplexMatrix&
		doubleComplexMatrix::resize(void) { free();
  doubleComplexSubMatrix::resize(); return *this; }
inline
doubleComplexMatrix&
		doubleComplexMatrix::resize(Extent m, Extent n) { free();
  doubleComplexSubMatrix::resize(
    allocate(m, n), (Offset)0, m, (Stride)n, n, (Stride)1);
#ifdef SVMT_DEBUG_MODE
  *this = doubleComplex(doubleBad, doubleBad);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
doubleComplexMatrix&
		doubleComplexMatrix::resize_(Extent n, Extent m) {
  return resize(m, n); }
inline
doubleComplexMatrix&
		doubleComplexMatrix::resize(Extent m, Extent n,
    const doubleComplex& s) { return resize(m, n) = s; }
inline
doubleComplexMatrix&
		doubleComplexMatrix::resize_(Extent n, Extent m,
    const doubleComplex& s) { return resize(m, n, s); }
inline
doubleComplexMatrix&
		doubleComplexMatrix::resize(const doubleSubMatrix& M) {
  return resize(M.extent2(), M.extent1()) = M; }
inline
doubleComplexMatrix&
		doubleComplexMatrix::resize(
    const doubleSubMatrix& M, const doubleSubMatrix& L) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexMatrix::resize(\n"
    "const doubleSubMatrix&, const doubleSubMatrix&)",
    M.extent2(), L.extent2(),
    M.extent1(), L.extent1());
#endif // SVMT_DEBUG_MODE
  resize(M.extent2(), M.extent1()); real() = M; imag() = L; return *this; }
inline
doubleComplexMatrix&
		doubleComplexMatrix::resize(
    const doubleComplexSubMatrix& M) {
  return resize(M.extent2(), M.extent1()) = M; }

inline
const
doubleComplexMatrix
		doubleComplexSubVector::dot_(
    const doubleComplexSubMatrix& M) const { return M.dot(*this); }
inline
const
doubleComplexMatrix
		doubleComplexSubMatrix::dot_(
    const doubleComplexSubMatrix& M) const { return M.dot(*this); }
inline
const
doubleComplexVector
		doubleComplexSubMatrix::dot_(
    const doubleComplexSubVector& v) const { return v.dot(*this); }
inline
const
doubleComplexMatrix
		doubleComplexSubVector::dot_(
    const doubleSubMatrix& M) const { return M.dot(*this); }
inline
const
doubleComplexMatrix
		doubleComplexSubMatrix::dot_(
    const doubleSubMatrix& M) const { return M.dot(*this); }
inline
const
doubleComplexVector
		doubleComplexSubMatrix::dot_(
    const doubleSubVector& v) const { return v.dot(*this); }
inline
const
doubleComplexMatrix
		doubleSubVector::dot_(
    const doubleComplexSubMatrix& M) const { return M.dot(*this); }
inline
const
doubleComplexMatrix
		doubleSubMatrix::dot_(
    const doubleComplexSubMatrix& M) const { return M.dot(*this); }
inline
const
doubleComplexVector
		doubleSubMatrix::dot_(
    const doubleComplexSubVector& v) const { return v.dot(*this); }

inline
const
doubleComplexMatrix	doubleComplexSubMatrix::pl_(
    const offsetSubVector& p, const doubleComplexSubMatrix& L) const {
  return up(L, p); }
inline
const
doubleComplexMatrix	doubleComplexSubMatrix::l_(
    const doubleComplexSubMatrix& L) const { return u(L); }
inline
const
doubleComplexMatrix	doubleComplexSubMatrix::pld_(
    const offsetSubVector& p, const doubleComplexSubMatrix& LD) const {
  return dup(LD, p); }
inline
const
doubleComplexMatrix	doubleComplexSubMatrix::ld_(
    const doubleComplexSubMatrix& LD) const { return du(LD); }
inline
const
doubleComplexMatrix	doubleComplexSubMatrix::u_(
    const doubleComplexSubMatrix& U) const { return l(U); }
inline
const
doubleComplexMatrix	doubleComplexSubMatrix::up_(
    const doubleComplexSubMatrix& U, const offsetSubVector& p) const {
  return pl(p, U); }
inline
const
doubleComplexMatrix	doubleComplexSubMatrix::du_(
    const doubleComplexSubMatrix& DU) const { return ld(DU); }
inline
const
doubleComplexMatrix	doubleComplexSubMatrix::dup_(
    const doubleComplexSubMatrix& DU, const offsetSubVector& p) const {
  return pld(p, DU); }

inline
const
doubleComplexMatrix
		doubleComplexSubVector::above(
    const doubleComplexSubVector& w) const {
  return submatrix().above(w.submatrix()); }
inline
const
doubleComplexMatrix
		doubleComplexSubVector::above(
    const doubleComplexSubMatrix& M) const { return submatrix().above(M); }
inline
const
doubleComplexMatrix
		doubleComplexSubMatrix::above(
    const doubleComplexSubVector& v) const { return above(v.submatrix()); }
inline
const
doubleComplexMatrix
		doubleComplexSubVector::aside_(
    const doubleComplexSubVector& w) const { return above(w); }
inline
const
doubleComplexMatrix
		doubleComplexSubVector::aside_(
    const doubleComplexSubMatrix& M) const { return above(M); }
inline
const
doubleComplexMatrix
		doubleComplexSubMatrix::above_(
    const doubleComplexSubMatrix& M) const { return aside(M); }
inline
const
doubleComplexMatrix
		doubleComplexSubMatrix::aside_(
    const doubleComplexSubMatrix& M) const { return above(M); }
inline
const
doubleComplexMatrix
		doubleComplexSubMatrix::aside_(
    const doubleComplexSubVector& v) const { return above(v); }
inline
const
doubleComplexMatrix
		doubleSubMatrix::operator *(
    const doubleComplexSubMatrix& N) const { return N*(*this); }
inline
const
doubleComplexMatrix
		doubleSubMatrix::operator +(
    const doubleComplexSubMatrix& N) const { return N + *this; }

inline
const
doubleComplexMatrix
		doubleComplexSubMatrix::operator *(const double& s) const {
  return operator *(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexMatrix
		doubleComplexSubMatrix::operator /(const double& s) const {
  return operator /(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexMatrix
		doubleComplexSubMatrix::operator +(const double& s) const {
  return operator +(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexMatrix
		doubleComplexSubMatrix::operator -(const double& s) const {
  return operator -(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexMatrix
		operator *(const double& s, const doubleComplexSubMatrix& M) {
  return doubleSubArray2((double*)&s, (Offset)0,
    M.extent2(), (Stride)0, M.extent1(), (Stride)0)*M; }
inline
const
doubleComplexMatrix
		operator /(const double& s, const doubleComplexSubMatrix& M) {
  return doubleSubArray2((double*)&s, (Offset)0,
    M.extent2(), (Stride)0, M.extent1(), (Stride)0)/M; }
inline
const
doubleComplexMatrix
		operator +(const double& s, const doubleComplexSubMatrix& M) {
  return M + doubleSubArray2((double*)&s, (Offset)0,
    M.extent2(), (Stride)0, M.extent1(), (Stride)0); }
inline
const
doubleComplexMatrix
		operator -(const double& s, const doubleComplexSubMatrix& M) {
  return doubleSubArray2((double*)&s, (Offset)0,
    M.extent2(), (Stride)0, M.extent1(), (Stride)0) - M; }

inline
const
doubleComplexMatrix
		doubleComplexSubMatrix::operator *(
    const doubleComplex& s) const {
  return operator *(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexMatrix
		doubleComplexSubMatrix::operator /(
    const doubleComplex& s) const {
  return operator /(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexMatrix
		doubleComplexSubMatrix::operator +(
    const doubleComplex& s) const {
  return operator +(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexMatrix
		doubleComplexSubMatrix::operator -(
    const doubleComplex& s) const {
  return operator -(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexMatrix
		doubleSubMatrix::operator *(const doubleComplex& s) const {
  return operator *(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexMatrix
		doubleSubMatrix::operator /(const doubleComplex& s) const {
  return operator /(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexMatrix
		doubleSubMatrix::operator +(const doubleComplex& s) const {
  return operator +(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexMatrix
		doubleSubMatrix::operator -(const doubleComplex& s) const {
  return operator -(doubleComplexSubArray2((double*)&s.real(), (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }

inline
const
doubleComplexMatrix
		operator *(const doubleComplex& s, const doubleSubMatrix& M) {
  return doubleComplexSubArray2((double*)&s.real(), (Offset)0,
    M.extent2(), (Stride)0, M.extent1(), (Stride)0)*M; }
inline
const
doubleComplexMatrix
		operator /(const doubleComplex& s, const doubleSubMatrix& M) {
  return doubleComplexSubArray2((double*)&s.real(), (Offset)0,
    M.extent2(), (Stride)0, M.extent1(), (Stride)0)/M; }
inline
const
doubleComplexMatrix
		operator +(const doubleComplex& s, const doubleSubMatrix& M) {
  return M + doubleComplexSubArray2((double*)&s.real(), (Offset)0,
    M.extent2(), (Stride)0, M.extent1(), (Stride)0); }
inline
const
doubleComplexMatrix
		operator -(const doubleComplex& s, const doubleSubMatrix& M) {
  return doubleComplexSubArray2((double*)&s.real(), (Offset)0,
    M.extent2(), (Stride)0, M.extent1(), (Stride)0) - M; }
inline
const
doubleComplexMatrix
		operator *(const doubleComplex& s,
    const doubleComplexSubMatrix& M) {
  return doubleComplexSubArray2((double*)&s.real(), (Offset)0,
    M.extent2(), (Stride)0, M.extent1(), (Stride)0)*M; }
inline
const
doubleComplexMatrix
		operator /(const doubleComplex& s,
    const doubleComplexSubMatrix& M) {
  return doubleComplexSubArray2((double*)&s.real(), (Offset)0,
    M.extent2(), (Stride)0, M.extent1(), (Stride)0)/M; }
inline
const
doubleComplexMatrix
		operator +(const doubleComplex& s,
    const doubleComplexSubMatrix& M) {
  return M + doubleComplexSubArray2((double*)&s.real(), (Offset)0,
    M.extent2(), (Stride)0, M.extent1(), (Stride)0); }
inline
const
doubleComplexMatrix
		operator -(const doubleComplex& s,
    const doubleComplexSubMatrix& M) {
  return doubleComplexSubArray2((double*)&s.real(), (Offset)0,
    M.extent2(), (Stride)0, M.extent1(), (Stride)0) - M; }


#endif /* _doubleComplexMatrix_h */

