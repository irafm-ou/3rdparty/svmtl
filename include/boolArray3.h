#ifndef boolArray3_h
#define boolArray3_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<boolTensor.h>


template<Extent l, Extent m = 1, Extent n = 1>
class boolArray3: public boolSubArray3 {
private:
  // Representation
  bool	A[l*m*n];
  // Functions			// prevent boolSubArray3 resize
  boolArray3<l, m, n>&
		resize(void);
  boolArray3<l, m, n>&
		resize(bool*, Offset, Extent, Stride, Extent, Stride, Extent, Stride);
  boolArray3<l, m, n>&
		resize_(bool*, Offset, Extent, Stride, Extent, Stride, Extent, Stride);
  boolArray3<l, m, n>&
		resize(const boolSubArray3&);
public:
  // Constructors
		boolArray3(void):
    boolSubArray3(A, 0, l, m*n, m, n, n, 1) { }
		boolArray3(const bool& s):
    boolSubArray3(A, 0, l, m*n, m, n, n, 1) {
    boolSubArray3::operator =(s); }

		boolArray3(const boolArray3<l, m, n>& T):
    boolSubArray3(A, 0, l, m*n, m, n, n, 1) {
    boolSubArray3::operator =(T); }
	       ~boolArray3(void) { }
  // Operators
  boolArray3<l, m, n>&
  operator =(const boolArray3<l, m, n>& T) {
    boolSubArray3::operator =(T);
    return *this; }
  };

#endif /* boolArray3_h */

