#ifndef _doubleComplexSquare_h
#define _doubleComplexSquare_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleSquare.h>
#include<doubleComplexMatrix.h>

class doubleComplexSubSquare: public doubleComplexSubMatrix {
  public:
  // Constructors
		doubleComplexSubSquare(void);
		doubleComplexSubSquare(
      const doubleComplexHandle& h, Offset o, Extent m, Stride s2, Stride s1);
		doubleComplexSubSquare(const doubleComplexSubSquare& M);
		~doubleComplexSubSquare(void);
  // Functions
private:
  static
  doubleComplexHandle			// OMITTED TO PREVENT INHERITANCE!
	      allocate(Extent m, Extent n);
  doubleComplexSubSquare&		// OMITTED TO PREVENT INHERITANCE!
		resize(const doubleComplexHandle& h,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleComplexSubSquare&		// OMITTED TO PREVENT INHERITANCE!
		resize(const doubleComplexSubMatrix& M);
public:
  static
  doubleComplexHandle
	      allocate(Extent m);
  doubleComplexSubSquare&
		  free(void);
  doubleComplexSubSquare&
		resize(void);
  doubleComplexSubSquare&
		resize(const doubleComplexHandle& h,
      Offset o, Extent m, Stride s2, Stride s1);
  doubleComplexSubSquare&
		resize(const doubleComplexSubSquare& M);
  doubleComplexSubSquare&
		resize_(const doubleComplexHandle& h,
      Offset o, Stride s1, Extent m, Stride s2);
  doubleComplexSubSquare
		     t(void);
  const
  doubleComplexSubSquare
		     t(void) const;
  doubleComplexSubSquare&
	     transpose(void);
  doubleComplexSubSquare
		     r1(void);
  const
  doubleComplexSubSquare
		     r1(void) const;
  doubleComplexSubSquare&
	       reverse1(void);
  doubleComplexSubSquare
		     r2(void);
  const
  doubleComplexSubSquare
		     r2(void) const;
  doubleComplexSubSquare&
	       reverse2(void);
  doubleComplexSubSquare
		     r(void);
  const
  doubleComplexSubSquare
		     r(void) const;
  doubleComplexSubSquare&
	       reverse(void);
  const
  offsetVector     lld(void);

  const
  doubleComplexSquare
		     i(double r = (double)0) const;
  doubleComplexSubSquare&
		invert(double r = (double)0);
  doubleSubSquare
		  real(void);
  const
  doubleSubSquare
		  real(void) const;
  doubleSubSquare
		  imag(void);
  const
  doubleSubSquare
		  imag(void) const;
  doubleComplexSubSquare&
		   dft(int sign = -1);	// complex to complex dft
  doubleComplexSubSquare&
		  swap(Offset i, Offset k);
  doubleComplexSubSquare&
		  swap(doubleComplexSubSquare& N);
  doubleComplexSubSquare&
		  swap_(Offset i, Offset k);
  doubleComplexSubSquare&
		rotate(Stride n);
  doubleComplexSubSquare&
		 shift(Stride n,const doubleComplex& s
		  = doubleComplex((double)0, (double)0));

  const
  doubleComplexSquare
		   dot(const doubleSubSquare& N) const;
  const
  doubleComplexSquare
		   dot(const doubleComplexSubSquare& N) const;
  const
  doubleComplexSquare
		   dot_(const doubleSubSquare& N) const;
  const
  doubleComplexSquare
		   dot_(const doubleComplexSubSquare& N) const;
  const
  doubleComplexSquare
	   permutation(const offsetVector& p) const;
  doubleComplexSubSquare&
		  pack(const doubleComplex& s = doubleComplex((double)0, (double)0));
  doubleComplexSubSquare&
		  pack(const doubleComplexSubMatrix& N);
  const
  doubleComplexSquare
		   svd(void);

  const
  boolSquare
		    eq(const double& s) const;
  const
  boolSquare
		    ne(const double& s) const;
  const
  boolSquare
		    eq(const doubleComplex& s) const;
  const
  boolSquare
		    ne(const doubleComplex& s) const;
  const
  boolSquare
		    eq(const doubleSubSquare& N) const;
  const
  boolSquare
		    ne(const doubleSubSquare& N) const;
  const
  boolSquare
		    eq(const doubleComplexSubSquare& N) const;
  const
  boolSquare
		    ne(const doubleComplexSubSquare& N) const;

  doubleComplexSubSquare&
	       scatter(const offsetSubVector& x,
		       const doubleComplexSubVector& t);
  const
  doubleComplexSquare
		  kron(const doubleComplexSubSquare& N) const;
  const
  doubleComplexSquare
		  kron(const doubleSubSquare& N) const;
  const
  doubleComplexSquare
		 apply(const doubleComplex (*f)(const doubleComplex&)) const;
  const
  doubleComplexSquare
		 apply(      doubleComplex (*f)(const doubleComplex&)) const;
  const
  doubleComplexSquare
		 apply(      doubleComplex (*f)(      doubleComplex )) const;

  // Operators
  const
  doubleComplexSquare
		operator -(void) const;
//  const
//  doubleComplexSubSquare
//		operator +(void) const;
  const
  doubleComplexSquare
		operator *(const double& s) const;
  const
  doubleComplexSquare
		operator /(const double& s) const;
  const
  doubleComplexSquare
		operator +(const double& s) const;
  const
  doubleComplexSquare
		operator -(const double& s) const;
  const
  doubleComplexSquare
		operator *(const doubleComplex& s) const;
  const
  doubleComplexSquare
		operator /(const doubleComplex& s) const;
  const
  doubleComplexSquare
		operator +(const doubleComplex& s) const;
  const
  doubleComplexSquare
		operator -(const doubleComplex& s) const;
  const
  doubleComplexSquare
		operator *(const doubleSubSquare& N) const;
  const
  doubleComplexSquare
		operator /(const doubleSubSquare& N) const;
  const
  doubleComplexSquare
		operator +(const doubleSubSquare& N) const;
  const
  doubleComplexSquare
		operator -(const doubleSubSquare& N) const;
  const
  doubleComplexSquare
		operator *(const doubleComplexSubSquare& N) const;
  const
  doubleComplexSquare
		operator /(const doubleComplexSubSquare& N) const;
  const
  doubleComplexSquare
		operator +(const doubleComplexSubSquare& N) const;
  const
  doubleComplexSquare
		operator -(const doubleComplexSubSquare& N) const;

  doubleComplexSubSquare&
		operator  =(const double& s);
  doubleComplexSubSquare&
		operator *=(const double& s);
  doubleComplexSubSquare&
		operator /=(const double& s);
  doubleComplexSubSquare&
		operator +=(const double& s);
  doubleComplexSubSquare&
		operator -=(const double& s);

  doubleComplexSubSquare&
		operator  =(const doubleComplex& s);
  doubleComplexSubSquare&
		operator *=(const doubleComplex& s);
  doubleComplexSubSquare&
		operator /=(const doubleComplex& s);
  doubleComplexSubSquare&
		operator +=(const doubleComplex& s);
  doubleComplexSubSquare&
		operator -=(const doubleComplex& s);
  doubleComplexSubSquare&
		operator  =(const doubleSubSquare& N);
  doubleComplexSubSquare&
		operator *=(const doubleSubSquare& N);
  doubleComplexSubSquare&
		operator /=(const doubleSubSquare& N);
  doubleComplexSubSquare&
		operator +=(const doubleSubSquare& N);
  doubleComplexSubSquare&
		operator -=(const doubleSubSquare& N);
  doubleComplexSubSquare&
		operator  =(const doubleComplexSubSquare& N);
  doubleComplexSubSquare&
		operator *=(const doubleComplexSubSquare& N);
  doubleComplexSubSquare&
		operator /=(const doubleComplexSubSquare& N);
  doubleComplexSubSquare&
		operator +=(const doubleComplexSubSquare& N);
  doubleComplexSubSquare&
		operator -=(const doubleComplexSubSquare& N);
  friend class doubleComplexSquare;
  };

// Constructors
inline		doubleComplexSubSquare::doubleComplexSubSquare(void):
  doubleComplexSubMatrix() { }
inline		doubleComplexSubSquare::doubleComplexSubSquare(
    const doubleComplexHandle& h, Offset o, Extent m, Stride s2, Stride s1):
  doubleComplexSubMatrix(h, o, m, s2, m, s1) { }
inline		doubleComplexSubSquare::doubleComplexSubSquare(
    const doubleComplexSubSquare& M): doubleComplexSubMatrix(M) { }
inline		doubleComplexSubSquare::~doubleComplexSubSquare(void) { }

// Functions
inline
doubleComplexHandle
		doubleComplexSubSquare::allocate(Extent m) {
  return doubleComplexSubMatrix::allocate(m, m); }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::free(void) {
  doubleComplexSubMatrix::free(); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::resize(void) {
  doubleComplexSubMatrix::resize(); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::resize(
    const doubleComplexHandle& h, Offset o, Extent m, Stride s2, Stride s1) {
  doubleComplexSubMatrix::resize(h, o, m, s2, m, s1); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::resize(
    const doubleComplexSubSquare& M) {
 doubleComplexSubMatrix::resize(M); return *this; } 
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::resize_(
    const doubleComplexHandle& h, Offset o, Stride s1, Extent m, Stride s2) {
  return resize(h, o, m, s2, s1); }

inline
doubleComplexSubSquare
		doubleComplexSubMatrix::subsquare(
    Offset i, Extent m, Stride s2, Offset j, Stride s1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleComplexSubMatrix::subsquare(\n"
    "Offset, Extent, Stride, Offset, Stride)",
    i, m, s2, extent2(),
    j, m, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubSquare(handle(),
    offset() + (Stride)i*stride2()
	     + (Stride)j*stride1(), m, s2*stride2(), s1*stride1()); }
inline
const
doubleComplexSubSquare
		doubleComplexSubMatrix::subsquare(
    Offset i, Extent m, Stride s2, Offset j, Stride s1) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleComplexSubMatrix::subsquare(\n"
    "Offset, Extent, Stride, Offset, Stride) const",
    i, m, s2, extent2(),
    j, m, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubSquare(handle(),
    offset() + (Stride)i*stride2()
	     + (Stride)j*stride1(), m, s2*stride2(), s1*stride1()); }
inline
doubleComplexSubSquare
		doubleComplexSubMatrix::subsquare_(
    Offset i, Extent m, Stride s2) { return subsquare(i-1, m, s2); }
inline
const
doubleComplexSubSquare
		doubleComplexSubMatrix::subsquare_(
    Offset i, Extent m, Stride s2) const { return subsquare(i-1, m, s2); }
inline
doubleComplexSubSquare
		doubleComplexSubMatrix::subsquare_(
    Offset j, Stride s1, Offset i, Extent m, Stride s2) {
  return subsquare(i, m, s2, j, s1); }
inline
const
doubleComplexSubSquare
		doubleComplexSubMatrix::subsquare_(
    Offset j, Stride s1, Offset i, Extent m, Stride s2) const {
  return subsquare(i, m, s2, j, s1); }

inline
doubleComplexSubSquare
		doubleComplexSubSquare::t(void) {
  return doubleComplexSubSquare(
    handle(), offset(), extent2(), stride1(), stride2()); }
inline
const
doubleComplexSubSquare
		doubleComplexSubSquare::t(void) const {
  return doubleComplexSubSquare(
    handle(), offset(), extent2(), stride1(), stride2()); }
inline
doubleComplexSubSquare
		doubleComplexSubSquare::r1(void) {
  return doubleComplexSubSquare(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent2(), +stride2(), -stride1()); }
inline
const
doubleComplexSubSquare
		doubleComplexSubSquare::r1(void) const {
  return doubleComplexSubSquare(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent2(), +stride2(), -stride1()); }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::reverse1(void) {
  doubleComplexSubMatrix::reverse1(); return *this; }
inline
doubleComplexSubSquare
		doubleComplexSubSquare::r2(void) {
  return doubleComplexSubSquare(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(), +stride1()); }
inline
const
doubleComplexSubSquare
		doubleComplexSubSquare::r2(void) const {
  return doubleComplexSubSquare(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(), +stride1()); }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::reverse2(void) {
  doubleComplexSubMatrix::reverse2(); return *this; }
inline
doubleComplexSubSquare
		doubleComplexSubSquare::r(void) {
  return doubleComplexSubSquare(handle(),
    offset() + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent2(), -stride2(), -stride1()); }
inline
const
doubleComplexSubSquare
		doubleComplexSubSquare::r(void) const {
  return doubleComplexSubSquare(handle(),
    offset() + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent2(), -stride2(), -stride1()); }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::reverse(void) {
  doubleComplexSubMatrix::reverse(); return *this; }

inline
doubleSubSquare
		doubleComplexSubSquare::real(void) {
  return doubleSubSquare(handle(),     (offset() << 1),
    extent2(), stride2(), stride1() << 1); }
inline
const
doubleSubSquare
		doubleComplexSubSquare::real(void) const {
  return doubleSubSquare(handle(),     (offset() << 1),
    extent2(), stride2(), stride1() << 1); }
inline
doubleSubSquare
		doubleComplexSubSquare::imag(void) {
  return doubleSubSquare(handle(), 1 + (offset() << 1),
    extent2(), stride2(), stride1() << 1); }
inline
const
doubleSubSquare
		doubleComplexSubSquare::imag(void) const {
  return doubleSubSquare(handle(), 1 + (offset() << 1),
    extent2(), stride2(), stride1() << 1); }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::dft(int sign) {
  doubleComplexSubMatrix::dft(sign); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::swap(Offset i, Offset k) {
  doubleComplexSubMatrix::swap(i, k); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::swap(doubleComplexSubSquare& N) {
  doubleComplexSubMatrix::swap(N); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::swap_(Offset i, Offset k) {
  return swap(i-1, k-1); }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::rotate(Stride n) {
  doubleComplexSubMatrix::rotate(n); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::shift(Stride n,const doubleComplex& s) {
  doubleComplexSubMatrix::shift(n,s); return *this; }

inline
doubleComplexSubSquare&
		doubleComplexSubSquare::pack(const doubleComplex& s) {
  doubleComplexSubMatrix::pack(s); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::pack(
    const doubleComplexSubMatrix& N) {
  doubleComplexSubMatrix::pack(N); return *this; }

inline
const
boolSquare	doubleComplexSubSquare::eq(const double& s) const {
  boolMatrix	L = doubleComplexSubMatrix::eq(s);
  return *(boolSquare*)&L; }
inline
const
boolSquare	doubleComplexSubSquare::ne(const double& s) const {
  boolMatrix	L = doubleComplexSubMatrix::ne(s);
  return *(boolSquare*)&L; }
inline
const
boolSquare	doubleComplexSubSquare::eq(const doubleComplex& s) const {
  boolMatrix	L = doubleComplexSubMatrix::eq(s);
  return *(boolSquare*)&L; }
inline
const
boolSquare	doubleComplexSubSquare::ne(const doubleComplex& s) const {
  boolMatrix	L = doubleComplexSubMatrix::ne(s);
  return *(boolSquare*)&L; }
inline
const
boolSquare	doubleSubSquare::eq(const doubleComplexSubSquare& N) const {
  return N.eq(*this); }
inline
const
boolSquare	doubleSubSquare::ne(const doubleComplexSubSquare& N) const {
  return N.ne(*this); }

inline
doubleComplexSubSquare&
		doubleComplexSubSquare::scatter(
    const offsetSubVector& x, const doubleComplexSubVector& t) {
  doubleComplexSubMatrix::scatter(x, t); return *this; }
// Operators
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::operator  =(const double& s) {
  doubleComplexSubMatrix::operator  =(s); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::operator *=(const double& s) {
  doubleComplexSubMatrix::operator *=(s); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::operator /=(const double& s) {
  doubleComplexSubMatrix::operator /=(s); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::operator +=(const double& s) {
  doubleComplexSubMatrix::operator +=(s); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::operator -=(const double& s) {
  doubleComplexSubMatrix::operator -=(s); return *this; }


inline
doubleComplexSubSquare&
		doubleComplexSubSquare::operator  =(const doubleComplex& s) {
  doubleComplexSubMatrix::operator  =(s); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::operator *=(const doubleComplex& s) {
  doubleComplexSubMatrix::operator *=(s); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::operator /=(const doubleComplex& s) {
  doubleComplexSubMatrix::operator /=(s); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::operator +=(const doubleComplex& s) {
  doubleComplexSubMatrix::operator +=(s); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::operator -=(const doubleComplex& s) {
  doubleComplexSubMatrix::operator -=(s); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::operator  =(
    const doubleComplexSubSquare& N) {
  doubleComplexSubMatrix::operator  =(N); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::operator *=(
    const doubleComplexSubSquare& N) {
  doubleComplexSubMatrix::operator *=(N); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::operator /=(
    const doubleComplexSubSquare& N) {
  doubleComplexSubMatrix::operator /=(N); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::operator +=(
    const doubleComplexSubSquare& N) {
  doubleComplexSubMatrix::operator +=(N); return *this; }
inline
doubleComplexSubSquare&
		doubleComplexSubSquare::operator -=(
    const doubleComplexSubSquare& N) {
  doubleComplexSubMatrix::operator -=(N); return *this; }

class doubleComplexSquare: public doubleComplexSubSquare {
  public:
  // Constructors
		doubleComplexSquare(void);
  explicit	doubleComplexSquare(Extent m);
		doubleComplexSquare(Extent m, const doubleComplex& s);
  explicit	doubleComplexSquare(const doubleSubSquare& M);
		doubleComplexSquare(const doubleSubSquare& M,
				     const doubleSubSquare& L);
		doubleComplexSquare(const doubleComplexSubSquare& M);
		doubleComplexSquare(const doubleComplexSquare& M);
		~doubleComplexSquare(void);
  // Functions
private:
  doubleComplexSquare&			// OMITTED TO PREVENT INHERITANCE
		resize(const doubleComplexHandle& h,
      Offset o, Extent m, Stride s2, Stride s1);
  doubleComplexSquare&
		free(void) { doubleComplexSubSquare::free(); return *this; }
public:
  doubleComplexSquare&
		resize(void);
  doubleComplexSquare&
		resize(Extent m);
  doubleComplexSquare&
		resize(Extent m, const doubleComplex& s);
  doubleComplexSquare&
		resize(const doubleSubSquare& M);
  doubleComplexSquare&
		resize(const doubleSubSquare& M,
		       const doubleSubSquare& L);
  doubleComplexSquare&
		resize(const doubleComplexSubSquare& M);
  // Operators
  doubleComplexSquare&
		operator  =(const double& s);
  doubleComplexSquare&
		operator  =(const doubleComplex& s);
  doubleComplexSquare&
		operator  =(const doubleSubSquare& N);
  doubleComplexSquare&
		operator  =(const doubleComplexSubSquare& N);
  doubleComplexSquare&
		operator  =(const doubleComplexSquare& N);
  };

// Constructors
inline		doubleComplexSquare::doubleComplexSquare(void):
  doubleComplexSubSquare() { }
inline		doubleComplexSquare::doubleComplexSquare(Extent m):
  doubleComplexSubSquare(allocate(m), (Offset)0, m, (Stride)m, (Stride)1) {
#ifdef SVMT_DEBUG_MODE
  doubleComplexSubSquare::operator =(doubleComplex(doubleBad, doubleBad));
#endif // SVMT_DEBUG_MODE
  }
inline		doubleComplexSquare::doubleComplexSquare(Extent m,
    const doubleComplex& s):
  doubleComplexSubSquare(allocate(m), (Offset)0, m, (Stride)m, (Stride)1) {
  doubleComplexSubSquare::operator  =(s); }
inline		doubleComplexSquare::doubleComplexSquare(
    const doubleSubSquare& M):
  doubleComplexSubSquare(allocate(M.extent2()), (Offset)0,
    M.extent2(), (Stride)M.extent1(), (Stride)1) {
  doubleComplexSubSquare::operator  =(M); }
inline		doubleComplexSquare::doubleComplexSquare(
  const doubleSubSquare& M, const doubleSubSquare& L):
  doubleComplexSubSquare(allocate(M.extent2(), M.extent1()), (Offset)0,
    M.extent2(), (Stride)M.extent1(), (Stride)1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSquare::doubleComplexSquare(\n"
    "const doubleSubSquare&, const doubleSubSquare&)",
    M.extent2(), L.extent2(),
    M.extent1(), L.extent1());
#endif // SVMT_DEBUG_MODE
  real() = M; imag() = L; }
inline		doubleComplexSquare::doubleComplexSquare(
    const doubleComplexSubSquare& M):
  doubleComplexSubSquare(allocate(M.extent2()), (Offset)0,
    M.extent2(), (Stride)M.extent1(), (Stride)1) {
  doubleComplexSubSquare::operator  =(M); }
inline		doubleComplexSquare::doubleComplexSquare(
    const doubleComplexSquare& M):
  doubleComplexSubSquare(allocate(M.extent2()), (Offset)0,
    M.extent2(), (Stride)M.extent1(), (Stride)1) {
  doubleComplexSubSquare::operator  =(M); }
inline		doubleComplexSquare::~doubleComplexSquare(void) { free(); }

// Assignment Operators
inline
doubleComplexSquare&	doubleComplexSquare::operator  =(const double& s) {
  doubleComplexSubSquare::operator =(s); return *this; }
inline
doubleComplexSquare&	doubleComplexSquare::operator  =(
    const doubleComplex& s) {
  doubleComplexSubSquare::operator =(s); return *this; }
inline
doubleComplexSquare&	doubleComplexSquare::operator  =(
    const doubleSubSquare& N) {
  doubleComplexSubSquare::operator =(N); return *this; }
inline
doubleComplexSquare&	doubleComplexSquare::operator  =(
    const doubleComplexSubSquare& N) {
  doubleComplexSubSquare::operator =(N); return *this; }
inline
doubleComplexSquare&	doubleComplexSquare::operator  =(
    const doubleComplexSquare& N) {
  doubleComplexSubSquare::operator =(N); return *this; }

// Functions
inline
doubleComplexSquare&
		doubleComplexSquare::resize(void) { free();
  doubleComplexSubSquare::resize(); return *this; }
inline
doubleComplexSquare&
		doubleComplexSquare::resize(Extent m) { free();
  doubleComplexSubSquare::resize(
    allocate(m), (Offset)0, m, (Stride)m, (Stride)1);
#ifdef SVMT_DEBUG_MODE
  *this = doubleComplex(doubleBad, doubleBad);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
doubleComplexSquare&
		doubleComplexSquare::resize(Extent m, const doubleComplex& s) {
  return resize(m) = s; }
inline
doubleComplexSquare&
		doubleComplexSquare::resize(const doubleSubSquare& M) {
  return resize(M.extent2()) = M; }
inline
doubleComplexSquare&
		doubleComplexSquare::resize(
    const doubleSubSquare& M, const doubleSubSquare& L) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_conformance(
    "doubleComplexSquare::resize(\n"
    "const doubleSubSquare&, const doubleSubSquare&)",
    M.extent2(), L.extent2(),
    M.extent1(), L.extent1());
#endif // SVMT_DEBUG_MODE
  resize(M.extent2()); real() = M; imag() = L; return *this; }
inline
doubleComplexSquare&
		doubleComplexSquare::resize(
    const doubleComplexSubSquare& M) {
  return resize(M.extent2()) = M; }

// Function definitions which require class doubleComplexSquare definitions.
inline
const
doubleComplexSquare
		doubleSubSquare::dot(
    const doubleComplexSubSquare& N) const {
  doubleComplexMatrix	L = doubleSubMatrix::dot(N);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleSubSquare::dot_(
    const doubleComplexSubSquare& N) const { return N.dot(*this); }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::dot(
    const doubleComplexSubSquare& N) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::dot(N);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::dot_(
    const doubleComplexSubSquare& N) const { return N.dot(*this); }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::dot(
    const doubleSubSquare& N) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::dot(N);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::dot_(
    const doubleSubSquare& N) const { return N.dot(*this); }
inline
const
doubleComplexSquare
		doubleComplexSubMatrix::square(void) const { return dot(); }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::svd(void) {
  doubleComplexMatrix	L = doubleComplexSubMatrix::svd();
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::permutation(
    const offsetVector& p) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::permutation(p);
  return *(doubleComplexSquare*)&L; }

inline
const
doubleComplexSquare
		doubleComplexSubSquare::kron(
    const doubleSubSquare& N) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::kron(N);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::kron(
    const doubleComplexSubSquare& N) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::kron(N);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleSubSquare::kron(
    const doubleComplexSubSquare& N) const {
  doubleComplexMatrix	L = doubleSubMatrix::kron(N);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::apply(
    const doubleComplex (*f)(const doubleComplex&)) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::apply(f);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::apply(
	  doubleComplex (*f)(const doubleComplex&)) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::apply(f);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::apply(
	  doubleComplex (*f)(      doubleComplex )) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::apply(f);
  return *(doubleComplexSquare*)&L; }

// Global function declarations
inline
const
doubleComplexSquare
		  conj(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =   conj((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		 iconj(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =  iconj((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		 polar(const doubleSubSquare& M,
		       const doubleSubSquare& N) {
  doubleComplexMatrix	L =  polar((doubleSubMatrix&)M,
				   (doubleSubMatrix&)N);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleSquare
		  norm(const doubleComplexSubSquare& M) {
  doubleMatrix		L =   norm((doubleComplexSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		   abs(const doubleComplexSubSquare& M) {
  return hypot(M.real(), M.imag()); }
inline
const
doubleSquare
		   arg(const doubleComplexSubSquare& M) {
  return atan2(M.imag(), M.real()); }

inline
const
doubleComplexSquare
		   log(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =    log((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		   exp(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =    exp((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		  sqrt(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =   sqrt((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		   cos(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =    cos((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		   sin(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =    sin((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		   tan(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =    tan((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		  acos(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =   acos((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		  asin(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =   asin((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		  atan(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =   atan((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }

inline
const
doubleComplexSquare
		  cosh(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =   cosh((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		  sinh(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =   sinh((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		  tanh(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =   tanh((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		 acosh(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =  acosh((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		 asinh(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =  asinh((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		 atanh(const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L =  atanh((doubleComplexSubMatrix&)M);
  return *(doubleComplexSquare*)&L; }

// Operator definitions which require class doubleComplexSquare definitions.
inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator -(void) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator -();
  return *(doubleComplexSquare*)&L; }
//inline
//const
//doubleComplexSubSquare
//		doubleComplexSubSquare::operator +(void) const {
//  return *this; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator *(const double& s) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator *(s);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator /(const double& s) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator /(s);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator +(const double& s) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator +(s);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator -(const double& s) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator -(s);
  return *(doubleComplexSquare*)&L; }

inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator *(
    const doubleComplex& s) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator *(s);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator /(
    const doubleComplex& s) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator /(s);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator +(
    const doubleComplex& s) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator +(s);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator -(
    const doubleComplex& s) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator -(s);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator *(
    const doubleSubSquare& N) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator *(N);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator /(
    const doubleSubSquare& N) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator /(N);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator +(
    const doubleSubSquare& N) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator +(N);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator -(
    const doubleSubSquare& N) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator -(N);
  return *(doubleComplexSquare*)&L; }

inline
const
doubleComplexSquare
		operator *(const doubleComplex& s, const doubleSubSquare& M) {
  doubleComplexMatrix	L = s*(doubleComplexSubMatrix&)M;
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		operator /(const doubleComplex& s, const doubleSubSquare& M) {
  doubleComplexMatrix	L = s/(doubleComplexSubMatrix&)M;
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		operator +(const doubleComplex& s, const doubleSubSquare& M) {
  doubleComplexMatrix	L = s + (doubleComplexSubMatrix&)M;
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		operator -(const doubleComplex& s, const doubleSubSquare& M) {
  doubleComplexMatrix	L = s - (doubleComplexSubMatrix&)M;
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		operator *(const doubleComplex& s,
    const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L = s*(doubleComplexSubMatrix&)M;
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		operator /(const doubleComplex& s,
    const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L = s/(doubleComplexSubMatrix&)M;
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		operator +(const doubleComplex& s,
    const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L = s + (doubleComplexSubMatrix&)M;
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		operator -(const doubleComplex& s,
    const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L = s - (doubleComplexSubMatrix&)M;
  return *(doubleComplexSquare*)&L; }

inline
const
doubleComplexSquare
		doubleSubSquare::operator *(
    const doubleComplex& s) const {
  doubleComplexMatrix	L = doubleSubMatrix::operator *(s);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleSubSquare::operator /(
    const doubleComplex& s) const {
  doubleComplexMatrix	L = doubleSubMatrix::operator /(s);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleSubSquare::operator +(
    const doubleComplex& s) const {
  doubleComplexMatrix	L = doubleSubMatrix::operator +(s);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleSubSquare::operator -(
    const doubleComplex& s) const {
  doubleComplexMatrix	L = doubleSubMatrix::operator -(s);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleSubSquare::operator *(
    const doubleComplexSubSquare& N) const {
  doubleComplexMatrix	L = doubleSubMatrix::operator *(N);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleSubSquare::operator /(
    const doubleComplexSubSquare& N) const {
  doubleComplexMatrix	L = doubleSubMatrix::operator /(N);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleSubSquare::operator +(
    const doubleComplexSubSquare& N) const {
  doubleComplexMatrix	L = doubleSubMatrix::operator +(N);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleSubSquare::operator -(
    const doubleComplexSubSquare& N) const {
  doubleComplexMatrix	L = doubleSubMatrix::operator -(N);
  return *(doubleComplexSquare*)&L; }

inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator *(
    const doubleComplexSubSquare& N) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator *(N);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator /(
    const doubleComplexSubSquare& N) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator /(N);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator +(
    const doubleComplexSubSquare& N) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator +(N);
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		doubleComplexSubSquare::operator -(
    const doubleComplexSubSquare& N) const {
  doubleComplexMatrix	L = doubleComplexSubMatrix::operator -(N);
  return *(doubleComplexSquare*)&L; }

inline
doubleComplexSquare
		operator *(const double& s, const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L = s*(doubleComplexSubMatrix&)M;
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		operator /(const double& s, const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L = s/(doubleComplexSubMatrix&)M;
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		operator +(const double& s, const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L = s + (doubleComplexSubMatrix&)M;
  return *(doubleComplexSquare*)&L; }
inline
const
doubleComplexSquare
		operator -(const double& s, const doubleComplexSubSquare& M) {
  doubleComplexMatrix	L = s - (doubleComplexSubMatrix&)M;
  return *(doubleComplexSquare*)&L; }

#endif /* _doubleComplexSquare_h */

