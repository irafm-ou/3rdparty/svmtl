#ifndef _doubleComplexHandle_h
#define _doubleComplexHandle_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleHandle.h>
#include<doubleComplex.h>

class doubleComplexHandle {
  private:
  // Representation
  double*	P;
  // Constructors
		doubleComplexHandle(double* p);
  public:
		doubleComplexHandle(const doubleComplexHandle& h);
		~doubleComplexHandle(void);
  // Operators
		operator double* (void);
		operator doubleHandle (void) const;
  private:
  doubleComplexHandle&
		operator  =(const doubleComplexHandle& h);
  public:
  // Functions
  bool		empty(void) const;
  doubleComplex		get(Offset o) const;
  doubleComplexHandle&
		put(Offset o, const double& s);
  doubleComplexHandle&
		put(Offset o, const doubleComplex& s);
  friend class doubleComplexSubArray0;
  friend class doubleComplexSubVector;	friend class doubleComplexVector;
  friend class doubleComplexSubArray1;
  friend class doubleComplexSubMatrix;	friend class doubleComplexMatrix;
  friend class doubleComplexSubArray2;
  friend class doubleComplexSubTensor;	friend class doubleComplexTensor;
  friend class doubleComplexSubArray3;
  };

// Constructors
inline		doubleComplexHandle::doubleComplexHandle(
  double* p): P(p) { }
inline		doubleComplexHandle::doubleComplexHandle(
  const doubleComplexHandle& h): P(h.P) { }
inline		doubleComplexHandle::~doubleComplexHandle(void) { }

// Operators
inline		doubleComplexHandle::operator double* (void) { return P; }
inline		doubleComplexHandle::operator doubleHandle (void) const {
  return doubleHandle(P); }
inline
doubleComplexHandle&
		doubleComplexHandle::operator  =(
  const doubleComplexHandle& h) { P = h.P; return *this; }

// Functions
inline
bool		doubleComplexHandle::empty(void) const { return 0 == P; }
inline
doubleComplex		doubleComplexHandle::get(Offset o) const {
  o <<= 1; return doubleComplex(P[o], P[1+o]); }

inline
doubleComplexHandle&
		doubleComplexHandle::put(Offset o, const double& s) {
  o <<= 1; P[o] = s; P[1+o] = (double)0; return *this; }
inline
doubleComplexHandle&
		doubleComplexHandle::put(Offset o, const doubleComplex& s) {
  o <<= 1; P[o] = s.real(); P[1+o] = s.imag(); return *this; }

#endif /* _doubleComplexHandle_h */

