#ifndef _doubleSquare_h
#define _doubleSquare_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<boolSquare.h>
#include<doubleMatrix.h>

class doubleSubSquare: public doubleSubMatrix {
  public:
  // Constructors
		doubleSubSquare(void);
		doubleSubSquare(
      const doubleHandle& h, Offset o, Extent m, Stride s2, Stride s1);
		doubleSubSquare(const doubleSubSquare& M);
		~doubleSubSquare(void);
  // Functions
private:
  static
  doubleHandle			// OMITTED TO PREVENT INHERITANCE!
	      allocate(Extent m, Extent n);
  doubleSubSquare&		// OMITTED TO PREVENT INHERITANCE!
		resize(const doubleHandle& h,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleSubSquare&		// OMITTED TO PREVENT INHERITANCE!
		resize(const doubleSubMatrix& M);
public:
  static
  doubleHandle
	      allocate(Extent m);
  doubleSubSquare&
		  free(void);
  doubleSubSquare&
		resize(void);
  doubleSubSquare&
		resize(const doubleHandle& h,
      Offset o, Extent m, Stride s2, Stride s1);
  doubleSubSquare&
		resize(const doubleSubSquare& M);
  doubleSubSquare&
		resize_(const doubleHandle& h,
      Offset o, Stride s1, Extent m, Stride s2);
  doubleSubSquare
		     t(void);
  const
  doubleSubSquare
		     t(void) const;
  doubleSubSquare&
	     transpose(void);
  doubleSubSquare
		     r1(void);
  const
  doubleSubSquare
		     r1(void) const;
  doubleSubSquare&
	       reverse1(void);
  doubleSubSquare
		     r2(void);
  const
  doubleSubSquare
		     r2(void) const;
  doubleSubSquare&
	       reverse2(void);
  doubleSubSquare
		     r(void);
  const
  doubleSubSquare
		     r(void) const;
  doubleSubSquare&
	       reverse(void);
  const
  offsetVector     lld(void);

  const
  doubleSquare
		     i(double r = (double)0) const;
  doubleSubSquare&
		invert(double r = (double)0);
  doubleSubSquare&
		  rdft(int sign = -1);	// complex to real    dft
  doubleSubSquare&
		  cdft(int sign = -1);	// real    to complex dft
  doubleSubSquare&
		  ramp(void);
  doubleSubSquare&
		  swap(Offset i, Offset k);
  doubleSubSquare&
		  swap(doubleSubSquare& N);
  doubleSubSquare&
		  swap_(Offset i, Offset k);
  doubleSubSquare&
		rotate(Stride n);
  doubleSubSquare&
		 shift(Stride n,const double& s
		  = (double)0);

  const
  doubleSquare
		   dot(const doubleSubSquare& N) const;
  const
  doubleComplexSquare
		   dot(const doubleComplexSubSquare& N) const;
  const
  doubleSquare
		   dot_(const doubleSubSquare& N) const;
  const
  doubleComplexSquare
		   dot_(const doubleComplexSubSquare& N) const;
  const
  doubleSquare
	   permutation(const offsetVector& p) const;
  doubleSubSquare&
		  pack(const double& s = (double)0);
  doubleSubSquare&
		  pack(const doubleSubMatrix& N);
  const
  doubleSquare
		   svd(void);

  const
  boolSquare
		    lt(const double& s) const;
  const
  boolSquare
		    le(const double& s) const;
  const
  boolSquare
		    gt(const double& s) const;
  const
  boolSquare
		    ge(const double& s) const;
  const
  boolSquare
		    lt(const doubleSubSquare& N) const;
  const
  boolSquare
		    le(const doubleSubSquare& N) const;
  const
  boolSquare
		    gt(const doubleSubSquare& N) const;
  const
  boolSquare
		    ge(const doubleSubSquare& N) const;
  const
  boolSquare
		    eq(const double& s) const;
  const
  boolSquare
		    ne(const double& s) const;
  const
  boolSquare
		    eq(const doubleComplexSubSquare& N) const;
  const
  boolSquare
		    ne(const doubleComplexSubSquare& N) const;
  const
  boolSquare
		    eq(const doubleSubSquare& N) const;
  const
  boolSquare
		    ne(const doubleSubSquare& N) const;

  doubleSubSquare&
	       scatter(const offsetSubVector& x,
		       const doubleSubVector& t);
  const
  doubleSquare
		  kron(const doubleSubSquare& N) const;
  const
  doubleComplexSquare
		  kron(const doubleComplexSubSquare& N) const;
  const
  doubleSquare
		 apply(const double (*f)(const double&)) const;
  const
  doubleSquare
		 apply(      double (*f)(const double&)) const;
  const
  doubleSquare
		 apply(      double (*f)(      double )) const;

  // Operators
  const
  doubleSquare
		operator -(void) const;
//  const
//  doubleSubSquare
//		operator +(void) const;
  const
  doubleSquare
		operator *(const double& s) const;
  const
  doubleSquare
		operator /(const double& s) const;
  const
  doubleSquare
		operator +(const double& s) const;
  const
  doubleSquare
		operator -(const double& s) const;
  const
  doubleComplexSquare
		operator *(const doubleComplex& s) const;
  const
  doubleComplexSquare
		operator /(const doubleComplex& s) const;
  const
  doubleComplexSquare
		operator +(const doubleComplex& s) const;
  const
  doubleComplexSquare
		operator -(const doubleComplex& s) const;
  const
  doubleComplexSquare
		operator *(const doubleComplexSubSquare& N) const;
  const
  doubleComplexSquare
		operator /(const doubleComplexSubSquare& N) const;
  const
  doubleComplexSquare
		operator +(const doubleComplexSubSquare& N) const;
  const
  doubleComplexSquare
		operator -(const doubleComplexSubSquare& N) const;
  const
  doubleSquare
		operator *(const doubleSubSquare& N) const;
  const
  doubleSquare
		operator /(const doubleSubSquare& N) const;
  const
  doubleSquare
		operator +(const doubleSubSquare& N) const;
  const
  doubleSquare
		operator -(const doubleSubSquare& N) const;

  doubleSubSquare&
		operator  =(const double& s);
  doubleSubSquare&
		operator *=(const double& s);
  doubleSubSquare&
		operator /=(const double& s);
  doubleSubSquare&
		operator +=(const double& s);
  doubleSubSquare&
		operator -=(const double& s);

  doubleSubSquare&
		operator  =(const doubleSubSquare& N);
  doubleSubSquare&
		operator *=(const doubleSubSquare& N);
  doubleSubSquare&
		operator /=(const doubleSubSquare& N);
  doubleSubSquare&
		operator +=(const doubleSubSquare& N);
  doubleSubSquare&
		operator -=(const doubleSubSquare& N);
  friend class doubleSquare;
  };

// Constructors
inline		doubleSubSquare::doubleSubSquare(void):
  doubleSubMatrix() { }
inline		doubleSubSquare::doubleSubSquare(
    const doubleHandle& h, Offset o, Extent m, Stride s2, Stride s1):
  doubleSubMatrix(h, o, m, s2, m, s1) { }
inline		doubleSubSquare::doubleSubSquare(
    const doubleSubSquare& M): doubleSubMatrix(M) { }
inline		doubleSubSquare::~doubleSubSquare(void) { }

// Functions
inline
doubleHandle
		doubleSubSquare::allocate(Extent m) {
  return doubleSubMatrix::allocate(m, m); }
inline
doubleSubSquare&
		doubleSubSquare::free(void) {
  doubleSubMatrix::free(); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::resize(void) {
  doubleSubMatrix::resize(); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::resize(
    const doubleHandle& h, Offset o, Extent m, Stride s2, Stride s1) {
  doubleSubMatrix::resize(h, o, m, s2, m, s1); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::resize(
    const doubleSubSquare& M) {
 doubleSubMatrix::resize(M); return *this; } 
inline
doubleSubSquare&
		doubleSubSquare::resize_(
    const doubleHandle& h, Offset o, Stride s1, Extent m, Stride s2) {
  return resize(h, o, m, s2, s1); }

inline
doubleSubSquare
		doubleSubMatrix::subsquare(
    Offset i, Extent m, Stride s2, Offset j, Stride s1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleSubMatrix::subsquare(\n"
    "Offset, Extent, Stride, Offset, Stride)",
    i, m, s2, extent2(),
    j, m, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleSubSquare(handle(),
    offset() + (Stride)i*stride2()
	     + (Stride)j*stride1(), m, s2*stride2(), s1*stride1()); }
inline
const
doubleSubSquare
		doubleSubMatrix::subsquare(
    Offset i, Extent m, Stride s2, Offset j, Stride s1) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleSubMatrix::subsquare(\n"
    "Offset, Extent, Stride, Offset, Stride) const",
    i, m, s2, extent2(),
    j, m, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleSubSquare(handle(),
    offset() + (Stride)i*stride2()
	     + (Stride)j*stride1(), m, s2*stride2(), s1*stride1()); }
inline
doubleSubSquare
		doubleSubMatrix::subsquare_(
    Offset i, Extent m, Stride s2) { return subsquare(i-1, m, s2); }
inline
const
doubleSubSquare
		doubleSubMatrix::subsquare_(
    Offset i, Extent m, Stride s2) const { return subsquare(i-1, m, s2); }
inline
doubleSubSquare
		doubleSubMatrix::subsquare_(
    Offset j, Stride s1, Offset i, Extent m, Stride s2) {
  return subsquare(i, m, s2, j, s1); }
inline
const
doubleSubSquare
		doubleSubMatrix::subsquare_(
    Offset j, Stride s1, Offset i, Extent m, Stride s2) const {
  return subsquare(i, m, s2, j, s1); }

inline
doubleSubSquare
		doubleSubSquare::t(void) {
  return doubleSubSquare(
    handle(), offset(), extent2(), stride1(), stride2()); }
inline
const
doubleSubSquare
		doubleSubSquare::t(void) const {
  return doubleSubSquare(
    handle(), offset(), extent2(), stride1(), stride2()); }
inline
doubleSubSquare
		doubleSubSquare::r1(void) {
  return doubleSubSquare(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent2(), +stride2(), -stride1()); }
inline
const
doubleSubSquare
		doubleSubSquare::r1(void) const {
  return doubleSubSquare(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent2(), +stride2(), -stride1()); }
inline
doubleSubSquare&
		doubleSubSquare::reverse1(void) {
  doubleSubMatrix::reverse1(); return *this; }
inline
doubleSubSquare
		doubleSubSquare::r2(void) {
  return doubleSubSquare(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(), +stride1()); }
inline
const
doubleSubSquare
		doubleSubSquare::r2(void) const {
  return doubleSubSquare(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(), +stride1()); }
inline
doubleSubSquare&
		doubleSubSquare::reverse2(void) {
  doubleSubMatrix::reverse2(); return *this; }
inline
doubleSubSquare
		doubleSubSquare::r(void) {
  return doubleSubSquare(handle(),
    offset() + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent2(), -stride2(), -stride1()); }
inline
const
doubleSubSquare
		doubleSubSquare::r(void) const {
  return doubleSubSquare(handle(),
    offset() + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent2(), -stride2(), -stride1()); }
inline
doubleSubSquare&
		doubleSubSquare::reverse(void) {
  doubleSubMatrix::reverse(); return *this; }

inline
doubleSubSquare&
		doubleSubSquare::rdft(int sign) {
  doubleSubMatrix::rdft(sign); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::cdft(int sign) {
  doubleSubMatrix::cdft(sign); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::ramp(void) {
  doubleSubMatrix::ramp(); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::swap(Offset i, Offset k) {
  doubleSubMatrix::swap(i, k); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::swap(doubleSubSquare& N) {
  doubleSubMatrix::swap(N); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::swap_(Offset i, Offset k) {
  return swap(i-1, k-1); }
inline
doubleSubSquare&
		doubleSubSquare::rotate(Stride n) {
  doubleSubMatrix::rotate(n); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::shift(Stride n,const double& s) {
  doubleSubMatrix::shift(n,s); return *this; }

inline
doubleSubSquare&
		doubleSubSquare::pack(const double& s) {
  doubleSubMatrix::pack(s); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::pack(
    const doubleSubMatrix& N) {
  doubleSubMatrix::pack(N); return *this; }
inline
const
boolSquare	doubleSubSquare::lt(const double& s) const {
  boolMatrix	L = doubleSubMatrix::lt(s);
  return *(boolSquare*)&L; }
inline
const
boolSquare	doubleSubSquare::le(const double& s) const {
  boolMatrix	L = doubleSubMatrix::le(s);
  return *(boolSquare*)&L; }
inline
const
boolSquare	doubleSubSquare::gt(const double& s) const {
  boolMatrix	L = doubleSubMatrix::gt(s);
  return *(boolSquare*)&L; }
inline
const
boolSquare	doubleSubSquare::ge(const double& s) const {
  boolMatrix	L = doubleSubMatrix::ge(s);
  return *(boolSquare*)&L; }
inline
const
boolSquare	doubleSubSquare::lt(
    const doubleSubSquare& N) const {
  boolMatrix	L = doubleSubMatrix::lt((doubleSubMatrix&)N);
  return *(boolSquare*)&L; }
inline
const
boolSquare	doubleSubSquare::le(
    const doubleSubSquare& N) const {
  boolMatrix	L = doubleSubMatrix::le((doubleSubMatrix&)N);
  return *(boolSquare*)&L; }
inline
const
boolSquare	doubleSubSquare::gt(
    const doubleSubSquare& N) const { return N.lt(*this); }
inline
const
boolSquare	doubleSubSquare::ge(
    const doubleSubSquare& N) const { return N.le(*this); }

inline
const
boolSquare	doubleSubSquare::eq(const double& s) const {
  boolMatrix	L = doubleSubMatrix::eq(s);
  return *(boolSquare*)&L; }
inline
const
boolSquare	doubleSubSquare::ne(const double& s) const {
  boolMatrix	L = doubleSubMatrix::ne(s);
  return *(boolSquare*)&L; }

inline
doubleSubSquare&
		doubleSubSquare::scatter(
    const offsetSubVector& x, const doubleSubVector& t) {
  doubleSubMatrix::scatter(x, t); return *this; }
// Operators
inline
doubleSubSquare&
		doubleSubSquare::operator  =(const double& s) {
  doubleSubMatrix::operator  =(s); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::operator *=(const double& s) {
  doubleSubMatrix::operator *=(s); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::operator /=(const double& s) {
  doubleSubMatrix::operator /=(s); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::operator +=(const double& s) {
  doubleSubMatrix::operator +=(s); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::operator -=(const double& s) {
  doubleSubMatrix::operator -=(s); return *this; }

inline
doubleSubSquare&
		doubleSubSquare::operator  =(
    const doubleSubSquare& N) {
  doubleSubMatrix::operator  =(N); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::operator *=(
    const doubleSubSquare& N) {
  doubleSubMatrix::operator *=(N); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::operator /=(
    const doubleSubSquare& N) {
  doubleSubMatrix::operator /=(N); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::operator +=(
    const doubleSubSquare& N) {
  doubleSubMatrix::operator +=(N); return *this; }
inline
doubleSubSquare&
		doubleSubSquare::operator -=(
    const doubleSubSquare& N) {
  doubleSubMatrix::operator -=(N); return *this; }

class doubleSquare: public doubleSubSquare {
  public:
  // Constructors
		doubleSquare(void);
  explicit	doubleSquare(Extent m);
		doubleSquare(Extent m, const double& s);
		doubleSquare(Extent m, const double& s, const double& t);
		doubleSquare(const doubleSubSquare& M);
		doubleSquare(const doubleSquare& M);
		~doubleSquare(void);
  // Functions
private:
  doubleSquare&			// OMITTED TO PREVENT INHERITANCE
		resize(const doubleHandle& h,
      Offset o, Extent m, Stride s2, Stride s1);
  doubleSquare&
		free(void) { doubleSubSquare::free(); return *this; }
public:
  doubleSquare&
		resize(void);
  doubleSquare&
		resize(Extent m);
  doubleSquare&
		resize(Extent m, const double& s);
  doubleSquare&
		resize(Extent m, const double& s, const double& t);
  doubleSquare&
		resize(const doubleSubSquare& M);
  // Operators
  doubleSquare&
		operator  =(const double& s);
  doubleSquare&
		operator  =(const doubleSubSquare& N);
  doubleSquare&
		operator  =(const doubleSquare& N);
  };

// Constructors
inline		doubleSquare::doubleSquare(void):
  doubleSubSquare() { }
inline		doubleSquare::doubleSquare(Extent m):
  doubleSubSquare(allocate(m), (Offset)0, m, (Stride)m, (Stride)1) {
#ifdef SVMT_DEBUG_MODE
  doubleSubSquare::operator =(doubleBad);
#endif // SVMT_DEBUG_MODE
  }
inline		doubleSquare::doubleSquare(Extent m,
    const double& s):
  doubleSubSquare(allocate(m), (Offset)0, m, (Stride)m, (Stride)1) {
  doubleSubSquare::operator  =(s); }
inline		doubleSquare::doubleSquare(Extent m,
    const double& s, const double& t):
  doubleSubSquare(allocate(m), (Offset)0, m, (Stride)m, (Stride)1) {
  ramp();
  doubleSubSquare::operator *=(t);
  doubleSubSquare::operator +=(s); }
inline		doubleSquare::doubleSquare(
    const doubleSubSquare& M):
  doubleSubSquare(allocate(M.extent2()), (Offset)0,
    M.extent2(), (Stride)M.extent1(), (Stride)1) {
  doubleSubSquare::operator  =(M); }
inline		doubleSquare::doubleSquare(
    const doubleSquare& M):
  doubleSubSquare(allocate(M.extent2()), (Offset)0,
    M.extent2(), (Stride)M.extent1(), (Stride)1) {
  doubleSubSquare::operator  =(M); }
inline		doubleSquare::~doubleSquare(void) { free(); }

// Assignment Operators
inline
doubleSquare&	doubleSquare::operator  =(const double& s) {
  doubleSubSquare::operator =(s); return *this; }
inline
doubleSquare&	doubleSquare::operator  =(
    const doubleSubSquare& N) {
  doubleSubSquare::operator =(N); return *this; }
inline
doubleSquare&	doubleSquare::operator  =(
    const doubleSquare& N) {
  doubleSubSquare::operator =(N); return *this; }

// Functions
inline
doubleSquare&
		doubleSquare::resize(void) { free();
  doubleSubSquare::resize(); return *this; }
inline
doubleSquare&
		doubleSquare::resize(Extent m) { free();
  doubleSubSquare::resize(
    allocate(m), (Offset)0, m, (Stride)m, (Stride)1);
#ifdef SVMT_DEBUG_MODE
  *this = doubleBad;
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
doubleSquare&
		doubleSquare::resize(Extent m, const double& s) {
  return resize(m) = s; }
inline
doubleSquare&
		doubleSquare::resize(Extent m,
    const double& s, const double& t) {
  resize(m); ramp();
  doubleSubSquare::operator *=(t);
  doubleSubSquare::operator +=(s);
  return *this; }
inline
doubleSquare&
		doubleSquare::resize(
    const doubleSubSquare& M) {
  return resize(M.extent2()) = M; }

// Function definitions which require class doubleSquare definitions.
inline
const
doubleSquare
		doubleSubSquare::dot(
    const doubleSubSquare& N) const {
  doubleMatrix	L = doubleSubMatrix::dot(N);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		doubleSubSquare::dot_(
    const doubleSubSquare& N) const { return N.dot(*this); }
inline
const
doubleSquare
		doubleSubMatrix::square(void) const { return dot(); }
inline
const
doubleSquare
		doubleSubSquare::svd(void) {
  doubleMatrix	L = doubleSubMatrix::svd();
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		doubleSubSquare::permutation(
    const offsetVector& p) const {
  doubleMatrix	L = doubleSubMatrix::permutation(p);
  return *(doubleSquare*)&L; }

inline
const
doubleSquare
		doubleSubSquare::kron(
    const doubleSubSquare& N) const {
  doubleMatrix	L = doubleSubMatrix::kron(N);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		doubleSubSquare::apply(
    const double (*f)(const double&)) const {
  doubleMatrix	L = doubleSubMatrix::apply(f);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		doubleSubSquare::apply(
	  double (*f)(const double&)) const {
  doubleMatrix	L = doubleSubMatrix::apply(f);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		doubleSubSquare::apply(
	  double (*f)(      double )) const {
  doubleMatrix	L = doubleSubMatrix::apply(f);
  return *(doubleSquare*)&L; }

// Global function declarations
inline
const
doubleSquare
		   min(const doubleSubSquare& M,
		       const doubleSubSquare& N) {
  doubleMatrix	L =    min((doubleSubMatrix&)M,
				   (doubleSubMatrix&)N);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		   max(const doubleSubSquare& M,
		       const doubleSubSquare& N) {
  doubleMatrix	L =    max((doubleSubMatrix&)M,
				   (doubleSubMatrix&)N);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		   sgn(const doubleSubSquare& M) {
  doubleMatrix	L =    sgn((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		   abs(const doubleSubSquare& M) {
  doubleMatrix	L =    abs((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		 floor(const doubleSubSquare& M) {
  doubleMatrix	L =  floor((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		  ceil(const doubleSubSquare& M) {
  doubleMatrix	L =   ceil((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		 hypot(const doubleSubSquare& M,
		       const doubleSubSquare& N) {
  doubleMatrix	L =  hypot((doubleSubMatrix&)M,
				   (doubleSubMatrix&)N);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		 atan2(const doubleSubSquare& N,
		       const doubleSubSquare& M) {
  doubleMatrix	L =  atan2((doubleSubMatrix&)M,
				   (doubleSubMatrix&)N);
  return *(doubleSquare*)&L; }

inline
const
doubleSquare
		   log(const doubleSubSquare& M) {
  doubleMatrix	L =    log((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		   exp(const doubleSubSquare& M) {
  doubleMatrix	L =    exp((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		  sqrt(const doubleSubSquare& M) {
  doubleMatrix	L =   sqrt((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		   cos(const doubleSubSquare& M) {
  doubleMatrix	L =    cos((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		   sin(const doubleSubSquare& M) {
  doubleMatrix	L =    sin((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		   tan(const doubleSubSquare& M) {
  doubleMatrix	L =    tan((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		  acos(const doubleSubSquare& M) {
  doubleMatrix	L =   acos((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		  asin(const doubleSubSquare& M) {
  doubleMatrix	L =   asin((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		  atan(const doubleSubSquare& M) {
  doubleMatrix	L =   atan((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }

inline
const
doubleSquare
		  cosh(const doubleSubSquare& M) {
  doubleMatrix	L =   cosh((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		  sinh(const doubleSubSquare& M) {
  doubleMatrix	L =   sinh((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		  tanh(const doubleSubSquare& M) {
  doubleMatrix	L =   tanh((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		 acosh(const doubleSubSquare& M) {
  doubleMatrix	L =  acosh((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		 asinh(const doubleSubSquare& M) {
  doubleMatrix	L =  asinh((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		 atanh(const doubleSubSquare& M) {
  doubleMatrix	L =  atanh((doubleSubMatrix&)M);
  return *(doubleSquare*)&L; }

// Operator definitions which require class doubleSquare definitions.
inline
const
doubleSquare
		doubleSubSquare::operator -(void) const {
  doubleMatrix	L = doubleSubMatrix::operator -();
  return *(doubleSquare*)&L; }
//inline
//const
//doubleSubSquare
//		doubleSubSquare::operator +(void) const {
//  return *this; }
inline
const
doubleSquare
		doubleSubSquare::operator *(const double& s) const {
  doubleMatrix	L = doubleSubMatrix::operator *(s);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		doubleSubSquare::operator /(const double& s) const {
  doubleMatrix	L = doubleSubMatrix::operator /(s);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		doubleSubSquare::operator +(const double& s) const {
  doubleMatrix	L = doubleSubMatrix::operator +(s);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		doubleSubSquare::operator -(const double& s) const {
  doubleMatrix	L = doubleSubMatrix::operator -(s);
  return *(doubleSquare*)&L; }


inline
const
doubleSquare
		doubleSubSquare::operator *(
    const doubleSubSquare& N) const {
  doubleMatrix	L = doubleSubMatrix::operator *(N);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		doubleSubSquare::operator /(
    const doubleSubSquare& N) const {
  doubleMatrix	L = doubleSubMatrix::operator /(N);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		doubleSubSquare::operator +(
    const doubleSubSquare& N) const {
  doubleMatrix	L = doubleSubMatrix::operator +(N);
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		doubleSubSquare::operator -(
    const doubleSubSquare& N) const {
  doubleMatrix	L = doubleSubMatrix::operator -(N);
  return *(doubleSquare*)&L; }

inline
doubleSquare
		operator *(const double& s, const doubleSubSquare& M) {
  doubleMatrix	L = s*(doubleSubMatrix&)M;
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		operator /(const double& s, const doubleSubSquare& M) {
  doubleMatrix	L = s/(doubleSubMatrix&)M;
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		operator +(const double& s, const doubleSubSquare& M) {
  doubleMatrix	L = s + (doubleSubMatrix&)M;
  return *(doubleSquare*)&L; }
inline
const
doubleSquare
		operator -(const double& s, const doubleSubSquare& M) {
  doubleMatrix	L = s - (doubleSubMatrix&)M;
  return *(doubleSquare*)&L; }

#endif /* _doubleSquare_h */

