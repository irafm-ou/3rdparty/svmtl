#ifndef _doubleSignal_h
#define _doubleSignal_h 1
/*
The C++ Digital Signal Processing classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleTensor.h>


const
doubleVector
		doubleBlackman(Extent n);
const
doubleVector
		doubleChebyshev(Extent n, double ripple);
const
doubleVector
		doubleHamming(Extent n);
const
doubleVector
		doubleHanning(Extent n);
const
doubleVector
		doubleKaiser(Extent n, double beta);

#endif /* _doubleSignal_h */

