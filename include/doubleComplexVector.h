#ifndef _doubleComplexVector_h
#define _doubleComplexVector_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleVector.h>
#include<doubleComplexScalar.h>

class doubleComplexSubVector {
  private:
  static
  Extent	C;
  // Representation
  doubleComplexHandle
		H;
  Offset	O;
  Extent	N;
  Stride	S;
  public:
  // Constructors
		doubleComplexSubVector(void);
		doubleComplexSubVector(const doubleComplexHandle& h,
      Offset o, Extent n, Stride s);
		doubleComplexSubVector(const doubleComplexSubVector& v);
		~doubleComplexSubVector(void);
  // Functions
  static
  Extent&      columns(void);
  const
  doubleComplexHandle&
		handle(void) const;
  Offset	offset(void) const;
  Extent	extent(void) const;
  Extent	length(void) const;
  Stride	stride(void) const;
  bool		 empty(void) const;
  static
  doubleComplexHandle
	      allocate(Extent n);
  doubleComplexSubVector&
		  free(void);
  doubleComplexSubVector&
		resize(void);
  doubleComplexSubVector&
		resize(const doubleComplexHandle& h,
      Offset o, Extent n, Stride s);
  doubleComplexSubVector&
		resize(const doubleComplexSubVector& v);
  bool	      contains(Offset j, Extent n, Stride s) const;
  bool	      contains_(Offset j, Extent n, Stride s) const;
  doubleComplexSubVector
		   sub(Offset j, Extent n, Stride s);
  const
  doubleComplexSubVector
		   sub(Offset j, Extent n, Stride s) const;
  doubleComplexSubVector
		   sub_(Offset j, Extent n, Stride s);
  const
  doubleComplexSubVector
		   sub_(Offset j, Extent n, Stride s) const;
  const
  doubleComplexSubMatrix
	     submatrix(Extent m = 1) const;
  const
  doubleComplexSubTensor
	     subtensor(Extent m = 1, Extent l = 1) const;

  doubleComplexSubVector
		     r(void);
  const
  doubleComplexSubVector
		     r(void) const;
  doubleComplexSubVector&
	       reverse(void);
  doubleComplexSubVector
		  even(void);
  const
  doubleComplexSubVector
		  even(void) const;
  doubleComplexSubVector
		   odd(void);
  const
  doubleComplexSubVector
		   odd(void) const;
  doubleComplexSubVector
		  even_(void);
  const
  doubleComplexSubVector
		  even_(void) const;
  doubleComplexSubVector
		   odd_(void);
  const
  doubleComplexSubVector
		   odd_(void) const;

  doubleSubVector
		  real(void);
  const
  doubleSubVector
		  real(void) const;
  doubleSubVector
		  imag(void);
  const
  doubleSubVector
		  imag(void) const;
private:
  doubleComplexSubVector&
	     transpose(bool b = false);
  doubleComplexSubVector&
	     transpose(Extent p, Extent q);
  doubleComplexSubVector&
		twiddle(int sign = -1);
  // Slow Discrete Fourier Transform Complex from Real  y = w.sdftcr(x)
  doubleVector	sdftcr(const doubleSubVector& x) const;
  friend
  doubleSubVector&
  doubleSubVector::rcsdft(const doubleComplexSubVector&);
  friend
  doubleSubVector&
  doubleSubVector::rcfdft(const doubleComplexSubVector&);
  friend
  doubleSubVector&
  doubleSubVector::cdft(int);
  // Slow Discrete Fourier Transform Real from Complex  x = w.sdftrc(y)
  doubleVector	sdftrc(const doubleSubVector& y) const;
  friend
  doubleSubVector&
  doubleSubVector::crsdft(const doubleComplexSubVector&);
  friend
  doubleSubVector&
  doubleSubVector::crfdft(const doubleComplexSubVector&);
  friend
  doubleSubVector&
  doubleSubVector::rdft(int);
  friend class doubleSubMatrix; friend class doubleComplexSubMatrix;
  friend class doubleSubTensor; friend class doubleComplexSubTensor;
  doubleComplexSubVector&		// Slow Discrete Fourier Transform
		  sdft(const doubleComplexSubVector& w);
  doubleComplexSubVector&		// Fast Discrete Fourier Transform
		  fdft(const doubleComplexSubVector& w);
public:
  doubleComplexSubVector&
		   dft(int sign = -1);	// complex to complex dft
  doubleComplexSubVector&
		  pack(const doubleSubVector& x);
  friend class double1DCCDDFT; friend class double1DCDDFTC;
  friend class double1DCCIDFT; friend class double1DCIDFTC;
  friend class double1DRCDDFT; friend class double1DCDDFTR;
  friend class double1DRCIDFT; friend class double1DCIDFTR;
  friend class double1DCRDDFT; friend class double1DRDDFTC;
  friend class double1DCRIDFT; friend class double1DRIDFTC;

  doubleComplexSubVector&
		  swap(Offset j, Offset k);
  doubleComplexSubVector&
		  swap(doubleComplexSubVector& w);
  doubleComplexSubVector&
		  swap_(Offset j, Offset k);
  doubleComplexSubVector&
		rotate(Stride n);
  doubleComplexSubVector&
		 shift(Stride n,const doubleComplex& s
		  = doubleComplex((double)0, (double)0));
  doubleComplex
		   sum(void) const;
  doubleComplex
		   dot(const doubleSubVector& w) const;
  const
  doubleComplexVector
		   dot(const doubleSubMatrix& M) const;
  doubleComplex
		   dot(const doubleComplexSubVector& w) const;
  const
  doubleComplexVector
		   dot(const doubleComplexSubMatrix& M) const;
  doubleComplex
		   dot_(const doubleSubVector& w) const;
  const
  doubleComplexMatrix
		   dot_(const doubleSubMatrix& M) const;
  doubleComplex
		   dot_(const doubleComplexSubVector& w) const;
  const
  doubleComplexMatrix
		   dot_(const doubleComplexSubMatrix& M) const;
  doubleComplex		   dot(void) const;

  const
  doubleComplexVector
	   permutation(const offsetSubVector& p) const;
  const
  doubleComplexVector
		    pl(const offsetSubVector& p,
		       const doubleComplexSubMatrix& L) const;
  const
  doubleComplexVector
		     l(const doubleComplexSubMatrix& L) const;
  const
  doubleComplexVector
		    lp(const doubleComplexSubMatrix& L,
		       const offsetSubVector& p) const;
  const
  doubleComplexVector
		   pld(const offsetSubVector& p,
		       const doubleComplexSubMatrix& LD) const;
  const
  doubleComplexVector
		    ld(const doubleComplexSubMatrix& LD) const;
  const
  doubleComplexVector
		   ldp(const doubleComplexSubMatrix& LD,
		       const offsetSubVector& p) const;
  const
  doubleComplexVector
		    pu(const offsetSubVector& p,
		       const doubleComplexSubMatrix& U) const;
  const
  doubleComplexVector
		     u(const doubleComplexSubMatrix& U) const;
  const
  doubleComplexVector
		    up(const doubleComplexSubMatrix& U,
		       const offsetSubVector& p) const;
  const
  doubleComplexVector
		   pdu(const offsetSubVector& p,
		       const doubleComplexSubMatrix& DU) const;
  const
  doubleComplexVector
		    du(const doubleComplexSubMatrix& DU) const;
  const
  doubleComplexVector
		   dup(const doubleComplexSubMatrix& DU,
		       const offsetSubVector& p) const;

  const
  doubleComplexVector
		    pl_(const offsetSubVector& p,
		       const doubleComplexSubMatrix& L) const;
  const
  doubleComplexVector
		     l_(const doubleComplexSubMatrix& L) const;
  const
  doubleComplexVector
		    lp_(const doubleComplexSubMatrix& L,
		       const offsetSubVector& p) const;
  const
  doubleComplexVector
		   pld_(const offsetSubVector& p,
		       const doubleComplexSubMatrix& LD) const;
  const
  doubleComplexVector
		    ld_(const doubleComplexSubMatrix& LD) const;
  const
  doubleComplexVector
		   ldp_(const doubleComplexSubMatrix& LD,
		       const offsetSubVector& p) const;
  const
  doubleComplexVector
		    pu_(const offsetSubVector& p,
		       const doubleComplexSubMatrix& U) const;
  const
  doubleComplexVector
		     u_(const doubleComplexSubMatrix& U) const;
  const
  doubleComplexVector
		    up_(const doubleComplexSubMatrix& U,
		       const offsetSubVector& p) const;
  const
  doubleComplexVector
		   pdu_(const offsetSubVector& p,
		       const doubleComplexSubMatrix& DU) const;
  const
  doubleComplexVector
		    du_(const doubleComplexSubMatrix& DU) const;
  const
  doubleComplexVector
		   dup_(const doubleComplexSubMatrix& DU,
		       const offsetSubVector& p) const;
  const
  doubleComplexVector
		    pq(const offsetSubVector& p,
		       const doubleComplexSubMatrix& L) const;
  const
  doubleComplexVector
		     q(const doubleComplexSubMatrix& L) const;

  const
  boolVector
		    eq(const double& s) const;
  const
  boolVector
		    ne(const double& s) const;
  const
  boolVector
		    eq(const doubleComplex& s) const;
  const
  boolVector
		    ne(const doubleComplex& s) const;
  const
  boolVector
		    eq(const doubleSubVector& w) const;
  const
  boolVector
		    ne(const doubleSubVector& w) const;
  const
  boolVector
		    eq(const doubleComplexSubVector& w) const;
  const
  boolVector
		    ne(const doubleComplexSubVector& w) const;
  Extent	 zeros(void) const;
  const
  offsetVector
		 index(void) const;

  const
  doubleComplexVector
		gather(const offsetSubVector& x) const;
  doubleComplexSubVector&
	       scatter(const offsetSubVector& x,
		       const doubleComplexSubVector& t);
  const
  doubleComplexVector
		 aside(const doubleComplexSubVector& w) const;
  const
  doubleComplexMatrix
		 above(const doubleComplexSubMatrix& M) const;
  const
  doubleComplexMatrix
		 above(const doubleComplexSubVector& w) const;
  const
  doubleComplexVector
		 above_(const doubleComplexSubVector& w) const;
  const
  doubleComplexMatrix
		 aside_(const doubleComplexSubVector& w) const;
  const
  doubleComplexMatrix
		 aside_(const doubleComplexSubMatrix& M) const;
  const
  doubleComplexVector
		  kron(const doubleComplexSubVector& w) const;
  const
  doubleComplexMatrix
		  kron(const doubleComplexSubMatrix& M) const;
  const
  doubleComplexTensor
		  kron(const doubleComplexSubTensor& T) const;
  const
  doubleComplexVector
		  kron(const doubleSubVector& w) const;
  const
  doubleComplexMatrix
		  kron(const doubleSubMatrix& M) const;
  const
  doubleComplexTensor
		  kron(const doubleSubTensor& T) const;
  const
  doubleComplexVector
		 apply(const doubleComplex (*f)(const doubleComplex&)) const;
  const
  doubleComplexVector
		 apply(      doubleComplex (*f)(const doubleComplex&)) const;
  const
  doubleComplexVector
		 apply(      doubleComplex (*f)(      doubleComplex )) const;

  // Operators
  doubleComplexSubScalar
		operator [](Offset j);
  const
  doubleComplexSubScalar
		operator [](Offset j) const;
  doubleComplexSubScalar
		operator ()(Offset j);
  const
  doubleComplexSubScalar
		operator ()(Offset j) const;
  const
  doubleComplexVector
		operator -(void) const;
//  const
//  doubleComplexSubVector
//		operator +(void) const;
  const
  doubleComplexVector
		operator *(const double& s) const;
  const
  doubleComplexVector
		operator /(const double& s) const;
  const
  doubleComplexVector
		operator +(const double& s) const;
  const
  doubleComplexVector
		operator -(const double& s) const;
  const
  doubleComplexVector
		operator *(const doubleComplex& s) const;
  const
  doubleComplexVector
		operator /(const doubleComplex& s) const;
  const
  doubleComplexVector
		operator +(const doubleComplex& s) const;
  const
  doubleComplexVector
		operator -(const doubleComplex& s) const;
  const
  doubleComplexVector
		operator *(const doubleSubVector& w) const;
  const
  doubleComplexVector
		operator /(const doubleSubVector& w) const;
  const
  doubleComplexVector
		operator +(const doubleSubVector& w) const;
  const
  doubleComplexVector
		operator -(const doubleSubVector& w) const;
  const
  doubleComplexVector
		operator *(const doubleComplexSubVector& w) const;
  const
  doubleComplexVector
		operator /(const doubleComplexSubVector& w) const;
  const
  doubleComplexVector
		operator +(const doubleComplexSubVector& w) const;
  const
  doubleComplexVector
		operator -(const doubleComplexSubVector& w) const;

  bool		operator ==(const double& s) const;
  bool		operator !=(const double& s) const;
  bool		operator ==(const doubleComplex& s) const;
  bool		operator !=(const doubleComplex& s) const;
  bool		operator ==(const doubleSubVector& w) const;
  bool		operator !=(const doubleSubVector& w) const;
  bool		operator ==(const doubleComplexSubVector& w) const;
  bool		operator !=(const doubleComplexSubVector& w) const;
  doubleComplexSubVector&
		operator  =(const double& s);
  doubleComplexSubVector&
		operator *=(const double& s);
  doubleComplexSubVector&
		operator /=(const double& s);
  doubleComplexSubVector&
		operator +=(const double& s);
  doubleComplexSubVector&
		operator -=(const double& s);

  doubleComplexSubVector&
		operator  =(const doubleComplex& s);
  doubleComplexSubVector&
		operator *=(const doubleComplex& s);
  doubleComplexSubVector&
		operator /=(const doubleComplex& s);
  doubleComplexSubVector&
		operator +=(const doubleComplex& s);
  doubleComplexSubVector&
		operator -=(const doubleComplex& s);
  doubleComplexSubVector&
		operator  =(const doubleSubVector& w);
  doubleComplexSubVector&
		operator *=(const doubleSubVector& w);
  doubleComplexSubVector&
		operator /=(const doubleSubVector& w);
  doubleComplexSubVector&
		operator +=(const doubleSubVector& w);
  doubleComplexSubVector&
		operator -=(const doubleSubVector& w);
  doubleComplexSubVector&
		operator  =(const doubleComplexSubVector& w);
  doubleComplexSubVector&
		operator *=(const doubleComplexSubVector& w);
  doubleComplexSubVector&
		operator /=(const doubleComplexSubVector& w);
  doubleComplexSubVector&
		operator +=(const doubleComplexSubVector& w);
  doubleComplexSubVector&
		operator -=(const doubleComplexSubVector& w);
  friend class doubleComplexVector;
  };

// Constructors
inline		doubleComplexSubVector::doubleComplexSubVector(void):
  H((double*)0), O((Offset)0), N((Extent)0), S((Stride)0) { }
inline		doubleComplexSubVector::doubleComplexSubVector(
    const doubleComplexHandle& h, Offset o, Extent n, Stride s):
  H(h), O(o), N(n), S(s) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleComplexSubVector::doubleComplexSubVector(\n"
    "const doubleComplexHandle&, Offset, Extent, Stride)", o, n, s);
#endif // SVMT_DEBUG_MODE
  }
inline		doubleComplexSubVector::doubleComplexSubVector(
    const doubleComplexSubVector& v): H(v.H), O(v.O), N(v.N), S(v.S) { }
inline		doubleComplexSubVector::~doubleComplexSubVector(void) { }

// Functions
inline
Extent&		doubleComplexSubVector::columns(void) { return C; }
inline
const
doubleComplexHandle&
		doubleComplexSubVector::handle(void) const { return H; }
inline
Offset		doubleComplexSubVector::offset(void) const { return O; }
inline
Extent		doubleComplexSubVector::extent(void) const { return N; }
inline
Extent		doubleComplexSubVector::length(void) const { return N; }
inline
Stride		doubleComplexSubVector::stride(void) const { return S; }
inline
bool		doubleComplexSubVector::empty(void) const {
  return handle().empty() || 0 == extent(); }
inline
doubleComplexHandle
		doubleComplexSubVector::allocate(Extent n) {
  return doubleComplexHandle(new double[n << 1]); }
inline
doubleComplexSubVector&
		doubleComplexSubVector::free(void) {
  delete [] (double*)H; return *this; }
inline
doubleComplexSubVector&
		doubleComplexSubVector::resize(void) {
  H = doubleComplexHandle((double*)0);
  O = (Offset)0; N = (Extent)0; S = (Stride)0; return *this; }
inline
doubleComplexSubVector&
		doubleComplexSubVector::resize(const doubleComplexHandle& h,
    Offset o, Extent n, Stride s) { H = h; O = o; N = n; S = s;
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleComplexSubVector::resize(const doubleComplexHandle&,\n"
    "Offset, Extent, Stride)", o, n, s);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
doubleComplexSubVector&
		doubleComplexSubVector::resize(
  const doubleComplexSubVector& v) {
  return resize(v.handle(), v.offset(), v.extent(), v.stride()); }

inline
bool		doubleComplexSubVector::contains(
    Offset j, Extent n, Stride s) const {
  return boolSubVector::contains(j, n, s, extent()); }
inline
bool		doubleComplexSubVector::contains_(
    Offset j, Extent n, Stride s) const {
  return contains(j-1, n, s); }
inline
doubleComplexSubVector
		doubleComplexSubVector::sub(
    Offset j, Extent n, Stride s) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleComplexSubVector::sub(Offset, Extent, Stride)",
    j, n, s, extent());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubVector(handle(), offset() + (Stride)j*stride(),
    n, s*stride()); }
inline
const
doubleComplexSubVector
		doubleComplexSubVector::sub(
    Offset j, Extent n, Stride s) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleComplexSubVector::sub(Offset, Extent, Stride) const",
    j, n, s, extent());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubVector(handle(), offset() + (Stride)j*stride(),
    n, s*stride()); }
inline
doubleComplexSubVector
		doubleComplexSubVector::sub_(
    Offset j, Extent n, Stride s) { return sub(j-1, n, s); }
inline
const
doubleComplexSubVector
		doubleComplexSubVector::sub_(
    Offset j, Extent n, Stride s) const { return sub(j-1, n, s); }
inline
const
doubleComplexSubVector
		doubleComplexSubScalar::subvector(Extent n) const {
  return doubleComplexSubVector(handle(), offset(), n, (Stride)0); }
inline
doubleComplexSubVector
		doubleComplexSubVector::r(void) {
  return doubleComplexSubVector(handle(),
    offset() + (Stride)(extent() - 1)*stride(), extent(), -stride()); }
inline
const
doubleComplexSubVector
		doubleComplexSubVector::r(void) const {
  return doubleComplexSubVector(handle(),
    offset() + (Stride)(extent() - 1)*stride(), extent(), -stride()); }

inline
doubleComplexSubVector
		doubleComplexSubVector::even(void) {
  return doubleComplexSubVector(handle(), offset(),
    (extent() + 1) >> 1, stride() << 1); }
inline
const
doubleComplexSubVector
		doubleComplexSubVector::even(void) const {
  return doubleComplexSubVector(handle(), offset(),
    (extent() + 1) >> 1, stride() << 1); }
inline
doubleComplexSubVector
		doubleComplexSubVector::odd(void) {
  return doubleComplexSubVector(handle(), offset() + stride(),
    extent() >> 1, stride() << 1); }
inline
const
doubleComplexSubVector
		doubleComplexSubVector::odd(void) const {
  return doubleComplexSubVector(handle(), offset() + stride(),
    extent() >> 1, stride() << 1); }
inline
doubleComplexSubVector
		doubleComplexSubVector::even_(void) { return odd(); }
inline
const
doubleComplexSubVector
		doubleComplexSubVector::even_(void) const { return odd(); }
inline
doubleComplexSubVector
		doubleComplexSubVector::odd_(void) { return even(); }
inline
const
doubleComplexSubVector
		doubleComplexSubVector::odd_(void) const { return even(); }

inline
doubleSubVector
		doubleComplexSubVector::real(void) {
  return doubleSubVector(handle(),     (offset() << 1),
    extent(), stride() << 1); }
inline
const
doubleSubVector
		doubleComplexSubVector::real(void) const {
  return doubleSubVector(handle(),     (offset() << 1),
    extent(), stride() << 1); }
inline
doubleSubVector
		doubleComplexSubVector::imag(void) {
  return doubleSubVector(handle(), 1 + (offset() << 1),
    extent(), stride() << 1); }
inline
const
doubleSubVector
		doubleComplexSubVector::imag(void) const {
  return doubleSubVector(handle(), 1 + (offset() << 1),
    extent(), stride() << 1); }

inline
doubleSubVector&
  doubleSubVector::rcsdft(const doubleComplexSubVector& w) {
  // (in-place) Real to Complex Slow Discrete Fourier Transform  x.rcsdft(w)
  doubleVector	y = w.sdftcr(*this);
  return *this = y;
  }
inline
doubleSubVector&
  doubleSubVector::crsdft(const doubleComplexSubVector& w) {
  // (in-place) Complex to Real Slow Discrete Fourier Transform  y.crsdft(w)
  doubleVector	x = w.sdftrc(*this);
  return *this = x;
  }
inline
doubleComplex
		doubleSubVector::dot(const doubleComplexSubVector& w) const {
  return w.dot(*this); }
// Global function declarations
const
doubleComplexVector
		  conj(const doubleComplexSubVector& v);
const
doubleComplexVector
		 iconj(const doubleComplexSubVector& v);
const
doubleComplexVector
		 polar(const doubleSubVector& v,
		       const doubleSubVector& w);
const
doubleVector
		  norm(const doubleComplexSubVector& v);
inline
const
doubleVector
		   abs(const doubleComplexSubVector& v) {
  return hypot(v.real(), v.imag()); }
inline
const
doubleVector
		   arg(const doubleComplexSubVector& v) {
  return atan2(v.imag(), v.real()); }

const
doubleComplexVector
		   log(const doubleComplexSubVector& v);
const
doubleComplexVector
		   exp(const doubleComplexSubVector& v);
const
doubleComplexVector
		  sqrt(const doubleComplexSubVector& v);
const
doubleComplexVector
		   cos(const doubleComplexSubVector& v);
const
doubleComplexVector
		   sin(const doubleComplexSubVector& v);
const
doubleComplexVector
		   tan(const doubleComplexSubVector& v);
const
doubleComplexVector
		  acos(const doubleComplexSubVector& v);
const
doubleComplexVector
		  asin(const doubleComplexSubVector& v);
const
doubleComplexVector
		  atan(const doubleComplexSubVector& v);
const
doubleComplexVector
		  cosh(const doubleComplexSubVector& v);
const
doubleComplexVector
		  sinh(const doubleComplexSubVector& v);
const
doubleComplexVector
		  tanh(const doubleComplexSubVector& v);
const
doubleComplexVector
		 acosh(const doubleComplexSubVector& v);
const
doubleComplexVector
		 asinh(const doubleComplexSubVector& v);
const
doubleComplexVector
		 atanh(const doubleComplexSubVector& v);

// Operators
inline
doubleComplexSubScalar
		doubleComplexSubVector::operator [](Offset j) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleComplexSubVector::operator [](Offset)", j, extent());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubScalar(handle(), offset() + (Stride)j*stride()); }
inline
const
doubleComplexSubScalar
		doubleComplexSubVector::operator [](Offset j) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleComplexSubVector::operator [](Offset) const", j, extent());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubScalar(handle(), offset() + (Stride)j*stride()); }
inline
doubleComplexSubScalar
		doubleComplexSubVector::operator ()(Offset j) {
  return operator [](j-1); }
inline
const
doubleComplexSubScalar
		doubleComplexSubVector::operator ()(Offset j) const {
  return operator [](j-1); }
inline
doubleComplexSubVector&
		doubleComplexSubVector::swap(Offset j, Offset k) {
  doubleComplexSubVector&	v = *this;
  doubleComplex t = v[j]; v[j] = v[k]; v[k] = t; return v; }
inline
doubleComplexSubVector&
		doubleComplexSubVector::swap_(Offset j, Offset k) {
  return swap(j-1, k-1); }
//inline
//const
//doubleComplexSubVector
//		doubleComplexSubVector::operator +(void) const {
//  return *this; }
std::istream&	operator >>(std::istream& s,       doubleComplexSubVector& v);
std::ostream&	operator <<(std::ostream& s, const doubleComplexSubVector& v);

class doubleComplexSubArray1: public doubleComplexSubVector {
  public:
  // Constructors
		doubleComplexSubArray1(void);
		doubleComplexSubArray1(double* p, Offset o,
      Extent n, Stride s);
		doubleComplexSubArray1(const doubleComplexSubArray1& v);
		~doubleComplexSubArray1(void);
  // Functions
  doubleComplexSubArray1&
		resize(void);
  doubleComplexSubArray1&		// resize from pointer
		resize(double* p, Offset o, Extent n, Stride s);
  doubleComplexSubArray1&		// resize from SubArray1
		resize(const doubleComplexSubArray1& v);
  private:
  doubleComplexSubArray1&		// prevent resize from Handle
		resize(const doubleComplexHandle& h,
      Offset o, Extent n, Stride s);
  doubleComplexSubArray1&		// prevent resize from SubVector
		resize(const doubleComplexSubVector& v);
  public:
  doubleComplexSubArray1
		   sub(Offset j, Extent n, Stride s);
  const
  doubleComplexSubArray1
		   sub(Offset j, Extent n, Stride s) const;
  doubleComplexSubArray1
		   sub_(Offset j, Extent n, Stride s);
  const
  doubleComplexSubArray1
		   sub_(Offset j, Extent n, Stride s) const;
  const
  doubleComplexSubArray2
	     submatrix(Extent m = 1) const;
  const
  doubleComplexSubArray3
	     subtensor(Extent l = 1, Extent m = 1) const;
  doubleComplexSubArray1
		     r(void);
  const
  doubleComplexSubArray1
		     r(void) const;
  doubleComplexSubArray1&
	       reverse(void);

  doubleComplexSubArray1
		  even(void);
  const
  doubleComplexSubArray1
		  even(void) const;
  doubleComplexSubArray1
		   odd(void);
  const
  doubleComplexSubArray1
		   odd(void) const;
  doubleComplexSubArray1
		  even_(void);
  const
  doubleComplexSubArray1
		  even_(void) const;
  doubleComplexSubArray1
		   odd_(void);
  const
  doubleComplexSubArray1
		   odd_(void) const;
  doubleSubArray1
		  real(void);
  const
  doubleSubArray1
		  real(void) const;
  doubleSubArray1
		  imag(void);
  const
  doubleSubArray1
		  imag(void) const;
  doubleComplexSubArray1&
		   dft(int sign = -1);	// complex to complex dft
  doubleComplexSubArray1&
		  swap(Offset j, Offset k);
  doubleComplexSubArray1&
		  swap(doubleComplexSubVector& w);
  doubleComplexSubArray1&
		  swap_(Offset j, Offset k);
  doubleComplexSubArray1&
		rotate(Stride n);
  doubleComplexSubArray1&
		 shift(Stride n,const doubleComplex& s
		  = doubleComplex((double)0, (double)0));
  doubleComplexSubArray1&
	       scatter(const offsetSubVector& x,
		       const doubleComplexSubVector& t);

  // Operators
  doubleComplexSubArray0
		operator [](Offset j);
  const
  doubleComplexSubArray0
		operator [](Offset j) const;
  doubleComplexSubArray0
		operator ()(Offset j);
  const
  doubleComplexSubArray0
		operator ()(Offset j) const;
//  const
//  doubleComplexSubArray1
//		operator +(void) const;
  doubleComplexSubArray1&
		operator  =(const double& s);
  doubleComplexSubArray1&
		operator *=(const double& s);
  doubleComplexSubArray1&
		operator /=(const double& s);
  doubleComplexSubArray1&
		operator +=(const double& s);
  doubleComplexSubArray1&
		operator -=(const double& s);
  doubleComplexSubArray1&
		operator  =(const doubleComplex& s);
  doubleComplexSubArray1&
		operator *=(const doubleComplex& s);
  doubleComplexSubArray1&
		operator /=(const doubleComplex& s);
  doubleComplexSubArray1&
		operator +=(const doubleComplex& s);
  doubleComplexSubArray1&
		operator -=(const doubleComplex& s);
  doubleComplexSubArray1&
		operator  =(const doubleSubVector& w);
  doubleComplexSubArray1&
		operator *=(const doubleSubVector& w);
  doubleComplexSubArray1&
		operator /=(const doubleSubVector& w);
  doubleComplexSubArray1&
		operator +=(const doubleSubVector& w);
  doubleComplexSubArray1&
		operator -=(const doubleSubVector& w);
  doubleComplexSubArray1&
		operator  =(const doubleComplexSubVector& w);
  doubleComplexSubArray1&
		operator *=(const doubleComplexSubVector& w);
  doubleComplexSubArray1&
		operator /=(const doubleComplexSubVector& w);
  doubleComplexSubArray1&
		operator +=(const doubleComplexSubVector& w);
  doubleComplexSubArray1&
		operator -=(const doubleComplexSubVector& w);
  };

// Constructors
inline		doubleComplexSubArray1::doubleComplexSubArray1(void):
  doubleComplexSubVector() { }
inline		doubleComplexSubArray1::doubleComplexSubArray1(double* p,
    Offset o, Extent n, Stride s):
  doubleComplexSubVector(doubleComplexHandle(p), o, n, s) { }
inline		doubleComplexSubArray1::doubleComplexSubArray1(
    const doubleComplexSubArray1& v): doubleComplexSubVector(v) { }
inline		doubleComplexSubArray1::~doubleComplexSubArray1(void) { }

// Functions
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::resize(void) {
  doubleComplexSubVector::resize(); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::resize(double* p,
    Offset o, Extent n, Stride s) {
  doubleComplexSubVector::resize(doubleComplexHandle(p), o, n, s);
  return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::resize(
    const doubleComplexSubArray1& v) {
  doubleComplexSubVector::resize(v); return *this; }
inline
doubleComplexSubArray1
		doubleComplexSubArray1::sub(
    Offset j, Extent n, Stride s) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleComplexSubArray1::sub(Offset, Extent, Stride)",
    j, n, s, extent());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray1((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)j*stride(), n, s*stride()); }
inline
const
doubleComplexSubArray1
		doubleComplexSubArray1::sub(
    Offset j, Extent n, Stride s) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleComplexSubArray1::sub(Offset, Extent, Stride) const",
    j, n, s, extent());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray1((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)j*stride(), n, s*stride()); }
inline
doubleComplexSubArray1
		doubleComplexSubArray1::sub_(
    Offset j, Extent n, Stride s) { return sub(j-1, n, s); }
inline
const
doubleComplexSubArray1
		doubleComplexSubArray1::sub_(
    Offset j, Extent n, Stride s) const { return sub(j-1, n, s); }

inline
const
doubleComplexSubArray1
		doubleComplexSubArray0::subvector(Extent n) const {
  return doubleComplexSubArray1((double*)(doubleComplexHandle&)handle(),
    offset(), n, (Stride)0); }
inline
doubleComplexSubArray1
		doubleComplexSubArray1::r(void) {
  return doubleComplexSubArray1((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)(extent() - 1)*stride(), extent(), -stride()); }
inline
const
doubleComplexSubArray1
		doubleComplexSubArray1::r(void) const {
  return doubleComplexSubArray1((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)(extent() - 1)*stride(), extent(), -stride()); }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::reverse(void) {
  doubleComplexSubVector::reverse(); return *this; }
inline
doubleComplexSubArray1
		doubleComplexSubArray1::even(void) {
  return doubleComplexSubArray1((double*)(doubleComplexHandle&)handle(),
    offset(), (extent() + 1) >> 1, stride() << 1); }
inline
const
doubleComplexSubArray1
		doubleComplexSubArray1::even(void) const {
  return doubleComplexSubArray1((double*)(doubleComplexHandle&)handle(),
    offset(), (extent() + 1) >> 1, stride() << 1); }
inline
doubleComplexSubArray1
		doubleComplexSubArray1::odd(void) {
  return doubleComplexSubArray1((double*)(doubleComplexHandle&)handle(),
    offset() + stride(), extent() >> 1, stride() << 1); }
inline
const
doubleComplexSubArray1
		doubleComplexSubArray1::odd(void) const {
  return doubleComplexSubArray1((double*)(doubleComplexHandle&)handle(),
    offset() + stride(), extent() >> 1, stride() << 1); }
inline
doubleComplexSubArray1
		doubleComplexSubArray1::even_(void) { return odd(); }
inline
const
doubleComplexSubArray1
		doubleComplexSubArray1::even_(void) const { return odd(); }
inline
doubleComplexSubArray1
		doubleComplexSubArray1::odd_(void) { return even(); }
inline
const
doubleComplexSubArray1
		doubleComplexSubArray1::odd_(void) const { return even(); }

inline
doubleSubArray1
		doubleComplexSubArray1::real(void) {
  return doubleSubArray1((double*)(doubleComplexHandle&)handle(),
    offset() << 1, extent(), stride() << 1); }
inline
const
doubleSubArray1
		doubleComplexSubArray1::real(void) const {
  return doubleSubArray1((double*)(doubleComplexHandle&)handle(),
    offset() << 1, extent(), stride() << 1); }
inline
doubleSubArray1
		doubleComplexSubArray1::imag(void) {
  return doubleSubArray1((double*)(doubleComplexHandle&)handle(),
    1 + (offset() << 1), extent(), stride() << 1); }
inline
const
doubleSubArray1
		doubleComplexSubArray1::imag(void) const {
  return doubleSubArray1((double*)(doubleComplexHandle&)handle(),
    1 + (offset() << 1), extent(), stride() << 1); }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::dft(int sign) {
  doubleComplexSubVector::dft(sign); return *this; }

inline
doubleComplexSubArray1&
		doubleComplexSubArray1::swap(Offset j, Offset k) {
  doubleComplexSubVector::swap(j, k); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::swap(doubleComplexSubVector& w) {
  doubleComplexSubVector::swap(w); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::swap_(Offset j, Offset k) {
  return swap(j-1, k-1); }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::rotate(Stride n) {
  doubleComplexSubVector::rotate(n); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::shift(Stride n,const doubleComplex& s) {
  doubleComplexSubVector::shift(n,s); return *this; }

// doubleComplexSubVector function definitions
// which use doubleComplexSubArray1

inline
const
boolVector
		doubleComplexSubVector::eq(const double& s) const {
  return eq(doubleSubArray1((double*)&s, (Offset)0, extent(), (Stride)0)); }
inline
const
boolVector
		doubleComplexSubVector::ne(const double& s) const {
  return ne(doubleSubArray1((double*)&s, (Offset)0, extent(), (Stride)0)); }
inline
const
boolVector
		doubleComplexSubVector::eq(const doubleComplex& s) const {
  return eq(doubleComplexSubArray1((double*)&s.real(), (Offset)0,
      extent(), (Stride)0)); }
inline
const
boolVector
		doubleComplexSubVector::ne(const doubleComplex& s) const {
  return ne(doubleComplexSubArray1((double*)&s.real(), (Offset)0,
      extent(), (Stride)0)); }
inline
const
boolVector
		doubleSubVector::eq(const doubleComplexSubVector& w) const {
  return w.eq(*this); }
inline
const
boolVector
		doubleSubVector::ne(const doubleComplexSubVector& w) const {
  return w.ne(*this); }
inline
doubleComplexSubArray1&
	       doubleComplexSubArray1::scatter(
  const offsetSubVector& x, const doubleComplexSubVector& t) {
  doubleComplexSubVector::scatter(x, t); return *this; }

// Operators
inline
doubleComplexSubArray0
		doubleComplexSubArray1::operator [](Offset j) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleComplexSubArray1::operator [](Offset)", j, extent());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray0((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)j*stride()); }
inline
const
doubleComplexSubArray0
		doubleComplexSubArray1::operator [](Offset j) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleComplexSubArray1::operator [](Offset) const", j, extent());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray0((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)j*stride()); }
inline
doubleComplexSubArray0
		doubleComplexSubArray1::operator ()(Offset j) {
  return operator [](j-1); }
inline
const
doubleComplexSubArray0
		doubleComplexSubArray1::operator ()(Offset j) const {
  return operator [](j-1); }

//inline
//const
//doubleComplexSubArray1
//		doubleComplexSubArray1::operator +(void) const {
//  return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator  =(const double& s) {
  doubleComplexSubVector::operator  =(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator *=(const double& s) {
  doubleComplexSubVector::operator *=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator /=(const double& s) {
  doubleComplexSubVector::operator /=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator +=(const double& s) {
  doubleComplexSubVector::operator +=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator -=(const double& s) {
  doubleComplexSubVector::operator -=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); return *this; }


inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator  =(const doubleComplex& s) {
  doubleComplexSubVector::operator  =(doubleComplexSubArray1(
      (double*)&s.real(), (Offset)0, extent(), (Stride)0)); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator *=(const doubleComplex& s) {
  doubleComplexSubVector::operator *=(doubleComplexSubArray1(
      (double*)&s.real(), (Offset)0, extent(), (Stride)0)); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator /=(const doubleComplex& s) {
  doubleComplexSubVector::operator /=(doubleComplexSubArray1(
      (double*)&s.real(), (Offset)0, extent(), (Stride)0)); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator +=(const doubleComplex& s) {
  doubleComplexSubVector::operator +=(doubleComplexSubArray1(
      (double*)&s.real(), (Offset)0, extent(), (Stride)0)); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator -=(const doubleComplex& s) {
  doubleComplexSubVector::operator -=(doubleComplexSubArray1(
      (double*)&s.real(), (Offset)0, extent(), (Stride)0)); return *this; }

inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator  =(
  const doubleSubVector& w) {
  doubleComplexSubVector::operator  =(w); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator *=(
    const doubleSubVector& w) {
  doubleComplexSubVector::operator *=(w); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator /=(
    const doubleSubVector& w) {
  doubleComplexSubVector::operator /=(w); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator +=(
    const doubleSubVector& w) {
  doubleComplexSubVector::operator +=(w); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator -=(
    const doubleSubVector& w) {
  doubleComplexSubVector::operator -=(w); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator  =(
    const doubleComplexSubVector& w) {
  doubleComplexSubVector::operator  =(w); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator *=(
    const doubleComplexSubVector& w) {
  doubleComplexSubVector::operator *=(w); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator /=(
    const doubleComplexSubVector& w) {
  doubleComplexSubVector::operator /=(w); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator +=(
    const doubleComplexSubVector& w) {
  doubleComplexSubVector::operator +=(w); return *this; }
inline
doubleComplexSubArray1&
		doubleComplexSubArray1::operator -=(
    const doubleComplexSubVector& w) {
  doubleComplexSubVector::operator -=(w); return *this; }


inline
bool		doubleComplexSubVector::operator ==(const double& s) const {
  return operator ==(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
bool		doubleComplexSubVector::operator !=(const double& s) const {
  return operator !=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
bool		operator ==(const double& s,
    const doubleComplexSubVector& v) { return v == s; }
inline
bool		operator !=(const double& s,
    const doubleComplexSubVector& v) { return v != s; }
inline
bool		doubleComplexSubVector::operator ==(
    const doubleComplex& s) const {
  return operator ==(doubleComplexSubArray1((double*)&s.real(), (Offset)0,
      extent(), (Stride)0)); }
inline
bool		doubleComplexSubVector::operator !=(
    const doubleComplex& s) const {
  return operator !=(doubleComplexSubArray1((double*)&s.real(), (Offset)0,
      extent(), (Stride)0)); }
inline
bool		operator ==(const doubleComplex& s,
    const doubleComplexSubVector& v) { return v == s; }
inline
bool		operator !=(const doubleComplex& s,
    const doubleComplexSubVector& v) { return v != s; }
inline
bool		doubleSubVector::operator ==(
    const doubleComplexSubVector& w) const { return w == *this; }
inline
bool		doubleSubVector::operator !=(
    const doubleComplexSubVector& w) const { return w != *this; }
inline
bool		doubleSubVector::operator ==(const doubleComplex& s) const {
  return operator ==(doubleComplexSubArray1((double*)&s.real(), (Offset)0,
      extent(), (Stride)0)); }
inline
bool		doubleSubVector::operator !=(const doubleComplex& s) const {
  return operator !=(doubleComplexSubArray1((double*)&s.real(), (Offset)0,
      extent(), (Stride)0)); }

inline
doubleComplexSubVector&
		doubleComplexSubVector::operator  =(const double& s) {
  return operator  =(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
doubleComplexSubVector&
		doubleComplexSubVector::operator *=(const double& s) {
  return operator *=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
doubleComplexSubVector&
		doubleComplexSubVector::operator /=(const double& s) {
  return operator /=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
doubleComplexSubVector&
		doubleComplexSubVector::operator +=(const double& s) {
  return operator +=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
doubleComplexSubVector&
		doubleComplexSubVector::operator -=(const double& s) {
  return operator -=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }


inline
doubleComplexSubVector&
		doubleComplexSubVector::operator  =(const doubleComplex& s) {
  return operator  =(doubleComplexSubArray1((double*)&s.real(), (Offset)0,
      extent(), (Stride)0)); }
inline
doubleComplexSubVector&
		doubleComplexSubVector::operator *=(const doubleComplex& s) {
  return operator *=(doubleComplexSubArray1((double*)&s.real(), (Offset)0,
      extent(), (Stride)0)); }
inline
doubleComplexSubVector&
		doubleComplexSubVector::operator /=(const doubleComplex& s) {
  return operator /=(doubleComplexSubArray1((double*)&s.real(), (Offset)0,
      extent(), (Stride)0)); }
inline
doubleComplexSubVector&
		doubleComplexSubVector::operator +=(const doubleComplex& s) {
  return operator +=(doubleComplexSubArray1((double*)&s.real(), (Offset)0,
      extent(), (Stride)0)); }
inline
doubleComplexSubVector&
		doubleComplexSubVector::operator -=(const doubleComplex& s) {
  return operator -=(doubleComplexSubArray1((double*)&s.real(), (Offset)0,
      extent(), (Stride)0)); }

class doubleComplexVector: public doubleComplexSubVector {
  public:
  // Constructors
		doubleComplexVector(void);
  explicit	doubleComplexVector(Extent n);
		doubleComplexVector(Extent n, const doubleComplex& s);
  explicit	doubleComplexVector(const doubleSubVector& v);
		doubleComplexVector(const doubleSubVector& v,
				     const doubleSubVector& u);
		doubleComplexVector(const doubleComplexSubVector& v);
		doubleComplexVector(const doubleComplexVector& v);
		~doubleComplexVector(void);
  // Functions
  doubleComplexVector&
		resize(void);
  doubleComplexVector&
		resize(Extent n);
  doubleComplexVector&
		resize(Extent n, const doubleComplex& s);
  doubleComplexVector&
		resize(const doubleSubVector& v);
  doubleComplexVector&
		resize(const doubleSubVector& v,
		       const doubleSubVector& u);
  doubleComplexVector&
		resize(const doubleComplexSubVector& v);
private:
  doubleComplexVector&
		resize(const doubleComplexHandle& h,
      Offset o, Extent n, Stride s);
  doubleComplexVector&
		free(void) { doubleComplexSubVector::free(); return *this; }
public:
  // Operators
  doubleComplexVector&
		operator  =(const double& s);
  doubleComplexVector&
		operator  =(const doubleComplex& s);
  doubleComplexVector&
		operator  =(const doubleSubVector& w);
  doubleComplexVector&
		operator  =(const doubleComplexSubVector& w);
  doubleComplexVector&
		operator  =(const doubleComplexVector& w);
  };

// Constructors
inline		doubleComplexVector::doubleComplexVector(void):
  doubleComplexSubVector() { }
inline		doubleComplexVector::doubleComplexVector(Extent n):
  doubleComplexSubVector(allocate(n), (Offset)0, n, (Stride)1) {
#ifdef SVMT_DEBUG_MODE
  doubleComplexSubVector::operator =(doubleComplex(doubleBad, doubleBad));
#endif // SVMT_DEBUG_MODE
  }
inline		doubleComplexVector::doubleComplexVector(
    Extent n, const doubleComplex& s):
  doubleComplexSubVector(allocate(n), (Offset)0, n, (Stride)1) {
    doubleComplexSubVector::operator  =(s); }
inline		doubleComplexVector::doubleComplexVector(
    const doubleSubVector& v): doubleComplexSubVector(
      allocate(v.extent()), (Offset)0, v.extent(), (Stride)1) {
  doubleComplexSubVector::operator  =(v); }
inline		doubleComplexVector::doubleComplexVector(
    const doubleSubVector& v, const doubleSubVector& u):
  doubleComplexSubVector(
      allocate(v.extent()), (Offset)0, v.extent(), (Stride)1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexVector::doubleComplexVector(\n"
    "const doubleSubVector&, const doubleSubVector&)", v.extent(), u.extent());
#endif // SVMT_DEBUG_MODE
  real() = v; imag() = u; }
inline		doubleComplexVector::doubleComplexVector(
    const doubleComplexSubVector& v):
  doubleComplexSubVector(
      allocate(v.extent()), (Offset)0, v.extent(), (Stride)1) {
  doubleComplexSubVector::operator  =(v); }
inline		doubleComplexVector::doubleComplexVector(
    const doubleComplexVector& v):
  doubleComplexSubVector(
      allocate(v.extent()), (Offset)0, v.extent(), (Stride)1) {
  doubleComplexSubVector::operator  =(v); }
inline		doubleComplexVector::~doubleComplexVector(void) { free(); }

// Assignment Operators
inline
doubleComplexVector&	doubleComplexVector::operator  =(const double& s) {
  doubleComplexSubVector::operator =(s); return *this; }
inline
doubleComplexVector&	doubleComplexVector::operator  =(
    const doubleComplex& s) {
  doubleComplexSubVector::operator =(s); return *this; }
inline
doubleComplexVector&	doubleComplexVector::operator  =(
    const doubleSubVector& w) {
  doubleComplexSubVector::operator =(w); return *this; }
inline
doubleComplexVector&	doubleComplexVector::operator  =(
    const doubleComplexSubVector& w) {
  doubleComplexSubVector::operator =(w); return *this; }
inline
doubleComplexVector&	doubleComplexVector::operator  =(
    const doubleComplexVector& w) {
  doubleComplexSubVector::operator =(w); return *this; }

// Functions
inline
doubleComplexVector&
		doubleComplexVector::resize(void) { free();
  doubleComplexSubVector::resize(); return *this; }
inline
doubleComplexVector&
		doubleComplexVector::resize(Extent n) { free();
  doubleComplexSubVector::resize(allocate(n), (Offset)0, n, (Stride)1);
#ifdef SVMT_DEBUG_MODE
  *this = doubleComplex(doubleBad, doubleBad);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
doubleComplexVector&
		doubleComplexVector::resize(
    Extent n, const doubleComplex& s) { return resize(n) = s; }
inline
doubleComplexVector&
		doubleComplexVector::resize(const doubleSubVector& v) {
  return resize(v.extent()) = v; }
inline
doubleComplexVector&
		doubleComplexVector::resize(
    const doubleSubVector& v, const doubleSubVector& u) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_conformance(
    "doubleComplexVector::resize(\n"
    "const doubleSubVector&, const doubleSubVector&)",
    v.extent(), u.extent());
#endif // SVMT_DEBUG_MODE
  resize(v.extent()); real() = v; imag() = u; return *this; }
inline
doubleComplexVector&
		doubleComplexVector::resize(
    const doubleComplexSubVector& v) { return resize(v.extent()) = v; }
inline
doubleComplex	doubleComplexSubVector::dot_(
    const doubleSubVector& w) const { return dot(w); }
inline
doubleComplex	doubleComplexSubVector::dot_(
    const doubleComplexSubVector& w) const { return dot(w); }
inline
doubleComplex	doubleSubVector::dot_(
    const doubleComplexSubVector& w) const { return dot(w); }
inline
doubleComplex		doubleComplexSubVector::dot(void) const { return dot(*this); }

inline
const
doubleComplexVector	doubleComplexSubVector::pl_(
    const offsetSubVector& p, const doubleComplexSubMatrix& L) const {
  return up(L, p); }
inline
const
doubleComplexVector	doubleComplexSubVector::l_(
    const doubleComplexSubMatrix& L) const { return u(L); }
inline
const
doubleComplexVector	doubleComplexSubVector::lp_(
    const doubleComplexSubMatrix& L, const offsetSubVector& p) const {
  return pu(p, L); }
inline
const
doubleComplexVector	doubleComplexSubVector::pld_(
    const offsetSubVector& p, const doubleComplexSubMatrix& LD) const {
  return dup(LD, p); }
inline
const
doubleComplexVector	doubleComplexSubVector::ld_(
    const doubleComplexSubMatrix& LD) const { return du(LD); }
inline
const
doubleComplexVector	doubleComplexSubVector::ldp_(
    const doubleComplexSubMatrix& LD, const offsetSubVector& p) const {
  return pdu(p, LD); }
inline
const
doubleComplexVector	doubleComplexSubVector::pu_(
    const offsetSubVector& p, const doubleComplexSubMatrix& U) const {
  return lp(U, p); }
inline
const
doubleComplexVector	doubleComplexSubVector::u_(
    const doubleComplexSubMatrix& U) const { return l(U); }
inline
const
doubleComplexVector	doubleComplexSubVector::up_(
    const doubleComplexSubMatrix& U, const offsetSubVector& p) const {
  return pl(p, U); }
inline
const
doubleComplexVector	doubleComplexSubVector::pdu_(
    const offsetSubVector& p, const doubleComplexSubMatrix& DU) const {
  return ldp(DU, p); }
inline
const
doubleComplexVector	doubleComplexSubVector::du_(
    const doubleComplexSubMatrix& DU) const { return ld(DU); }
inline
const
doubleComplexVector	doubleComplexSubVector::dup_(
    const doubleComplexSubMatrix& DU, const offsetSubVector& p) const {
  return pld(p, DU); }
inline
const
doubleComplexVector
		doubleComplexSubVector::above_(
    const doubleComplexSubVector& v) const { return aside(v); }

inline
const
doubleComplexVector
		doubleSubVector::operator *(
    const doubleComplexSubVector& w) const { return w*(*this); }
inline
const
doubleComplexVector
		doubleSubVector::operator +(
    const doubleComplexSubVector& w) const { return w + *this; }
inline
const
doubleComplexVector
		doubleComplexSubVector::operator *(const double& s) const {
  return operator *(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
const
doubleComplexVector
		doubleComplexSubVector::operator /(const double& s) const {
  return operator /(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
const
doubleComplexVector
		doubleComplexSubVector::operator +(const double& s) const {
  return operator +(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
const
doubleComplexVector
		doubleComplexSubVector::operator -(const double& s) const {
  return operator -(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
const
doubleComplexVector
		operator *(const double& s, const doubleComplexSubVector& v) {
  return doubleSubArray1((double*)&s, (Offset)0, v.extent(), (Stride)0)*v; }
inline
const
doubleComplexVector
		operator /(const double& s, const doubleComplexSubVector& v) {
  return doubleSubArray1((double*)&s, (Offset)0, v.extent(), (Stride)0)/v; }

inline
const
doubleComplexVector
		operator +(const double& s, const doubleComplexSubVector& v) {
  return doubleSubArray1((double*)&s, (Offset)0, v.extent(), (Stride)0) + v; }
inline
const
doubleComplexVector
		operator -(const double& s, const doubleComplexSubVector& v) {
  return doubleSubArray1((double*)&s, (Offset)0, v.extent(), (Stride)0) - v; }
inline
const
doubleComplexVector
		doubleComplexSubVector::operator *(
    const doubleComplex& s) const { return operator *(doubleComplexSubArray1(
      (double*)&s.real(), (Offset)0, extent(), (Stride)0)); }
inline
const
doubleComplexVector
		doubleComplexSubVector::operator /(
    const doubleComplex& s) const { return operator /(doubleComplexSubArray1(
      (double*)&s.real(), (Offset)0, extent(), (Stride)0)); }
inline
const
doubleComplexVector
		doubleComplexSubVector::operator +(
    const doubleComplex& s) const { return operator +(doubleComplexSubArray1(
      (double*)&s.real(), (Offset)0, extent(), (Stride)0)); }
inline
const
doubleComplexVector
		doubleComplexSubVector::operator -(
    const doubleComplex& s) const { return operator -(doubleComplexSubArray1(
      (double*)&s.real(), (Offset)0, extent(), (Stride)0)); }
inline
const
doubleComplexVector
		doubleSubVector::operator *(const doubleComplex& s) const {
  return operator *(doubleComplexSubArray1((double*)&s.real(), (Offset)0,
      extent(), (Stride)0)); }
inline
const
doubleComplexVector
		doubleSubVector::operator /(const doubleComplex& s) const {
  return operator /(doubleComplexSubArray1((double*)&s.real(), (Offset)0,
      extent(), (Stride)0)); }
inline
const
doubleComplexVector
		doubleSubVector::operator +(const doubleComplex& s) const {
  return operator +(doubleComplexSubArray1((double*)&s.real(), (Offset)0,
      extent(), (Stride)0)); }
inline
const
doubleComplexVector
		doubleSubVector::operator -(const doubleComplex& s) const {
  return operator -(doubleComplexSubArray1((double*)&s.real(), (Offset)0,
      extent(), (Stride)0)); }

inline
const
doubleComplexVector
		operator *(const doubleComplex& s, const doubleSubVector& v) {
  return doubleComplexSubArray1((double*)&s.real(), (Offset)0,
    v.extent(), (Stride)0)*v; }
inline
const
doubleComplexVector
		operator /(const doubleComplex& s, const doubleSubVector& v) {
  return doubleComplexSubArray1((double*)&s.real(), (Offset)0,
    v.extent(), (Stride)0)/v; }
inline
const
doubleComplexVector
		operator +(const doubleComplex& s, const doubleSubVector& v) {
  return v + doubleComplexSubArray1((double*)&s.real(), (Offset)0,
    v.extent(), (Stride)0); }
inline
const
doubleComplexVector
		operator -(const doubleComplex& s, const doubleSubVector& v) {
  return doubleComplexSubArray1((double*)&s.real(), (Offset)0,
    v.extent(), (Stride)0) - v; }
inline
const
doubleComplexVector
		operator *(const doubleComplex& s,
    const doubleComplexSubVector& v) { return doubleComplexSubArray1(
    (double*)&s.real(), (Offset)0, v.extent(), (Stride)0)*v; }
inline
const
doubleComplexVector
		operator /(const doubleComplex& s,
    const doubleComplexSubVector& v) { return doubleComplexSubArray1(
    (double*)&s.real(), (Offset)0, v.extent(), (Stride)0)/v; }
inline
const
doubleComplexVector
		operator +(const doubleComplex& s,
    const doubleComplexSubVector& v) { return doubleComplexSubArray1(
    (double*)&s.real(), (Offset)0, v.extent(), (Stride)0) + v; }
inline
const
doubleComplexVector
		operator -(const doubleComplex& s,
    const doubleComplexSubVector& v) { return doubleComplexSubArray1(
    (double*)&s.real(), (Offset)0, v.extent(), (Stride)0) - v; }


#endif /* _doubleComplexVector_h */

