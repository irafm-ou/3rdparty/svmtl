#ifndef _doubleArray1_h
#define _doubleArray1_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleVector.h>


template<Extent n>
class doubleArray1: public doubleSubArray1 {
private:
  // Representation
  double	A[n];
  // Functions			// prevent doubleSubArray1 resize
  doubleArray1<n>&
		resize(void);
  doubleArray1<n>&
		resize(double*, Offset, Extent, Stride);
  doubleArray1<n>&
		resize(const doubleSubArray1&);
public:
  // Constructors
		doubleArray1(void):
    doubleSubArray1(A, 0, n, 1) { }
		doubleArray1(const double& s):
    doubleSubArray1(A, 0, n, 1) {
    doubleSubArray1::operator =(s); }
		doubleArray1(const double& s, const double& t):
    doubleSubArray1(A, 0, n, 1) {
    ramp();
    doubleSubArray1::operator*=(t);
    doubleSubArray1::operator+=(s); }
		doubleArray1(const doubleArray1<n>& v):
    doubleSubArray1(A, 0, n, 1) {
    doubleSubArray1::operator =(v); }
	       ~doubleArray1(void) { }
  // Operators
  doubleArray1<n>&
  operator =(const doubleArray1<n>& v) {
    doubleSubArray1::operator =(v);
    return *this; }
  };

#endif /* _doubleArray1_h */

