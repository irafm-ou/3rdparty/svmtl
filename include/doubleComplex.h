#ifndef _doubleComplex_h
#define _doubleComplex_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleMath.h>

class doubleComplex {
  private:
  // Representation
  double	R;			// real
  double	I;			// imaginary
  public:
  // Constructors
		doubleComplex(void);
  explicit	doubleComplex(const double& r);
		doubleComplex(const double& r, const double& i);
		doubleComplex(const doubleComplex& c);
		~doubleComplex(void);
  // Functions
  const
  double&	real(void) const;
  double&	real(void);
  const
  double&	imag(void) const;
  double&	imag(void);
  // Operators
  doubleComplex
		operator -(void) const;
  doubleComplex
		operator +(void) const;
  doubleComplex&
		operator  =(const double& r);
  doubleComplex&
		operator *=(const double& r);
  doubleComplex&
		operator /=(const double& r);
  doubleComplex&
		operator +=(const double& r);
  doubleComplex&
		operator -=(const double& r);
  doubleComplex&
		operator  =(const doubleComplex& c);
  doubleComplex&
		operator *=(const doubleComplex& c);
  doubleComplex&
		operator /=(const doubleComplex& c);
  doubleComplex&
		operator +=(const doubleComplex& c);
  doubleComplex&
		operator -=(const doubleComplex& c);
  };

// Constructors
inline		doubleComplex::doubleComplex(void):
  R((double)0), I((double)0) { }
inline		doubleComplex::doubleComplex(
  const double& r): R(r), I((double)0) { }
inline		doubleComplex::doubleComplex(
  const double& r, const double& i): R(r), I(i) { }
inline		doubleComplex::doubleComplex(
  const doubleComplex& c): R(c.R), I(c.I) { }
inline		doubleComplex::~doubleComplex(void) { }

// Functions
inline
const
double&		doubleComplex::real(void) const { return R; }
inline
double&		doubleComplex::real(void) { return R; }
inline
const
double&		doubleComplex::imag(void) const { return I; }
inline
double&		doubleComplex::imag(void) { return I; }
inline
doubleComplex	conj(const doubleComplex& c) {
  return doubleComplex(c.real(), -c.imag()); }
inline
doubleComplex	iconj(const doubleComplex& c) {
  return doubleComplex(c.imag(), c.real()); }
inline
double		norm(const doubleComplex& c) {
  return c.real()*c.real() + c.imag()*c.imag(); }

// Operators
inline
doubleComplex	doubleComplex::operator -(void) const {
  return doubleComplex(-real(), -imag()); }
inline
doubleComplex	doubleComplex::operator +(void) const {
  return *this; }
inline
doubleComplex	operator *(const doubleComplex& z, const double& r) {
  return doubleComplex(z.real()*r, z.imag()*r); }
inline
doubleComplex	operator /(const doubleComplex& z, const double& r) {
  return doubleComplex(z.real()/r, z.imag()/r); }
inline
doubleComplex	operator +(const doubleComplex& z, const double& r) {
  return doubleComplex(z.real() + r, z.imag()); }
inline
doubleComplex	operator -(const doubleComplex& z, const double& r) {
  return doubleComplex(z.real() - r, z.imag()); }

inline
doubleComplex	operator *(const doubleComplex& z,
			   const doubleComplex& c) {
  return doubleComplex(z.real()*c.real() - z.imag()*c.imag(),
			z.real()*c.imag() + z.imag()*c.real()); }
inline
doubleComplex	operator /(const doubleComplex& z,
			   const doubleComplex& c) {
  return z*conj(c)/norm(c); }
inline
doubleComplex	operator +(const doubleComplex& z,
			   const doubleComplex& c) {
  return doubleComplex(z.real() + c.real(), z.imag() + c.imag()); }
inline
doubleComplex	operator -(const doubleComplex& z,
			   const doubleComplex& c) {
  return doubleComplex(z.real() - c.real(), z.imag() - c.imag()); }

inline
doubleComplex	operator *(const double& r, const doubleComplex& c) {
  return doubleComplex(r*c.real(), r*c.imag()); }
inline
doubleComplex	operator /(const double& r, const doubleComplex& c) {
  return r*conj(c)/norm(c); }
inline
doubleComplex	operator +(const double& r, const doubleComplex& c) {
  return c + r; }
inline
doubleComplex	operator -(const double& r, const doubleComplex& c) {
  return -c + r; }

std::istream&	operator >>(std::istream& s, doubleComplex& c);
std::ostream&	operator <<(std::ostream& s, const doubleComplex& c);

inline
bool		operator ==(const doubleComplex& z, const double& r) {
  return z.real() == r && z.imag() == (double)0; }
inline
bool		operator !=(const doubleComplex& z, const double& r) {
  return !(z == r); }
inline
bool		operator ==(const double& r, const doubleComplex& z) {
  return z == r; }
inline
bool		operator !=(const double& r, const doubleComplex& z) {
  return z != r; }
inline
bool		operator ==(const doubleComplex& z,
			    const doubleComplex& c) {
  return z.real() == c.real() && z.imag() == c.imag(); }
inline
bool		operator !=(const doubleComplex& z,
			    const doubleComplex& c) {
  return !(z == c); }

inline
doubleComplex&	doubleComplex::operator  =(const double& r) {
  real()  = r; imag()  = (double)0; return *this; }
inline
doubleComplex&	doubleComplex::operator *=(const double& r) {
  real() *= r; imag() *= r; return *this; }
inline
doubleComplex&	doubleComplex::operator /=(const double& r) {
  real() /= r; imag() /= r; return *this; }
inline
doubleComplex&	doubleComplex::operator +=(const double& r) {
  real() += r; return *this; }
inline
doubleComplex&	doubleComplex::operator -=(const double& r) {
  real() -= r; return *this; }

inline
doubleComplex&	doubleComplex::operator  =(const doubleComplex& c) {
  real()  = c.real(); imag()  = c.imag(); return *this; }
inline
doubleComplex&	doubleComplex::operator *=(const doubleComplex& c) {
  return operator  =(*this*c); }
inline
doubleComplex&	doubleComplex::operator /=(const doubleComplex& c) {
  return operator  =(*this/c); }
inline
doubleComplex&	doubleComplex::operator +=(const doubleComplex& c) {
  real() += c.real(); imag() += c.imag(); return *this; }
inline
doubleComplex&	doubleComplex::operator -=(const doubleComplex& c) {
  real() -= c.real(); imag() -= c.imag(); return *this; }

// Functions
inline
doubleComplex	polar(const double& r, const double& t) {
  return doubleComplex(r*cos(t), r*sin(t)); }
inline
double		 abs(const doubleComplex& c) {
  return hypot(c.real(), c.imag()); }
inline
double		 arg(const doubleComplex& c) {
  return atan2(c.imag(), c.real()); }
inline
doubleComplex	 log(const doubleComplex& c) {
  return doubleComplex(log(abs(c)), arg(c)); }
inline
doubleComplex	 exp(const doubleComplex& c) {
  return polar(exp(c.real()), c.imag()); }
doubleComplex	sqrt(const doubleComplex& c);
inline
doubleComplex	 cos(const doubleComplex& c) {
  return doubleComplex(
    cos(c.real())*cosh(c.imag()),
    sin(c.real())*sinh(c.imag())); }
inline
doubleComplex	 sin(const doubleComplex& c) {
  return doubleComplex(
    sin(c.real())*cosh(c.imag()),
    cos(c.real())*sinh(c.imag())); }
inline
doubleComplex	 tan(const doubleComplex& c) {
  return sin(c)/cos(c); }
inline
doubleComplex	 cosh(const doubleComplex& c) {
  return doubleComplex(
    cosh(c.real())*cos(c.imag()),
    sinh(c.real())*sin(c.imag())); }
inline
doubleComplex	 sinh(const doubleComplex& c) {
  return doubleComplex(
    cosh(c.real())*cos(c.imag()),
    sinh(c.real())*sin(c.imag())); }
inline
doubleComplex	 tanh(const doubleComplex& c) {
  return sinh(c)/cosh(c); }
inline
doubleComplex	acosh(const doubleComplex& c) {
  return log(c + sqrt(c*c - (double)1)); }
inline
doubleComplex	asinh(const doubleComplex& c) {
  return log(c + sqrt(c*c + (double)1)); }
inline
doubleComplex	atanh(const doubleComplex& c) {
  return 0.5*log(((double)1 + c)/((double)1 - c)); }
inline
doubleComplex	acos(const doubleComplex& c) {
  return -iconj(asinh(conj(c))); }
inline
doubleComplex	asin(const doubleComplex& c) {
  return -iconj(acosh(conj(c))) + 0.5*M_PI; }
inline
doubleComplex	atan(const doubleComplex& c) {
  return +iconj(atanh(iconj(c))); }

#endif /* _doubleComplex_h */

