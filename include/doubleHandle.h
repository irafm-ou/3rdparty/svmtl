#ifndef _doubleHandle_h
#define _doubleHandle_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleMath.h>
#include<svmt_typedefs.h>

class doubleHandle {
  private:
  // Representation
  double*	P;
  // Constructors
		doubleHandle(double* p);
  public:
		doubleHandle(const doubleHandle& h);
		~doubleHandle(void);
  // Operators
		operator double* (void);
  private:
  doubleHandle&
		operator  =(const doubleHandle& h);
  public:
  // Functions
  bool		empty(void) const;
  double		get(Offset o) const;
  doubleHandle&
		put(Offset o, const double& s);
  friend class doubleComplexHandle;
  friend class doubleSubArray0;
  friend class doubleSubVector;	friend class doubleVector;
  friend class doubleSubArray1;
  friend class doubleSubMatrix;	friend class doubleMatrix;
  friend class doubleSubArray2;
  friend class doubleSubTensor;	friend class doubleTensor;
  friend class doubleSubArray3;
  };

// Constructors
inline		doubleHandle::doubleHandle(
  double* p): P(p) { }
inline		doubleHandle::doubleHandle(
  const doubleHandle& h): P(h.P) { }
inline		doubleHandle::~doubleHandle(void) { }

// Operators
inline		doubleHandle::operator double* (void) { return P; }
inline
doubleHandle&
		doubleHandle::operator  =(
  const doubleHandle& h) { P = h.P; return *this; }

// Functions
inline
bool		doubleHandle::empty(void) const { return 0 == P; }
inline
double		doubleHandle::get(Offset o) const {
  return P[o]; }

inline
doubleHandle&
		doubleHandle::put(Offset o, const double& s) {
  P[o] = s; return *this; }

#endif /* _doubleHandle_h */

