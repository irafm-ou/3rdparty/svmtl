#ifndef _offsetFactor_h
#define _offsetFactor_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<iomanip>
#include<svmt_typedefs.h>

class	offsetReversePrimeFactorRadix {
  static
  const
  Extent	bits = CHAR_BIT*sizeof(Extent);

  // Representation
  Offset	k;
  Extent	l;
  Offset	d[bits];
  Extent	p[bits];
  Extent	q[bits];
public:
  // Constructors
  explicit	offsetReversePrimeFactorRadix(Extent n);

  // Functions
  Extent	digits(void) const;

  // Operators
		operator Offset(void) const;
  Offset	operator [](Offset digit) const;
  offsetReversePrimeFactorRadix&
		operator ++(void);
  offsetReversePrimeFactorRadix&
		operator  =(Offset i);
  };

// Functions
inline
Extent	offsetReversePrimeFactorRadix::digits(void) const { return l; }

// Operators
inline
	offsetReversePrimeFactorRadix::operator Offset(void) const {
  return k; }
inline
Offset	offsetReversePrimeFactorRadix::operator [](Offset digit) const {
  return d[digit]; }

ostream&
	operator <<(ostream& o, const offsetReversePrimeFactorRadix& k);

#endif /* _offsetFactor_h */

