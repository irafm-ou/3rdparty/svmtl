#ifndef _doubleComplexArray1_h
#define _doubleComplexArray1_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleComplexVector.h>


template<Extent n>
class doubleComplexArray1: public doubleComplexSubArray1 {
private:
  // Representation
  double	A[n*2];
  // Functions			// prevent doubleComplexSubArray1 resize
  doubleComplexArray1<n>&
		resize(void);
  doubleComplexArray1<n>&
		resize(double*, Offset, Extent, Stride);
  doubleComplexArray1<n>&
		resize(const doubleComplexSubArray1&);
public:
  // Constructors
		doubleComplexArray1(void):
    doubleComplexSubArray1(A, 0, n, 1) { }
		doubleComplexArray1(const doubleComplex& s):
    doubleComplexSubArray1(A, 0, n, 1) {
    doubleComplexSubArray1::operator =(s); }
		doubleComplexArray1(const doubleComplexArray1<n>& v):
    doubleComplexSubArray1(A, 0, n, 1) {
    doubleComplexSubArray1::operator =(v); }
	       ~doubleComplexArray1(void) { }
  // Operators
  doubleComplexArray1<n>&
  operator =(const doubleComplexArray1<n>& v) {
    doubleComplexSubArray1::operator =(v);
    return *this; }
  };

#endif /* _doubleComplexArray1_h */

