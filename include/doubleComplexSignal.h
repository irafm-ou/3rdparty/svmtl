#ifndef _doubleComplexSignal_h
#define _doubleComplexSignal_h 1
/*
The C++ Digital Signal Processing classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleSignal.h>
#include<doubleComplexTensor.h>


// 1 Dimensional Complex to Complex Direct Discrete Fourier Transform
class double1DCCDDFT: doubleComplexVector {	// in-place
public:
  // Constructors
		double1DCCDDFT(void);
  explicit	double1DCCDDFT(Extent n, Extent invocations = MaximumExtent);
		double1DCCDDFT(const double1DCCDDFT& dft);
	       ~double1DCCDDFT(void);
  // Functions
  double1DCCDDFT&
		resize(void);
  double1DCCDDFT&
		resize(Extent n, Extent invocations = MaximumExtent);
  // Operators
  doubleComplexSubVector&
		operator ()(doubleComplexSubVector& v) const;
  doubleComplexSubMatrix&
		operator ()(doubleComplexSubMatrix& M) const;
  doubleComplexSubTensor&
		operator ()(doubleComplexSubTensor& T) const;
  double1DCCDDFT&
		operator  =(const double1DCCDDFT& dft);
  };

// Constructors
inline		double1DCCDDFT::double1DCCDDFT(void): doubleComplexVector() { }
inline		double1DCCDDFT::double1DCCDDFT(const double1DCCDDFT& dft):
  doubleComplexVector(dft) { }
inline		double1DCCDDFT::double1DCCDDFT(Extent n, Extent invocations):
  doubleComplexVector(n) { twiddle(-1); }
inline		double1DCCDDFT::~double1DCCDDFT(void) { }

// Functions
inline
double1DCCDDFT&	double1DCCDDFT::resize(void) {
  doubleComplexVector::resize();
  return *this; }

// Operators
inline
double1DCCDDFT&	double1DCCDDFT::operator  =(const double1DCCDDFT& dft) {
  doubleComplexVector::operator  =(dft);
  return *this;
  }

// 1 Dimensional Complex Direct Discrete Fourier Transform from Complex
class double1DCDDFTC: doubleComplexVector {	// return by value
public:
  // Constructors
		double1DCDDFTC(void);
  explicit	double1DCDDFTC(Extent n, Extent invocations = MaximumExtent);
		double1DCDDFTC(const double1DCDDFTC& dft);
	       ~double1DCDDFTC(void);
  // Functions
  double1DCDDFTC&
		resize(void);
  double1DCDDFTC&
		resize(Extent n, Extent invocations = MaximumExtent);
  // Operators
  doubleComplexVector
		operator ()(const doubleComplexSubVector& v) const;
  doubleComplexMatrix
		operator ()(const doubleComplexSubMatrix& M) const;
  doubleComplexTensor
		operator ()(const doubleComplexSubTensor& T) const;
  double1DCDDFTC&
		operator  =(const double1DCDDFTC& dft);
  };

// Constructors
inline		double1DCDDFTC::double1DCDDFTC(void): doubleComplexVector() { }
inline		double1DCDDFTC::double1DCDDFTC(const double1DCDDFTC& dft):
  doubleComplexVector(dft) { }
inline		double1DCDDFTC::double1DCDDFTC(Extent n, Extent invocations):
  doubleComplexVector(n) { twiddle(-1); }
inline		double1DCDDFTC::~double1DCDDFTC(void) { }

// Functions
inline
double1DCDDFTC&	double1DCDDFTC::resize(void) {
  doubleComplexVector::resize();
  return *this; }

// Operators
inline
double1DCDDFTC&	double1DCDDFTC::operator  =(const double1DCDDFTC& dft) {
  doubleComplexVector::operator  =(dft);
  return *this;
  }

// 1 Dimensional Complex to Complex Inverse Discrete Fourier Transform
class double1DCCIDFT: doubleComplexVector {	// in-place
public:
  // Constructors
		double1DCCIDFT(void);
  explicit	double1DCCIDFT(Extent n, Extent invocations = MaximumExtent);
		double1DCCIDFT(const double1DCCIDFT& dft);
	       ~double1DCCIDFT(void);
  // Functions
  double1DCCIDFT&
		resize(void);
  double1DCCIDFT&
		resize(Extent n, Extent invocations = MaximumExtent);
  // Operators
  doubleComplexSubVector&
		operator ()(doubleComplexSubVector& v) const;
  doubleComplexSubMatrix&
		operator ()(doubleComplexSubMatrix& M) const;
  doubleComplexSubTensor&
		operator ()(doubleComplexSubTensor& T) const;
  double1DCCIDFT&
		operator  =(const double1DCCIDFT& dft);
  };

// Constructors
inline		double1DCCIDFT::double1DCCIDFT(void): doubleComplexVector() { }
inline		double1DCCIDFT::double1DCCIDFT(const double1DCCIDFT& dft):
  doubleComplexVector(dft) { }
inline		double1DCCIDFT::double1DCCIDFT(Extent n, Extent invocations):
  doubleComplexVector(n) { twiddle(+1); }
inline		double1DCCIDFT::~double1DCCIDFT(void) { }

// Functions
inline
double1DCCIDFT&	double1DCCIDFT::resize(void) {
  doubleComplexVector::resize();
  return *this; }

// Operators
inline
double1DCCIDFT&	double1DCCIDFT::operator  =(const double1DCCIDFT& dft) {
  doubleComplexVector::operator  =(dft);
  return *this;
  }

// 1 Dimensional Complex Inverse Discrete Fourier Transform from Complex
class double1DCIDFTC: doubleComplexVector {	// return by value
public:
  // Constructors
		double1DCIDFTC(void);
  explicit	double1DCIDFTC(Extent n, Extent invocations = MaximumExtent);
		double1DCIDFTC(const double1DCIDFTC& dft);
	       ~double1DCIDFTC(void);
  // Functions
  double1DCIDFTC&
		resize(void);
  double1DCIDFTC&
		resize(Extent n, Extent invocations = MaximumExtent);
  // Operators
  doubleComplexVector
		operator ()(const doubleComplexSubVector& v) const;
  doubleComplexMatrix
		operator ()(const doubleComplexSubMatrix& M) const;
  doubleComplexTensor
		operator ()(const doubleComplexSubTensor& T) const;
  double1DCIDFTC&
		operator  =(const double1DCIDFTC& dft);
  };

// Constructors
inline		double1DCIDFTC::double1DCIDFTC(void): doubleComplexVector() { }
inline		double1DCIDFTC::double1DCIDFTC(const double1DCIDFTC& dft):
  doubleComplexVector(dft) { }
inline		double1DCIDFTC::double1DCIDFTC(Extent n, Extent invocations):
  doubleComplexVector(n) { twiddle(+1); }
inline		double1DCIDFTC::~double1DCIDFTC(void) { }

// Functions
inline
double1DCIDFTC&	double1DCIDFTC::resize(void) {
  doubleComplexVector::resize();
  return *this; }

// Operators
inline
double1DCIDFTC&	double1DCIDFTC::operator  =(const double1DCIDFTC& dft) {
  doubleComplexVector::operator  =(dft);
  return *this;
  }

// 1 Dimensional Real to Complex Direct Discrete Fourier Transform
class double1DRCDDFT: doubleComplexVector {	// in-place
public:
  // Constructors
		double1DRCDDFT(void);
  explicit	double1DRCDDFT(Extent n, Extent invocations = MaximumExtent);
		double1DRCDDFT(const double1DRCDDFT& dft);
	       ~double1DRCDDFT(void);
  // Functions
  double1DRCDDFT&
		resize(void);
  double1DRCDDFT&
		resize(Extent n, Extent invocations = MaximumExtent);
  // Operators
  doubleSubVector&
		operator ()(doubleSubVector& v) const;
  doubleSubMatrix&
		operator ()(doubleSubMatrix& M) const;
  doubleSubTensor&
		operator ()(doubleSubTensor& T) const;
  double1DRCDDFT&
		operator  =(const double1DRCDDFT& dft);
  };

// Constructors
inline		double1DRCDDFT::double1DRCDDFT(void): doubleComplexVector() { }
inline		double1DRCDDFT::double1DRCDDFT(const double1DRCDDFT& dft):
  doubleComplexVector(dft) { }
inline		double1DRCDDFT::double1DRCDDFT(Extent n, Extent invocations):
  doubleComplexVector(n) { twiddle(-1); }
inline		double1DRCDDFT::~double1DRCDDFT(void) { }

// Functions
inline
double1DRCDDFT&	double1DRCDDFT::resize(void) {
  doubleComplexVector::resize();
  return *this; }

// Operators
inline
double1DRCDDFT&	double1DRCDDFT::operator  =(const double1DRCDDFT& dft) {
  doubleComplexVector::operator  =(dft);
  return *this;
  }

// 1 Dimensional Complex Direct Discrete Fourier Transform from Real
class double1DCDDFTR: doubleComplexVector {	// return by value
public:
  // Constructors
		double1DCDDFTR(void);
  explicit	double1DCDDFTR(Extent n, Extent invocations = MaximumExtent);
		double1DCDDFTR(const double1DCDDFTR& dft);
	       ~double1DCDDFTR(void);
  // Functions
  double1DCDDFTR&
		resize(void);
  double1DCDDFTR&
		resize(Extent n, Extent invocations = MaximumExtent);
  // Operators
  doubleVector	operator ()(const doubleSubVector& v) const;
  doubleMatrix	operator ()(const doubleSubMatrix& M) const;
  doubleTensor	operator ()(const doubleSubTensor& T) const;
  double1DCDDFTR&
		operator  =(const double1DCDDFTR& dft);
  };

// Constructors
inline		double1DCDDFTR::double1DCDDFTR(void): doubleComplexVector() { }
inline		double1DCDDFTR::double1DCDDFTR(const double1DCDDFTR& dft):
  doubleComplexVector(dft) { }
inline		double1DCDDFTR::double1DCDDFTR(Extent n, Extent invocations):
  doubleComplexVector(n) { twiddle(-1); }
inline		double1DCDDFTR::~double1DCDDFTR(void) { }

// Functions
inline
double1DCDDFTR&	double1DCDDFTR::resize(void) {
  doubleComplexVector::resize();
  return *this; }

// Operators
inline
double1DCDDFTR&	double1DCDDFTR::operator  =(const double1DCDDFTR& dft) {
  doubleComplexVector::operator  =(dft);
  return *this;
  }

// 1 Dimensional Real to Complex Inverse Discrete Fourier Transform
class double1DRCIDFT: doubleComplexVector {	// in-place
public:
  // Constructors
		double1DRCIDFT(void);
  explicit	double1DRCIDFT(Extent n, Extent invocations = MaximumExtent);
		double1DRCIDFT(const double1DRCIDFT& dft);
	       ~double1DRCIDFT(void);
  // Functions
  double1DRCIDFT&
		resize(void);
  double1DRCIDFT&
		resize(Extent n, Extent invocations = MaximumExtent);
  // Operators
  doubleSubVector&
		operator ()(doubleSubVector& v) const;
  doubleSubMatrix&
		operator ()(doubleSubMatrix& M) const;
  doubleSubTensor&
		operator ()(doubleSubTensor& T) const;
  double1DRCIDFT&
		operator  =(const double1DRCIDFT& dft);
  };

// Constructors
inline		double1DRCIDFT::double1DRCIDFT(void): doubleComplexVector() { }
inline		double1DRCIDFT::double1DRCIDFT(const double1DRCIDFT& dft):
  doubleComplexVector(dft) { }
inline		double1DRCIDFT::double1DRCIDFT(Extent n, Extent invocations):
  doubleComplexVector(n) { twiddle(+1); }
inline		double1DRCIDFT::~double1DRCIDFT(void) { }

// Functions
inline
double1DRCIDFT&	double1DRCIDFT::resize(void) {
  doubleComplexVector::resize();
  return *this; }

// Operators
inline
double1DRCIDFT&	double1DRCIDFT::operator  =(const double1DRCIDFT& dft) {
  doubleComplexVector::operator  =(dft);
  return *this;
  }

// 1 Dimensional Complex Inverse Discrete Fourier Transform from Real
class double1DCIDFTR: doubleComplexVector {	// return by value
public:
  // Constructors
		double1DCIDFTR(void);
  explicit	double1DCIDFTR(Extent n, Extent invocations = MaximumExtent);
		double1DCIDFTR(const double1DCIDFTR& dft);
	       ~double1DCIDFTR(void);
  // Functions
  double1DCIDFTR&
		resize(void);
  double1DCIDFTR&
		resize(Extent n, Extent invocations = MaximumExtent);
  // Operators
  doubleVector	operator ()(const doubleSubVector& v) const;
  doubleMatrix	operator ()(const doubleSubMatrix& M) const;
  doubleTensor	operator ()(const doubleSubTensor& T) const;
  double1DCIDFTR&
		operator  =(const double1DCIDFTR& dft);
  };

// Constructors
inline		double1DCIDFTR::double1DCIDFTR(void): doubleComplexVector() { }
inline		double1DCIDFTR::double1DCIDFTR(const double1DCIDFTR& dft):
  doubleComplexVector(dft) { }
inline		double1DCIDFTR::double1DCIDFTR(Extent n, Extent invocations):
  doubleComplexVector(n) { twiddle(+1); }
inline		double1DCIDFTR::~double1DCIDFTR(void) { }

// Functions
inline
double1DCIDFTR&	double1DCIDFTR::resize(void) {
  doubleComplexVector::resize();
  return *this; }

// Operators
inline
double1DCIDFTR&	double1DCIDFTR::operator  =(const double1DCIDFTR& dft) {
  doubleComplexVector::operator  =(dft);
  return *this;
  }

// 1 Dimensional Complex to Real Direct Discrete Fourier Transform
class double1DCRDDFT: doubleComplexVector {	// in-place
public:
  // Constructors
		double1DCRDDFT(void);
  explicit	double1DCRDDFT(Extent n, Extent invocations = MaximumExtent);
		double1DCRDDFT(const double1DCRDDFT& dft);
	       ~double1DCRDDFT(void);
  // Functions
  double1DCRDDFT&
		resize(void);
  double1DCRDDFT&
		resize(Extent n, Extent invocations = MaximumExtent);
  // Operators
  doubleSubVector&
		operator ()(doubleSubVector& v) const;
  doubleSubMatrix&
		operator ()(doubleSubMatrix& M) const;
  doubleSubTensor&
		operator ()(doubleSubTensor& T) const;
  double1DCRDDFT&
		operator  =(const double1DCRDDFT& dft);
  };

// Constructors
inline		double1DCRDDFT::double1DCRDDFT(void): doubleComplexVector() { }
inline		double1DCRDDFT::double1DCRDDFT(const double1DCRDDFT& dft):
  doubleComplexVector(dft) { }
inline		double1DCRDDFT::double1DCRDDFT(Extent n, Extent invocations):
  doubleComplexVector(n) { twiddle(-1); }
inline		double1DCRDDFT::~double1DCRDDFT(void) { }

// Functions
inline
double1DCRDDFT&	double1DCRDDFT::resize(void) {
  doubleComplexVector::resize();
  return *this; }

// Operators
inline
double1DCRDDFT&	double1DCRDDFT::operator  =(const double1DCRDDFT& dft) {
  doubleComplexVector::operator  =(dft);
  return *this;
  }

// 1 Dimensional Real Direct Discrete Fourier Transform from Complex
class double1DRDDFTC: doubleComplexVector {	// return by value
public:
  // Constructors
		double1DRDDFTC(void);
  explicit	double1DRDDFTC(Extent n, Extent invocations = MaximumExtent);
		double1DRDDFTC(const double1DRDDFTC& dft);
	       ~double1DRDDFTC(void);
  // Functions
  double1DRDDFTC&
		resize(void);
  double1DRDDFTC&
		resize(Extent n, Extent invocations = MaximumExtent);
  // Operators
  doubleVector	operator ()(const doubleSubVector& v) const;
  doubleMatrix	operator ()(const doubleSubMatrix& M) const;
  doubleTensor	operator ()(const doubleSubTensor& T) const;
  double1DRDDFTC&
		operator  =(const double1DRDDFTC& dft);
  };

// Constructors
inline		double1DRDDFTC::double1DRDDFTC(void): doubleComplexVector() { }
inline		double1DRDDFTC::double1DRDDFTC(const double1DRDDFTC& dft):
  doubleComplexVector(dft) { }
inline		double1DRDDFTC::double1DRDDFTC(Extent n, Extent invocations):
  doubleComplexVector(n) { twiddle(-1); }
inline		double1DRDDFTC::~double1DRDDFTC(void) { }

// Functions
inline
double1DRDDFTC&	double1DRDDFTC::resize(void) {
  doubleComplexVector::resize();
  return *this; }

// Operators
inline
double1DRDDFTC&	double1DRDDFTC::operator  =(const double1DRDDFTC& dft) {
  doubleComplexVector::operator  =(dft);
  return *this;
  }

// 1 Dimensional Complex to Real Inverse Discrete Fourier Transform
class double1DCRIDFT: doubleComplexVector {	// in-place
public:
  // Constructors
		double1DCRIDFT(void);
  explicit	double1DCRIDFT(Extent n, Extent invocations = MaximumExtent);
		double1DCRIDFT(const double1DCRIDFT& dft);
	       ~double1DCRIDFT(void);
  // Functions
  double1DCRIDFT&
		resize(void);
  double1DCRIDFT&
		resize(Extent n, Extent invocations = MaximumExtent);
  // Operators
  doubleSubVector&
		operator ()(doubleSubVector& v) const;
  doubleSubMatrix&
		operator ()(doubleSubMatrix& M) const;
  doubleSubTensor&
		operator ()(doubleSubTensor& T) const;
  double1DCRIDFT&
		operator  =(const double1DCRIDFT& dft);
  };

// Constructors
inline		double1DCRIDFT::double1DCRIDFT(void): doubleComplexVector() { }
inline		double1DCRIDFT::double1DCRIDFT(const double1DCRIDFT& dft):
  doubleComplexVector(dft) { }
inline		double1DCRIDFT::double1DCRIDFT(Extent n, Extent invocations):
  doubleComplexVector(n) { twiddle(+1); }
inline		double1DCRIDFT::~double1DCRIDFT(void) { }

// Functions
inline
double1DCRIDFT&	double1DCRIDFT::resize(void) {
  doubleComplexVector::resize();
  return *this; }

// Operators
inline
double1DCRIDFT&	double1DCRIDFT::operator  =(const double1DCRIDFT& dft) {
  doubleComplexVector::operator  =(dft);
  return *this;
  }

// 1 Dimensional Real Inverse Discrete Fourier Transform from Complex
class double1DRIDFTC: doubleComplexVector {	// return by value
public:
  // Constructors
		double1DRIDFTC(void);
  explicit	double1DRIDFTC(Extent n, Extent invocations = MaximumExtent);
		double1DRIDFTC(const double1DRIDFTC& dft);
	       ~double1DRIDFTC(void);
  // Functions
  double1DRIDFTC&
		resize(void);
  double1DRIDFTC&
		resize(Extent n, Extent invocations = MaximumExtent);
  // Operators
  doubleVector	operator ()(const doubleSubVector& v) const;
  doubleMatrix	operator ()(const doubleSubMatrix& M) const;
  doubleTensor	operator ()(const doubleSubTensor& T) const;
  double1DRIDFTC&
		operator  =(const double1DRIDFTC& dft);
  };

// Constructors
inline		double1DRIDFTC::double1DRIDFTC(void): doubleComplexVector() { }
inline		double1DRIDFTC::double1DRIDFTC(const double1DRIDFTC& dft):
  doubleComplexVector(dft) { }
inline		double1DRIDFTC::double1DRIDFTC(Extent n, Extent invocations):
  doubleComplexVector(n) { twiddle(+1); }
inline		double1DRIDFTC::~double1DRIDFTC(void) { }

// Functions
inline
double1DRIDFTC&	double1DRIDFTC::resize(void) {
  doubleComplexVector::resize();
  return *this; }

// Operators
inline
double1DRIDFTC&	double1DRIDFTC::operator  =(const double1DRIDFTC& dft) {
  doubleComplexVector::operator  =(dft);
  return *this;
  }

#endif /* _doubleComplexSignal_h */

