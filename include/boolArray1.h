#ifndef _boolArray1_h
#define _boolArray1_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<boolVector.h>


template<Extent n>
class boolArray1: public boolSubArray1 {
private:
  // Representation
  bool	A[n];
  // Functions			// prevent boolSubArray1 resize
  boolArray1<n>&
		resize(void);
  boolArray1<n>&
		resize(bool*, Offset, Extent, Stride);
  boolArray1<n>&
		resize(const boolSubArray1&);
public:
  // Constructors
		boolArray1(void):
    boolSubArray1(A, 0, n, 1) { }
		boolArray1(const bool& s):
    boolSubArray1(A, 0, n, 1) {
    boolSubArray1::operator =(s); }
		boolArray1(const boolArray1<n>& v):
    boolSubArray1(A, 0, n, 1) {
    boolSubArray1::operator =(v); }
	       ~boolArray1(void) { }
  // Operators
  boolArray1<n>&
  operator =(const boolArray1<n>& v) {
    boolSubArray1::operator =(v);
    return *this; }
  };

#endif /* _boolArray1_h */

