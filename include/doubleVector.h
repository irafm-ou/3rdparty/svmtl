#ifndef _doubleVector_h
#define _doubleVector_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<boolVector.h>
#include<doubleScalar.h>

class doubleSubVector {
  private:
  static
  Extent	C;
  // Representation
  doubleHandle
		H;
  Offset	O;
  Extent	N;
  Stride	S;
  public:
  // Constructors
		doubleSubVector(void);
		doubleSubVector(const doubleHandle& h,
      Offset o, Extent n, Stride s);
		doubleSubVector(const doubleSubVector& v);
		~doubleSubVector(void);
  // Functions
  static
  Extent&      columns(void);
  const
  doubleHandle&
		handle(void) const;
  Offset	offset(void) const;
  Extent	extent(void) const;
  Extent  size(void) const;
  Extent	length(void) const;
  Stride	stride(void) const;
  bool		 empty(void) const;
  static
  doubleHandle
	      allocate(Extent n);
  doubleSubVector&
		  free(void);
  doubleSubVector&
		resize(void);
  doubleSubVector&
		resize(const doubleHandle& h,
      Offset o, Extent n, Stride s);
  doubleSubVector&
		resize(const doubleSubVector& v);
  bool	      contains(Offset j, Extent n, Stride s) const;
  bool	      contains_(Offset j, Extent n, Stride s) const;
  doubleSubVector
		   sub(Offset j, Extent n, Stride s);
  const
  doubleSubVector
		   sub(Offset j, Extent n, Stride s) const;
  doubleSubVector
		   sub_(Offset j, Extent n, Stride s);
  const
  doubleSubVector
		   sub_(Offset j, Extent n, Stride s) const;
  const
  doubleSubMatrix
	     submatrix(Extent m = 1) const;
  const
  doubleSubTensor
	     subtensor(Extent m = 1, Extent l = 1) const;

  doubleSubVector
		     r(void);
  const
  doubleSubVector
		     r(void) const;
  doubleSubVector&
	       reverse(void);
  doubleSubVector
		  even(void);
  const
  doubleSubVector
		  even(void) const;
  doubleSubVector
		   odd(void);
  const
  doubleSubVector
		   odd(void) const;
  doubleSubVector
		  even_(void);
  const
  doubleSubVector
		  even_(void) const;
  doubleSubVector
		   odd_(void);
  const
  doubleSubVector
		   odd_(void) const;

private:
  doubleSubVector&
	     transpose(bool b = false);
  doubleSubVector&
	     transpose(Extent p, Extent q);
  // Real to Complex Slow Discrete Fourier Transform  x.rcsdft(w)
  doubleSubVector&
		rcsdft(const doubleComplexSubVector& w);
  // Real to Complex Fast Discrete Fourier Transform  x.rcfdft(w)
  doubleSubVector&
		rcfdft(const doubleComplexSubVector& w);
  // Complex to Real Slow Discrete Fourier Transform  y.crsdft(w)
  doubleSubVector&
		crsdft(const doubleComplexSubVector& w);
  // Complex to Real Fast Discrete Fourier Transform  y.crfdft(w)
  doubleSubVector&
		crfdft(const doubleComplexSubVector& w);
public:
  doubleSubVector&
		  cdft(int sign = -1);	// real to complex dft
  doubleSubVector&
		  rdft(int sign = -1);	// complex to real dft
  friend class doubleComplexSubVector;
  friend class doubleSubMatrix;
  friend class doubleComplexSubMatrix;
  friend class doubleSubTensor;
  friend class doubleComplexSubTensor;
  doubleSubVector&
		  pack(const doubleComplexSubVector& y);
  doubleSubVector&
		  ramp(void);
  friend class double1DCCDDFT; friend class double1DCDDFTC;
  friend class double1DCCIDFT; friend class double1DCIDFTC;
  friend class double1DRCDDFT; friend class double1DCDDFTR;
  friend class double1DRCIDFT; friend class double1DCIDFTR;
  friend class double1DCRDDFT; friend class double1DRDDFTC;
  friend class double1DCRIDFT; friend class double1DRIDFTC;

  doubleSubVector&
		  swap(Offset j, Offset k);
  doubleSubVector&
		  swap(doubleSubVector& w);
  doubleSubVector&
		  swap_(Offset j, Offset k);
  doubleSubVector&
		rotate(Stride n);
  doubleSubVector&
		 shift(Stride n,const double& s
		  = (double)0);
  Offset	   min(void) const;
  Offset	   max(void) const;
  Offset	   min_(void) const;
  Offset	   max_(void) const;
  double
		   sum(void) const;
  double
		   dot(const doubleSubVector& w) const;
  const
  doubleVector
		   dot(const doubleSubMatrix& M) const;
  doubleComplex
		   dot(const doubleComplexSubVector& w) const;
  const
  doubleComplexVector
		   dot(const doubleComplexSubMatrix& M) const;
  double
		   dot_(const doubleSubVector& w) const;
  const
  doubleMatrix
		   dot_(const doubleSubMatrix& M) const;
  doubleComplex
		   dot_(const doubleComplexSubVector& w) const;
  const
  doubleComplexMatrix
		   dot_(const doubleComplexSubMatrix& M) const;
  double		   dot(void) const;

  const
  doubleVector
	   permutation(const offsetSubVector& p) const;
  const
  doubleVector
		    pl(const offsetSubVector& p,
		       const doubleSubMatrix& L) const;
  const
  doubleVector
		     l(const doubleSubMatrix& L) const;
  const
  doubleVector
		    lp(const doubleSubMatrix& L,
		       const offsetSubVector& p) const;
  const
  doubleVector
		   pld(const offsetSubVector& p,
		       const doubleSubMatrix& LD) const;
  const
  doubleVector
		    ld(const doubleSubMatrix& LD) const;
  const
  doubleVector
		   ldp(const doubleSubMatrix& LD,
		       const offsetSubVector& p) const;
  const
  doubleVector
		    pu(const offsetSubVector& p,
		       const doubleSubMatrix& U) const;
  const
  doubleVector
		     u(const doubleSubMatrix& U) const;
  const
  doubleVector
		    up(const doubleSubMatrix& U,
		       const offsetSubVector& p) const;
  const
  doubleVector
		   pdu(const offsetSubVector& p,
		       const doubleSubMatrix& DU) const;
  const
  doubleVector
		    du(const doubleSubMatrix& DU) const;
  const
  doubleVector
		   dup(const doubleSubMatrix& DU,
		       const offsetSubVector& p) const;

  const
  doubleVector
		    pl_(const offsetSubVector& p,
		       const doubleSubMatrix& L) const;
  const
  doubleVector
		     l_(const doubleSubMatrix& L) const;
  const
  doubleVector
		    lp_(const doubleSubMatrix& L,
		       const offsetSubVector& p) const;
  const
  doubleVector
		   pld_(const offsetSubVector& p,
		       const doubleSubMatrix& LD) const;
  const
  doubleVector
		    ld_(const doubleSubMatrix& LD) const;
  const
  doubleVector
		   ldp_(const doubleSubMatrix& LD,
		       const offsetSubVector& p) const;
  const
  doubleVector
		    pu_(const offsetSubVector& p,
		       const doubleSubMatrix& U) const;
  const
  doubleVector
		     u_(const doubleSubMatrix& U) const;
  const
  doubleVector
		    up_(const doubleSubMatrix& U,
		       const offsetSubVector& p) const;
  const
  doubleVector
		   pdu_(const offsetSubVector& p,
		       const doubleSubMatrix& DU) const;
  const
  doubleVector
		    du_(const doubleSubMatrix& DU) const;
  const
  doubleVector
		   dup_(const doubleSubMatrix& DU,
		       const offsetSubVector& p) const;
  const
  doubleVector
		    pq(const offsetSubVector& p,
		       const doubleSubMatrix& L) const;
  const
  doubleVector
		     q(const doubleSubMatrix& L) const;

  const
  boolVector
		    lt(const double& s) const;
  const
  boolVector
		    le(const double& s) const;
  const
  boolVector
		    gt(const double& s) const;
  const
  boolVector
		    ge(const double& s) const;
  const
  boolVector
		    lt(const doubleSubVector& w) const;
  const
  boolVector
		    le(const doubleSubVector& w) const;
  const
  boolVector
		    gt(const doubleSubVector& w) const;
  const
  boolVector
		    ge(const doubleSubVector& w) const;
  const
  boolVector
		    eq(const double& s) const;
  const
  boolVector
		    ne(const double& s) const;
  const
  boolVector
		    eq(const doubleComplexSubVector& w) const;
  const
  boolVector
		    ne(const doubleComplexSubVector& w) const;
  const
  boolVector
		    eq(const doubleSubVector& w) const;
  const
  boolVector
		    ne(const doubleSubVector& w) const;
  Extent	 zeros(void) const;
  const
  offsetVector
		 index(void) const;

  const
  doubleVector
		gather(const offsetSubVector& x) const;
  doubleSubVector&
	       scatter(const offsetSubVector& x,
		       const doubleSubVector& t);
  const
  doubleVector
		 aside(const doubleSubVector& w) const;
  const
  doubleMatrix
		 above(const doubleSubMatrix& M) const;
  const
  doubleMatrix
		 above(const doubleSubVector& w) const;
  const
  doubleVector
		 above_(const doubleSubVector& w) const;
  const
  doubleMatrix
		 aside_(const doubleSubVector& w) const;
  const
  doubleMatrix
		 aside_(const doubleSubMatrix& M) const;
  const
  doubleVector
		  kron(const doubleSubVector& w) const;
  const
  doubleMatrix
		  kron(const doubleSubMatrix& M) const;
  const
  doubleTensor
		  kron(const doubleSubTensor& T) const;
  const
  doubleComplexVector
		  kron(const doubleComplexSubVector& w) const;
  const
  doubleComplexMatrix
		  kron(const doubleComplexSubMatrix& M) const;
  const
  doubleComplexTensor
		  kron(const doubleComplexSubTensor& T) const;
  const
  doubleVector
		 apply(const double (*f)(const double&)) const;
  const
  doubleVector
		 apply(      double (*f)(const double&)) const;
  const
  doubleVector
		 apply(      double (*f)(      double )) const;

  // Operators
  doubleSubScalar
		operator [](Offset j);
  const
  doubleSubScalar
		operator [](Offset j) const;
  doubleSubScalar
		operator ()(Offset j);
  const
  doubleSubScalar
		operator ()(Offset j) const;
  const
  doubleVector
		operator -(void) const;
//  const
//  doubleSubVector
//		operator +(void) const;
  const
  doubleVector
		operator *(const double& s) const;
  const
  doubleVector
		operator /(const double& s) const;
  const
  doubleVector
		operator +(const double& s) const;
  const
  doubleVector
		operator -(const double& s) const;
  const
  doubleComplexVector
		operator *(const doubleComplex& s) const;
  const
  doubleComplexVector
		operator /(const doubleComplex& s) const;
  const
  doubleComplexVector
		operator +(const doubleComplex& s) const;
  const
  doubleComplexVector
		operator -(const doubleComplex& s) const;
  const
  doubleComplexVector
		operator *(const doubleComplexSubVector& w) const;
  const
  doubleComplexVector
		operator /(const doubleComplexSubVector& w) const;
  const
  doubleComplexVector
		operator +(const doubleComplexSubVector& w) const;
  const
  doubleComplexVector
		operator -(const doubleComplexSubVector& w) const;
  const
  doubleVector
		operator *(const doubleSubVector& w) const;
  const
  doubleVector
		operator /(const doubleSubVector& w) const;
  const
  doubleVector
		operator +(const doubleSubVector& w) const;
  const
  doubleVector
		operator -(const doubleSubVector& w) const;

  bool		operator < (const double& s) const;
  bool		operator <=(const double& s) const;
  bool		operator > (const double& s) const;
  bool		operator >=(const double& s) const;
  bool		operator < (const doubleSubVector& w) const;
  bool		operator <=(const doubleSubVector& w) const;
  bool		operator > (const doubleSubVector& w) const;
  bool		operator >=(const doubleSubVector& w) const;
  bool		operator ==(const double& s) const;
  bool		operator !=(const double& s) const;
  bool		operator ==(const doubleComplex& s) const;
  bool		operator !=(const doubleComplex& s) const;
  bool		operator ==(const doubleComplexSubVector& w) const;
  bool		operator !=(const doubleComplexSubVector& w) const;
  bool		operator ==(const doubleSubVector& w) const;
  bool		operator !=(const doubleSubVector& w) const;
  doubleSubVector&
		operator  =(const double& s);
  doubleSubVector&
		operator *=(const double& s);
  doubleSubVector&
		operator /=(const double& s);
  doubleSubVector&
		operator +=(const double& s);
  doubleSubVector&
		operator -=(const double& s);

  doubleSubVector&
		operator  =(const doubleSubVector& w);
  doubleSubVector&
		operator *=(const doubleSubVector& w);
  doubleSubVector&
		operator /=(const doubleSubVector& w);
  doubleSubVector&
		operator +=(const doubleSubVector& w);
  doubleSubVector&
		operator -=(const doubleSubVector& w);
  friend class doubleVector;
  };

// Constructors
inline		doubleSubVector::doubleSubVector(void):
  H((double*)0), O((Offset)0), N((Extent)0), S((Stride)0) { }
inline		doubleSubVector::doubleSubVector(
    const doubleHandle& h, Offset o, Extent n, Stride s):
  H(h), O(o), N(n), S(s) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleSubVector::doubleSubVector(\n"
    "const doubleHandle&, Offset, Extent, Stride)", o, n, s);
#endif // SVMT_DEBUG_MODE
  }
inline		doubleSubVector::doubleSubVector(
    const doubleSubVector& v): H(v.H), O(v.O), N(v.N), S(v.S) { }
inline		doubleSubVector::~doubleSubVector(void) { }

// Functions
inline
Extent&		doubleSubVector::columns(void) { return C; }
inline
const
doubleHandle&
		doubleSubVector::handle(void) const { return H; }
inline
Offset		doubleSubVector::offset(void) const { return O; }
inline
Extent		doubleSubVector::extent(void) const { return N; }
inline
Extent    doubleSubVector::size(void) const { return extent(); }
inline
Extent		doubleSubVector::length(void) const { return N; }
inline
Stride		doubleSubVector::stride(void) const { return S; }
inline
bool		doubleSubVector::empty(void) const {
  return handle().empty() || 0 == extent(); }
inline
doubleHandle
		doubleSubVector::allocate(Extent n) {
  return doubleHandle(new double[n]); }
inline
doubleSubVector&
		doubleSubVector::free(void) {
  delete [] (double*)H; return *this; }
inline
doubleSubVector&
		doubleSubVector::resize(void) {
  H = doubleHandle((double*)0);
  O = (Offset)0; N = (Extent)0; S = (Stride)0; return *this; }
inline
doubleSubVector&
		doubleSubVector::resize(const doubleHandle& h,
    Offset o, Extent n, Stride s) { H = h; O = o; N = n; S = s;
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleSubVector::resize(const doubleHandle&,\n"
    "Offset, Extent, Stride)", o, n, s);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
doubleSubVector&
		doubleSubVector::resize(
  const doubleSubVector& v) {
  return resize(v.handle(), v.offset(), v.extent(), v.stride()); }

inline
bool		doubleSubVector::contains(
    Offset j, Extent n, Stride s) const {
  return boolSubVector::contains(j, n, s, extent()); }
inline
bool		doubleSubVector::contains_(
    Offset j, Extent n, Stride s) const {
  return contains(j-1, n, s); }
inline
doubleSubVector
		doubleSubVector::sub(
    Offset j, Extent n, Stride s) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleSubVector::sub(Offset, Extent, Stride)",
    j, n, s, extent());
#endif // SVMT_DEBUG_MODE
  return doubleSubVector(handle(), offset() + (Stride)j*stride(),
    n, s*stride()); }
inline
const
doubleSubVector
		doubleSubVector::sub(
    Offset j, Extent n, Stride s) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleSubVector::sub(Offset, Extent, Stride) const",
    j, n, s, extent());
#endif // SVMT_DEBUG_MODE
  return doubleSubVector(handle(), offset() + (Stride)j*stride(),
    n, s*stride()); }
inline
doubleSubVector
		doubleSubVector::sub_(
    Offset j, Extent n, Stride s) { return sub(j-1, n, s); }
inline
const
doubleSubVector
		doubleSubVector::sub_(
    Offset j, Extent n, Stride s) const { return sub(j-1, n, s); }
inline
const
doubleSubVector
		doubleSubScalar::subvector(Extent n) const {
  return doubleSubVector(handle(), offset(), n, (Stride)0); }
inline
doubleSubVector
		doubleSubVector::r(void) {
  return doubleSubVector(handle(),
    offset() + (Stride)(extent() - 1)*stride(), extent(), -stride()); }
inline
const
doubleSubVector
		doubleSubVector::r(void) const {
  return doubleSubVector(handle(),
    offset() + (Stride)(extent() - 1)*stride(), extent(), -stride()); }

inline
doubleSubVector
		doubleSubVector::even(void) {
  return doubleSubVector(handle(), offset(),
    (extent() + 1) >> 1, stride() << 1); }
inline
const
doubleSubVector
		doubleSubVector::even(void) const {
  return doubleSubVector(handle(), offset(),
    (extent() + 1) >> 1, stride() << 1); }
inline
doubleSubVector
		doubleSubVector::odd(void) {
  return doubleSubVector(handle(), offset() + stride(),
    extent() >> 1, stride() << 1); }
inline
const
doubleSubVector
		doubleSubVector::odd(void) const {
  return doubleSubVector(handle(), offset() + stride(),
    extent() >> 1, stride() << 1); }
inline
doubleSubVector
		doubleSubVector::even_(void) { return odd(); }
inline
const
doubleSubVector
		doubleSubVector::even_(void) const { return odd(); }
inline
doubleSubVector
		doubleSubVector::odd_(void) { return even(); }
inline
const
doubleSubVector
		doubleSubVector::odd_(void) const { return even(); }

inline
Offset		   doubleSubVector::min_(void) const {
  return 1 + min(); }
inline
Offset		   doubleSubVector::max_(void) const {
  return 1 + max(); }
inline
double		   min(const doubleSubVector& v) {
  return v.handle().get(v.offset() + (Stride)v.min()*v.stride()); }
inline
double		   max(const doubleSubVector& v) {
  return v.handle().get(v.offset() + (Stride)v.max()*v.stride()); }
const
doubleVector
		   min(const doubleSubVector& v,
		       const doubleSubVector& w);
const
doubleVector
		   max(const doubleSubVector& v,
		       const doubleSubVector& w);
const
doubleVector
		   sgn(const doubleSubVector& v);
const
doubleVector
		   abs(const doubleSubVector& v);
const
doubleVector
		 floor(const doubleSubVector& v);
const
doubleVector
		  ceil(const doubleSubVector& v);
const
doubleVector
		 hypot(const doubleSubVector& v,
		       const doubleSubVector& w);
const
doubleVector
		 atan2(const doubleSubVector& w,
		       const doubleSubVector& v);

const
doubleVector
		   log(const doubleSubVector& v);
const
doubleVector
		   exp(const doubleSubVector& v);
const
doubleVector
		  sqrt(const doubleSubVector& v);
const
doubleVector
		   cos(const doubleSubVector& v);
const
doubleVector
		   sin(const doubleSubVector& v);
const
doubleVector
		   tan(const doubleSubVector& v);
const
doubleVector
		  acos(const doubleSubVector& v);
const
doubleVector
		  asin(const doubleSubVector& v);
const
doubleVector
		  atan(const doubleSubVector& v);
const
doubleVector
		  cosh(const doubleSubVector& v);
const
doubleVector
		  sinh(const doubleSubVector& v);
const
doubleVector
		  tanh(const doubleSubVector& v);
const
doubleVector
		 acosh(const doubleSubVector& v);
const
doubleVector
		 asinh(const doubleSubVector& v);
const
doubleVector
		 atanh(const doubleSubVector& v);

// Operators
inline
doubleSubScalar
		doubleSubVector::operator [](Offset j) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleSubVector::operator [](Offset)", j, extent());
#endif // SVMT_DEBUG_MODE
  return doubleSubScalar(handle(), offset() + (Stride)j*stride()); }
inline
const
doubleSubScalar
		doubleSubVector::operator [](Offset j) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleSubVector::operator [](Offset) const", j, extent());
#endif // SVMT_DEBUG_MODE
  return doubleSubScalar(handle(), offset() + (Stride)j*stride()); }
inline
doubleSubScalar
		doubleSubVector::operator ()(Offset j) {
  return operator [](j-1); }
inline
const
doubleSubScalar
		doubleSubVector::operator ()(Offset j) const {
  return operator [](j-1); }
inline
doubleSubVector&
		doubleSubVector::swap(Offset j, Offset k) {
  doubleSubVector&	v = *this;
  double t = v[j]; v[j] = v[k]; v[k] = t; return v; }
inline
doubleSubVector&
		doubleSubVector::swap_(Offset j, Offset k) {
  return swap(j-1, k-1); }
//inline
//const
//doubleSubVector
//		doubleSubVector::operator +(void) const {
//  return *this; }
std::istream&	operator >>(std::istream& s,       doubleSubVector& v);
std::ostream&	operator <<(std::ostream& s, const doubleSubVector& v);
inline
bool		doubleSubVector::operator > (
    const doubleSubVector& w) const { return w <  *this; }
inline
bool		doubleSubVector::operator >=(
    const doubleSubVector& w) const { return w <= *this; }

class doubleSubArray1: public doubleSubVector {
  public:
  // Constructors
		doubleSubArray1(void);
		doubleSubArray1(double* p, Offset o,
      Extent n, Stride s);
		doubleSubArray1(const doubleSubArray1& v);
		~doubleSubArray1(void);
  // Functions
  doubleSubArray1&
		resize(void);
  doubleSubArray1&		// resize from pointer
		resize(double* p, Offset o, Extent n, Stride s);
  doubleSubArray1&		// resize from SubArray1
		resize(const doubleSubArray1& v);
  private:
  doubleSubArray1&		// prevent resize from Handle
		resize(const doubleHandle& h,
      Offset o, Extent n, Stride s);
  doubleSubArray1&		// prevent resize from SubVector
		resize(const doubleSubVector& v);
  public:
  doubleSubArray1
		   sub(Offset j, Extent n, Stride s);
  const
  doubleSubArray1
		   sub(Offset j, Extent n, Stride s) const;
  doubleSubArray1
		   sub_(Offset j, Extent n, Stride s);
  const
  doubleSubArray1
		   sub_(Offset j, Extent n, Stride s) const;
  const
  doubleSubArray2
	     submatrix(Extent m = 1) const;
  const
  doubleSubArray3
	     subtensor(Extent l = 1, Extent m = 1) const;
  doubleSubArray1
		     r(void);
  const
  doubleSubArray1
		     r(void) const;
  doubleSubArray1&
	       reverse(void);

  doubleSubArray1
		  even(void);
  const
  doubleSubArray1
		  even(void) const;
  doubleSubArray1
		   odd(void);
  const
  doubleSubArray1
		   odd(void) const;
  doubleSubArray1
		  even_(void);
  const
  doubleSubArray1
		  even_(void) const;
  doubleSubArray1
		   odd_(void);
  const
  doubleSubArray1
		   odd_(void) const;
  doubleSubArray1&
		  rdft(int sign = -1);	// complex to real    dft
  doubleSubArray1&
		  cdft(int sign = -1);	// real    to complex dft
  doubleSubArray1&
		  ramp(void);
  doubleSubArray1&
		  swap(Offset j, Offset k);
  doubleSubArray1&
		  swap(doubleSubVector& w);
  doubleSubArray1&
		  swap_(Offset j, Offset k);
  doubleSubArray1&
		rotate(Stride n);
  doubleSubArray1&
		 shift(Stride n,const double& s
		  = (double)0);
  doubleSubArray1&
	       scatter(const offsetSubVector& x,
		       const doubleSubVector& t);

  // Operators
  doubleSubArray0
		operator [](Offset j);
  const
  doubleSubArray0
		operator [](Offset j) const;
  doubleSubArray0
		operator ()(Offset j);
  const
  doubleSubArray0
		operator ()(Offset j) const;
//  const
//  doubleSubArray1
//		operator +(void) const;
  doubleSubArray1&
		operator  =(const double& s);
  doubleSubArray1&
		operator *=(const double& s);
  doubleSubArray1&
		operator /=(const double& s);
  doubleSubArray1&
		operator +=(const double& s);
  doubleSubArray1&
		operator -=(const double& s);
  doubleSubArray1&
		operator  =(const doubleSubVector& w);
  doubleSubArray1&
		operator *=(const doubleSubVector& w);
  doubleSubArray1&
		operator /=(const doubleSubVector& w);
  doubleSubArray1&
		operator +=(const doubleSubVector& w);
  doubleSubArray1&
		operator -=(const doubleSubVector& w);
  };

// Constructors
inline		doubleSubArray1::doubleSubArray1(void):
  doubleSubVector() { }
inline		doubleSubArray1::doubleSubArray1(double* p,
    Offset o, Extent n, Stride s):
  doubleSubVector(doubleHandle(p), o, n, s) { }
inline		doubleSubArray1::doubleSubArray1(
    const doubleSubArray1& v): doubleSubVector(v) { }
inline		doubleSubArray1::~doubleSubArray1(void) { }

// Functions
inline
doubleSubArray1&
		doubleSubArray1::resize(void) {
  doubleSubVector::resize(); return *this; }
inline
doubleSubArray1&
		doubleSubArray1::resize(double* p,
    Offset o, Extent n, Stride s) {
  doubleSubVector::resize(doubleHandle(p), o, n, s);
  return *this; }
inline
doubleSubArray1&
		doubleSubArray1::resize(
    const doubleSubArray1& v) {
  doubleSubVector::resize(v); return *this; }
inline
doubleSubArray1
		doubleSubArray1::sub(
    Offset j, Extent n, Stride s) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleSubArray1::sub(Offset, Extent, Stride)",
    j, n, s, extent());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray1((double*)(doubleHandle&)handle(),
    offset() + (Stride)j*stride(), n, s*stride()); }
inline
const
doubleSubArray1
		doubleSubArray1::sub(
    Offset j, Extent n, Stride s) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleSubArray1::sub(Offset, Extent, Stride) const",
    j, n, s, extent());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray1((double*)(doubleHandle&)handle(),
    offset() + (Stride)j*stride(), n, s*stride()); }
inline
doubleSubArray1
		doubleSubArray1::sub_(
    Offset j, Extent n, Stride s) { return sub(j-1, n, s); }
inline
const
doubleSubArray1
		doubleSubArray1::sub_(
    Offset j, Extent n, Stride s) const { return sub(j-1, n, s); }

inline
const
doubleSubArray1
		doubleSubArray0::subvector(Extent n) const {
  return doubleSubArray1((double*)(doubleHandle&)handle(),
    offset(), n, (Stride)0); }
inline
doubleSubArray1
		doubleSubArray1::r(void) {
  return doubleSubArray1((double*)(doubleHandle&)handle(),
    offset() + (Stride)(extent() - 1)*stride(), extent(), -stride()); }
inline
const
doubleSubArray1
		doubleSubArray1::r(void) const {
  return doubleSubArray1((double*)(doubleHandle&)handle(),
    offset() + (Stride)(extent() - 1)*stride(), extent(), -stride()); }
inline
doubleSubArray1&
		doubleSubArray1::reverse(void) {
  doubleSubVector::reverse(); return *this; }
inline
doubleSubArray1
		doubleSubArray1::even(void) {
  return doubleSubArray1((double*)(doubleHandle&)handle(),
    offset(), (extent() + 1) >> 1, stride() << 1); }
inline
const
doubleSubArray1
		doubleSubArray1::even(void) const {
  return doubleSubArray1((double*)(doubleHandle&)handle(),
    offset(), (extent() + 1) >> 1, stride() << 1); }
inline
doubleSubArray1
		doubleSubArray1::odd(void) {
  return doubleSubArray1((double*)(doubleHandle&)handle(),
    offset() + stride(), extent() >> 1, stride() << 1); }
inline
const
doubleSubArray1
		doubleSubArray1::odd(void) const {
  return doubleSubArray1((double*)(doubleHandle&)handle(),
    offset() + stride(), extent() >> 1, stride() << 1); }
inline
doubleSubArray1
		doubleSubArray1::even_(void) { return odd(); }
inline
const
doubleSubArray1
		doubleSubArray1::even_(void) const { return odd(); }
inline
doubleSubArray1
		doubleSubArray1::odd_(void) { return even(); }
inline
const
doubleSubArray1
		doubleSubArray1::odd_(void) const { return even(); }

inline
doubleSubArray1&
		doubleSubArray1::rdft(int sign) {
  doubleSubVector::rdft(sign); return *this; }
inline
doubleSubArray1&
		doubleSubArray1::cdft(int sign) {
  doubleSubVector::cdft(sign); return *this; }
inline
doubleSubArray1&
		doubleSubArray1::ramp(void) {
  doubleSubVector::ramp(); return *this; }

inline
doubleSubArray1&
		doubleSubArray1::swap(Offset j, Offset k) {
  doubleSubVector::swap(j, k); return *this; }
inline
doubleSubArray1&
		doubleSubArray1::swap(doubleSubVector& w) {
  doubleSubVector::swap(w); return *this; }
inline
doubleSubArray1&
		doubleSubArray1::swap_(Offset j, Offset k) {
  return swap(j-1, k-1); }
inline
doubleSubArray1&
		doubleSubArray1::rotate(Stride n) {
  doubleSubVector::rotate(n); return *this; }
inline
doubleSubArray1&
		doubleSubArray1::shift(Stride n,const double& s) {
  doubleSubVector::shift(n,s); return *this; }

// doubleSubVector function definitions
// which use doubleSubArray1
inline
const
boolVector
		doubleSubVector::lt(const double& s) const {
  return lt(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
const
boolVector
		doubleSubVector::le(const double& s) const {
  return le(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
const
boolVector
		doubleSubVector::gt(const double& s) const {
  return doubleSubArray1((double*)&s, (Offset)0,
    extent(), (Stride)0).lt(*this); }
inline
const
boolVector
		doubleSubVector::ge(const double& s) const {
  return doubleSubArray1((double*)&s, (Offset)0,
    extent(), (Stride)0).le(*this); }
inline
const
boolVector
		doubleSubVector::gt(
    const doubleSubVector& w) const { return w.lt(*this); }
inline
const
boolVector
		doubleSubVector::ge(
    const doubleSubVector& w) const { return w.le(*this); }

inline
const
boolVector
		doubleSubVector::eq(const double& s) const {
  return eq(doubleSubArray1((double*)&s, (Offset)0, extent(), (Stride)0)); }
inline
const
boolVector
		doubleSubVector::ne(const double& s) const {
  return ne(doubleSubArray1((double*)&s, (Offset)0, extent(), (Stride)0)); }
inline
doubleSubArray1&
	       doubleSubArray1::scatter(
  const offsetSubVector& x, const doubleSubVector& t) {
  doubleSubVector::scatter(x, t); return *this; }

// Operators
inline
doubleSubArray0
		doubleSubArray1::operator [](Offset j) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleSubArray1::operator [](Offset)", j, extent());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray0((double*)(doubleHandle&)handle(),
    offset() + (Stride)j*stride()); }
inline
const
doubleSubArray0
		doubleSubArray1::operator [](Offset j) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleSubArray1::operator [](Offset) const", j, extent());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray0((double*)(doubleHandle&)handle(),
    offset() + (Stride)j*stride()); }
inline
doubleSubArray0
		doubleSubArray1::operator ()(Offset j) {
  return operator [](j-1); }
inline
const
doubleSubArray0
		doubleSubArray1::operator ()(Offset j) const {
  return operator [](j-1); }

//inline
//const
//doubleSubArray1
//		doubleSubArray1::operator +(void) const {
//  return *this; }
inline
doubleSubArray1&
		doubleSubArray1::operator  =(const double& s) {
  doubleSubVector::operator  =(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); return *this; }
inline
doubleSubArray1&
		doubleSubArray1::operator *=(const double& s) {
  doubleSubVector::operator *=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); return *this; }
inline
doubleSubArray1&
		doubleSubArray1::operator /=(const double& s) {
  doubleSubVector::operator /=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); return *this; }
inline
doubleSubArray1&
		doubleSubArray1::operator +=(const double& s) {
  doubleSubVector::operator +=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); return *this; }
inline
doubleSubArray1&
		doubleSubArray1::operator -=(const double& s) {
  doubleSubVector::operator -=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); return *this; }

inline
doubleSubArray1&
		doubleSubArray1::operator  =(
    const doubleSubVector& w) {
  doubleSubVector::operator  =(w); return *this; }
inline
doubleSubArray1&
		doubleSubArray1::operator *=(
    const doubleSubVector& w) {
  doubleSubVector::operator *=(w); return *this; }
inline
doubleSubArray1&
		doubleSubArray1::operator /=(
    const doubleSubVector& w) {
  doubleSubVector::operator /=(w); return *this; }
inline
doubleSubArray1&
		doubleSubArray1::operator +=(
    const doubleSubVector& w) {
  doubleSubVector::operator +=(w); return *this; }
inline
doubleSubArray1&
		doubleSubArray1::operator -=(
    const doubleSubVector& w) {
  doubleSubVector::operator -=(w); return *this; }


// doubleSubVector operator definitions
// which use doubleSubArray1
inline
bool		doubleSubVector::operator < (const double& s) const {
  return operator < (doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
bool		doubleSubVector::operator <=(const double& s) const {
  return operator <=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
bool		doubleSubVector::operator > (const double& s) const {
  return operator > (doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
bool		doubleSubVector::operator >=(const double& s) const {
  return operator >=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
bool		operator < (const double& s,
    const doubleSubVector& v) { return v >  s; }
inline
bool		operator <=(const double& s,
    const doubleSubVector& v) { return v >= s; }
inline
bool		operator > (const double& s,
    const doubleSubVector& v) { return v <  s; }
inline
bool		operator >=(const double& s,
    const doubleSubVector& v) { return v <= s; }

inline
bool		doubleSubVector::operator ==(const double& s) const {
  return operator ==(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
bool		doubleSubVector::operator !=(const double& s) const {
  return operator !=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
bool		operator ==(const double& s,
    const doubleSubVector& v) { return v == s; }
inline
bool		operator !=(const double& s,
    const doubleSubVector& v) { return v != s; }

inline
doubleSubVector&
		doubleSubVector::operator  =(const double& s) {
  return operator  =(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
doubleSubVector&
		doubleSubVector::operator *=(const double& s) {
  return operator *=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
doubleSubVector&
		doubleSubVector::operator /=(const double& s) {
  return operator /=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
doubleSubVector&
		doubleSubVector::operator +=(const double& s) {
  return operator +=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
doubleSubVector&
		doubleSubVector::operator -=(const double& s) {
  return operator -=(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }


class doubleVector: public doubleSubVector {
  public:
  // Constructors
		doubleVector(void);
  explicit	doubleVector(Extent n);
		doubleVector(Extent n, const double& s);
		doubleVector(Extent n, const double& s, const double& t);
		doubleVector(const doubleSubVector& v);
		doubleVector(const doubleVector& v);
		~doubleVector(void);
  // Functions
  doubleVector&
		resize(void);
  doubleVector&
		resize(Extent n);
  doubleVector&
		resize(Extent n, const double& s);
  doubleVector&
		resize(Extent n, const double& s, const double& t);
  doubleVector&
		resize(const doubleSubVector& v);
private:
  doubleVector&
		resize(const doubleHandle& h,
      Offset o, Extent n, Stride s);
  doubleVector&
		free(void) { doubleSubVector::free(); return *this; }
public:
  // Operators
  doubleVector&
		operator  =(const double& s);
  doubleVector&
		operator  =(const doubleSubVector& w);
  doubleVector&
		operator  =(const doubleVector& w);
  };

// Constructors
inline		doubleVector::doubleVector(void):
  doubleSubVector() { }
inline		doubleVector::doubleVector(Extent n):
  doubleSubVector(allocate(n), (Offset)0, n, (Stride)1) {
#ifdef SVMT_DEBUG_MODE
  doubleSubVector::operator =(doubleBad);
#endif // SVMT_DEBUG_MODE
  }
inline		doubleVector::doubleVector(
    Extent n, const double& s):
  doubleSubVector(allocate(n), (Offset)0, n, (Stride)1) {
    doubleSubVector::operator  =(s); }
inline		doubleVector::doubleVector(
    Extent n, const double& s, const double& t):
  doubleSubVector(allocate(n), (Offset)0, n, (Stride)1) {
    ramp();
    doubleSubVector::operator *=(t);
    doubleSubVector::operator +=(s); }
inline		doubleVector::doubleVector(
    const doubleSubVector& v):
  doubleSubVector(
      allocate(v.extent()), (Offset)0, v.extent(), (Stride)1) {
  doubleSubVector::operator  =(v); }
inline		doubleVector::doubleVector(
    const doubleVector& v):
  doubleSubVector(
      allocate(v.extent()), (Offset)0, v.extent(), (Stride)1) {
  doubleSubVector::operator  =(v); }
inline		doubleVector::~doubleVector(void) { free(); }

// Assignment Operators
inline
doubleVector&	doubleVector::operator  =(const double& s) {
  doubleSubVector::operator =(s); return *this; }
inline
doubleVector&	doubleVector::operator  =(
    const doubleSubVector& w) {
  doubleSubVector::operator =(w); return *this; }
inline
doubleVector&	doubleVector::operator  =(
    const doubleVector& w) {
  doubleSubVector::operator =(w); return *this; }

// Functions
inline
doubleVector&
		doubleVector::resize(void) { free();
  doubleSubVector::resize(); return *this; }
inline
doubleVector&
		doubleVector::resize(Extent n) { free();
  doubleSubVector::resize(allocate(n), (Offset)0, n, (Stride)1);
#ifdef SVMT_DEBUG_MODE
  *this = doubleBad;
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
doubleVector&
		doubleVector::resize(
    Extent n, const double& s) { return resize(n) = s; }
inline
doubleVector&
		doubleVector::resize(
    Extent n, const double& s, const double& t) {
  resize(n); ramp(); 
  doubleSubVector::operator *=(t);
  doubleSubVector::operator +=(s);
  return *this;
  }
inline
doubleVector&
		doubleVector::resize(
    const doubleSubVector& v) { return resize(v.extent()) = v; }
inline
double		doubleSubVector::dot_(
    const doubleSubVector& w) const { return dot(w); }
inline
double		doubleSubVector::dot(void) const { return dot(*this); }

inline
const
doubleVector	doubleSubVector::pl_(
    const offsetSubVector& p, const doubleSubMatrix& L) const {
  return up(L, p); }
inline
const
doubleVector	doubleSubVector::l_(
    const doubleSubMatrix& L) const { return u(L); }
inline
const
doubleVector	doubleSubVector::lp_(
    const doubleSubMatrix& L, const offsetSubVector& p) const {
  return pu(p, L); }
inline
const
doubleVector	doubleSubVector::pld_(
    const offsetSubVector& p, const doubleSubMatrix& LD) const {
  return dup(LD, p); }
inline
const
doubleVector	doubleSubVector::ld_(
    const doubleSubMatrix& LD) const { return du(LD); }
inline
const
doubleVector	doubleSubVector::ldp_(
    const doubleSubMatrix& LD, const offsetSubVector& p) const {
  return pdu(p, LD); }
inline
const
doubleVector	doubleSubVector::pu_(
    const offsetSubVector& p, const doubleSubMatrix& U) const {
  return lp(U, p); }
inline
const
doubleVector	doubleSubVector::u_(
    const doubleSubMatrix& U) const { return l(U); }
inline
const
doubleVector	doubleSubVector::up_(
    const doubleSubMatrix& U, const offsetSubVector& p) const {
  return pl(p, U); }
inline
const
doubleVector	doubleSubVector::pdu_(
    const offsetSubVector& p, const doubleSubMatrix& DU) const {
  return ldp(DU, p); }
inline
const
doubleVector	doubleSubVector::du_(
    const doubleSubMatrix& DU) const { return ld(DU); }
inline
const
doubleVector	doubleSubVector::dup_(
    const doubleSubMatrix& DU, const offsetSubVector& p) const {
  return pld(p, DU); }
inline
const
doubleVector
		doubleSubVector::above_(
    const doubleSubVector& v) const { return aside(v); }

inline
const
doubleVector
		doubleSubVector::operator *(const double& s) const {
  return operator *(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
const
doubleVector
		doubleSubVector::operator /(const double& s) const {
  return operator /(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
const
doubleVector
		doubleSubVector::operator +(const double& s) const {
  return operator +(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
const
doubleVector
		doubleSubVector::operator -(const double& s) const {
  return operator -(doubleSubArray1((double*)&s, (Offset)0,
      extent(), (Stride)0)); }
inline
const
doubleVector
		operator *(const double& s, const doubleSubVector& v) {
  return doubleSubArray1((double*)&s, (Offset)0, v.extent(), (Stride)0)*v; }
inline
const
doubleVector
		operator /(const double& s, const doubleSubVector& v) {
  return doubleSubArray1((double*)&s, (Offset)0, v.extent(), (Stride)0)/v; }

inline
const
doubleVector
		operator +(const double& s, const doubleSubVector& v) {
  return doubleSubArray1((double*)&s, (Offset)0, v.extent(), (Stride)0) + v; }
inline
const
doubleVector
		operator -(const double& s, const doubleSubVector& v) {
  return doubleSubArray1((double*)&s, (Offset)0, v.extent(), (Stride)0) - v; }


#endif /* _doubleVector_h */

