#ifndef _doubleScalar_h
#define _doubleScalar_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleHandle.h>
#include<iostream>
class doubleSubVector;	class doubleVector;
class doubleSubArray1;
class doubleSubSquare;	class doubleSquare;
class doubleSubMatrix;	class doubleMatrix;
class doubleSubArray2;
class doubleSubTensor;	class doubleTensor;
class doubleSubArray3;
class doubleComplex;
class doubleComplexSubVector;	class doubleComplexVector;
class doubleComplexSubArray1;
class doubleComplexSubSquare;	class doubleComplexSquare;
class doubleComplexSubMatrix;	class doubleComplexMatrix;
class doubleComplexSubArray2;
class doubleComplexSubTensor;	class doubleComplexTensor;
class doubleComplexSubArray3;

class doubleSubScalar {
  private:
  // Representation
  doubleHandle
		H;
  Offset	O;
  public:
  // Constructors
		doubleSubScalar(
    const doubleHandle& h, Offset o);
		doubleSubScalar(
    const doubleSubScalar& s);
		~doubleSubScalar(void);
  // Functions
  const
  doubleHandle&
		handle(void) const;
  Offset	offset(void) const;
  bool		 empty(void) const;
  const
  doubleSubVector
	     subvector(Extent n = 1) const;
  const
  doubleSubMatrix
	     submatrix(Extent n = 1, Extent m = 1) const;
  const
  doubleSubTensor
	     subtensor(Extent n = 1, Extent m = 1, Extent l = 1) const;

  // Operators
  double		operator -(void) const;
		operator double (void) const;
  doubleSubScalar&
		operator  =(const double& s);
  doubleSubScalar&
		operator *=(const double& s);
  doubleSubScalar&
		operator /=(const double& s);
  doubleSubScalar&
		operator +=(const double& s);
  doubleSubScalar&
		operator -=(const double& s);
  doubleSubScalar&
		operator  =(const doubleSubScalar& s);
  };

// Constructors
inline		doubleSubScalar::doubleSubScalar(
  const doubleHandle& h, Offset o): H(h), O(o) { }
inline		doubleSubScalar::doubleSubScalar(
  const doubleSubScalar& s): H(s.H), O(s.O) { }
inline		doubleSubScalar::~doubleSubScalar(void) { }

// Functions
inline
const
doubleHandle&
		doubleSubScalar::handle(void) const { return H; }
inline
Offset		doubleSubScalar::offset(void) const { return O; }
inline
bool		doubleSubScalar::empty(void) const {
  return handle().empty(); }

// Operators
inline
double		doubleSubScalar::operator -(void) const {
  return -H.get(offset()); }
inline		doubleSubScalar::operator double (void) const {
  return H.get(offset()); }
inline
std::istream&	operator >>(std::istream& is,       doubleSubScalar& s) {
  double		x;
  if (is >> x)
    ((doubleHandle)s.handle()).put(s.offset(), x);
  return is; }
inline
std::ostream&	operator <<(std::ostream& os, const doubleSubScalar& s) {
  return os << s.handle().get(s.offset()); }

inline
doubleSubScalar&
		doubleSubScalar::operator  =(const double& s) {
  H.put(offset(), s); return *this; }
inline
doubleSubScalar&
		doubleSubScalar::operator *=(const double& s) {
  H.put(offset(), H.get(offset())*s); return *this; }
inline
doubleSubScalar&
		doubleSubScalar::operator /=(const double& s) {
  H.put(offset(), H.get(offset())/s); return *this; }
inline
doubleSubScalar&
		doubleSubScalar::operator +=(const double& s) {
  H.put(offset(), H.get(offset()) + s); return *this; }
inline
doubleSubScalar&
		doubleSubScalar::operator -=(const double& s) {
  H.put(offset(), H.get(offset()) - s); return *this; }
inline
doubleSubScalar&
		doubleSubScalar::operator  =(
  const doubleSubScalar& s) { return operator =((double)s); }

class doubleSubArray0: public doubleSubScalar {
  public:
  // Constructors
		doubleSubArray0(double* p, Offset o);
		doubleSubArray0(const doubleSubArray0& s);
		~doubleSubArray0(void);
  // Functions
  const
  doubleSubArray1
	     subvector(Extent n = 1) const;
  const
  doubleSubArray2
	     submatrix(Extent n = 1, Extent m = 1) const;
  const
  doubleSubArray3
	     subtensor(Extent n = 1, Extent m = 1, Extent l = 1) const;
  // Operators
  doubleSubArray0&
		operator  =(const double& s);
  doubleSubArray0&
		operator *=(const double& s);
  doubleSubArray0&
		operator /=(const double& s);
  doubleSubArray0&
		operator +=(const double& s);
  doubleSubArray0&
		operator -=(const double& s);
  doubleSubArray0&
		operator  =(const doubleSubScalar& s);
  };

// Constructors
inline		doubleSubArray0::doubleSubArray0(
  double* p, Offset o): doubleSubScalar(doubleHandle(p), o) { }
inline		doubleSubArray0::doubleSubArray0(
  const doubleSubArray0& s): doubleSubScalar(s) { }
inline		doubleSubArray0::~doubleSubArray0(void) { }

// Operators
inline
doubleSubArray0&
		doubleSubArray0::operator  =(const double& s) {
  doubleSubScalar::operator  =(s); return *this; }
inline
doubleSubArray0&
		doubleSubArray0::operator *=(const double& s) {
  doubleSubScalar::operator *=(s); return *this; }
inline
doubleSubArray0&
		doubleSubArray0::operator /=(const double& s) {
  doubleSubScalar::operator /=(s); return *this; }
inline
doubleSubArray0&
		doubleSubArray0::operator +=(const double& s) {
  doubleSubScalar::operator +=(s); return *this; }
inline
doubleSubArray0&
		doubleSubArray0::operator -=(const double& s) {
  doubleSubScalar::operator -=(s); return *this; }
inline
doubleSubArray0&
		doubleSubArray0::operator  =(
  const doubleSubScalar& s) {
  doubleSubScalar::operator =(s); return *this; }

#endif /* _doubleScalar_h */

