#ifndef _svmt_typedefs_h
#define _svmt_typedefs_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<climits>

typedef unsigned int	Offset;
typedef	  signed int	Stride;
typedef unsigned int	Extent;
typedef Extent		Length;

#define MaximumOffset UINT_MAX
#define MinimumStride  INT_MIN
#define MaximumStride  INT_MAX
#define MaximumExtent UINT_MAX

inline
Extent	gcd(Extent x, Extent y) {	// greatest common denominator of x & y
  Extent	u = x;
  Extent	v = y;
  while(x != y) {
    if (x < y) {
      y -= x; v += u;
      }
    else {
      x -= y; u += v;
      }
    }
  return x;
  }

inline
Extent	lpf(Extent n) {			// least prime factor of n
  if (n&1) {				// n is odd
    Extent	p = (Extent)3;		// try the odd integers
    while(p*p < n && n%p) {		// up to sqrt(n)
      p += (Extent)2;
      }
    return n%p? n: p;
    }
  else {				// n is even
    return (Extent)2;
    }
  }

#endif /* _svmt_typedefs_h */

