#ifndef _boolArray2_h
#define _boolArray2_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<boolMatrix.h>


template<Extent m, Extent n = 1>
class boolArray2: public boolSubArray2 {
private:
  // Representation
  bool	A[m*n];
  // Functions			// prevent boolSubArray2 resize
  boolArray2<m, n>&
		resize(void);
  boolArray2<m, n>&
		resize(bool*, Offset, Extent, Stride, Extent, Stride);
  boolArray2<m, n>&
		resize_(bool*, Offset, Extent, Stride, Extent, Stride);
  boolArray2<m, n>&
		resize(const boolSubArray2&);
public:
  // Constructors
		boolArray2(void):
    boolSubArray2(A, 0, m, n, n, 1) { }
		boolArray2(const bool& s):
    boolSubArray2(A, 0, m, n, n, 1) {
    boolSubArray2::operator =(s); }
		boolArray2(const boolArray2<m, n>& T):
    boolSubArray2(A, 0, m, n, n, 1) {
    boolSubArray2::operator =(T); }
	       ~boolArray2(void) { }
  // Operators
  boolArray2<m, n>&
  operator =(const boolArray2<m, n>& T) {
    boolSubArray2::operator =(T);
    return *this; }
  };

#endif /* _boolArray2_h */

