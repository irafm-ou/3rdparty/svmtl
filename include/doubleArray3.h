#ifndef doubleArray3_h
#define doubleArray3_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleTensor.h>


template<Extent l, Extent m = 1, Extent n = 1>
class doubleArray3: public doubleSubArray3 {
private:
  // Representation
  double	A[l*m*n];
  // Functions			// prevent doubleSubArray3 resize
  doubleArray3<l, m, n>&
		resize(void);
  doubleArray3<l, m, n>&
		resize(double*, Offset, Extent, Stride, Extent, Stride, Extent, Stride);
  doubleArray3<l, m, n>&
		resize_(double*, Offset, Extent, Stride, Extent, Stride, Extent, Stride);
  doubleArray3<l, m, n>&
		resize(const doubleSubArray3&);
public:
  // Constructors
		doubleArray3(void):
    doubleSubArray3(A, 0, l, m*n, m, n, n, 1) { }
		doubleArray3(const double& s):
    doubleSubArray3(A, 0, l, m*n, m, n, n, 1) {
    doubleSubArray3::operator =(s); }
		doubleArray3(const double& s, const double& t):
    doubleSubArray3(A, 0, l, m*n, m, n, n, 1) {
    ramp();
    doubleSubArray3::operator*=(t);
    doubleSubArray3::operator+=(s); }

		doubleArray3(const doubleArray3<l, m, n>& T):
    doubleSubArray3(A, 0, l, m*n, m, n, n, 1) {
    doubleSubArray3::operator =(T); }
	       ~doubleArray3(void) { }
  // Operators
  doubleArray3<l, m, n>&
  operator =(const doubleArray3<l, m, n>& T) {
    doubleSubArray3::operator =(T);
    return *this; }
  };

#endif /* doubleArray3_h */

