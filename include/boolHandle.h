#ifndef _boolHandle_h
#define _boolHandle_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<svmt_typedefs.h>

class boolHandle {
  private:
  // Representation
  bool*	P;
  // Constructors
		boolHandle(bool* p);
  public:	boolHandle(const boolHandle& h);
		~boolHandle(void);
  // Operators
		operator bool* (void);
  boolHandle&	operator  =(const boolHandle& h);
  // Functions
  bool		empty(void) const;
  bool		get(Offset o) const;
  boolHandle&	put(Offset o, bool b);
  friend class boolSubArray0;
  friend class boolSubVector;	friend class boolVector;
  friend class boolSubArray1;
  friend class boolSubMatrix;	friend class boolMatrix;
  friend class boolSubArray2;
  friend class boolSubTensor;	friend class boolTensor;
  friend class boolSubArray3;
  };

// Constructors
inline		boolHandle::boolHandle(bool* p): P(p) { }
inline		boolHandle::boolHandle(const boolHandle& h): P(h.P) { }
inline		boolHandle::~boolHandle(void) { }

// Operators
inline		boolHandle::operator bool* (void) { return P; }
inline
boolHandle&	boolHandle::operator  =(const boolHandle& h) {
  P = h.P; return *this; }

// Functions
inline
bool		boolHandle::empty(void) const { return 0 == P; }
inline
bool		boolHandle::get(Offset o) const { return P[o]; }

inline
boolHandle&	boolHandle::put(Offset o, bool b) { P[o] = b; return *this; }

#endif /* _boolHandle_h */

