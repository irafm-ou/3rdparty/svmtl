#ifndef _doubleComplexTensor_h
#define _doubleComplexTensor_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleTensor.h>
#include<doubleComplexMatrix.h>

class doubleComplexSubTensor {
  private:
  // Representation
  doubleComplexHandle
		H;
  Offset	O;
  Extent	N1;
  Stride	S1;
  Extent	N2;
  Stride	S2;
  Extent	N3;
  Stride	S3;
  public:
  // Constructors
		doubleComplexSubTensor(void);
		doubleComplexSubTensor(const doubleComplexHandle& h,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
		doubleComplexSubTensor(const doubleComplexSubTensor& T);
		~doubleComplexSubTensor(void);
  // Functions
  const
  doubleComplexHandle&
		handle(void) const;
  Offset	offset(void) const;
  Extent	extent1(void) const;
  Extent	length1(void) const;
  Stride	stride1(void) const;
  Extent	extent2(void) const;
  Extent	length2(void) const;
  Stride	stride2(void) const;
  Extent	extent3(void) const;
  Extent	length3(void) const;
  Stride	stride3(void) const;
  bool		 empty(void) const;
  static
  doubleComplexHandle
	      allocate(Extent l, Extent m, Extent n);
  static
  doubleComplexHandle
	      allocate_(Extent n, Extent m, Extent l);
  doubleComplexSubTensor&
		  free(void);
  doubleComplexSubTensor&
		resize(void);
  doubleComplexSubTensor&
		resize(const doubleComplexHandle& h,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleComplexSubTensor&
		resize(const doubleComplexSubTensor& T);
  doubleComplexSubTensor&
		resize_(const doubleComplexHandle& h,
      Offset o, Extent n1, Stride s1,
		Extent n2, Stride s2,
		Extent n3, Stride s3);

  bool	      contains(Offset h, Extent n3, Stride s3) const;
  bool	      contains(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2) const;
  bool	      contains(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  bool	      contains_(Offset h, Extent n3, Stride s3) const;
  bool	      contains_(Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;
  bool	      contains_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;
  doubleComplexSubTensor
		   sub(Offset h, Extent n3, Stride s3);
  const
  doubleComplexSubTensor
		   sub(Offset h, Extent n3, Stride s3) const;
  doubleComplexSubTensor
		   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2);
  const
  doubleComplexSubTensor
		   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2) const;
  doubleComplexSubTensor
		   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1);
  const
  doubleComplexSubTensor
		   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  doubleComplexSubTensor
		   sub_(Offset h, Extent n3, Stride s3);
  const
  doubleComplexSubTensor
		   sub_(Offset h, Extent n3, Stride s3) const;
  doubleComplexSubTensor
		   sub_(Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3);
  const
  doubleComplexSubTensor
		   sub_(Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;
  doubleComplexSubTensor
		   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3);
  const
  doubleComplexSubTensor
		   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;

  doubleComplexSubTensor
		     t12(void);
  const
  doubleComplexSubTensor
		     t12(void) const;
  doubleComplexSubTensor
		     t23(void);
  const
  doubleComplexSubTensor
		     t23(void) const;
  doubleComplexSubTensor
		     t31(void);
  const
  doubleComplexSubTensor
		     t31(void) const;
  doubleComplexSubMatrix
		  diag12(void);
  const
  doubleComplexSubMatrix
		  diag12(void) const;
  doubleComplexSubMatrix
		  diag23(void);
  const
  doubleComplexSubMatrix
		  diag23(void) const;
  doubleComplexSubMatrix
		  diag31(void);
  const
  doubleComplexSubMatrix
		  diag31(void) const;
  doubleComplexSubTensor
		     r1(void);
  const
  doubleComplexSubTensor
		     r1(void) const;
  doubleComplexSubTensor&
	       reverse1(void);
  doubleComplexSubTensor
		     r2(void);
  const
  doubleComplexSubTensor
		     r2(void) const;
  doubleComplexSubTensor&
	       reverse2(void);
  doubleComplexSubTensor
		     r3(void);
  const
  doubleComplexSubTensor
		     r3(void) const;
  doubleComplexSubTensor&
	       reverse3(void);
  doubleComplexSubTensor
		     r(void);
  const
  doubleComplexSubTensor
		     r(void) const;
  doubleComplexSubTensor&
	       reverse(void);

  doubleComplexSubTensor
		  even(void);
  const
  doubleComplexSubTensor
		  even(void) const;
  doubleComplexSubTensor
		   odd(void);
  const
  doubleComplexSubTensor
		   odd(void) const;
  doubleComplexSubTensor
		  even_(void);
  const
  doubleComplexSubTensor
		  even_(void) const;
  doubleComplexSubTensor
		   odd_(void);
  const
  doubleComplexSubTensor
		   odd_(void) const;
  doubleSubTensor
		  real(void);
  const
  doubleSubTensor
		  real(void) const;
  doubleSubTensor
		  imag(void);
  const
  doubleSubTensor
		  imag(void) const;
  doubleComplexSubTensor&
		   dft(int sign = -1);	// complex to complex dft
  doubleComplexSubTensor&
		  swap(Offset h, Offset k);
  doubleComplexSubTensor&
		  swap(doubleComplexSubTensor& U);
  doubleComplexSubTensor&
		  swap_(Offset h, Offset k);
  doubleComplexSubTensor&
		rotate(Stride n);
  doubleComplexSubTensor&
		 shift(Stride n,const doubleComplex& s
		  = doubleComplex((double)0, (double)0));

  const
  doubleComplexMatrix
		   sum(void) const;
  const
  doubleComplexTensor
	   permutation(const offsetSubVector& p) const;
  const
  boolTensor
		    eq(const double& s) const;
  const
  boolTensor
		    ne(const double& s) const;
  const
  boolTensor
		    eq(const doubleComplex& s) const;
  const
  boolTensor
		    ne(const doubleComplex& s) const;
  const
  boolTensor
		    eq(const doubleSubTensor& U) const;
  const
  boolTensor
		    ne(const doubleSubTensor& U) const;
  const
  boolTensor
		    eq(const doubleComplexSubTensor& U) const;
  const
  boolTensor
		    ne(const doubleComplexSubTensor& U) const;

  Extent	 zeros(void) const;
  const
  offsetVector
		 index(void) const;
  const
  doubleComplexVector
		gather(const offsetSubVector& x) const;
  doubleComplexSubTensor&
	       scatter(const offsetSubVector& x,
		       const doubleComplexSubVector& t);
  const
  doubleComplexTensor
		 aside(const doubleComplexSubTensor& U) const;
  const
  doubleComplexTensor
		 above(const doubleComplexSubTensor& U) const;
  const
  doubleComplexTensor
		 afore(const doubleComplexSubTensor& U) const;
  const
  doubleComplexTensor
		 afore(const doubleComplexSubMatrix& M) const;
  const
  doubleComplexTensor
		 above_(const doubleComplexSubTensor& U) const;
  const
  doubleComplexTensor
		 aside_(const doubleComplexSubTensor& U) const;

  const
  doubleComplexTensor
		  kron(const doubleComplexSubVector& v) const;
  const
  doubleComplexTensor
		  kron(const doubleComplexSubMatrix& M) const;
  const
  doubleComplexTensor
		  kron(const doubleComplexSubTensor& U) const;
  const
  doubleComplexTensor
		  kron(const doubleSubVector& v) const;
  const
  doubleComplexTensor
		  kron(const doubleSubMatrix& M) const;
  const
  doubleComplexTensor
		  kron(const doubleSubTensor& U) const;
  const
  doubleComplexTensor
		 apply(const doubleComplex (*f)(const doubleComplex&)) const;
  const
  doubleComplexTensor
		 apply(      doubleComplex (*f)(const doubleComplex&)) const;
  const
  doubleComplexTensor
		 apply(      doubleComplex (*f)(      doubleComplex )) const;

  // Operators
  doubleComplexSubMatrix
		operator [](Offset h);
  const
  doubleComplexSubMatrix
		operator [](Offset h) const;
  doubleComplexSubMatrix
		operator ()(Offset h);
  const
  doubleComplexSubMatrix
		operator ()(Offset h) const;
  doubleComplexSubVector
		operator ()(Offset i, Offset h);
  const
  doubleComplexSubVector
		operator ()(Offset i, Offset h) const;
  doubleComplexSubScalar
		operator ()(Offset j, Offset i, Offset h);
  const
  doubleComplexSubScalar
		operator ()(Offset j, Offset i, Offset h) const;

  const
  doubleComplexTensor
		operator -(void) const;
//  const
//  doubleComplexSubTensor
//		operator +(void) const;
  const
  doubleComplexTensor
		operator *(const double& s) const;
  const
  doubleComplexTensor
		operator /(const double& s) const;
  const
  doubleComplexTensor
		operator +(const double& s) const;
  const
  doubleComplexTensor
		operator -(const double& s) const;
  const
  doubleComplexTensor
		operator *(const doubleComplex& s) const;
  const
  doubleComplexTensor
		operator /(const doubleComplex& s) const;
  const
  doubleComplexTensor
		operator +(const doubleComplex& s) const;
  const
  doubleComplexTensor
		operator -(const doubleComplex& s) const;
  const
  doubleComplexTensor
		operator *(const doubleSubTensor& U) const;
  const
  doubleComplexTensor
		operator /(const doubleSubTensor& U) const;
  const
  doubleComplexTensor
		operator +(const doubleSubTensor& U) const;
  const
  doubleComplexTensor
		operator -(const doubleSubTensor& U) const;
  const
  doubleComplexTensor
		operator *(const doubleComplexSubTensor& U) const;
  const
  doubleComplexTensor
		operator /(const doubleComplexSubTensor& U) const;
  const
  doubleComplexTensor
		operator +(const doubleComplexSubTensor& U) const;
  const
  doubleComplexTensor
		operator -(const doubleComplexSubTensor& U) const;

  bool		operator ==(const double& s) const;
  bool		operator !=(const double& s) const;
  bool		operator ==(const doubleComplex& s) const;
  bool		operator !=(const doubleComplex& s) const;
  bool		operator ==(const doubleSubTensor& U) const;
  bool		operator !=(const doubleSubTensor& U) const;
  bool		operator ==(const doubleComplexSubTensor& U) const;
  bool		operator !=(const doubleComplexSubTensor& U) const;
  doubleComplexSubTensor&
		operator  =(const double& s);
  doubleComplexSubTensor&
		operator *=(const double& s);
  doubleComplexSubTensor&
		operator /=(const double& s);
  doubleComplexSubTensor&
		operator +=(const double& s);
  doubleComplexSubTensor&
		operator -=(const double& s);

  doubleComplexSubTensor&
		operator  =(const doubleComplex& s);
  doubleComplexSubTensor&
		operator *=(const doubleComplex& s);
  doubleComplexSubTensor&
		operator /=(const doubleComplex& s);
  doubleComplexSubTensor&
		operator +=(const doubleComplex& s);
  doubleComplexSubTensor&
		operator -=(const doubleComplex& s);
  doubleComplexSubTensor&
		operator  =(const doubleSubTensor& U);
  doubleComplexSubTensor&
		operator *=(const doubleSubTensor& U);
  doubleComplexSubTensor&
		operator /=(const doubleSubTensor& U);
  doubleComplexSubTensor&
		operator +=(const doubleSubTensor& U);
  doubleComplexSubTensor&
		operator -=(const doubleSubTensor& U);
  doubleComplexSubTensor&
		operator  =(const doubleComplexSubTensor& U);
  doubleComplexSubTensor&
		operator *=(const doubleComplexSubTensor& U);
  doubleComplexSubTensor&
		operator /=(const doubleComplexSubTensor& U);
  doubleComplexSubTensor&
		operator +=(const doubleComplexSubTensor& U);
  doubleComplexSubTensor&
		operator -=(const doubleComplexSubTensor& U);
  friend class doubleComplexTensor;
  };

// Constructors
inline		doubleComplexSubTensor::doubleComplexSubTensor(void):
  H((double*)0), O((Offset)0), N1((Extent)0), S1((Stride)0),
  N2((Extent)0), S2((Stride)0), N3((Extent)0), S3((Stride)0) { }
inline		doubleComplexSubTensor::doubleComplexSubTensor(
    const doubleComplexHandle& h, Offset o,
    Extent n3, Stride s3, Extent n2, Stride s2, Extent n1, Stride s1):
  H(h), O(o), N1(n1), S1(s1), N2(n2), S2(s2), N3(n3), S3(s3) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment(
    "doubleComplexSubTensor::doubleComplexSubTensor(\n"
    "const doubleComplexHandle&, Offset,\n"
    "Extent, Stride, Extent, Stride, Extent, Stride)",
    o, n3, s3, n2, s2, n1, s1);
#endif // SVMT_DEBUG_MODE
  }
inline		doubleComplexSubTensor::doubleComplexSubTensor(
    const doubleComplexSubTensor& T): H(T.H), O(T.O),
    N1(T.N1), S1(T.S1), N2(T.N2), S2(T.S2), N3(T.N3), S3(T.S3) { }
inline		doubleComplexSubTensor::~doubleComplexSubTensor(void) { }

// Functions
inline
const
doubleComplexHandle&
		doubleComplexSubTensor::handle(void) const { return H; }
inline
Offset		doubleComplexSubTensor::offset(void) const { return O; }
inline
Extent		doubleComplexSubTensor::extent1(void) const { return N1; }
inline
Extent		doubleComplexSubTensor::length1(void) const { return N1; }
inline
Stride		doubleComplexSubTensor::stride1(void) const { return S1; }
inline
Extent		doubleComplexSubTensor::extent2(void) const { return N2; }
inline
Extent		doubleComplexSubTensor::length2(void) const { return N2; }
inline
Stride		doubleComplexSubTensor::stride2(void) const { return S2; }
inline
Extent		doubleComplexSubTensor::extent3(void) const { return N3; }
inline
Extent		doubleComplexSubTensor::length3(void) const { return N3; }
inline
Stride		doubleComplexSubTensor::stride3(void) const { return S3; }
inline
bool		doubleComplexSubTensor::empty(void) const {
  return handle().empty()||0 == extent1()||0 == extent2()||0 == extent3(); }
inline
doubleComplexHandle
		doubleComplexSubTensor::allocate(Extent l, Extent m, Extent n)
  { return doubleComplexHandle(new double[l*m*n << 1]); }
inline
doubleComplexHandle
		doubleComplexSubTensor::allocate_(Extent n, Extent m, Extent l)
  { return allocate(l, m, n); }
inline
doubleComplexSubTensor&
		doubleComplexSubTensor::free(void) {
  delete [] (double*)H; return *this; }

inline
doubleComplexSubTensor&
		doubleComplexSubTensor::resize(void) {
  H = doubleComplexHandle((double*)0); O = (Offset)0;
  N1 = (Extent)0; S1 = (Stride)0;
  N2 = (Extent)0; S2 = (Stride)0;
  N3 = (Extent)0; S3 = (Stride)0; return *this; }
inline
doubleComplexSubTensor&
		doubleComplexSubTensor::resize(const doubleComplexHandle& h,
    Offset o, Extent n3, Stride s3,
	      Extent n2, Stride s2,
	      Extent n1, Stride s1) {
  H = h; O = o; N1 = n1; S1 = s1; N2 = n2; S2 = s2; N3 = n3; S3 = s3;
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment(
    "doubleComplexSubTensor::resize(const doubleComplexHandle&,\n"
    "Offset, Extent, Stride, Extent, Stride, Extent, Stride)",
    o, n3, s3, n2, s2, n1, s1);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
doubleComplexSubTensor&
		doubleComplexSubTensor::resize(
    const doubleComplexSubTensor& T) { return resize(T.handle(),
    T.offset(), T.extent3(), T.stride3(),
		T.extent2(), T.stride2(),
		T.extent1(), T.stride1()); }
inline
doubleComplexSubTensor&
		doubleComplexSubTensor::resize_(const doubleComplexHandle& h,
    Offset o, Extent n1, Stride s1,
	      Extent n2, Stride s2,
	      Extent n3, Stride s3) {
  return resize(h, o, n3, s3, n2, s2, n1, s1); }

inline
bool		doubleComplexSubTensor::contains(
    Offset h, Extent n3, Stride s3) const {
  return boolSubVector::contains(
    h, n3, s3, extent3()); }
inline
bool		doubleComplexSubTensor::contains(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2) const {
  return boolSubMatrix::contains(
    h, n3, s3, extent3(),
    i, n2, s2, extent2()); }
inline
bool		doubleComplexSubTensor::contains(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) const {
  return boolSubTensor::contains(
    h, n3, s3, extent3(),
    i, n2, s2, extent2(),
    j, n1, s1, extent1()); }
inline
bool		doubleComplexSubTensor::contains_(
    Offset h, Extent n3, Stride s3) const {
  return contains(h-1, n3, s3); }
inline
bool		doubleComplexSubTensor::contains_(
    Offset i, Extent n2, Stride s2,
    Offset h, Extent n3, Stride s3) const {
  return contains(h-1, n3, s3, i-1, n2, s2); }
inline
bool		doubleComplexSubTensor::contains_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2,
    Offset h, Extent n3, Stride s3) const {
  return contains(h-1, n3, s3, i-1, n2, s2, j-1, n1, s1); }

inline
doubleComplexSubTensor
		doubleComplexSubTensor::sub(
    Offset h, Extent n3, Stride s3) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleComplexSubTensor::sub(Offset, Extent, Stride)",
    h, n3, s3, extent3());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubTensor(handle(), offset() + (Stride)h*stride3(),
    n3, s3*stride3(), extent2(), stride2(), extent1(), stride1()); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::sub(
    Offset h, Extent n3, Stride s3) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleComplexSubTensor::sub(Offset, Extent, Stride) const",
    h, n3, s3, extent3());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubTensor(handle(), offset() + (Stride)h*stride3(),
    n3, s3*stride3(), extent2(), stride2(), extent1(), stride1()); }
inline
doubleComplexSubTensor
		doubleComplexSubTensor::sub(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleComplexSubTensor::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride)",
    h, n3, s3, extent3(),
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubTensor(handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2(),
    n3, s3*stride3(), n2, s2*stride2(), extent1(), stride1()); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::sub(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleComplexSubTensor::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride) const",
    h, n3, s3, extent3(),
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubTensor(handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2(),
    n3, s3*stride3(), n2, s2*stride2(), extent1(), stride1()); }

inline
doubleComplexSubTensor
		doubleComplexSubTensor::sub(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment("doubleComplexSubTensor::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride, Offset, Extent, Stride)",
    h, n3, s3, extent3(),
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubTensor(handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2() + (Stride)j*stride1(),
    n3, s3*stride3(), n2, s2*stride2(), n1, s1*stride1()); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::sub(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment("doubleComplexSubTensor::sub(\n"
"Offset, Extent, Stride, Offset, Extent, Stride, Offset, Extent, Stride) const",
    h, n3, s3, extent3(),
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubTensor(handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2() + (Stride)j*stride1(),
    n3, s3*stride3(), n2, s2*stride2(), n1, s1*stride1()); }
inline
doubleComplexSubTensor
		doubleComplexSubTensor::sub_(
    Offset h, Extent n3, Stride s3) { return sub(h-1, n3, s3); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::sub_(
    Offset h, Extent n3, Stride s3) const { return sub(h-1, n3, s3); }
inline
doubleComplexSubTensor
		doubleComplexSubTensor::sub_(
    Offset i, Extent n2, Stride s2,
    Offset h, Extent n3, Stride s3) {
  return sub(h-1, n3, s3, i-1, n2, s2); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::sub_(
    Offset i, Extent n2, Stride s2,
    Offset h, Extent n3, Stride s3) const {
  return sub(h-1, n3, s3, i-1, n2, s2); }

inline
doubleComplexSubTensor
		doubleComplexSubTensor::sub_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2,
    Offset h, Extent n3, Stride s3) {
  return sub(h-1, n3, s3, i-1, n2, s2, j-1, n1, s1); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::sub_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2,
    Offset h, Extent n3, Stride s3) const {
  return sub(h-1, n3, s3, i-1, n2, s2, j-1, n1, s1); }
inline
const
doubleComplexSubTensor
		doubleComplexSubScalar::subtensor(
    Extent n, Extent m, Extent l) const {
  return doubleComplexSubTensor(handle(),
    offset(), l, (Stride)0, m, (Stride)0, n, (Stride)0); }
inline
const
doubleComplexSubTensor
		doubleComplexSubVector::subtensor(
    Extent m, Extent l) const {
  return doubleComplexSubTensor(handle(),
    offset(), l, (Stride)0, m, (Stride)0, extent(), stride()); }
inline
const
doubleComplexSubTensor
		doubleComplexSubMatrix::subtensor(
    Extent l) const {
  return doubleComplexSubTensor(handle(),
    offset(), l, (Stride)0, extent2(), stride2(), extent1(), stride1()); }

inline
doubleComplexSubTensor
		doubleComplexSubTensor::t12(void) {
  return doubleComplexSubTensor(handle(), offset(),
    extent3(), stride3(), extent1(), stride1(), extent2(), stride2()); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::t12(void) const {
  return doubleComplexSubTensor(handle(), offset(),
    extent3(), stride3(), extent1(), stride1(), extent2(), stride2()); }
inline
doubleComplexSubTensor
		doubleComplexSubTensor::t23(void) {
  return doubleComplexSubTensor(handle(), offset(),
    extent2(), stride2(), extent3(), stride3(), extent1(), stride1()); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::t23(void) const {
  return doubleComplexSubTensor(handle(), offset(),
    extent2(), stride2(), extent3(), stride3(), extent1(), stride1()); }
inline
doubleComplexSubTensor
		doubleComplexSubTensor::t31(void) {
  return doubleComplexSubTensor(handle(), offset(),
    extent1(), stride1(), extent2(), stride2(), extent3(), stride3()); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::t31(void) const {
  return doubleComplexSubTensor(handle(), offset(),
    extent1(), stride1(), extent2(), stride2(), extent3(), stride3()); }

inline
doubleComplexSubMatrix
		doubleComplexSubTensor::diag12(void) {
  return doubleComplexSubMatrix(handle(), offset(), extent3(), stride3(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubTensor::diag12(void) const {
  return doubleComplexSubMatrix(handle(), offset(), extent3(), stride3(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
doubleComplexSubMatrix
		doubleComplexSubTensor::diag23(void) {
  return doubleComplexSubMatrix(handle(), offset(), extent1(), stride1(),
    (extent2() < extent3())? extent2(): extent3(), stride3() + stride2()); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubTensor::diag23(void) const {
  return doubleComplexSubMatrix(handle(), offset(), extent1(), stride1(),
    (extent2() < extent3())? extent2(): extent3(), stride3() + stride2()); }
inline
doubleComplexSubMatrix
		doubleComplexSubTensor::diag31(void) {
  return doubleComplexSubMatrix(handle(), offset(), extent2(), stride2(),
    (extent3() < extent1())? extent3(): extent1(), stride1() + stride3()); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubTensor::diag31(void) const {
  return doubleComplexSubMatrix(handle(), offset(), extent2(), stride2(),
    (extent3() < extent1())? extent3(): extent1(), stride1() + stride3()); }

inline
doubleComplexSubTensor
		doubleComplexSubTensor::r1(void) {
  return doubleComplexSubTensor(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent3(), +stride3(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::r1(void) const {
  return doubleComplexSubTensor(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent3(), +stride3(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
doubleComplexSubTensor
		doubleComplexSubTensor::r2(void) {
  return doubleComplexSubTensor(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent3(), +stride3(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::r2(void) const {
  return doubleComplexSubTensor(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent3(), +stride3(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
doubleComplexSubTensor
		doubleComplexSubTensor::r3(void) {
  return doubleComplexSubTensor(handle(),
    offset() + (Stride)(extent3() - 1)*stride3(),
    extent3(), -stride3(),
    extent2(), +stride2(),
    extent1(), +stride1()); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::r3(void) const {
  return doubleComplexSubTensor(handle(),
    offset() + (Stride)(extent3() - 1)*stride3(),
    extent3(), -stride3(),
    extent2(), +stride2(),
    extent1(), +stride1()); }

inline
doubleComplexSubTensor
		doubleComplexSubTensor::r(void) {
  return doubleComplexSubTensor(handle(),
    offset() + (Stride)(extent3() - 1)*stride3()
	     + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent3(), -stride3(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::r(void) const {
  return doubleComplexSubTensor(handle(),
    offset() + (Stride)(extent3() - 1)*stride3()
	     + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent3(), -stride3(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
doubleComplexSubTensor&
		doubleComplexSubTensor::reverse(void) {
  return reverse1().reverse2().reverse3(); }

inline
doubleComplexSubTensor
		doubleComplexSubTensor::even(void) {
  return doubleComplexSubTensor(handle(), offset(),
    extent3(), stride3(), extent2(), stride2(),
    (extent1() + 1) >> 1, stride1() << 1); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::even(void) const {
  return doubleComplexSubTensor(handle(), offset(),
    extent3(), stride3(), extent2(), stride2(),
    (extent1() + 1) >> 1, stride1() << 1); }
inline
doubleComplexSubTensor
		doubleComplexSubTensor::odd(void) {
  return doubleComplexSubTensor(handle(), offset() + stride1(),
    extent3(), stride3(), extent2(), stride2(),
    extent1() >> 1, stride1() << 1); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::odd(void) const {
  return doubleComplexSubTensor(handle(), offset() + stride1(),
    extent3(), stride3(), extent2(), stride2(),
    extent1() >> 1, stride1() << 1); }
inline
doubleComplexSubTensor
		doubleComplexSubTensor::even_(void) { return odd(); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::even_(void) const { return odd(); }
inline
doubleComplexSubTensor
		doubleComplexSubTensor::odd_(void) { return even(); }
inline
const
doubleComplexSubTensor
		doubleComplexSubTensor::odd_(void) const { return even(); }
inline
doubleSubTensor
		doubleComplexSubTensor::real(void) {
  return doubleSubTensor(handle(),     (offset() << 1),
    extent3(), stride3(), extent2(), stride2(), extent1(), stride1() << 1); }
inline
const
doubleSubTensor
		doubleComplexSubTensor::real(void) const {
  return doubleSubTensor(handle(),     (offset() << 1),
    extent3(), stride3(), extent2(), stride2(), extent1(), stride1() << 1); }
inline
doubleSubTensor
		doubleComplexSubTensor::imag(void) {
  return doubleSubTensor(handle(), 1 + (offset() << 1),
    extent3(), stride3(), extent2(), stride2(), extent1(), stride1() << 1); }
inline
const
doubleSubTensor
		doubleComplexSubTensor::imag(void) const {
  return doubleSubTensor(handle(), 1 + (offset() << 1),
    extent3(), stride3(), extent2(), stride2(), extent1(), stride1() << 1); }

// Global function declarations
const
doubleComplexTensor
		  conj(const doubleComplexSubTensor& T);
const
doubleComplexTensor
		 iconj(const doubleComplexSubTensor& T);
const
doubleComplexTensor
		 polar(const doubleSubTensor& T,
		       const doubleSubTensor& U);
const
doubleTensor
		  norm(const doubleComplexSubTensor& T);
inline
const
doubleTensor
		   abs(const doubleComplexSubTensor& T) {
  return hypot(T.real(), T.imag()); }
inline
const
doubleTensor
		   arg(const doubleComplexSubTensor& T) {
  return atan2(T.imag(), T.real()); }

const
doubleComplexTensor
		   log(const doubleComplexSubTensor& T);
const
doubleComplexTensor
		   exp(const doubleComplexSubTensor& T);
const
doubleComplexTensor
		  sqrt(const doubleComplexSubTensor& T);
const
doubleComplexTensor
		   cos(const doubleComplexSubTensor& T);
const
doubleComplexTensor
		   sin(const doubleComplexSubTensor& T);
const
doubleComplexTensor
		   tan(const doubleComplexSubTensor& T);
const
doubleComplexTensor
		  acos(const doubleComplexSubTensor& T);
const
doubleComplexTensor
		  asin(const doubleComplexSubTensor& T);
const
doubleComplexTensor
		  atan(const doubleComplexSubTensor& T);
const
doubleComplexTensor
		  cosh(const doubleComplexSubTensor& T);
const
doubleComplexTensor
		  sinh(const doubleComplexSubTensor& T);
const
doubleComplexTensor
		  tanh(const doubleComplexSubTensor& T);
const
doubleComplexTensor
		 acosh(const doubleComplexSubTensor& T);
const
doubleComplexTensor
		 asinh(const doubleComplexSubTensor& T);
const
doubleComplexTensor
		 atanh(const doubleComplexSubTensor& T);

// Operators
inline
doubleComplexSubMatrix
		doubleComplexSubTensor::operator [](Offset h) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleComplexSubTensor::operator [](Offset)", h, extent3());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubMatrix(handle(), offset() + (Stride)h*stride3(),
    extent2(), stride2(), extent1(), stride1()); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubTensor::operator [](Offset h) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleComplexSubTensor::operator [](Offset) const", h, extent3());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubMatrix(handle(), offset() + (Stride)h*stride3(),
    extent2(), stride2(), extent1(), stride1()); }
inline
doubleComplexSubTensor&
		doubleComplexSubTensor::swap(Offset h, Offset k) {
  doubleComplexSubTensor	T = *this;
  doubleComplexSubMatrix	M = T[h];
  doubleComplexSubMatrix	N = T[k];
  M.swap(N); return *this; }
inline
doubleComplexSubTensor&
		doubleComplexSubTensor::swap_(Offset h, Offset k) {
  return swap(h-1, k-1); }
// Operators
inline
doubleComplexSubMatrix
		doubleComplexSubTensor::operator ()(Offset h) {
  return operator [](h-1); }
inline
const
doubleComplexSubMatrix
		doubleComplexSubTensor::operator ()(Offset h) const {
  return operator [](h-1); }
inline
doubleComplexSubVector
		doubleComplexSubTensor::operator ()(Offset i, Offset h) {
  doubleComplexSubTensor&	T = *this; return T[h-1][i-1]; }
inline
const
doubleComplexSubVector
		doubleComplexSubTensor::operator ()(Offset i, Offset h) const {
  const
  doubleComplexSubTensor&	T = *this; return T[h-1][i-1]; }
inline
doubleComplexSubScalar
		doubleComplexSubTensor::operator ()(
    Offset j, Offset i, Offset h) {
  doubleComplexSubTensor&	T = *this; return T[h-1][i-1][j-1]; }
inline
const
doubleComplexSubScalar
		doubleComplexSubTensor::operator ()(
    Offset j, Offset i, Offset h) const {
  const
  doubleComplexSubTensor&	T = *this; return T[h-1][i-1][j-1]; }

//inline
//const
//doubleComplexSubTensor
//		doubleComplexSubTensor::operator +(void) const {
//  return *this; }
std::istream&	operator >>(std::istream& s,       doubleComplexSubTensor& T);
std::ostream&	operator <<(std::ostream& s, const doubleComplexSubTensor& T);

class doubleComplexSubArray3: public doubleComplexSubTensor {
  public:
  // Constructors
		doubleComplexSubArray3(void);
		doubleComplexSubArray3(double* p,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
		doubleComplexSubArray3(const doubleComplexSubArray3& T);
		~doubleComplexSubArray3(void);
  // Functions
  doubleComplexSubArray3&
		resize(void);
  doubleComplexSubArray3&		// resize from pointer
		resize(double* p,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleComplexSubArray3&		// resize from SubArray3
		resize(const doubleComplexSubArray3& T);
  doubleComplexSubArray3&		// resize from pointer
		resize_(double* p,
      Offset o, Extent n1, Stride s1,
		Extent n2, Stride s2,
		Extent n3, Stride s3);
  private:
  doubleComplexSubArray3&		// prevent resize from Handle
		resize(const doubleComplexHandle& h,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleComplexSubArray3&		// prevent resize from SubTensor
		resize(const doubleComplexSubTensor& T);

  public:
  doubleComplexSubArray3
		   sub(Offset h, Extent n3, Stride s3);
  const
  doubleComplexSubArray3
		   sub(Offset h, Extent n3, Stride s3) const;
  doubleComplexSubArray3
		   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2);
  const
  doubleComplexSubArray3
		   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2) const;
  doubleComplexSubArray3
		   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1);
  const
  doubleComplexSubArray3
		   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  doubleComplexSubArray3
		   sub_(Offset h, Extent n3, Stride s3);
  const
  doubleComplexSubArray3
		   sub_(Offset h, Extent n3, Stride s3) const;
  doubleComplexSubArray3
		   sub_(Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3);
  const
  doubleComplexSubArray3
		   sub_(Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;
  doubleComplexSubArray3
		   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3);
  const
  doubleComplexSubArray3
		   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;

  doubleComplexSubArray3
		     t12(void);
  const
  doubleComplexSubArray3
		     t12(void) const;
  doubleComplexSubArray3
		     t23(void);
  const
  doubleComplexSubArray3
		     t23(void) const;
  doubleComplexSubArray3
		     t31(void);
  const
  doubleComplexSubArray3
		     t31(void) const;
  doubleComplexSubArray2
		  diag12(void);
  const
  doubleComplexSubArray2
		  diag12(void) const;
  doubleComplexSubArray2
		  diag23(void);
  const
  doubleComplexSubArray2
		  diag23(void) const;
  doubleComplexSubArray2
		  diag31(void);
  const
  doubleComplexSubArray2
		  diag31(void) const;
  doubleComplexSubArray3
		     r1(void);
  const
  doubleComplexSubArray3
		     r1(void) const;
  doubleComplexSubArray3&
	       reverse1(void);
  doubleComplexSubArray3
		     r2(void);
  const
  doubleComplexSubArray3
		     r2(void) const;
  doubleComplexSubArray3&
	       reverse2(void);
  doubleComplexSubArray3
		     r3(void);
  const
  doubleComplexSubArray3
		     r3(void) const;
  doubleComplexSubArray3&
	       reverse3(void);
  doubleComplexSubArray3
		     r(void);
  const
  doubleComplexSubArray3
		     r(void) const;
  doubleComplexSubArray3&
	       reverse(void);

  doubleComplexSubArray3
		  even(void);
  const
  doubleComplexSubArray3
		  even(void) const;
  doubleComplexSubArray3
		   odd(void);
  const
  doubleComplexSubArray3
		   odd(void) const;
  doubleComplexSubArray3
		  even_(void);
  const
  doubleComplexSubArray3
		  even_(void) const;
  doubleComplexSubArray3
		   odd_(void);
  const
  doubleComplexSubArray3
		   odd_(void) const;
  doubleSubArray3
		  real(void);
  const
  doubleSubArray3
		  real(void) const;
  doubleSubArray3
		  imag(void);
  const
  doubleSubArray3
		  imag(void) const;
  doubleComplexSubArray3&
		   dft(int sign = -1);	// complex to complex dft
  doubleComplexSubArray3&
		  swap(Offset h, Offset k);
  doubleComplexSubArray3&
		  swap(doubleComplexSubTensor& U);
  doubleComplexSubArray3&
		  swap_(Offset h, Offset k);
  doubleComplexSubArray3&
		rotate(Stride n);
  doubleComplexSubArray3&
		 shift(Stride n,const doubleComplex& s
		  = doubleComplex((double)0, (double)0));
  doubleComplexSubArray3&
	       scatter(const offsetSubVector& x,
		       const doubleComplexSubVector& t);

  // Operators
  doubleComplexSubArray2
		operator [](Offset h);
  const
  doubleComplexSubArray2
		operator [](Offset h) const;
  doubleComplexSubArray2
		operator ()(Offset h);
  const
  doubleComplexSubArray2
		operator ()(Offset h) const;
  doubleComplexSubArray1
		operator ()(Offset i, Offset h);
  const
  doubleComplexSubArray1
		operator ()(Offset i, Offset h) const;
  doubleComplexSubArray0
		operator ()(Offset j, Offset i, Offset h);
  const
  doubleComplexSubArray0
		operator ()(Offset j, Offset i, Offset h) const;
//  const
//  doubleComplexSubArray3
//		operator +(void) const;
  doubleComplexSubArray3&
		operator  =(const double& s);
  doubleComplexSubArray3&
		operator *=(const double& s);
  doubleComplexSubArray3&
		operator /=(const double& s);
  doubleComplexSubArray3&
		operator +=(const double& s);
  doubleComplexSubArray3&
		operator -=(const double& s);

  doubleComplexSubArray3&
		operator  =(const doubleComplex& s);
  doubleComplexSubArray3&
		operator *=(const doubleComplex& s);
  doubleComplexSubArray3&
		operator /=(const doubleComplex& s);
  doubleComplexSubArray3&
		operator +=(const doubleComplex& s);
  doubleComplexSubArray3&
		operator -=(const doubleComplex& s);
  doubleComplexSubArray3&
		operator  =(const doubleSubTensor& U);
  doubleComplexSubArray3&
		operator *=(const doubleSubTensor& U);
  doubleComplexSubArray3&
		operator /=(const doubleSubTensor& U);
  doubleComplexSubArray3&
		operator +=(const doubleSubTensor& U);
  doubleComplexSubArray3&
		operator -=(const doubleSubTensor& U);
  doubleComplexSubArray3&
		operator  =(const doubleComplexSubTensor& U);
  doubleComplexSubArray3&
		operator *=(const doubleComplexSubTensor& U);
  doubleComplexSubArray3&
		operator /=(const doubleComplexSubTensor& U);
  doubleComplexSubArray3&
		operator +=(const doubleComplexSubTensor& U);
  doubleComplexSubArray3&
		operator -=(const doubleComplexSubTensor& U);
  };

// Constructors
inline		doubleComplexSubArray3::doubleComplexSubArray3(void):
  doubleComplexSubTensor() { }
inline		doubleComplexSubArray3::doubleComplexSubArray3(double* p,
    Offset o, Extent n3, Stride s3, Extent n2, Stride s2, Extent n1, Stride s1):
  doubleComplexSubTensor(doubleComplexHandle(p), o, n3, s3, n2, s2, n1, s1) {
  }
inline		doubleComplexSubArray3::doubleComplexSubArray3(
    const doubleComplexSubArray3& T): doubleComplexSubTensor(T) { }
inline		doubleComplexSubArray3::~doubleComplexSubArray3(void) { }

// Functions
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::resize(void) {
  doubleComplexSubTensor::resize(); return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::resize(double* p,
    Offset o, Extent n3, Stride s3,
	      Extent n2, Stride s2,
	      Extent n1, Stride s1) {
  doubleComplexSubTensor::resize(doubleComplexHandle(p),
    o, n3, s3, n2, s2, n1, s1); return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::resize(
    const doubleComplexSubArray3& T) {
  doubleComplexSubTensor::resize(T); return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::resize_(double* p,
    Offset o, Extent n1, Stride s1,
	      Extent n2, Stride s2,
	      Extent n3, Stride s3) {
  return resize(p, o, n3, s3, n2, s2, n1, s1); }
inline
doubleComplexSubArray3
		doubleComplexSubArray3::sub(
    Offset h, Extent n3, Stride s3) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleComplexSubArray3::sub(Offset, Extent, Stride)",
    h, n3, s3, extent3());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)h*stride3(),
    n3, s3*stride3(), extent2(), stride2(), extent1(), stride1()); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::sub(
    Offset h, Extent n3, Stride s3) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleComplexSubArray3::sub(Offset, Extent, Stride) const",
    h, n3, s3, extent3());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)h*stride3(),
    n3, s3*stride3(), extent2(), stride2(), extent1(), stride1()); }

inline
doubleComplexSubArray3
		doubleComplexSubArray3::sub(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleComplexSubArray3::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride)",
    h, n3, s3, extent3(),
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2(),
    n3, s3*stride3(), n2, s2*stride2(), extent1(), stride1()); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::sub(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleComplexSubArray3::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride) const",
    h, n3, s3, extent3(),
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2(),
    n3, s3*stride3(), n2, s2*stride2(), extent1(), stride1()); }
inline
doubleComplexSubArray3
		doubleComplexSubArray3::sub(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment("doubleComplexSubArray3::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride, Offset, Extent, Stride)",
    h, n3, s3, extent3(),
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2() + (Stride)j*stride1(),
    n3, s3*stride3(), n2, s2*stride2(), n1, s1*stride1()); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::sub(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment("doubleComplexSubArray3::sub(\n"
"Offset, Extent, Stride, Offset, Extent, Stride, Offset, Extent, Stride) const",
    h, n3, s3, extent3(),
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2() + (Stride)j*stride1(),
    n3, s3*stride3(), n2, s2*stride2(), n1, s1*stride1()); }

inline
doubleComplexSubArray3
		doubleComplexSubArray3::sub_(
    Offset h, Extent n3, Stride s3) { return sub(h-1, n3, s3); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::sub_(
    Offset h, Extent n3, Stride s3) const { return sub(h-1, n3, s3); }
inline
doubleComplexSubArray3
		doubleComplexSubArray3::sub_(
    Offset i, Extent n2, Stride s2,
    Offset h, Extent n3, Stride s3) {
  return sub(h-1, n3, s3, i-1, n2, s2); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::sub_(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2) const {
  return sub(h-1, n3, s3, i-1, n2, s2); }
inline
doubleComplexSubArray3
		doubleComplexSubArray3::sub_(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) {
  return sub(h-1, n3, s3, i-1, n2, s2, j-1, n1, s1); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::sub_(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) const {
  return sub(h-1, n3, s3, i-1, n2, s2, j-1, n1, s1); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray0::subtensor(
    Extent n, Extent m, Extent l) const {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset(), l, (Stride)0, m, (Stride)0, n, (Stride)0); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray1::subtensor(Extent m, Extent l) const {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset(), l, (Stride)0, m, (Stride)0, extent(), stride()); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray2::subtensor(Extent l) const {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset(), l, (Stride)0, extent2(), stride2(), extent1(), stride1()); }

inline
doubleComplexSubArray3
		doubleComplexSubArray3::t12(void) {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
  offset(), extent3(), stride3(), extent1(), stride1(), extent2(), stride2()); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::t12(void) const {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
  offset(), extent3(), stride3(), extent1(), stride1(), extent2(), stride2()); }
inline
doubleComplexSubArray3
		doubleComplexSubArray3::t23(void) {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
  offset(), extent2(), stride2(), extent3(), stride3(), extent1(), stride1()); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::t23(void) const {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
  offset(), extent2(), stride2(), extent3(), stride3(), extent1(), stride1()); }
inline
doubleComplexSubArray3
		doubleComplexSubArray3::t31(void) {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
  offset(), extent1(), stride1(), extent2(), stride2(), extent3(), stride3()); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::t31(void) const {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
  offset(), extent1(), stride1(), extent2(), stride2(), extent3(), stride3()); }

inline
doubleComplexSubArray2
		doubleComplexSubArray3::diag12(void) {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset(), extent3(), stride3(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray3::diag12(void) const {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset(), extent3(), stride3(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
doubleComplexSubArray2
		doubleComplexSubArray3::diag23(void) {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset(), extent1(), stride1(),
    (extent2() < extent3())? extent2(): extent3(), stride3() + stride2()); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray3::diag23(void) const {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset(), extent1(), stride1(),
    (extent2() < extent3())? extent2(): extent3(), stride3() + stride2()); }
inline
doubleComplexSubArray2
		doubleComplexSubArray3::diag31(void) {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset(), extent2(), stride2(),
    (extent3() < extent1())? extent3(): extent1(), stride1() + stride3()); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray3::diag31(void) const {
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
    offset(), extent2(), stride2(),
    (extent3() < extent1())? extent3(): extent1(), stride1() + stride3()); }

inline
doubleComplexSubArray3
		doubleComplexSubArray3::r1(void) {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent3(), +stride3(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::r1(void) const {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent3(), +stride3(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::reverse1(void) {
  doubleComplexSubTensor::reverse1(); return *this; }
inline
doubleComplexSubArray3
		doubleComplexSubArray3::r2(void) {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent3(), +stride3(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::r2(void) const {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent3(), +stride3(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::reverse2(void) {
  doubleComplexSubTensor::reverse2(); return *this; }
inline
doubleComplexSubArray3
		doubleComplexSubArray3::r3(void) {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)(extent3() - 1)*stride3(),
    extent3(), -stride3(),
    extent2(), +stride2(),
    extent1(), +stride1()); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::r3(void) const {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)(extent3() - 1)*stride3(),
    extent3(), -stride3(),
    extent2(), +stride2(),
    extent1(), +stride1()); }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::reverse3(void) {
  doubleComplexSubTensor::reverse3(); return *this; }

inline
doubleComplexSubArray3
		doubleComplexSubArray3::r(void) {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)(extent3() - 1)*stride3()
	     + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent3(), -stride3(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::r(void) const {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() + (Stride)(extent3() - 1)*stride3()
	     + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent3(), -stride3(),
    extent2(), -stride2(),
    extent1(), -stride1()) ; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::reverse(void) {
  doubleComplexSubTensor::reverse(); return *this; }
inline
doubleComplexSubArray3
		doubleComplexSubArray3::even(void) {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset(), extent3(), stride3(), extent2(), stride2(),
    (extent1() + 1) >> 1, stride1() << 1); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::even(void) const {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset(), extent3(), stride3(), extent2(), stride2(),
    (extent1() + 1) >> 1, stride1() << 1); }
inline
doubleComplexSubArray3
		doubleComplexSubArray3::odd(void) {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() + stride1(), extent3(), stride3(), extent2(), stride2(),
    extent1() >> 1, stride1() << 1); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::odd(void) const {
  return doubleComplexSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() + stride1(), extent3(), stride3(), extent2(), stride2(),
    extent1() >> 1, stride1() << 1); }
inline
doubleComplexSubArray3
		doubleComplexSubArray3::even_(void) { return odd(); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::even_(void) const { return odd(); }
inline
doubleComplexSubArray3
		doubleComplexSubArray3::odd_(void) { return even(); }
inline
const
doubleComplexSubArray3
		doubleComplexSubArray3::odd_(void) const { return even(); }

inline
doubleSubArray3
		doubleComplexSubArray3::real(void) {
  return doubleSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() << 1, extent3(), stride3(), extent2(), stride2(),
    extent1(), stride1() << 1); }
inline
const
doubleSubArray3
		doubleComplexSubArray3::real(void) const {
  return doubleSubArray3((double*)(doubleComplexHandle&)handle(),
    offset() << 1, extent3(), stride3(), extent2(), stride2(),
    extent1(), stride1() << 1); }
inline
doubleSubArray3
		doubleComplexSubArray3::imag(void) {
  return doubleSubArray3((double*)(doubleComplexHandle&)handle(),
    1 + (offset() << 1), extent3(), stride3(), extent2(), stride2(),
    extent1(), stride1() << 1); }
inline
const
doubleSubArray3
		doubleComplexSubArray3::imag(void) const {
  return doubleSubArray3((double*)(doubleComplexHandle&)handle(),
    1 + (offset() << 1), extent3(), stride3(), extent2(), stride2(),
    extent1(), stride1() << 1); }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::dft(int sign) {
  doubleComplexSubTensor::dft(sign); return *this; }

inline
doubleComplexSubArray3&
		doubleComplexSubArray3::swap(Offset h, Offset k) {
  doubleComplexSubTensor::swap(h, k); return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::swap(doubleComplexSubTensor& U) {
  doubleComplexSubTensor::swap(U); return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::swap_(Offset h, Offset k) {
  return swap(h-1, k-1); }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::rotate(Stride n) {
  doubleComplexSubTensor::rotate(n); return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::shift(Stride n,const doubleComplex& s) {
  doubleComplexSubTensor::shift(n,s); return *this; }

// doubleComplexSubTensor function definitions
// which use doubleComplexSubArray3

inline
const
boolTensor
		doubleComplexSubTensor::eq(const double& s) const {
  return eq(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolTensor
		doubleComplexSubTensor::ne(const double& s) const {
  return ne(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolTensor
		doubleComplexSubTensor::eq(const doubleComplex& s) const {
  return eq(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolTensor
		doubleComplexSubTensor::ne(const doubleComplex& s) const {
  return ne(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolTensor
		doubleSubTensor::eq(const doubleComplexSubTensor& U) const {
  return U.eq(*this); }
inline
const
boolTensor
		doubleSubTensor::ne(const doubleComplexSubTensor& U) const {
  return U.ne(*this); }
inline
doubleComplexSubArray3&
	       doubleComplexSubArray3::scatter(
    const offsetSubVector& x, const doubleComplexSubVector& t) {
  doubleComplexSubTensor::scatter(x, t); return *this; }

// Operators
inline
doubleComplexSubArray2
		doubleComplexSubArray3::operator [](Offset h) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleComplexSubArray3::operator [](Offset)", h, extent3());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
  offset() + (Stride)h*stride3(), extent2(), stride2(), extent1(), stride1()); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray3::operator [](Offset h) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleComplexSubArray3::operator [](Offset) const", h, extent3());
#endif // SVMT_DEBUG_MODE
  return doubleComplexSubArray2((double*)(doubleComplexHandle&)handle(),
  offset() + (Stride)h*stride3(), extent2(), stride2(), extent1(), stride1()); }
inline
doubleComplexSubArray2
		doubleComplexSubArray3::operator ()(Offset h) {
  return operator [](h-1); }
inline
const
doubleComplexSubArray2
		doubleComplexSubArray3::operator ()(Offset h) const {
  return operator [](h-1); }
inline
doubleComplexSubArray1
		doubleComplexSubArray3::operator ()(
    Offset i, Offset h) {
  doubleComplexSubArray3&	T = *this; return T[h-1][i-1]; }
inline
const
doubleComplexSubArray1
		doubleComplexSubArray3::operator ()(
    Offset i, Offset h) const {
  const
  doubleComplexSubArray3&	T = *this; return T[h-1][i-1]; }
inline
doubleComplexSubArray0
		doubleComplexSubArray3::operator ()(
    Offset j, Offset i, Offset h) {
  doubleComplexSubArray3&	T = *this; return T[h-1][i-1][j-1]; }
inline
const
doubleComplexSubArray0
		doubleComplexSubArray3::operator ()(
    Offset j, Offset i, Offset h) const {
  const
  doubleComplexSubArray3&	T = *this; return T[h-1][i-1][j-1]; }

//inline
//const
//doubleComplexSubArray3
//		doubleComplexSubArray3::operator +(void) const {
//  return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator  =(const double& s) {
  doubleComplexSubTensor::operator  =(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator *=(const double& s) {
  doubleComplexSubTensor::operator *=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator /=(const double& s) {
  doubleComplexSubTensor::operator /=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator +=(const double& s) {
  doubleComplexSubTensor::operator +=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator -=(const double& s) {
  doubleComplexSubTensor::operator -=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }


inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator  =(const doubleComplex& s) {
  doubleComplexSubTensor::operator  =(
    doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator *=(const doubleComplex& s) {
  doubleComplexSubTensor::operator *=(
    doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator /=(const doubleComplex& s) {
  doubleComplexSubTensor::operator /=(
    doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator +=(const doubleComplex& s) {
  doubleComplexSubTensor::operator +=(
    doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator -=(const doubleComplex& s) {
  doubleComplexSubTensor::operator -=(
    doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator  =(
    const doubleSubTensor& U) {
  doubleComplexSubTensor::operator  =(U); return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator *=(
    const doubleSubTensor& U) {
  doubleComplexSubTensor::operator *=(U); return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator /=(
    const doubleSubTensor& U) {
  doubleComplexSubTensor::operator /=(U); return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator +=(
    const doubleSubTensor& U) {
  doubleComplexSubTensor::operator +=(U); return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator -=(
    const doubleSubTensor& U) {
  doubleComplexSubTensor::operator -=(U); return *this; }

inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator  =(
    const doubleComplexSubTensor& U) {
  doubleComplexSubTensor::operator  =(U); return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator *=(
    const doubleComplexSubTensor& U) {
  doubleComplexSubTensor::operator *=(U); return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator /=(
    const doubleComplexSubTensor& U) {
  doubleComplexSubTensor::operator /=(U); return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator +=(
    const doubleComplexSubTensor& U) {
  doubleComplexSubTensor::operator +=(U); return *this; }
inline
doubleComplexSubArray3&
		doubleComplexSubArray3::operator -=(
    const doubleComplexSubTensor& U) {
  doubleComplexSubTensor::operator -=(U); return *this; }


inline
bool		doubleComplexSubTensor::operator ==(const double& s) const {
  return operator ==(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		doubleComplexSubTensor::operator !=(const double& s) const {
  return operator !=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		operator ==(
    const double& s, const doubleComplexSubTensor& T) { return T == s; }
inline
bool		operator !=(
    const double& s, const doubleComplexSubTensor& T) { return T != s; }
inline
bool		doubleComplexSubTensor::operator ==(
    const doubleComplex& s) const {
  return operator ==(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		doubleComplexSubTensor::operator !=(
    const doubleComplex& s) const {
  return operator !=(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		operator ==(const doubleComplex& s,
    const doubleComplexSubTensor& T) { return T == s; }
inline
bool		operator !=(const doubleComplex& s,
    const doubleComplexSubTensor& T) { return T != s; }
inline
bool		doubleSubTensor::operator ==(
    const doubleComplexSubTensor& U) const { return U == *this; }
inline
bool		doubleSubTensor::operator !=(
    const doubleComplexSubTensor& U) const { return U != *this; }
inline
bool		doubleSubTensor::operator ==(const doubleComplex& s) const {
  return operator ==(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		doubleSubTensor::operator !=(const doubleComplex& s) const {
  return operator !=(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }

inline
doubleComplexSubTensor&
		doubleComplexSubTensor::operator  =(const double& s) {
  return operator  =(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleComplexSubTensor&
		doubleComplexSubTensor::operator *=(const double& s) {
  return operator *=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleComplexSubTensor&
		doubleComplexSubTensor::operator /=(const double& s) {
  return operator /=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleComplexSubTensor&
		doubleComplexSubTensor::operator +=(const double& s) {
  return operator +=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleComplexSubTensor&
		doubleComplexSubTensor::operator -=(const double& s) {
  return operator -=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }


inline
doubleComplexSubTensor&
		doubleComplexSubTensor::operator  =(const doubleComplex& s) {
  return operator  =(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleComplexSubTensor&
		doubleComplexSubTensor::operator *=(const doubleComplex& s) {
  return operator *=(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleComplexSubTensor&
		doubleComplexSubTensor::operator /=(const doubleComplex& s) {
  return operator /=(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleComplexSubTensor&
		doubleComplexSubTensor::operator +=(const doubleComplex& s) {
  return operator +=(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleComplexSubTensor&
		doubleComplexSubTensor::operator -=(const doubleComplex& s) {
  return operator -=(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }

class doubleComplexTensor: public doubleComplexSubTensor {
  public:
  // Constructors
		doubleComplexTensor(void);
  explicit	doubleComplexTensor(Extent l, Extent m = 1, Extent n = 1);
		doubleComplexTensor(Extent l, Extent m, Extent n,
      const doubleComplex& s);
  explicit	doubleComplexTensor(const doubleSubTensor& T);
		doubleComplexTensor(const doubleSubTensor& T,
				     const doubleSubTensor& S);
		doubleComplexTensor(const doubleComplexSubTensor& T);
		doubleComplexTensor(const doubleComplexTensor& T);
		~doubleComplexTensor(void);
  // Functions
  doubleComplexTensor&
		resize(void);
  doubleComplexTensor&
		resize(Extent l, Extent m = 1, Extent n = 1);
  doubleComplexTensor&
		resize_(Extent m, Extent l);
  doubleComplexTensor&
		resize_(Extent n, Extent m, Extent l);
  doubleComplexTensor&
		resize(Extent l, Extent m, Extent n,
      const doubleComplex& s);
  doubleComplexTensor&
		resize_(Extent n, Extent m, Extent l,
      const doubleComplex& s);
  doubleComplexTensor&
		resize(const doubleSubTensor& T);
  doubleComplexTensor&
		resize(const doubleSubTensor& T,
		       const doubleSubTensor& S);
  doubleComplexTensor&
		resize(const doubleComplexSubTensor& T);
private:
  doubleComplexTensor&
		resize(const doubleComplexHandle& h,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleComplexTensor&
		free(void) { doubleComplexSubTensor::free(); return *this; }
public:
  // Operators
  doubleComplexTensor&
		operator  =(const double& s);
  doubleComplexTensor&
		operator  =(const doubleComplex& s);
  doubleComplexTensor&
		operator  =(const doubleSubTensor& U);
  doubleComplexTensor&
		operator  =(const doubleComplexSubTensor& U);
  doubleComplexTensor&
		operator  =(const doubleComplexTensor& U);
  };

// Constructors
inline		doubleComplexTensor::doubleComplexTensor(void):
  doubleComplexSubTensor() { }
inline		doubleComplexTensor::doubleComplexTensor(
    Extent l, Extent m, Extent n):
  doubleComplexSubTensor(allocate(l, m, n), (Offset)0,
    l, (Stride)n*(Stride)m, m, (Stride)n, n,  (Stride)1) {
#ifdef SVMT_DEBUG_MODE
  doubleComplexSubTensor::operator =(doubleComplex(doubleBad, doubleBad));
#endif // SVMT_DEBUG_MODE
  }
inline		doubleComplexTensor::doubleComplexTensor(
    Extent l, Extent m, Extent n, const doubleComplex& s):
  doubleComplexSubTensor(allocate(l, m, n), (Offset)0,
    l, (Stride)n*(Stride)m, m, (Stride)n, n, (Stride)1) {
  doubleComplexSubTensor::operator  =(s); }
inline		doubleComplexTensor::doubleComplexTensor(
    const doubleSubTensor& T): doubleComplexSubTensor(
      allocate(T.extent3(), T.extent2(), T.extent1()), (Offset)0,
    T.extent3(), (Stride)T.extent1()*(Stride)T.extent2(),
    T.extent2(), (Stride)T.extent1(), T.extent1(), (Stride)1) {
  doubleComplexSubTensor::operator  =(T); }
inline		doubleComplexTensor::doubleComplexTensor(
    const doubleSubTensor& T, const doubleSubTensor& S):
  doubleComplexSubTensor(
      allocate(T.extent3(), T.extent2(), T.extent1()), (Offset)0,
    T.extent3(), (Stride)T.extent1()*(Stride)T.extent2(),
    T.extent2(), (Stride)T.extent1(), T.extent1(), (Stride)1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexTensor::doubleComplexTensor(\n"
    "const doubleSubTensor&, const doubleSubTensor&)",
    T.extent3(), S.extent3(),
    T.extent2(), S.extent2(),
    T.extent1(), S.extent1());
#endif // SVMT_DEBUG_MODE
  real() = T; imag() = S; }
inline		doubleComplexTensor::doubleComplexTensor(
    const doubleComplexSubTensor& T):
  doubleComplexSubTensor(
      allocate(T.extent3(), T.extent2(), T.extent1()), (Offset)0,
    T.extent3(), (Stride)T.extent1()*(Stride)T.extent2(),
    T.extent2(), (Stride)T.extent1(), T.extent1(), (Stride)1) {
  doubleComplexSubTensor::operator  =(T); }
inline		doubleComplexTensor::doubleComplexTensor(
    const doubleComplexTensor& T):
  doubleComplexSubTensor(
      allocate(T.extent3(), T.extent2(), T.extent1()), (Offset)0,
    T.extent3(), (Stride)T.extent1()*(Stride)T.extent2(),
    T.extent2(), (Stride)T.extent1(), T.extent1(), (Stride)1) {
  doubleComplexSubTensor::operator  =(T); }
inline		doubleComplexTensor::~doubleComplexTensor(void) { free(); }

// Assignment Operators
inline
doubleComplexTensor&	doubleComplexTensor::operator  =(const double& s) {
  doubleComplexSubTensor::operator =(s); return *this; }
inline
doubleComplexTensor&	doubleComplexTensor::operator  =(
    const doubleComplex& s) {
  doubleComplexSubTensor::operator =(s); return *this; }
inline
doubleComplexTensor&	doubleComplexTensor::operator  =(
    const doubleSubTensor& U) {
  doubleComplexSubTensor::operator =(U); return *this; }
inline
doubleComplexTensor&	doubleComplexTensor::operator  =(
    const doubleComplexSubTensor& U) {
  doubleComplexSubTensor::operator =(U); return *this; }
inline
doubleComplexTensor&	doubleComplexTensor::operator  =(
    const doubleComplexTensor& U) {
  doubleComplexSubTensor::operator =(U); return *this; }

// Functions
inline
doubleComplexTensor&
		doubleComplexTensor::resize(void) {
  delete [] (double*)H; doubleComplexSubTensor::resize(); return *this; }
inline
doubleComplexTensor&
		doubleComplexTensor::resize(Extent l, Extent m, Extent n) {
  free(); doubleComplexSubTensor::resize(allocate(l, m, n), (Offset)0,
      l, (Stride)n*(Stride)m, m, (Stride)n, n, (Stride)1);
#ifdef SVMT_DEBUG_MODE
  *this = doubleComplex(doubleBad, doubleBad);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
doubleComplexTensor&
		doubleComplexTensor::resize_(Extent m, Extent l) {
  return resize(l, m); }
inline
doubleComplexTensor&
		doubleComplexTensor::resize_(Extent n, Extent m, Extent l) {
  return resize(l, m, n); }
inline
doubleComplexTensor&
		doubleComplexTensor::resize(Extent l, Extent m, Extent n,
    const doubleComplex& s) { return resize(l, m, n) = s; }
inline
doubleComplexTensor&
		doubleComplexTensor::resize_(Extent n, Extent m, Extent l,
    const doubleComplex& s) { return resize(l, m, n, s); }
inline
doubleComplexTensor&
		doubleComplexTensor::resize(const doubleSubTensor& T) {
  return resize(T.extent3(), T.extent2(), T.extent1()) = T; }
inline
doubleComplexTensor&
		doubleComplexTensor::resize(
    const doubleSubTensor& T, const doubleSubTensor& S) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_conformance(
    "doubleComplexTensor::resize(\n"
    "const doubleSubTensor&, const doubleSubTensor&)",
    T.extent3(), S.extent3(),
    T.extent2(), S.extent2(),
    T.extent1(), S.extent1());
#endif // SVMT_DEBUG_MODE
  resize(T.extent3(), T.extent2(), T.extent1()); real() = T; imag() = S;
  return *this; }
inline
doubleComplexTensor&
		doubleComplexTensor::resize(
    const doubleComplexSubTensor& T) {
  return resize(T.extent3(), T.extent2(), T.extent1()) = T; }

inline
const
doubleComplexTensor
		doubleComplexSubMatrix::afore(
    const doubleComplexSubMatrix& N) const {
  return subtensor().afore(N.subtensor()); }
inline
const
doubleComplexTensor
		doubleComplexSubMatrix::afore(
    const doubleComplexSubTensor& T) const { return subtensor().afore(T); }
inline
const
doubleComplexTensor
		doubleComplexSubTensor::afore(
    const doubleComplexSubMatrix& M) const { return afore(M.subtensor()); }
inline
const
doubleComplexTensor
		doubleComplexSubTensor::above_(
    const doubleComplexSubTensor& T) const { return aside(T); }
inline
const
doubleComplexTensor
		doubleComplexSubTensor::aside_(
    const doubleComplexSubTensor& T) const { return above(T); }

inline
const
doubleComplexTensor
		doubleSubTensor::operator *(
    const doubleComplexSubTensor& U) const { return U*(*this); }
inline
const
doubleComplexTensor
		doubleSubTensor::operator +(
    const doubleComplexSubTensor& U) const { return U + *this; }

inline
const
doubleComplexTensor
		doubleComplexSubTensor::operator *(const double& s) const {
  return operator *(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexTensor
		doubleComplexSubTensor::operator /(const double& s) const {
  return operator /(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexTensor
		doubleComplexSubTensor::operator +(const double& s) const {
  return operator +(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexTensor
		doubleComplexSubTensor::operator -(const double& s) const {
  return operator -(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexTensor
		operator *(const double& s, const doubleComplexSubTensor& T) {
  return doubleSubArray3((double*)&s, (Offset)0,
    T.extent3(), (Stride)0, T.extent2(), (Stride)0, T.extent1(), (Stride)0)*T; }
inline
const
doubleComplexTensor
		operator /(const double& s, const doubleComplexSubTensor& T) {
  return doubleSubArray3((double*)&s, (Offset)0,
    T.extent3(), (Stride)0, T.extent2(), (Stride)0, T.extent1(), (Stride)0)/T; }
inline
const
doubleComplexTensor
		operator +(const double& s, const doubleComplexSubTensor& T) {
  return doubleSubArray3((double*)&s, (Offset)0,
  T.extent3(), (Stride)0, T.extent2(), (Stride)0, T.extent1(), (Stride)0) + T; }
inline
const
doubleComplexTensor
		operator -(const double& s, const doubleComplexSubTensor& T) {
  return doubleSubArray3((double*)&s, (Offset)0,
  T.extent3(), (Stride)0, T.extent2(), (Stride)0, T.extent1(), (Stride)0) - T; }

inline
const
doubleComplexTensor
		doubleComplexSubTensor::operator *(
    const doubleComplex& s) const {
  return operator *(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexTensor
		doubleComplexSubTensor::operator /(
    const doubleComplex& s) const {
  return operator /(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexTensor
		doubleComplexSubTensor::operator +(
    const doubleComplex& s) const {
  return operator +(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexTensor
		doubleComplexSubTensor::operator -(
    const doubleComplex& s) const {
  return operator -(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexTensor
		doubleSubTensor::operator *(const doubleComplex& s) const {
  return operator *(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexTensor
		doubleSubTensor::operator /(const doubleComplex& s) const {
  return operator /(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexTensor
		doubleSubTensor::operator +(const doubleComplex& s) const {
  return operator +(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleComplexTensor
		doubleSubTensor::operator -(const doubleComplex& s) const {
  return operator -(doubleComplexSubArray3((double*)&s.real(), (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }

inline
const
doubleComplexTensor
		operator *(const doubleComplex& s, const doubleSubTensor& T) {
  return doubleComplexSubArray3((double*)&s.real(), (Offset)0,
    T.extent3(), (Stride)0, T.extent2(), (Stride)0, T.extent1(), (Stride)0)*T; }
inline
const
doubleComplexTensor
		operator /(const doubleComplex& s, const doubleSubTensor& T) {
  return doubleComplexSubArray3((double*)&s.real(), (Offset)0,
    T.extent3(), (Stride)0, T.extent2(), (Stride)0, T.extent1(), (Stride)0)/T; }
inline
const
doubleComplexTensor
		operator +(const doubleComplex& s, const doubleSubTensor& T) {
  return doubleComplexSubArray3((double*)&s.real(), (Offset)0,
  T.extent3(), (Stride)0, T.extent2(), (Stride)0, T.extent1(), (Stride)0) + T; }
inline
const
doubleComplexTensor
		operator -(const doubleComplex& s, const doubleSubTensor& T) {
  return doubleComplexSubArray3((double*)&s.real(), (Offset)0,
  T.extent3(), (Stride)0, T.extent2(), (Stride)0, T.extent1(), (Stride)0) - T; }
inline
const
doubleComplexTensor
		operator *(const doubleComplex& s,
    const doubleComplexSubTensor& T) {
  return doubleComplexSubArray3((double*)&s.real(), (Offset)0,
    T.extent3(), (Stride)0, T.extent2(), (Stride)0, T.extent1(), (Stride)0)*T; }
inline
const
doubleComplexTensor
		operator /(const doubleComplex& s,
    const doubleComplexSubTensor& T) {
  return doubleComplexSubArray3((double*)&s.real(), (Offset)0,
    T.extent3(), (Stride)0, T.extent2(), (Stride)0, T.extent1(), (Stride)0)/T; }
inline
const
doubleComplexTensor
		operator +(const doubleComplex& s,
    const doubleComplexSubTensor& T) {
  return doubleComplexSubArray3((double*)&s.real(), (Offset)0,
  T.extent3(), (Stride)0, T.extent2(), (Stride)0, T.extent1(), (Stride)0) + T; }
inline
const
doubleComplexTensor
		operator -(const doubleComplex& s,
    const doubleComplexSubTensor& T) {
  return doubleComplexSubArray3((double*)&s.real(), (Offset)0,
  T.extent3(), (Stride)0, T.extent2(), (Stride)0, T.extent1(), (Stride)0) - T; }


#endif /* _doubleComplexTensor_h */

