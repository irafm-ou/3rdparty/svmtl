#ifndef _boolVector_h
#define _boolVector_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<offsetVector.h>
#include<boolScalar.h>

class boolSubVector {
  private:
  static
  Extent	C;
  // Representation
  boolHandle
		H;
  Offset	O;
  Extent	N;
  Stride	S;
  public:
  // Constructors
		boolSubVector(void);
		boolSubVector(const boolHandle& h,
      Offset o, Extent n, Stride s);
		boolSubVector(const boolSubVector& v);
		~boolSubVector(void);
  // Functions
  static
  Extent&      columns(void);
  const
  boolHandle&	handle(void) const;
  Offset	offset(void) const;
  Extent	extent(void) const;
  Extent	length(void) const;
  Stride	stride(void) const;
  bool		 empty(void) const;
  static
  boolHandle allocate(Extent n);
  boolSubVector&
		  free(void);
  boolSubVector&
		resize(void);
  boolSubVector&
		resize(const boolHandle& h,
      Offset o, Extent n, Stride s);
  boolSubVector&
		resize(const boolSubVector& v);
  static
  bool	      contains(Offset j, Extent n, Stride s, Extent l);
  bool	      contains(Offset j, Extent n, Stride s) const;
  static
  bool	      contains_(Offset j, Extent n, Stride s, Extent l);
  bool	      contains_(Offset j, Extent n, Stride s) const;
  boolSubVector	   sub(Offset j, Extent n, Stride s);
  const
  boolSubVector	   sub(Offset j, Extent n, Stride s) const;
  boolSubVector	   sub_(Offset j, Extent n, Stride s);
  const
  boolSubVector	   sub_(Offset j, Extent n, Stride s) const;
  const
  boolSubMatrix
	     submatrix(Extent m = 1) const;
  const
  boolSubTensor
	     subtensor(Extent m = 1, Extent l = 1) const;

  boolSubVector
		     r(void);
  const
  boolSubVector
		     r(void) const;
  boolSubVector&
	       reverse(void);
  boolSubVector	  even(void);
  const
  boolSubVector	  even(void) const;
  boolSubVector	   odd(void);
  const
  boolSubVector	   odd(void) const;
  boolSubVector	  even_(void);
  const
  boolSubVector	  even_(void) const;
  boolSubVector	   odd_(void);
  const
  boolSubVector	   odd_(void) const;
  boolSubVector&  swap(Offset j, Offset k);
  boolSubVector&  swap(boolSubVector& w);
  boolSubVector&  swap_(Offset j, Offset k);
  boolSubVector&
		rotate(Stride n);
  boolSubVector&
		 shift(Stride n, bool b = false);
  const
  boolVector	    eq(bool b) const;
  const
  boolVector	    ne(bool b) const;
  const
  boolVector	    eq(const boolSubVector& w) const;
  const
  boolVector	    ne(const boolSubVector& w) const;
  Extent	 zeros(void) const;
  const
  offsetVector	 index(void) const;
  const
  boolVector	 aside(const boolSubVector& w) const;
  const
  boolMatrix	 above(const boolSubMatrix& M) const;
  const
  boolVector	 above_(const boolSubVector& w) const;
  const
  boolMatrix	 aside_(const boolSubMatrix& M) const;
  const
  boolVector	 apply(const bool (*f)(const bool&)) const;
  const
  boolVector	 apply(      bool (*f)(const bool&)) const;
  const
  boolVector	 apply(      bool (*f)(      bool )) const;

  // Operators
  boolSubScalar	operator [](Offset j);
  const
  boolSubScalar	operator [](Offset j) const;
  boolSubScalar	operator ()(Offset j);
  const
  boolSubScalar	operator ()(Offset j) const;
  const
  boolVector	operator !(void) const;
  bool		operator ==(bool b) const;
  bool		operator !=(bool b) const;
  bool		operator ==(const boolSubVector& w) const;
  bool		operator !=(const boolSubVector& w) const;
  const
  boolVector	operator &&(bool b) const;
  const
  boolVector	operator ||(bool b) const;
  const
  boolVector	operator &&(const boolSubVector& w) const;
  const
  boolVector	operator ||(const boolSubVector& w) const;
  boolSubVector&
		operator  =(bool b);
  boolSubVector&
		operator  =(const boolSubVector& w);
  friend class boolVector;
  };

// Constructors
inline		boolSubVector::boolSubVector(void):
  H((bool*)0), O((Offset)0),
  N((Extent)0), S((Stride)0) { }
inline		boolSubVector::boolSubVector(const boolHandle& h, Offset o,
    Extent n, Stride s): H(h), O(o), N(n), S(s) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment("boolSubVector::boolSubVector(\n"
    "const boolHandle&, Offset, Extent, Stride)",
    o, n, s);
#endif // SVMT_DEBUG_MODE
  }
inline		boolSubVector::boolSubVector(const boolSubVector& v):
  H(v.H), O(v.O), N(v.N), S(v.S) { }
inline		boolSubVector::~boolSubVector(void) { }

// Functions
inline
Extent&		boolSubVector::columns(void) { return C; }
inline
const
boolHandle&	boolSubVector::handle(void) const { return H; }
inline
Offset		boolSubVector::offset(void) const { return O; }
inline
Extent		boolSubVector::extent(void) const { return N; }
inline
Extent		boolSubVector::length(void) const { return N; }
inline
Stride		boolSubVector::stride(void) const { return S; }
inline
bool		boolSubVector::empty(void) const {
  return handle().empty()||0 == extent(); }
inline
boolHandle	boolSubVector::allocate(Extent n) {
  return boolHandle(new bool[n]); }
inline
boolSubVector&	boolSubVector::free(void) {
  delete [] (bool*)H; return *this; }
inline
boolSubVector&	boolSubVector::resize(void) {
  H = boolHandle((bool*)0); O = (Offset)0;
  N = (Extent)0; S = (Stride)0; return *this; }
inline
boolSubVector&	boolSubVector::resize(const boolHandle& h, Offset o,
    Extent n, Stride s) { H = h; O = o; N = n; S = s;
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment("boolSubVector::resize(const boolHandle&,\n"
    "Offset, Extent, Stride)",
    o, n, s);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
boolSubVector&	boolSubVector::resize(const boolSubVector& v) {
  return resize(v.handle(), v.offset(),
    v.extent(), v.stride()); }
inline
bool		boolSubVector::contains(
  Offset j, Extent n, Stride s, Extent l) {
  return 0 == n || j < l && (s < 0)? -s*(n - 1) <= j: j + s*(n - 1) < l; }
inline
bool		boolSubVector::contains(Offset j, Extent n, Stride s) const {
  return boolSubVector::contains(j, n, s, extent()); }
inline
bool		boolSubVector::contains_(
    Offset j, Extent n, Stride s, Extent l) { return contains(j-1, n, s, l); }
inline
bool		boolSubVector::contains_(
    Offset j, Extent n, Stride s) const { return contains(j-1, n, s); }

inline
boolSubVector	boolSubVector::sub(Offset j, Extent n, Stride s) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "boolSubVector::sub(Offset, Extent, Stride)",
    j, n, s, extent());
#endif // SVMT_DEBUG_MODE
  return boolSubVector(handle(),
    offset() + (Stride)j*stride(),
    n, s*stride()); }
inline
const
boolSubVector	boolSubVector::sub(Offset j, Extent n, Stride s) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "boolSubVector::sub(Offset, Extent, Stride) const",
    j, n, s, extent());
#endif // SVMT_DEBUG_MODE
  return boolSubVector(handle(),
    offset() + (Stride)j*stride(),
    n, s*stride()); }
inline
boolSubVector	boolSubVector::sub_(Offset j, Extent n, Stride s) {
  return sub(j-1, n, s); }
inline
const
boolSubVector	boolSubVector::sub_(Offset j, Extent n, Stride s) const {
  return sub(j-1, n, s); }
inline
const
boolSubVector	boolSubScalar::subvector(Extent n) const {
  return boolSubVector(handle(), offset(), n, (Stride)0); }
inline
boolSubVector	boolSubVector::r(void) { return boolSubVector(
  handle(), offset() + (Stride)(extent() - 1)*stride(), extent(), -stride()); }
inline
const
boolSubVector	boolSubVector::r(void) const { return boolSubVector(
  handle(), offset() + (Stride)(extent() - 1)*stride(), extent(), -stride()); }

inline
boolSubVector	boolSubVector::even(void) { return boolSubVector(
  handle(), offset(), (extent() + 1) >> 1, stride() << 1); }
inline
const
boolSubVector	boolSubVector::even(void) const { return boolSubVector(
  handle(), offset(), (extent() + 1) >> 1, stride() << 1); }
inline
boolSubVector	boolSubVector::odd(void) { return boolSubVector(
  handle(), offset() + stride(), extent() >> 1, stride() << 1); }
inline
const
boolSubVector	boolSubVector::odd(void) const { return boolSubVector(
  handle(), offset() + stride(), extent() >> 1, stride() << 1); }
inline
boolSubVector	boolSubVector::even_(void) { return odd(); }
inline
const
boolSubVector	boolSubVector::even_(void) const { return odd(); }
inline
boolSubVector	boolSubVector::odd_(void) { return even(); }
inline
const
boolSubVector	boolSubVector::odd_(void) const { return even(); }

// Operators
inline
boolSubScalar	boolSubVector::operator [](Offset j) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "boolSubVector::operator [](Offset)", j, extent());
#endif // SVMT_DEBUG_MODE
  return boolSubScalar(handle(), offset() + (Stride)j*stride()); }
inline
const
boolSubScalar	boolSubVector::operator [](Offset j) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "boolSubVector::operator [](Offset) const", j, extent());
#endif // SVMT_DEBUG_MODE
  return boolSubScalar(handle(), offset() + (Stride)j*stride()); }
inline
boolSubScalar	boolSubVector::operator ()(Offset j) {
  return operator [](j-1); }
inline
const
boolSubScalar	boolSubVector::operator ()(Offset j) const {
  return operator [](j-1); }

std::istream&	operator >>(std::istream& s,       boolSubVector& v);
std::ostream&	operator <<(std::ostream& s, const boolSubVector& v);

inline
boolSubVector&	boolSubVector::swap(Offset j, Offset k) {
  boolSubVector&	v = *this;
  bool t = v[j]; v[j] = v[k]; v[k] = t; return *this; }
inline
boolSubVector&	boolSubVector::swap_(Offset j, Offset k) {
  return swap(j-1, k-1); }

class boolSubArray1: public boolSubVector {
  public:
  // Constructors
		boolSubArray1(void);
		boolSubArray1(bool* p,
      Offset o, Extent n, Stride s);
		boolSubArray1(const boolSubArray1& v);
		~boolSubArray1(void);
  // Functions
  boolSubArray1&
		resize(void);
  boolSubArray1&			// resize from pointer
		resize(bool* p,
      Offset o, Extent n, Stride s);
  boolSubArray1&			// resize from SubArray1
		resize(const boolSubArray1& v);
  private:
  boolSubArray1&			// prevent resize from Handle
		resize(const boolHandle& h,
      Offset o, Extent n, Stride s);
  boolSubArray1&			// prevent resize from SubVector
		resize(const boolSubVector& v);
  public:
  boolSubArray1	   sub(Offset j, Extent n, Stride s);
  const
  boolSubArray1	   sub(Offset j, Extent n, Stride s) const;
  boolSubArray1	   sub_(Offset j, Extent n, Stride s);
  const
  boolSubArray1	   sub_(Offset j, Extent n, Stride s) const;
  const
  boolSubArray2
	     submatrix(Extent m = 1) const;
  const
  boolSubArray3
	     subtensor(Extent m = 1, Extent l = 1) const;
  boolSubArray1
		     r(void);
  const
  boolSubArray1
		     r(void) const;
  boolSubArray1&
	       reverse(void);
  boolSubArray1	  even(void);
  const
  boolSubArray1	  even(void) const;
  boolSubArray1	   odd(void);
  const
  boolSubArray1	   odd(void) const;
  boolSubArray1	  even_(void);
  const
  boolSubArray1	  even_(void) const;
  boolSubArray1	   odd_(void);
  const
  boolSubArray1	   odd_(void) const;
  boolSubArray1&  swap(Offset j, Offset k);
  boolSubArray1&  swap_(Offset j, Offset k);
  boolSubArray1&  swap(boolSubVector& w);
  boolSubArray1&
		rotate(Stride n);
  boolSubArray1&
		 shift(Stride n, bool b = false);

  // Operators
  boolSubArray0	operator [](Offset j);
  const
  boolSubArray0	operator [](Offset j) const;
  boolSubArray0	operator ()(Offset j);
  const
  boolSubArray0	operator ()(Offset j) const;
  boolSubArray1&
		operator  =(bool b);
  boolSubArray1&
		operator  =(const boolSubVector& w);
  };

// Constructors
inline		boolSubArray1::boolSubArray1(void): boolSubVector() { }
inline		boolSubArray1::boolSubArray1(bool* p,
    Offset o, Extent n, Stride s):
  boolSubVector(boolHandle(p), o, n, s) { }
inline		boolSubArray1::boolSubArray1(const boolSubArray1& v):
  boolSubVector(v) { }
inline		boolSubArray1::~boolSubArray1(void) { }

// Functions
inline
boolSubArray1&	boolSubArray1::resize(void) {
  boolSubVector::resize(); return *this; }
inline
boolSubArray1&	boolSubArray1::resize(bool* p,
    Offset o, Extent n, Stride s) {
  boolSubVector::resize(boolHandle(p), o, n, s); return *this; }
inline
boolSubArray1&	boolSubArray1::resize(const boolSubArray1& v) {
  boolSubVector::resize(v); return *this; }
inline
boolSubArray1	boolSubArray1::sub(Offset j, Extent n, Stride s) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "boolSubArray1::sub(Offset, Extent, Stride)",
    j, n, s, extent());
#endif // SVMT_DEBUG_MODE
  return boolSubArray1((bool*)(boolHandle&)handle(),
    offset() + (Stride)j*stride(),
    n, s*stride()); }
inline
const
boolSubArray1	boolSubArray1::sub(Offset j, Extent n, Stride s) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "boolSubArray1::sub(Offset, Extent, Stride) const",
    j, n, s, extent());
#endif // SVMT_DEBUG_MODE
  return boolSubArray1((bool*)(boolHandle&)handle(),
    offset() + (Stride)j*stride(),
    n, s*stride()); }
inline
boolSubArray1	boolSubArray1::sub_(Offset j, Extent n, Stride s) {
  return sub(j-1, n, s); }
inline
const
boolSubArray1	boolSubArray1::sub_(Offset j, Extent n, Stride s) const {
  return sub(j-1, n, s); }

inline
const
boolSubArray1	boolSubArray0::subvector(Extent n) const {
  return boolSubArray1((bool*)(boolHandle&)handle(),
    offset(), n, (Stride)0); }
inline
boolSubArray1	boolSubArray1::r(void) {
  return boolSubArray1((bool*)(boolHandle&)handle(),
    offset() + (Stride)(extent() - 1)*stride(), extent(), -stride()); }
inline
const
boolSubArray1	boolSubArray1::r(void) const {
  return boolSubArray1((bool*)(boolHandle&)handle(),
    offset() + (Stride)(extent() - 1)*stride(), extent(), -stride()); }
inline
boolSubArray1&	boolSubArray1::reverse(void) {
  boolSubVector::reverse(); return *this; }
inline
boolSubArray1	boolSubArray1::even(void) {
  return boolSubArray1((bool*)(boolHandle&)handle(),
    offset(), (extent() + 1) >> 1, stride() << 1); }
inline
const
boolSubArray1	boolSubArray1::even(void) const {
  return boolSubArray1((bool*)(boolHandle&)handle(),
    offset(), (extent() + 1) >> 1, stride() << 1); }
inline
boolSubArray1	boolSubArray1::odd(void) {
  return boolSubArray1((bool*)(boolHandle&)handle(),
    offset() + stride(), extent() >> 1, stride() << 1); }
inline
const
boolSubArray1	boolSubArray1::odd(void) const {
  return boolSubArray1((bool*)(boolHandle&)handle(),
    offset() + stride(), extent() >> 1, stride() << 1); }
inline
boolSubArray1	boolSubArray1::even_(void) { return odd(); }
inline
const
boolSubArray1	boolSubArray1::even_(void) const { return odd(); }
inline
boolSubArray1	boolSubArray1::odd_(void) { return even(); }
inline
const
boolSubArray1	boolSubArray1::odd_(void) const { return even(); }
inline
boolSubArray1&	boolSubArray1::swap(Offset j, Offset k) {
  boolSubVector::swap(j, k); return *this; }
inline
boolSubArray1&	boolSubArray1::swap(boolSubVector& w) {
  boolSubVector::swap(w); return *this; }
inline
boolSubArray1&	boolSubArray1::swap_(Offset j, Offset k) {
  return swap(j-1, k-1); }
inline
boolSubArray1&  boolSubArray1::rotate(Stride n) {
  boolSubVector::rotate(n); return *this; }
inline
boolSubArray1&  boolSubArray1::shift(Stride n, bool b) {
  boolSubVector::shift(n, b); return *this; }

// Operators
inline
boolSubArray0	boolSubArray1::operator [](Offset j) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "boolSubArray1::operator [](Offset)", j, extent());
#endif // SVMT_DEBUG_MODE
  return boolSubArray0(
    (bool*)(boolHandle&)handle(), offset() + (Stride)j*stride()); }
inline
const
boolSubArray0	boolSubArray1::operator [](Offset j) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "boolSubArray1::operator [](Offset) const", j, extent());
#endif // SVMT_DEBUG_MODE
  return boolSubArray0(
    (bool*)(boolHandle&)handle(), offset() + (Stride)j*stride()); }
inline
boolSubArray0	boolSubArray1::operator ()(Offset j) {
  return operator [](j-1); }
inline
const
boolSubArray0	boolSubArray1::operator ()(Offset j) const {
  return operator [](j-1); }
inline
boolSubArray1&	boolSubArray1::operator  =(bool b) {
  boolSubVector::operator  =(boolSubArray1((bool*)&b,
    (Offset)0, extent(), (Stride)0)); return *this; }
inline
boolSubArray1&	boolSubArray1::operator  =(const boolSubVector& w) {
  boolSubVector::operator  =(w); return *this; }

// boolSubVector operator definitions which use boolSubArray1
inline
bool		boolSubVector::operator ==(bool b) const {
  return operator ==(boolSubArray1(
      (bool*)&b, (Offset)0, extent(), (Stride)0)); }
inline
bool		boolSubVector::operator !=(bool b) const {
  return operator !=(boolSubArray1(
      (bool*)&b, (Offset)0, extent(), (Stride)0)); }
inline
bool		operator ==(bool b, const boolSubVector& v) { return v == b; }
inline
bool		operator !=(bool b, const boolSubVector& v) { return v != b; }
inline
bool		any(const boolSubVector& v) { return !(v == false); }
inline
bool		all(const boolSubVector& v) { return  (v == true); }
inline
boolSubVector&	boolSubVector::operator  =(bool b) {
  return operator  =(boolSubArray1(
  (bool*)&b, (Offset)0, extent(), (Stride)0)); }

class boolVector: public boolSubVector {
  public:
  // Constructors
		boolVector(void);
  explicit	boolVector(Extent n);
		boolVector(Extent n, bool b);
		boolVector(const boolSubVector& v);
		boolVector(const boolVector& v);
		~boolVector(void);
  // Functions
  boolVector&	resize(void);
  boolVector&	resize(Extent n);
  boolVector&	resize(Extent n, bool b);
  boolVector&	resize(const boolSubVector& v);
private:
  boolVector&	resize(const boolHandle& h, Offset o, Extent n, Stride s);
  boolVector&	free(void) { boolSubVector::free(); return *this; }
public:
  // Operators
  boolVector&	operator  =(bool b);
  boolVector&	operator  =(const boolSubVector& v);
  boolVector&	operator  =(const boolVector& v);
  };

// Constructors
inline		boolVector::boolVector(void): boolSubVector() { }
inline		boolVector::boolVector(Extent n):
  boolSubVector(allocate(n), (Offset)0, n, (Stride)1) {
#ifdef SVMT_DEBUG_MODE
  boolSubVector::operator =(true);
#endif // SVMT_DEBUG_MODE
  }
inline		boolVector::boolVector(Extent n, bool b):
  boolSubVector(allocate(n), (Offset)0,
    n, (Stride)1) { boolSubVector::operator =(b); }
inline		boolVector::boolVector(const boolSubVector& v):
  boolSubVector(allocate(v.extent()), (Offset)0,
    v.extent(), (Stride)1) { boolSubVector::operator =(v); }
inline		boolVector::boolVector(const boolVector& v):
  boolSubVector(allocate(v.extent()), (Offset)0,
    v.extent(), (Stride)1) { boolSubVector::operator =(v); }
inline		boolVector::~boolVector(void) { free(); }

// Assignment Operators
inline
boolVector&	boolVector::operator  =(bool b) {
  boolSubVector::operator =(b); return *this; }
inline
boolVector&	boolVector::operator  =(const boolSubVector& w) {
  boolSubVector::operator =(w); return *this; }
inline
boolVector&	boolVector::operator  =(const boolVector& w) {
  boolSubVector::operator =(w); return *this; }

// Functions
inline
boolVector&	boolVector::resize(void) { free();
  boolSubVector::resize(); return *this; }
inline
boolVector&	boolVector::resize(Extent n) { free();
  boolSubVector::resize(allocate(n), (Offset)0, n, (Stride)1);
#ifdef SVMT_DEBUG_MODE
  boolSubVector::operator =(true);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
boolVector&	boolVector::resize(Extent n, bool b) { return resize(n) = b; }
inline
boolVector&	boolVector::resize(const boolSubVector& v) {
  return resize(v.extent()) = v; }

// boolSubVector operator definitions which return boolVector
inline
const
boolVector	boolSubVector::operator &&(bool b) const {
  return operator &&(boolSubArray1((bool*)&b,
    (Offset)0, extent(), (Stride)0)); }
inline
const
boolVector	boolSubVector::operator ||(bool b) const {
  return operator ||(boolSubArray1((bool*)&b,
    (Offset)0, extent(), (Stride)0)); }
inline
const
boolVector	operator &&(bool b, const boolSubVector& v) { return v && b; }
inline
const
boolVector	operator ||(bool b, const boolSubVector& v) { return v || b; }

#endif /* _boolVector_h */

