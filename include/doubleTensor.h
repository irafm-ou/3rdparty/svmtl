#ifndef _doubleTensor_h
#define _doubleTensor_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<boolTensor.h>
#include<doubleMatrix.h>

class doubleSubTensor {
  private:
  // Representation
  doubleHandle
		H;
  Offset	O;
  Extent	N1;
  Stride	S1;
  Extent	N2;
  Stride	S2;
  Extent	N3;
  Stride	S3;
  public:
  // Constructors
		doubleSubTensor(void);
		doubleSubTensor(const doubleHandle& h,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
		doubleSubTensor(const doubleSubTensor& T);
		~doubleSubTensor(void);
  // Functions
  const
  doubleHandle&
		handle(void) const;
  Offset	offset(void) const;
  Extent	extent1(void) const;
  Extent	length1(void) const;
  Stride	stride1(void) const;
  Extent	extent2(void) const;
  Extent	length2(void) const;
  Stride	stride2(void) const;
  Extent	extent3(void) const;
  Extent	length3(void) const;
  Stride	stride3(void) const;
  bool		 empty(void) const;
  static
  doubleHandle
	      allocate(Extent l, Extent m, Extent n);
  static
  doubleHandle
	      allocate_(Extent n, Extent m, Extent l);
  doubleSubTensor&
		  free(void);
  doubleSubTensor&
		resize(void);
  doubleSubTensor&
		resize(const doubleHandle& h,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleSubTensor&
		resize(const doubleSubTensor& T);
  doubleSubTensor&
		resize_(const doubleHandle& h,
      Offset o, Extent n1, Stride s1,
		Extent n2, Stride s2,
		Extent n3, Stride s3);

  bool	      contains(Offset h, Extent n3, Stride s3) const;
  bool	      contains(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2) const;
  bool	      contains(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  bool	      contains_(Offset h, Extent n3, Stride s3) const;
  bool	      contains_(Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;
  bool	      contains_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;
  doubleSubTensor
		   sub(Offset h, Extent n3, Stride s3);
  const
  doubleSubTensor
		   sub(Offset h, Extent n3, Stride s3) const;
  doubleSubTensor
		   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2);
  const
  doubleSubTensor
		   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2) const;
  doubleSubTensor
		   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1);
  const
  doubleSubTensor
		   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  doubleSubTensor
		   sub_(Offset h, Extent n3, Stride s3);
  const
  doubleSubTensor
		   sub_(Offset h, Extent n3, Stride s3) const;
  doubleSubTensor
		   sub_(Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3);
  const
  doubleSubTensor
		   sub_(Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;
  doubleSubTensor
		   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3);
  const
  doubleSubTensor
		   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;

  doubleSubTensor
		     t12(void);
  const
  doubleSubTensor
		     t12(void) const;
  doubleSubTensor
		     t23(void);
  const
  doubleSubTensor
		     t23(void) const;
  doubleSubTensor
		     t31(void);
  const
  doubleSubTensor
		     t31(void) const;
  doubleSubMatrix
		  diag12(void);
  const
  doubleSubMatrix
		  diag12(void) const;
  doubleSubMatrix
		  diag23(void);
  const
  doubleSubMatrix
		  diag23(void) const;
  doubleSubMatrix
		  diag31(void);
  const
  doubleSubMatrix
		  diag31(void) const;
  doubleSubTensor
		     r1(void);
  const
  doubleSubTensor
		     r1(void) const;
  doubleSubTensor&
	       reverse1(void);
  doubleSubTensor
		     r2(void);
  const
  doubleSubTensor
		     r2(void) const;
  doubleSubTensor&
	       reverse2(void);
  doubleSubTensor
		     r3(void);
  const
  doubleSubTensor
		     r3(void) const;
  doubleSubTensor&
	       reverse3(void);
  doubleSubTensor
		     r(void);
  const
  doubleSubTensor
		     r(void) const;
  doubleSubTensor&
	       reverse(void);

  doubleSubTensor
		  even(void);
  const
  doubleSubTensor
		  even(void) const;
  doubleSubTensor
		   odd(void);
  const
  doubleSubTensor
		   odd(void) const;
  doubleSubTensor
		  even_(void);
  const
  doubleSubTensor
		  even_(void) const;
  doubleSubTensor
		   odd_(void);
  const
  doubleSubTensor
		   odd_(void) const;
  doubleSubTensor&
		  rdft(int sign = -1);	// complex to real    dft
  doubleSubTensor&
		  cdft(int sign = -1);	// real    to complex dft
  doubleSubTensor&
		  ramp(void);
  doubleSubTensor&
		  swap(Offset h, Offset k);
  doubleSubTensor&
		  swap(doubleSubTensor& U);
  doubleSubTensor&
		  swap_(Offset h, Offset k);
  doubleSubTensor&
		rotate(Stride n);
  doubleSubTensor&
		 shift(Stride n,const double& s
		  = (double)0);

  const
  doubleMatrix
		   sum(void) const;
  const
  doubleTensor
	   permutation(const offsetSubVector& p) const;
  const
  boolTensor
		    lt(const double& s) const;
  const
  boolTensor
		    le(const double& s) const;
  const
  boolTensor
		    gt(const double& s) const;
  const
  boolTensor
		    ge(const double& s) const;
  const
  boolTensor
		    lt(const doubleSubTensor& U) const;
  const
  boolTensor
		    le(const doubleSubTensor& U) const;
  const
  boolTensor
		    gt(const doubleSubTensor& U) const;
  const
  boolTensor
		    ge(const doubleSubTensor& U) const;
  const
  boolTensor
		    eq(const double& s) const;
  const
  boolTensor
		    ne(const double& s) const;
  const
  boolTensor
		    eq(const doubleComplexSubTensor& U) const;
  const
  boolTensor
		    ne(const doubleComplexSubTensor& U) const;
  const
  boolTensor
		    eq(const doubleSubTensor& U) const;
  const
  boolTensor
		    ne(const doubleSubTensor& U) const;

  Extent	 zeros(void) const;
  const
  offsetVector
		 index(void) const;
  const
  doubleVector
		gather(const offsetSubVector& x) const;
  doubleSubTensor&
	       scatter(const offsetSubVector& x,
		       const doubleSubVector& t);
  const
  doubleTensor
		 aside(const doubleSubTensor& U) const;
  const
  doubleTensor
		 above(const doubleSubTensor& U) const;
  const
  doubleTensor
		 afore(const doubleSubTensor& U) const;
  const
  doubleTensor
		 afore(const doubleSubMatrix& M) const;
  const
  doubleTensor
		 above_(const doubleSubTensor& U) const;
  const
  doubleTensor
		 aside_(const doubleSubTensor& U) const;

  const
  doubleTensor
		  kron(const doubleSubVector& v) const;
  const
  doubleTensor
		  kron(const doubleSubMatrix& M) const;
  const
  doubleTensor
		  kron(const doubleSubTensor& U) const;
  const
  doubleComplexTensor
		  kron(const doubleComplexSubVector& v) const;
  const
  doubleComplexTensor
		  kron(const doubleComplexSubMatrix& M) const;
  const
  doubleComplexTensor
		  kron(const doubleComplexSubTensor& U) const;
  const
  doubleTensor
		 apply(const double (*f)(const double&)) const;
  const
  doubleTensor
		 apply(      double (*f)(const double&)) const;
  const
  doubleTensor
		 apply(      double (*f)(      double )) const;

  // Operators
  doubleSubMatrix
		operator [](Offset h);
  const
  doubleSubMatrix
		operator [](Offset h) const;
  doubleSubMatrix
		operator ()(Offset h);
  const
  doubleSubMatrix
		operator ()(Offset h) const;
  doubleSubVector
		operator ()(Offset i, Offset h);
  const
  doubleSubVector
		operator ()(Offset i, Offset h) const;
  doubleSubScalar
		operator ()(Offset j, Offset i, Offset h);
  const
  doubleSubScalar
		operator ()(Offset j, Offset i, Offset h) const;

  const
  doubleTensor
		operator -(void) const;
//  const
//  doubleSubTensor
//		operator +(void) const;
  const
  doubleTensor
		operator *(const double& s) const;
  const
  doubleTensor
		operator /(const double& s) const;
  const
  doubleTensor
		operator +(const double& s) const;
  const
  doubleTensor
		operator -(const double& s) const;
  const
  doubleComplexTensor
		operator *(const doubleComplex& s) const;
  const
  doubleComplexTensor
		operator /(const doubleComplex& s) const;
  const
  doubleComplexTensor
		operator +(const doubleComplex& s) const;
  const
  doubleComplexTensor
		operator -(const doubleComplex& s) const;
  const
  doubleComplexTensor
		operator *(const doubleComplexSubTensor& U) const;
  const
  doubleComplexTensor
		operator /(const doubleComplexSubTensor& U) const;
  const
  doubleComplexTensor
		operator +(const doubleComplexSubTensor& U) const;
  const
  doubleComplexTensor
		operator -(const doubleComplexSubTensor& U) const;
  const
  doubleTensor
		operator *(const doubleSubTensor& U) const;
  const
  doubleTensor
		operator /(const doubleSubTensor& U) const;
  const
  doubleTensor
		operator +(const doubleSubTensor& U) const;
  const
  doubleTensor
		operator -(const doubleSubTensor& U) const;

  bool		operator < (const double& s) const;
  bool		operator <=(const double& s) const;
  bool		operator > (const double& s) const;
  bool		operator >=(const double& s) const;
  bool		operator < (const doubleSubTensor& U) const;
  bool		operator <=(const doubleSubTensor& U) const;
  bool		operator > (const doubleSubTensor& U) const;
  bool		operator >=(const doubleSubTensor& U) const;
  bool		operator ==(const double& s) const;
  bool		operator !=(const double& s) const;
  bool		operator ==(const doubleComplex& s) const;
  bool		operator !=(const doubleComplex& s) const;
  bool		operator ==(const doubleComplexSubTensor& U) const;
  bool		operator !=(const doubleComplexSubTensor& U) const;
  bool		operator ==(const doubleSubTensor& U) const;
  bool		operator !=(const doubleSubTensor& U) const;
  doubleSubTensor&
		operator  =(const double& s);
  doubleSubTensor&
		operator *=(const double& s);
  doubleSubTensor&
		operator /=(const double& s);
  doubleSubTensor&
		operator +=(const double& s);
  doubleSubTensor&
		operator -=(const double& s);

  doubleSubTensor&
		operator  =(const doubleSubTensor& U);
  doubleSubTensor&
		operator *=(const doubleSubTensor& U);
  doubleSubTensor&
		operator /=(const doubleSubTensor& U);
  doubleSubTensor&
		operator +=(const doubleSubTensor& U);
  doubleSubTensor&
		operator -=(const doubleSubTensor& U);
  friend class doubleTensor;
  };

// Constructors
inline		doubleSubTensor::doubleSubTensor(void):
  H((double*)0), O((Offset)0), N1((Extent)0), S1((Stride)0),
  N2((Extent)0), S2((Stride)0), N3((Extent)0), S3((Stride)0) { }
inline		doubleSubTensor::doubleSubTensor(
    const doubleHandle& h, Offset o,
    Extent n3, Stride s3, Extent n2, Stride s2, Extent n1, Stride s1):
  H(h), O(o), N1(n1), S1(s1), N2(n2), S2(s2), N3(n3), S3(s3) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment(
    "doubleSubTensor::doubleSubTensor(\n"
    "const doubleHandle&, Offset,\n"
    "Extent, Stride, Extent, Stride, Extent, Stride)",
    o, n3, s3, n2, s2, n1, s1);
#endif // SVMT_DEBUG_MODE
  }
inline		doubleSubTensor::doubleSubTensor(
    const doubleSubTensor& T): H(T.H), O(T.O),
    N1(T.N1), S1(T.S1), N2(T.N2), S2(T.S2), N3(T.N3), S3(T.S3) { }
inline		doubleSubTensor::~doubleSubTensor(void) { }

// Functions
inline
const
doubleHandle&
		doubleSubTensor::handle(void) const { return H; }
inline
Offset		doubleSubTensor::offset(void) const { return O; }
inline
Extent		doubleSubTensor::extent1(void) const { return N1; }
inline
Extent		doubleSubTensor::length1(void) const { return N1; }
inline
Stride		doubleSubTensor::stride1(void) const { return S1; }
inline
Extent		doubleSubTensor::extent2(void) const { return N2; }
inline
Extent		doubleSubTensor::length2(void) const { return N2; }
inline
Stride		doubleSubTensor::stride2(void) const { return S2; }
inline
Extent		doubleSubTensor::extent3(void) const { return N3; }
inline
Extent		doubleSubTensor::length3(void) const { return N3; }
inline
Stride		doubleSubTensor::stride3(void) const { return S3; }
inline
bool		doubleSubTensor::empty(void) const {
  return handle().empty()||0 == extent1()||0 == extent2()||0 == extent3(); }
inline
doubleHandle
		doubleSubTensor::allocate(Extent l, Extent m, Extent n)
  { return doubleHandle(new double[l*m*n]); }
inline
doubleHandle
		doubleSubTensor::allocate_(Extent n, Extent m, Extent l)
  { return allocate(l, m, n); }
inline
doubleSubTensor&
		doubleSubTensor::free(void) {
  delete [] (double*)H; return *this; }

inline
doubleSubTensor&
		doubleSubTensor::resize(void) {
  H = doubleHandle((double*)0); O = (Offset)0;
  N1 = (Extent)0; S1 = (Stride)0;
  N2 = (Extent)0; S2 = (Stride)0;
  N3 = (Extent)0; S3 = (Stride)0; return *this; }
inline
doubleSubTensor&
		doubleSubTensor::resize(const doubleHandle& h,
    Offset o, Extent n3, Stride s3,
	      Extent n2, Stride s2,
	      Extent n1, Stride s1) {
  H = h; O = o; N1 = n1; S1 = s1; N2 = n2; S2 = s2; N3 = n3; S3 = s3;
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment(
    "doubleSubTensor::resize(const doubleHandle&,\n"
    "Offset, Extent, Stride, Extent, Stride, Extent, Stride)",
    o, n3, s3, n2, s2, n1, s1);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
doubleSubTensor&
		doubleSubTensor::resize(
    const doubleSubTensor& T) { return resize(T.handle(),
    T.offset(), T.extent3(), T.stride3(),
		T.extent2(), T.stride2(),
		T.extent1(), T.stride1()); }
inline
doubleSubTensor&
		doubleSubTensor::resize_(const doubleHandle& h,
    Offset o, Extent n1, Stride s1,
	      Extent n2, Stride s2,
	      Extent n3, Stride s3) {
  return resize(h, o, n3, s3, n2, s2, n1, s1); }

inline
bool		doubleSubTensor::contains(
    Offset h, Extent n3, Stride s3) const {
  return boolSubVector::contains(
    h, n3, s3, extent3()); }
inline
bool		doubleSubTensor::contains(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2) const {
  return boolSubMatrix::contains(
    h, n3, s3, extent3(),
    i, n2, s2, extent2()); }
inline
bool		doubleSubTensor::contains(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) const {
  return boolSubTensor::contains(
    h, n3, s3, extent3(),
    i, n2, s2, extent2(),
    j, n1, s1, extent1()); }
inline
bool		doubleSubTensor::contains_(
    Offset h, Extent n3, Stride s3) const {
  return contains(h-1, n3, s3); }
inline
bool		doubleSubTensor::contains_(
    Offset i, Extent n2, Stride s2,
    Offset h, Extent n3, Stride s3) const {
  return contains(h-1, n3, s3, i-1, n2, s2); }
inline
bool		doubleSubTensor::contains_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2,
    Offset h, Extent n3, Stride s3) const {
  return contains(h-1, n3, s3, i-1, n2, s2, j-1, n1, s1); }

inline
doubleSubTensor
		doubleSubTensor::sub(
    Offset h, Extent n3, Stride s3) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleSubTensor::sub(Offset, Extent, Stride)",
    h, n3, s3, extent3());
#endif // SVMT_DEBUG_MODE
  return doubleSubTensor(handle(), offset() + (Stride)h*stride3(),
    n3, s3*stride3(), extent2(), stride2(), extent1(), stride1()); }
inline
const
doubleSubTensor
		doubleSubTensor::sub(
    Offset h, Extent n3, Stride s3) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleSubTensor::sub(Offset, Extent, Stride) const",
    h, n3, s3, extent3());
#endif // SVMT_DEBUG_MODE
  return doubleSubTensor(handle(), offset() + (Stride)h*stride3(),
    n3, s3*stride3(), extent2(), stride2(), extent1(), stride1()); }
inline
doubleSubTensor
		doubleSubTensor::sub(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleSubTensor::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride)",
    h, n3, s3, extent3(),
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleSubTensor(handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2(),
    n3, s3*stride3(), n2, s2*stride2(), extent1(), stride1()); }
inline
const
doubleSubTensor
		doubleSubTensor::sub(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleSubTensor::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride) const",
    h, n3, s3, extent3(),
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleSubTensor(handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2(),
    n3, s3*stride3(), n2, s2*stride2(), extent1(), stride1()); }

inline
doubleSubTensor
		doubleSubTensor::sub(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment("doubleSubTensor::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride, Offset, Extent, Stride)",
    h, n3, s3, extent3(),
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleSubTensor(handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2() + (Stride)j*stride1(),
    n3, s3*stride3(), n2, s2*stride2(), n1, s1*stride1()); }
inline
const
doubleSubTensor
		doubleSubTensor::sub(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment("doubleSubTensor::sub(\n"
"Offset, Extent, Stride, Offset, Extent, Stride, Offset, Extent, Stride) const",
    h, n3, s3, extent3(),
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleSubTensor(handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2() + (Stride)j*stride1(),
    n3, s3*stride3(), n2, s2*stride2(), n1, s1*stride1()); }
inline
doubleSubTensor
		doubleSubTensor::sub_(
    Offset h, Extent n3, Stride s3) { return sub(h-1, n3, s3); }
inline
const
doubleSubTensor
		doubleSubTensor::sub_(
    Offset h, Extent n3, Stride s3) const { return sub(h-1, n3, s3); }
inline
doubleSubTensor
		doubleSubTensor::sub_(
    Offset i, Extent n2, Stride s2,
    Offset h, Extent n3, Stride s3) {
  return sub(h-1, n3, s3, i-1, n2, s2); }
inline
const
doubleSubTensor
		doubleSubTensor::sub_(
    Offset i, Extent n2, Stride s2,
    Offset h, Extent n3, Stride s3) const {
  return sub(h-1, n3, s3, i-1, n2, s2); }

inline
doubleSubTensor
		doubleSubTensor::sub_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2,
    Offset h, Extent n3, Stride s3) {
  return sub(h-1, n3, s3, i-1, n2, s2, j-1, n1, s1); }
inline
const
doubleSubTensor
		doubleSubTensor::sub_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2,
    Offset h, Extent n3, Stride s3) const {
  return sub(h-1, n3, s3, i-1, n2, s2, j-1, n1, s1); }
inline
const
doubleSubTensor
		doubleSubScalar::subtensor(
    Extent n, Extent m, Extent l) const {
  return doubleSubTensor(handle(),
    offset(), l, (Stride)0, m, (Stride)0, n, (Stride)0); }
inline
const
doubleSubTensor
		doubleSubVector::subtensor(
    Extent m, Extent l) const {
  return doubleSubTensor(handle(),
    offset(), l, (Stride)0, m, (Stride)0, extent(), stride()); }
inline
const
doubleSubTensor
		doubleSubMatrix::subtensor(
    Extent l) const {
  return doubleSubTensor(handle(),
    offset(), l, (Stride)0, extent2(), stride2(), extent1(), stride1()); }

inline
doubleSubTensor
		doubleSubTensor::t12(void) {
  return doubleSubTensor(handle(), offset(),
    extent3(), stride3(), extent1(), stride1(), extent2(), stride2()); }
inline
const
doubleSubTensor
		doubleSubTensor::t12(void) const {
  return doubleSubTensor(handle(), offset(),
    extent3(), stride3(), extent1(), stride1(), extent2(), stride2()); }
inline
doubleSubTensor
		doubleSubTensor::t23(void) {
  return doubleSubTensor(handle(), offset(),
    extent2(), stride2(), extent3(), stride3(), extent1(), stride1()); }
inline
const
doubleSubTensor
		doubleSubTensor::t23(void) const {
  return doubleSubTensor(handle(), offset(),
    extent2(), stride2(), extent3(), stride3(), extent1(), stride1()); }
inline
doubleSubTensor
		doubleSubTensor::t31(void) {
  return doubleSubTensor(handle(), offset(),
    extent1(), stride1(), extent2(), stride2(), extent3(), stride3()); }
inline
const
doubleSubTensor
		doubleSubTensor::t31(void) const {
  return doubleSubTensor(handle(), offset(),
    extent1(), stride1(), extent2(), stride2(), extent3(), stride3()); }

inline
doubleSubMatrix
		doubleSubTensor::diag12(void) {
  return doubleSubMatrix(handle(), offset(), extent3(), stride3(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
const
doubleSubMatrix
		doubleSubTensor::diag12(void) const {
  return doubleSubMatrix(handle(), offset(), extent3(), stride3(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
doubleSubMatrix
		doubleSubTensor::diag23(void) {
  return doubleSubMatrix(handle(), offset(), extent1(), stride1(),
    (extent2() < extent3())? extent2(): extent3(), stride3() + stride2()); }
inline
const
doubleSubMatrix
		doubleSubTensor::diag23(void) const {
  return doubleSubMatrix(handle(), offset(), extent1(), stride1(),
    (extent2() < extent3())? extent2(): extent3(), stride3() + stride2()); }
inline
doubleSubMatrix
		doubleSubTensor::diag31(void) {
  return doubleSubMatrix(handle(), offset(), extent2(), stride2(),
    (extent3() < extent1())? extent3(): extent1(), stride1() + stride3()); }
inline
const
doubleSubMatrix
		doubleSubTensor::diag31(void) const {
  return doubleSubMatrix(handle(), offset(), extent2(), stride2(),
    (extent3() < extent1())? extent3(): extent1(), stride1() + stride3()); }

inline
doubleSubTensor
		doubleSubTensor::r1(void) {
  return doubleSubTensor(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent3(), +stride3(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
const
doubleSubTensor
		doubleSubTensor::r1(void) const {
  return doubleSubTensor(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent3(), +stride3(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
doubleSubTensor
		doubleSubTensor::r2(void) {
  return doubleSubTensor(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent3(), +stride3(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
const
doubleSubTensor
		doubleSubTensor::r2(void) const {
  return doubleSubTensor(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent3(), +stride3(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
doubleSubTensor
		doubleSubTensor::r3(void) {
  return doubleSubTensor(handle(),
    offset() + (Stride)(extent3() - 1)*stride3(),
    extent3(), -stride3(),
    extent2(), +stride2(),
    extent1(), +stride1()); }
inline
const
doubleSubTensor
		doubleSubTensor::r3(void) const {
  return doubleSubTensor(handle(),
    offset() + (Stride)(extent3() - 1)*stride3(),
    extent3(), -stride3(),
    extent2(), +stride2(),
    extent1(), +stride1()); }

inline
doubleSubTensor
		doubleSubTensor::r(void) {
  return doubleSubTensor(handle(),
    offset() + (Stride)(extent3() - 1)*stride3()
	     + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent3(), -stride3(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
const
doubleSubTensor
		doubleSubTensor::r(void) const {
  return doubleSubTensor(handle(),
    offset() + (Stride)(extent3() - 1)*stride3()
	     + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent3(), -stride3(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
doubleSubTensor&
		doubleSubTensor::reverse(void) {
  return reverse1().reverse2().reverse3(); }

inline
doubleSubTensor
		doubleSubTensor::even(void) {
  return doubleSubTensor(handle(), offset(),
    extent3(), stride3(), extent2(), stride2(),
    (extent1() + 1) >> 1, stride1() << 1); }
inline
const
doubleSubTensor
		doubleSubTensor::even(void) const {
  return doubleSubTensor(handle(), offset(),
    extent3(), stride3(), extent2(), stride2(),
    (extent1() + 1) >> 1, stride1() << 1); }
inline
doubleSubTensor
		doubleSubTensor::odd(void) {
  return doubleSubTensor(handle(), offset() + stride1(),
    extent3(), stride3(), extent2(), stride2(),
    extent1() >> 1, stride1() << 1); }
inline
const
doubleSubTensor
		doubleSubTensor::odd(void) const {
  return doubleSubTensor(handle(), offset() + stride1(),
    extent3(), stride3(), extent2(), stride2(),
    extent1() >> 1, stride1() << 1); }
inline
doubleSubTensor
		doubleSubTensor::even_(void) { return odd(); }
inline
const
doubleSubTensor
		doubleSubTensor::even_(void) const { return odd(); }
inline
doubleSubTensor
		doubleSubTensor::odd_(void) { return even(); }
inline
const
doubleSubTensor
		doubleSubTensor::odd_(void) const { return even(); }
const
doubleMatrix
		   min(const doubleSubTensor& T);
const
doubleMatrix
		   max(const doubleSubTensor& T);
const
doubleTensor
		   min(const doubleSubTensor& T,
		       const doubleSubTensor& U);
const
doubleTensor
		   max(const doubleSubTensor& T,
		       const doubleSubTensor& U);
const
doubleTensor
		   sgn(const doubleSubTensor& T);
const
doubleTensor
		   abs(const doubleSubTensor& T);
const
doubleTensor
		 floor(const doubleSubTensor& T);
const
doubleTensor
		  ceil(const doubleSubTensor& T);
const
doubleTensor
		 hypot(const doubleSubTensor& T,
		       const doubleSubTensor& U);
const
doubleTensor
		 atan2(const doubleSubTensor& U,
		       const doubleSubTensor& T);

const
doubleTensor
		   log(const doubleSubTensor& T);
const
doubleTensor
		   exp(const doubleSubTensor& T);
const
doubleTensor
		  sqrt(const doubleSubTensor& T);
const
doubleTensor
		   cos(const doubleSubTensor& T);
const
doubleTensor
		   sin(const doubleSubTensor& T);
const
doubleTensor
		   tan(const doubleSubTensor& T);
const
doubleTensor
		  acos(const doubleSubTensor& T);
const
doubleTensor
		  asin(const doubleSubTensor& T);
const
doubleTensor
		  atan(const doubleSubTensor& T);
const
doubleTensor
		  cosh(const doubleSubTensor& T);
const
doubleTensor
		  sinh(const doubleSubTensor& T);
const
doubleTensor
		  tanh(const doubleSubTensor& T);
const
doubleTensor
		 acosh(const doubleSubTensor& T);
const
doubleTensor
		 asinh(const doubleSubTensor& T);
const
doubleTensor
		 atanh(const doubleSubTensor& T);

// Operators
inline
doubleSubMatrix
		doubleSubTensor::operator [](Offset h) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleSubTensor::operator [](Offset)", h, extent3());
#endif // SVMT_DEBUG_MODE
  return doubleSubMatrix(handle(), offset() + (Stride)h*stride3(),
    extent2(), stride2(), extent1(), stride1()); }
inline
const
doubleSubMatrix
		doubleSubTensor::operator [](Offset h) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleSubTensor::operator [](Offset) const", h, extent3());
#endif // SVMT_DEBUG_MODE
  return doubleSubMatrix(handle(), offset() + (Stride)h*stride3(),
    extent2(), stride2(), extent1(), stride1()); }
inline
doubleSubTensor&
		doubleSubTensor::swap(Offset h, Offset k) {
  doubleSubTensor	T = *this;
  doubleSubMatrix	M = T[h];
  doubleSubMatrix	N = T[k];
  M.swap(N); return *this; }
inline
doubleSubTensor&
		doubleSubTensor::swap_(Offset h, Offset k) {
  return swap(h-1, k-1); }
// Operators
inline
doubleSubMatrix
		doubleSubTensor::operator ()(Offset h) {
  return operator [](h-1); }
inline
const
doubleSubMatrix
		doubleSubTensor::operator ()(Offset h) const {
  return operator [](h-1); }
inline
doubleSubVector
		doubleSubTensor::operator ()(Offset i, Offset h) {
  doubleSubTensor&	T = *this; return T[h-1][i-1]; }
inline
const
doubleSubVector
		doubleSubTensor::operator ()(Offset i, Offset h) const {
  const
  doubleSubTensor&	T = *this; return T[h-1][i-1]; }
inline
doubleSubScalar
		doubleSubTensor::operator ()(
    Offset j, Offset i, Offset h) {
  doubleSubTensor&	T = *this; return T[h-1][i-1][j-1]; }
inline
const
doubleSubScalar
		doubleSubTensor::operator ()(
    Offset j, Offset i, Offset h) const {
  const
  doubleSubTensor&	T = *this; return T[h-1][i-1][j-1]; }

//inline
//const
//doubleSubTensor
//		doubleSubTensor::operator +(void) const {
//  return *this; }
std::istream&	operator >>(std::istream& s,       doubleSubTensor& T);
std::ostream&	operator <<(std::ostream& s, const doubleSubTensor& T);
inline
bool		doubleSubTensor::operator > (
    const doubleSubTensor& U) const { return U <  *this; }
inline
bool		doubleSubTensor::operator >=(
    const doubleSubTensor& U) const { return U <= *this; }

class doubleSubArray3: public doubleSubTensor {
  public:
  // Constructors
		doubleSubArray3(void);
		doubleSubArray3(double* p,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
		doubleSubArray3(const doubleSubArray3& T);
		~doubleSubArray3(void);
  // Functions
  doubleSubArray3&
		resize(void);
  doubleSubArray3&		// resize from pointer
		resize(double* p,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleSubArray3&		// resize from SubArray3
		resize(const doubleSubArray3& T);
  doubleSubArray3&		// resize from pointer
		resize_(double* p,
      Offset o, Extent n1, Stride s1,
		Extent n2, Stride s2,
		Extent n3, Stride s3);
  private:
  doubleSubArray3&		// prevent resize from Handle
		resize(const doubleHandle& h,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleSubArray3&		// prevent resize from SubTensor
		resize(const doubleSubTensor& T);

  public:
  doubleSubArray3
		   sub(Offset h, Extent n3, Stride s3);
  const
  doubleSubArray3
		   sub(Offset h, Extent n3, Stride s3) const;
  doubleSubArray3
		   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2);
  const
  doubleSubArray3
		   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2) const;
  doubleSubArray3
		   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1);
  const
  doubleSubArray3
		   sub(Offset h, Extent n3, Stride s3,
		       Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  doubleSubArray3
		   sub_(Offset h, Extent n3, Stride s3);
  const
  doubleSubArray3
		   sub_(Offset h, Extent n3, Stride s3) const;
  doubleSubArray3
		   sub_(Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3);
  const
  doubleSubArray3
		   sub_(Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;
  doubleSubArray3
		   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3);
  const
  doubleSubArray3
		   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2,
			Offset h, Extent n3, Stride s3) const;

  doubleSubArray3
		     t12(void);
  const
  doubleSubArray3
		     t12(void) const;
  doubleSubArray3
		     t23(void);
  const
  doubleSubArray3
		     t23(void) const;
  doubleSubArray3
		     t31(void);
  const
  doubleSubArray3
		     t31(void) const;
  doubleSubArray2
		  diag12(void);
  const
  doubleSubArray2
		  diag12(void) const;
  doubleSubArray2
		  diag23(void);
  const
  doubleSubArray2
		  diag23(void) const;
  doubleSubArray2
		  diag31(void);
  const
  doubleSubArray2
		  diag31(void) const;
  doubleSubArray3
		     r1(void);
  const
  doubleSubArray3
		     r1(void) const;
  doubleSubArray3&
	       reverse1(void);
  doubleSubArray3
		     r2(void);
  const
  doubleSubArray3
		     r2(void) const;
  doubleSubArray3&
	       reverse2(void);
  doubleSubArray3
		     r3(void);
  const
  doubleSubArray3
		     r3(void) const;
  doubleSubArray3&
	       reverse3(void);
  doubleSubArray3
		     r(void);
  const
  doubleSubArray3
		     r(void) const;
  doubleSubArray3&
	       reverse(void);

  doubleSubArray3
		  even(void);
  const
  doubleSubArray3
		  even(void) const;
  doubleSubArray3
		   odd(void);
  const
  doubleSubArray3
		   odd(void) const;
  doubleSubArray3
		  even_(void);
  const
  doubleSubArray3
		  even_(void) const;
  doubleSubArray3
		   odd_(void);
  const
  doubleSubArray3
		   odd_(void) const;
  doubleSubArray3&
		  rdft(int sign = -1);	// complex to real    dft
  doubleSubArray3&
		  cdft(int sign = -1);	// real    to complex dft
  doubleSubArray3&
		  ramp(void);
  doubleSubArray3&
		  swap(Offset h, Offset k);
  doubleSubArray3&
		  swap(doubleSubTensor& U);
  doubleSubArray3&
		  swap_(Offset h, Offset k);
  doubleSubArray3&
		rotate(Stride n);
  doubleSubArray3&
		 shift(Stride n,const double& s
		  = (double)0);
  doubleSubArray3&
	       scatter(const offsetSubVector& x,
		       const doubleSubVector& t);

  // Operators
  doubleSubArray2
		operator [](Offset h);
  const
  doubleSubArray2
		operator [](Offset h) const;
  doubleSubArray2
		operator ()(Offset h);
  const
  doubleSubArray2
		operator ()(Offset h) const;
  doubleSubArray1
		operator ()(Offset i, Offset h);
  const
  doubleSubArray1
		operator ()(Offset i, Offset h) const;
  doubleSubArray0
		operator ()(Offset j, Offset i, Offset h);
  const
  doubleSubArray0
		operator ()(Offset j, Offset i, Offset h) const;
//  const
//  doubleSubArray3
//		operator +(void) const;
  doubleSubArray3&
		operator  =(const double& s);
  doubleSubArray3&
		operator *=(const double& s);
  doubleSubArray3&
		operator /=(const double& s);
  doubleSubArray3&
		operator +=(const double& s);
  doubleSubArray3&
		operator -=(const double& s);

  doubleSubArray3&
		operator  =(const doubleSubTensor& U);
  doubleSubArray3&
		operator *=(const doubleSubTensor& U);
  doubleSubArray3&
		operator /=(const doubleSubTensor& U);
  doubleSubArray3&
		operator +=(const doubleSubTensor& U);
  doubleSubArray3&
		operator -=(const doubleSubTensor& U);
  };

// Constructors
inline		doubleSubArray3::doubleSubArray3(void):
  doubleSubTensor() { }
inline		doubleSubArray3::doubleSubArray3(double* p,
    Offset o, Extent n3, Stride s3, Extent n2, Stride s2, Extent n1, Stride s1):
  doubleSubTensor(doubleHandle(p), o, n3, s3, n2, s2, n1, s1) {
  }
inline		doubleSubArray3::doubleSubArray3(
    const doubleSubArray3& T): doubleSubTensor(T) { }
inline		doubleSubArray3::~doubleSubArray3(void) { }

// Functions
inline
doubleSubArray3&
		doubleSubArray3::resize(void) {
  doubleSubTensor::resize(); return *this; }
inline
doubleSubArray3&
		doubleSubArray3::resize(double* p,
    Offset o, Extent n3, Stride s3,
	      Extent n2, Stride s2,
	      Extent n1, Stride s1) {
  doubleSubTensor::resize(doubleHandle(p),
    o, n3, s3, n2, s2, n1, s1); return *this; }
inline
doubleSubArray3&
		doubleSubArray3::resize(
    const doubleSubArray3& T) {
  doubleSubTensor::resize(T); return *this; }
inline
doubleSubArray3&
		doubleSubArray3::resize_(double* p,
    Offset o, Extent n1, Stride s1,
	      Extent n2, Stride s2,
	      Extent n3, Stride s3) {
  return resize(p, o, n3, s3, n2, s2, n1, s1); }
inline
doubleSubArray3
		doubleSubArray3::sub(
    Offset h, Extent n3, Stride s3) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleSubArray3::sub(Offset, Extent, Stride)",
    h, n3, s3, extent3());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset() + (Stride)h*stride3(),
    n3, s3*stride3(), extent2(), stride2(), extent1(), stride1()); }
inline
const
doubleSubArray3
		doubleSubArray3::sub(
    Offset h, Extent n3, Stride s3) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleSubArray3::sub(Offset, Extent, Stride) const",
    h, n3, s3, extent3());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset() + (Stride)h*stride3(),
    n3, s3*stride3(), extent2(), stride2(), extent1(), stride1()); }

inline
doubleSubArray3
		doubleSubArray3::sub(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleSubArray3::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride)",
    h, n3, s3, extent3(),
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2(),
    n3, s3*stride3(), n2, s2*stride2(), extent1(), stride1()); }
inline
const
doubleSubArray3
		doubleSubArray3::sub(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleSubArray3::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride) const",
    h, n3, s3, extent3(),
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2(),
    n3, s3*stride3(), n2, s2*stride2(), extent1(), stride1()); }
inline
doubleSubArray3
		doubleSubArray3::sub(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment("doubleSubArray3::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride, Offset, Extent, Stride)",
    h, n3, s3, extent3(),
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2() + (Stride)j*stride1(),
    n3, s3*stride3(), n2, s2*stride2(), n1, s1*stride1()); }
inline
const
doubleSubArray3
		doubleSubArray3::sub(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_tensor_containment("doubleSubArray3::sub(\n"
"Offset, Extent, Stride, Offset, Extent, Stride, Offset, Extent, Stride) const",
    h, n3, s3, extent3(),
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset() + (Stride)h*stride3() + (Stride)i*stride2() + (Stride)j*stride1(),
    n3, s3*stride3(), n2, s2*stride2(), n1, s1*stride1()); }

inline
doubleSubArray3
		doubleSubArray3::sub_(
    Offset h, Extent n3, Stride s3) { return sub(h-1, n3, s3); }
inline
const
doubleSubArray3
		doubleSubArray3::sub_(
    Offset h, Extent n3, Stride s3) const { return sub(h-1, n3, s3); }
inline
doubleSubArray3
		doubleSubArray3::sub_(
    Offset i, Extent n2, Stride s2,
    Offset h, Extent n3, Stride s3) {
  return sub(h-1, n3, s3, i-1, n2, s2); }
inline
const
doubleSubArray3
		doubleSubArray3::sub_(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2) const {
  return sub(h-1, n3, s3, i-1, n2, s2); }
inline
doubleSubArray3
		doubleSubArray3::sub_(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) {
  return sub(h-1, n3, s3, i-1, n2, s2, j-1, n1, s1); }
inline
const
doubleSubArray3
		doubleSubArray3::sub_(
    Offset h, Extent n3, Stride s3,
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) const {
  return sub(h-1, n3, s3, i-1, n2, s2, j-1, n1, s1); }
inline
const
doubleSubArray3
		doubleSubArray0::subtensor(
    Extent n, Extent m, Extent l) const {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset(), l, (Stride)0, m, (Stride)0, n, (Stride)0); }
inline
const
doubleSubArray3
		doubleSubArray1::subtensor(Extent m, Extent l) const {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset(), l, (Stride)0, m, (Stride)0, extent(), stride()); }
inline
const
doubleSubArray3
		doubleSubArray2::subtensor(Extent l) const {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset(), l, (Stride)0, extent2(), stride2(), extent1(), stride1()); }

inline
doubleSubArray3
		doubleSubArray3::t12(void) {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
  offset(), extent3(), stride3(), extent1(), stride1(), extent2(), stride2()); }
inline
const
doubleSubArray3
		doubleSubArray3::t12(void) const {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
  offset(), extent3(), stride3(), extent1(), stride1(), extent2(), stride2()); }
inline
doubleSubArray3
		doubleSubArray3::t23(void) {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
  offset(), extent2(), stride2(), extent3(), stride3(), extent1(), stride1()); }
inline
const
doubleSubArray3
		doubleSubArray3::t23(void) const {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
  offset(), extent2(), stride2(), extent3(), stride3(), extent1(), stride1()); }
inline
doubleSubArray3
		doubleSubArray3::t31(void) {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
  offset(), extent1(), stride1(), extent2(), stride2(), extent3(), stride3()); }
inline
const
doubleSubArray3
		doubleSubArray3::t31(void) const {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
  offset(), extent1(), stride1(), extent2(), stride2(), extent3(), stride3()); }

inline
doubleSubArray2
		doubleSubArray3::diag12(void) {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset(), extent3(), stride3(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
const
doubleSubArray2
		doubleSubArray3::diag12(void) const {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset(), extent3(), stride3(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
doubleSubArray2
		doubleSubArray3::diag23(void) {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset(), extent1(), stride1(),
    (extent2() < extent3())? extent2(): extent3(), stride3() + stride2()); }
inline
const
doubleSubArray2
		doubleSubArray3::diag23(void) const {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset(), extent1(), stride1(),
    (extent2() < extent3())? extent2(): extent3(), stride3() + stride2()); }
inline
doubleSubArray2
		doubleSubArray3::diag31(void) {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset(), extent2(), stride2(),
    (extent3() < extent1())? extent3(): extent1(), stride1() + stride3()); }
inline
const
doubleSubArray2
		doubleSubArray3::diag31(void) const {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset(), extent2(), stride2(),
    (extent3() < extent1())? extent3(): extent1(), stride1() + stride3()); }

inline
doubleSubArray3
		doubleSubArray3::r1(void) {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent3(), +stride3(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
const
doubleSubArray3
		doubleSubArray3::r1(void) const {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent3(), +stride3(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
doubleSubArray3&
		doubleSubArray3::reverse1(void) {
  doubleSubTensor::reverse1(); return *this; }
inline
doubleSubArray3
		doubleSubArray3::r2(void) {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent3(), +stride3(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
const
doubleSubArray3
		doubleSubArray3::r2(void) const {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent3(), +stride3(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
doubleSubArray3&
		doubleSubArray3::reverse2(void) {
  doubleSubTensor::reverse2(); return *this; }
inline
doubleSubArray3
		doubleSubArray3::r3(void) {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset() + (Stride)(extent3() - 1)*stride3(),
    extent3(), -stride3(),
    extent2(), +stride2(),
    extent1(), +stride1()); }
inline
const
doubleSubArray3
		doubleSubArray3::r3(void) const {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset() + (Stride)(extent3() - 1)*stride3(),
    extent3(), -stride3(),
    extent2(), +stride2(),
    extent1(), +stride1()); }
inline
doubleSubArray3&
		doubleSubArray3::reverse3(void) {
  doubleSubTensor::reverse3(); return *this; }

inline
doubleSubArray3
		doubleSubArray3::r(void) {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset() + (Stride)(extent3() - 1)*stride3()
	     + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent3(), -stride3(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
const
doubleSubArray3
		doubleSubArray3::r(void) const {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset() + (Stride)(extent3() - 1)*stride3()
	     + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent3(), -stride3(),
    extent2(), -stride2(),
    extent1(), -stride1()) ; }
inline
doubleSubArray3&
		doubleSubArray3::reverse(void) {
  doubleSubTensor::reverse(); return *this; }
inline
doubleSubArray3
		doubleSubArray3::even(void) {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset(), extent3(), stride3(), extent2(), stride2(),
    (extent1() + 1) >> 1, stride1() << 1); }
inline
const
doubleSubArray3
		doubleSubArray3::even(void) const {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset(), extent3(), stride3(), extent2(), stride2(),
    (extent1() + 1) >> 1, stride1() << 1); }
inline
doubleSubArray3
		doubleSubArray3::odd(void) {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset() + stride1(), extent3(), stride3(), extent2(), stride2(),
    extent1() >> 1, stride1() << 1); }
inline
const
doubleSubArray3
		doubleSubArray3::odd(void) const {
  return doubleSubArray3((double*)(doubleHandle&)handle(),
    offset() + stride1(), extent3(), stride3(), extent2(), stride2(),
    extent1() >> 1, stride1() << 1); }
inline
doubleSubArray3
		doubleSubArray3::even_(void) { return odd(); }
inline
const
doubleSubArray3
		doubleSubArray3::even_(void) const { return odd(); }
inline
doubleSubArray3
		doubleSubArray3::odd_(void) { return even(); }
inline
const
doubleSubArray3
		doubleSubArray3::odd_(void) const { return even(); }

inline
doubleSubArray3&
		doubleSubArray3::rdft(int sign) {
  doubleSubTensor::rdft(sign); return *this; }
inline
doubleSubArray3&
		doubleSubArray3::cdft(int sign) {
  doubleSubTensor::cdft(sign); return *this; }
inline
doubleSubArray3&
		doubleSubArray3::ramp(void) {
  doubleSubTensor::ramp(); return *this; }

inline
doubleSubArray3&
		doubleSubArray3::swap(Offset h, Offset k) {
  doubleSubTensor::swap(h, k); return *this; }
inline
doubleSubArray3&
		doubleSubArray3::swap(doubleSubTensor& U) {
  doubleSubTensor::swap(U); return *this; }
inline
doubleSubArray3&
		doubleSubArray3::swap_(Offset h, Offset k) {
  return swap(h-1, k-1); }
inline
doubleSubArray3&
		doubleSubArray3::rotate(Stride n) {
  doubleSubTensor::rotate(n); return *this; }
inline
doubleSubArray3&
		doubleSubArray3::shift(Stride n,const double& s) {
  doubleSubTensor::shift(n,s); return *this; }

// doubleSubTensor function definitions
// which use doubleSubArray3
inline
const
boolTensor
		doubleSubTensor::lt(const double& s) const {
  return lt(doubleSubArray3((double*)&s, (Offset)0,
    extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolTensor
		doubleSubTensor::le(const double& s) const {
  return le(doubleSubArray3((double*)&s, (Offset)0,
    extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolTensor
		doubleSubTensor::gt(const double& s) const {
  return doubleSubArray3((double*)&s, (Offset)0,
  extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0).lt(*this); }
inline
const
boolTensor
		doubleSubTensor::ge(const double& s) const {
  return doubleSubArray3((double*)&s, (Offset)0,
  extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0).le(*this); }
inline
const
boolTensor
		doubleSubTensor::gt(
  const doubleSubTensor& U) const { return U.lt(*this); }
inline
const
boolTensor
		doubleSubTensor::ge(
  const doubleSubTensor& U) const { return U.le(*this); }

inline
const
boolTensor
		doubleSubTensor::eq(const double& s) const {
  return eq(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolTensor
		doubleSubTensor::ne(const double& s) const {
  return ne(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleSubArray3&
	       doubleSubArray3::scatter(
    const offsetSubVector& x, const doubleSubVector& t) {
  doubleSubTensor::scatter(x, t); return *this; }

// Operators
inline
doubleSubArray2
		doubleSubArray3::operator [](Offset h) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleSubArray3::operator [](Offset)", h, extent3());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray2((double*)(doubleHandle&)handle(),
  offset() + (Stride)h*stride3(), extent2(), stride2(), extent1(), stride1()); }
inline
const
doubleSubArray2
		doubleSubArray3::operator [](Offset h) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleSubArray3::operator [](Offset) const", h, extent3());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray2((double*)(doubleHandle&)handle(),
  offset() + (Stride)h*stride3(), extent2(), stride2(), extent1(), stride1()); }
inline
doubleSubArray2
		doubleSubArray3::operator ()(Offset h) {
  return operator [](h-1); }
inline
const
doubleSubArray2
		doubleSubArray3::operator ()(Offset h) const {
  return operator [](h-1); }
inline
doubleSubArray1
		doubleSubArray3::operator ()(
    Offset i, Offset h) {
  doubleSubArray3&	T = *this; return T[h-1][i-1]; }
inline
const
doubleSubArray1
		doubleSubArray3::operator ()(
    Offset i, Offset h) const {
  const
  doubleSubArray3&	T = *this; return T[h-1][i-1]; }
inline
doubleSubArray0
		doubleSubArray3::operator ()(
    Offset j, Offset i, Offset h) {
  doubleSubArray3&	T = *this; return T[h-1][i-1][j-1]; }
inline
const
doubleSubArray0
		doubleSubArray3::operator ()(
    Offset j, Offset i, Offset h) const {
  const
  doubleSubArray3&	T = *this; return T[h-1][i-1][j-1]; }

//inline
//const
//doubleSubArray3
//		doubleSubArray3::operator +(void) const {
//  return *this; }
inline
doubleSubArray3&
		doubleSubArray3::operator  =(const double& s) {
  doubleSubTensor::operator  =(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleSubArray3&
		doubleSubArray3::operator *=(const double& s) {
  doubleSubTensor::operator *=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleSubArray3&
		doubleSubArray3::operator /=(const double& s) {
  doubleSubTensor::operator /=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleSubArray3&
		doubleSubArray3::operator +=(const double& s) {
  doubleSubTensor::operator +=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }
inline
doubleSubArray3&
		doubleSubArray3::operator -=(const double& s) {
  doubleSubTensor::operator -=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0));
  return *this; }


inline
doubleSubArray3&
		doubleSubArray3::operator  =(
    const doubleSubTensor& U) {
  doubleSubTensor::operator  =(U); return *this; }
inline
doubleSubArray3&
		doubleSubArray3::operator *=(
    const doubleSubTensor& U) {
  doubleSubTensor::operator *=(U); return *this; }
inline
doubleSubArray3&
		doubleSubArray3::operator /=(
    const doubleSubTensor& U) {
  doubleSubTensor::operator /=(U); return *this; }
inline
doubleSubArray3&
		doubleSubArray3::operator +=(
    const doubleSubTensor& U) {
  doubleSubTensor::operator +=(U); return *this; }
inline
doubleSubArray3&
		doubleSubArray3::operator -=(
    const doubleSubTensor& U) {
  doubleSubTensor::operator -=(U); return *this; }


// doubleSubTensor operator definitions
// which use doubleSubArray3
inline
bool		doubleSubTensor::operator < (const double& s) const {
  return operator < (doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		doubleSubTensor::operator <=(const double& s) const {
  return operator <=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		doubleSubTensor::operator > (const double& s) const {
  return operator > (doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		doubleSubTensor::operator >=(const double& s) const {
  return operator >=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		operator < (
    const double& s, const doubleSubTensor& T) { return T >  s; }
inline
bool		operator <=(
    const double& s, const doubleSubTensor& T) { return T >= s; }
inline
bool		operator > (
    const double& s, const doubleSubTensor& T) { return T <  s; }
inline
bool		operator >=(
    const double& s, const doubleSubTensor& T) { return T <= s; }

inline
bool		doubleSubTensor::operator ==(const double& s) const {
  return operator ==(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		doubleSubTensor::operator !=(const double& s) const {
  return operator !=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		operator ==(
    const double& s, const doubleSubTensor& T) { return T == s; }
inline
bool		operator !=(
    const double& s, const doubleSubTensor& T) { return T != s; }

inline
doubleSubTensor&
		doubleSubTensor::operator  =(const double& s) {
  return operator  =(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleSubTensor&
		doubleSubTensor::operator *=(const double& s) {
  return operator *=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleSubTensor&
		doubleSubTensor::operator /=(const double& s) {
  return operator /=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleSubTensor&
		doubleSubTensor::operator +=(const double& s) {
  return operator +=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleSubTensor&
		doubleSubTensor::operator -=(const double& s) {
  return operator -=(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }


class doubleTensor: public doubleSubTensor {
  public:
  // Constructors
		doubleTensor(void);
  explicit	doubleTensor(Extent l, Extent m = 1, Extent n = 1);
		doubleTensor(Extent l, Extent m, Extent n,
      const double& s);
		doubleTensor(Extent l, Extent m, Extent n,
      const double& s, const double& t);
		doubleTensor(const doubleSubTensor& T);
		doubleTensor(const doubleTensor& T);
		~doubleTensor(void);
  // Functions
  doubleTensor&
		resize(void);
  doubleTensor&
		resize(Extent l, Extent m = 1, Extent n = 1);
  doubleTensor&
		resize_(Extent m, Extent l);
  doubleTensor&
		resize_(Extent n, Extent m, Extent l);
  doubleTensor&
		resize(Extent l, Extent m, Extent n,
      const double& s);
  doubleTensor&
		resize_(Extent n, Extent m, Extent l,
      const double& s);
  doubleTensor&
		resize(Extent l, Extent m, Extent n,
      const double& s, const double& t);
  doubleTensor&
		resize_(Extent n, Extent m, Extent l,
      const double& s, const double& t);
  doubleTensor&
		resize(const doubleSubTensor& T);
private:
  doubleTensor&
		resize(const doubleHandle& h,
      Offset o, Extent n3, Stride s3,
		Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleTensor&
		free(void) { doubleSubTensor::free(); return *this; }
public:
  // Operators
  doubleTensor&
		operator  =(const double& s);
  doubleTensor&
		operator  =(const doubleSubTensor& U);
  doubleTensor&
		operator  =(const doubleTensor& U);
  };

// Constructors
inline		doubleTensor::doubleTensor(void):
  doubleSubTensor() { }
inline		doubleTensor::doubleTensor(
    Extent l, Extent m, Extent n):
  doubleSubTensor(allocate(l, m, n), (Offset)0,
    l, (Stride)n*(Stride)m, m, (Stride)n, n,  (Stride)1) {
#ifdef SVMT_DEBUG_MODE
  doubleSubTensor::operator =(doubleBad);
#endif // SVMT_DEBUG_MODE
  }
inline		doubleTensor::doubleTensor(
    Extent l, Extent m, Extent n, const double& s):
  doubleSubTensor(allocate(l, m, n), (Offset)0,
    l, (Stride)n*(Stride)m, m, (Stride)n, n, (Stride)1) {
  doubleSubTensor::operator  =(s); }
inline		doubleTensor::doubleTensor(
    Extent l, Extent m, Extent n, const double& s, const double& t):
  doubleSubTensor(allocate(l, m, n), (Offset)0,
    l, (Stride)n*(Stride)m, m, (Stride)n, n, (Stride)1) {
  ramp();
  doubleSubTensor::operator *=(t);
  doubleSubTensor::operator +=(s); }
inline		doubleTensor::doubleTensor(
    const doubleSubTensor& T):
  doubleSubTensor(
      allocate(T.extent3(), T.extent2(), T.extent1()), (Offset)0,
    T.extent3(), (Stride)T.extent1()*(Stride)T.extent2(),
    T.extent2(), (Stride)T.extent1(), T.extent1(), (Stride)1) {
  doubleSubTensor::operator  =(T); }
inline		doubleTensor::doubleTensor(
    const doubleTensor& T):
  doubleSubTensor(
      allocate(T.extent3(), T.extent2(), T.extent1()), (Offset)0,
    T.extent3(), (Stride)T.extent1()*(Stride)T.extent2(),
    T.extent2(), (Stride)T.extent1(), T.extent1(), (Stride)1) {
  doubleSubTensor::operator  =(T); }
inline		doubleTensor::~doubleTensor(void) { free(); }

// Assignment Operators
inline
doubleTensor&	doubleTensor::operator  =(const double& s) {
  doubleSubTensor::operator =(s); return *this; }
inline
doubleTensor&	doubleTensor::operator  =(
    const doubleSubTensor& U) {
  doubleSubTensor::operator =(U); return *this; }
inline
doubleTensor&	doubleTensor::operator  =(
    const doubleTensor& U) {
  doubleSubTensor::operator =(U); return *this; }

// Functions
inline
doubleTensor&
		doubleTensor::resize(void) {
  delete [] (double*)H; doubleSubTensor::resize(); return *this; }
inline
doubleTensor&
		doubleTensor::resize(Extent l, Extent m, Extent n) {
  free(); doubleSubTensor::resize(allocate(l, m, n), (Offset)0,
      l, (Stride)n*(Stride)m, m, (Stride)n, n, (Stride)1);
#ifdef SVMT_DEBUG_MODE
  *this = doubleBad;
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
doubleTensor&
		doubleTensor::resize_(Extent m, Extent l) {
  return resize(l, m); }
inline
doubleTensor&
		doubleTensor::resize_(Extent n, Extent m, Extent l) {
  return resize(l, m, n); }
inline
doubleTensor&
		doubleTensor::resize(Extent l, Extent m, Extent n,
    const double& s) { return resize(l, m, n) = s; }
inline
doubleTensor&
		doubleTensor::resize_(Extent n, Extent m, Extent l,
    const double& s) { return resize(l, m, n, s); }
inline
doubleTensor&
		doubleTensor::resize(Extent l, Extent m, Extent n,
    const double& s, const double& t) {
  resize(l, m, n); ramp();
  doubleSubTensor::operator *=(t);
  doubleSubTensor::operator +=(s);
  return *this;
  }
inline
doubleTensor&
		doubleTensor::resize_(Extent n, Extent m, Extent l,
    const double& s, const double& t) { return resize(l, m, n, s, t); }
inline
doubleTensor&
		doubleTensor::resize(
    const doubleSubTensor& T) {
  return resize(T.extent3(), T.extent2(), T.extent1()) = T; }

inline
const
doubleTensor
		doubleSubMatrix::afore(
    const doubleSubMatrix& N) const {
  return subtensor().afore(N.subtensor()); }
inline
const
doubleTensor
		doubleSubMatrix::afore(
    const doubleSubTensor& T) const { return subtensor().afore(T); }
inline
const
doubleTensor
		doubleSubTensor::afore(
    const doubleSubMatrix& M) const { return afore(M.subtensor()); }
inline
const
doubleTensor
		doubleSubTensor::above_(
    const doubleSubTensor& T) const { return aside(T); }
inline
const
doubleTensor
		doubleSubTensor::aside_(
    const doubleSubTensor& T) const { return above(T); }


inline
const
doubleTensor
		doubleSubTensor::operator *(const double& s) const {
  return operator *(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleTensor
		doubleSubTensor::operator /(const double& s) const {
  return operator /(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleTensor
		doubleSubTensor::operator +(const double& s) const {
  return operator +(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleTensor
		doubleSubTensor::operator -(const double& s) const {
  return operator -(doubleSubArray3((double*)&s, (Offset)0,
      extent3(), (Stride)0, extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleTensor
		operator *(const double& s, const doubleSubTensor& T) {
  return doubleSubArray3((double*)&s, (Offset)0,
    T.extent3(), (Stride)0, T.extent2(), (Stride)0, T.extent1(), (Stride)0)*T; }
inline
const
doubleTensor
		operator /(const double& s, const doubleSubTensor& T) {
  return doubleSubArray3((double*)&s, (Offset)0,
    T.extent3(), (Stride)0, T.extent2(), (Stride)0, T.extent1(), (Stride)0)/T; }
inline
const
doubleTensor
		operator +(const double& s, const doubleSubTensor& T) {
  return doubleSubArray3((double*)&s, (Offset)0,
  T.extent3(), (Stride)0, T.extent2(), (Stride)0, T.extent1(), (Stride)0) + T; }
inline
const
doubleTensor
		operator -(const double& s, const doubleSubTensor& T) {
  return doubleSubArray3((double*)&s, (Offset)0,
  T.extent3(), (Stride)0, T.extent2(), (Stride)0, T.extent1(), (Stride)0) - T; }



#endif /* _doubleTensor_h */

