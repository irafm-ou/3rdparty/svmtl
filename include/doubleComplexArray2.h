#ifndef _doubleComplexArray2_h
#define _doubleComplexArray2_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleComplexMatrix.h>


template<Extent m, Extent n = 1>
class doubleComplexArray2: public doubleComplexSubArray2 {
private:
  // Representation
  double	A[m*n*2];
  // Functions			// prevent doubleComplexSubArray2 resize
  doubleComplexArray2<m, n>&
		resize(void);
  doubleComplexArray2<m, n>&
		resize(double*, Offset, Extent, Stride, Extent, Stride);
  doubleComplexArray2<m, n>&
		resize_(double*, Offset, Extent, Stride, Extent, Stride);
  doubleComplexArray2<m, n>&
		resize(const doubleComplexSubArray2&);
public:
  // Constructors
		doubleComplexArray2(void):
    doubleComplexSubArray2(A, 0, m, n, n, 1) { }
		doubleComplexArray2(const doubleComplex& s):
    doubleComplexSubArray2(A, 0, m, n, n, 1) {
    doubleComplexSubArray2::operator =(s); }
		doubleComplexArray2(const doubleComplexArray2<m, n>& T):
    doubleComplexSubArray2(A, 0, m, n, n, 1) {
    doubleComplexSubArray2::operator =(T); }
	       ~doubleComplexArray2(void) { }
  // Operators
  doubleComplexArray2<m, n>&
  operator =(const doubleComplexArray2<m, n>& T) {
    doubleComplexSubArray2::operator =(T);
    return *this; }
  };

#endif /* _doubleComplexArray2_h */

