#ifndef _boolSquare_h
#define _boolSquare_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<boolMatrix.h>

class boolSubSquare: public boolSubMatrix {
  // Constructors
		boolSubSquare(void);
		boolSubSquare(
      const boolHandle& h, Offset o, Extent m, Stride s2, Stride s1);
		boolSubSquare(const boolSubSquare& M);
		~boolSubSquare(void);
  // Functions
private:
  static
  boolHandle				// OMITTED TO PREVENT INHERITANCE
	      allocate(Extent m, Extent n);
  static
  boolHandle				// OMITTED TO PREVENT INHERITANCE
	      allocate_(Extent n, Extent m);
  boolSubSquare&			// OMITTED TO PREVENT INHERITANCE
		resize(const boolHandle& h,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
  boolSubSquare&			// OMITTED TO PREVENT INHERITANCE
		resize(const boolSubMatrix& N);
  boolSubSquare&			// OMITTED TO PREVENT INHERITANCE
		resize_(const boolHandle& h,
      Offset o, Extent n1, Stride s1,
		Extent n2, Stride s2);
public:
  static
  boolHandle allocate(Extent m);
  boolSubSquare&
		  free(void);
  boolSubSquare&
		resize(void);
  boolSubSquare&
		resize(const boolHandle& h,
      Offset o, Extent m, Stride s2, Stride s1);
  boolSubSquare&
		resize(const boolSubSquare& N);
  boolSubSquare&
		resize_(const boolHandle& h,
      Offset o, Stride s1, Extent m, Stride s2);

  boolSubSquare      r1(void);
  const
  boolSubSquare      r1(void) const;
  boolSubSquare&
	       reverse1(void);
  boolSubSquare      r2(void);
  const
  boolSubSquare      r2(void) const;
  boolSubSquare&
	       reverse2(void);
  boolSubSquare      r(void);
  const
  boolSubSquare      r(void) const;
  boolSubSquare&
	       reverse(void);
  boolSubSquare	     t(void);
  const
  boolSubSquare	     t(void) const;
  boolSubSquare&
	     transpose(void);
  boolSubSquare&  swap(Offset i, Offset k);
  boolSubSquare&  swap(boolSubSquare& N);
  boolSubSquare&  swap_(Offset i, Offset k);
  boolSubSquare&
		rotate(Stride n);
  boolSubSquare&
		 shift(Stride n, bool b = false);
  const
  boolSquare	    eq(bool b) const;
  const
  boolSquare	    ne(bool b) const;
  const
  boolSquare	    eq(const boolSubSquare& N) const;
  const
  boolSquare	    ne(const boolSubSquare& N) const;
  const
  boolSquare	 apply(const bool (*f)(const bool&)) const;
  const
  boolSquare	 apply(      bool (*f)(const bool&)) const;
  const
  boolSquare	 apply(      bool (*f)(      bool )) const;

  // Operators
  const
  boolSquare	operator !(void) const;
  const
  boolSquare	operator &&(bool b) const;
  const
  boolSquare	operator ||(bool b) const;
  const
  boolSquare	operator &&(const boolSubSquare& N) const;
  const
  boolSquare	operator ||(const boolSubSquare& N) const;
  boolSubSquare&
		operator  =(bool b);
  boolSubSquare&
		operator  =(const boolSubSquare& N);
  friend class boolSquare;
  };

// Constructors
inline		boolSubSquare::boolSubSquare(void): boolSubMatrix() { }
inline		boolSubSquare::boolSubSquare(
    const boolHandle& h, Offset o, Extent m, Stride s2, Stride s1):
  boolSubMatrix(h, o, m, s2, m, s1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("boolSubSquare::boolSubSquare(\n"
    "const boolHandle&, Offset, Extent, Stride, Stride)",
    o, m, s2, m, s1);
#endif // SVMT_DEBUG_MODE
  }
inline		boolSubSquare::boolSubSquare(const boolSubSquare& M):
  boolSubMatrix(M) { }
inline		boolSubSquare::~boolSubSquare(void) { }

// Functions
inline
boolHandle	boolSubSquare::allocate(Extent m) {
  return boolSubMatrix::allocate(m, m); }
inline
boolSubSquare&	boolSubSquare::free(void) {
  boolSubMatrix::free(); return *this; }
inline
boolSubSquare&	boolSubSquare::resize(void) {
  boolSubMatrix::resize(); return *this; }
inline
boolSubSquare&	boolSubSquare::resize(
    const boolHandle& h, Offset o, Extent m, Stride s2, Stride s1) {
  boolSubMatrix::resize(h, o, m, s2, m, s1);
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("boolSubMatrix::resize(\n"
    "const boolHandle&, Offset, Extent, Stride, Stride)",
    o, m, s2, m, s1);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
boolSubSquare&	boolSubSquare::resize(const boolSubSquare& N) {
  boolSubMatrix::resize(N); return *this; }
inline
boolSubSquare&	boolSubSquare::resize_(
    const boolHandle& h, Offset o, Stride s1, Extent m, Stride s2) {
  return resize(h, o, m, s2, s1);
  return *this; }

inline
boolSubSquare	boolSubSquare::r1(void) {
  return boolSubSquare(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent2(), +stride2(), -stride1()); }
inline
const
boolSubSquare	boolSubSquare::r1(void) const {
  return boolSubSquare(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent2(), +stride2(), -stride1()); }
inline
boolSubSquare&	boolSubSquare::reverse1(void) {
  boolSubMatrix::reverse1(); return *this; }
inline
boolSubSquare	boolSubSquare::r2(void) {
  return boolSubSquare(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(), +stride1()); }
inline
const
boolSubSquare	boolSubSquare::r2(void) const {
  return boolSubSquare(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(), +stride1()); }
inline
boolSubSquare&	boolSubSquare::reverse2(void) {
  boolSubMatrix::reverse2(); return *this; }
inline
boolSubSquare	boolSubSquare::r(void) {
  return boolSubSquare(handle(),
    offset() + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent2(), -stride2(), -stride1()); }
inline
const
boolSubSquare	boolSubSquare::r(void) const {
  return boolSubSquare(handle(),
    offset() + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent2(), -stride2(), -stride1()); }
inline
boolSubSquare&	boolSubSquare::reverse(void) {
  boolSubMatrix::reverse(); return *this; }
inline
boolSubSquare	boolSubSquare::t(void) {
  return boolSubSquare(handle(), offset(),
    extent1(), stride1(), stride2()); }
inline
const
boolSubSquare	boolSubSquare::t(void) const {
  return boolSubSquare(handle(), offset(),
    extent1(), stride1(), stride2()); }
inline
boolSubSquare&	boolSubSquare::swap(Offset i, Offset k) {
  boolSubMatrix::swap(i, k); return *this; }
inline
boolSubSquare&	boolSubSquare::swap_(Offset i, Offset k) {
  return swap(i-1, k-1); }
inline
boolSubSquare&	boolSubSquare::rotate(Stride n) {
  boolSubMatrix::rotate(n); return *this; }
inline
boolSubSquare&	boolSubSquare::shift(Stride n, bool b) {
  boolSubMatrix::shift(n, b); return *this; }

// Operators
inline
boolSubSquare&	boolSubSquare::operator  =(bool b) {
  boolSubMatrix::operator  =(b); return *this; }
inline
boolSubSquare&	boolSubSquare::operator  =(const boolSubSquare& N) {
  boolSubMatrix::operator  =(N); return *this; }

class boolSquare: public boolSubSquare {
  public:
  // Constructors
  		boolSquare(void);
  explicit	boolSquare(Extent m);
		boolSquare(Extent m, bool b);
		boolSquare(const boolSubSquare& M);
		boolSquare(const boolSquare& M);
		~boolSquare(void);
  // Functions
private:
  boolSquare&	  free(void) { boolSubSquare::free(); return *this; }
  boolSquare&				// OMITTED TO PREVENT INHERITANCE
		resize(const boolHandle& h,
      Offset o, Extent n2, Stride s2, Stride s1);
public:
  boolSquare&	resize(void);
  boolSquare&	resize(Extent m);
  boolSquare&	resize(Extent m, bool b);
  boolSquare&	resize(const boolSubSquare& N);
  // Operators
  boolSquare&	operator  =(bool b);
  boolSquare&	operator  =(const boolSubSquare& N);
  boolSquare&	operator  =(const boolSquare& N);
  };

// Constructors
inline		boolSquare::boolSquare(void): boolSubSquare() { }
inline		boolSquare::boolSquare(Extent m):
  boolSubSquare(allocate(m), (Offset)0, m, (Stride)m, (Stride)1) {
#ifdef SVMT_DEBUG_MODE
  boolSubSquare::operator =(true);
#endif // SVMT_DEBUG_MODE
  }
inline		boolSquare::boolSquare(Extent m, bool b):
  boolSubSquare(allocate(m), (Offset)0, m, (Stride)m, (Stride)1) {
  boolSubSquare::operator =(b); }
inline		boolSquare::boolSquare(const boolSubSquare& M):
  boolSubSquare(allocate(M.extent2()), (Offset)0,
      M.extent2(), (Stride)M.extent1(), (Stride)1) {
  boolSubSquare::operator =(M); }
inline		boolSquare::boolSquare(const boolSquare& M):
  boolSubSquare(allocate(M.extent2()), (Offset)0,
      M.extent2(), (Stride)M.extent1(), (Stride)1) {
  boolSubSquare::operator =(M); }
inline		boolSquare::~boolSquare(void) { free(); }

// Assignment Operators
inline
boolSquare&	boolSquare::operator  =(bool b) {
  boolSubSquare::operator =(b); return *this; }
inline
boolSquare&	boolSquare::operator  =(const boolSubSquare& N) {
  boolSubSquare::operator =(N); return *this; }
inline
boolSquare&	boolSquare::operator  =(const boolSquare& N) {
  boolSubSquare::operator =(N); return *this; }

// Functions
inline
boolSquare&	boolSquare::resize(void) { free();
  boolSubSquare::resize(); return *this; }
inline
boolSquare&	boolSquare::resize(Extent m) { free();
  boolSubSquare::resize(allocate(m), (Offset)0, m, (Stride)m, (Stride)1);
#ifdef SVMT_DEBUG_MODE
  boolSubSquare::operator =(true);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
boolSquare&	boolSquare::resize(Extent m, bool b) {
  return resize(m) = b; }
inline
boolSquare&	boolSquare::resize(const boolSubSquare& N) {
  return resize(N.extent2()) = N; }
// boolSubSquare function definitions which return boolSquare
inline
const
boolSquare	boolSubSquare::eq(bool b) const {
  boolMatrix	L = boolSubMatrix::eq(b);
  return *(boolSquare*)&L; }
inline
const
boolSquare	boolSubSquare::ne(bool b) const {
  boolMatrix	L = boolSubMatrix::ne(b);
  return *(boolSquare*)&L; }
inline
const
boolSquare	boolSubSquare::eq(const boolSubSquare& N) const {
  boolMatrix	L = boolSubMatrix::eq(N);
  return *(boolSquare*)&L; }
inline
const
boolSquare	boolSubSquare::ne(const boolSubSquare& N) const {
  boolMatrix	L = boolSubMatrix::ne(N);
  return *(boolSquare*)&L; }
inline
const
boolSquare	boolSubSquare::apply(const bool (*f)(const bool&)) const {
  boolMatrix	L = boolSubMatrix::apply(f);
  return *(boolSquare*)&L; }
inline
const
boolSquare	boolSubSquare::apply(      bool (*f)(const bool&)) const {
  boolMatrix	L = boolSubMatrix::apply(f);
  return *(boolSquare*)&L; }
inline
const
boolSquare	boolSubSquare::apply(      bool (*f)(      bool )) const {
  boolMatrix	L = boolSubMatrix::apply(f);
  return *(boolSquare*)&L; }

// boolSubMatrix operator definitions which return boolMatrix
inline
const
boolSquare	boolSubSquare::operator !(void) const {
  boolMatrix    L = boolSubMatrix::operator !();
  return *(boolSquare*)&L; }
inline
const
boolSquare	boolSubSquare::operator &&(const boolSubSquare& N) const {
  boolMatrix	L = boolSubMatrix::operator &&(N);
  return *(boolSquare*)&L; }
inline
const
boolSquare	boolSubSquare::operator ||(const boolSubSquare& N) const {
  boolMatrix	L = boolSubMatrix::operator ||(N);
  return *(boolSquare*)&L; }
inline
const
boolSquare	boolSubSquare::operator &&(bool b) const {
  boolMatrix	L = boolSubMatrix::operator &&(b);
  return *(boolSquare*)&L; }
inline
const
boolSquare	boolSubSquare::operator ||(bool b) const {
  boolMatrix	L = boolSubMatrix::operator ||(b);
  return *(boolSquare*)&L; }
inline
const
boolSquare	operator &&(bool b, const boolSubSquare& M) { return M && b; }
inline
const
boolSquare	operator ||(bool b, const boolSubSquare& M) { return M || b; }
#endif /* _boolSquare_h */

