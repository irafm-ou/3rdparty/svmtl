#ifndef _doubleMatrix_h
#define _doubleMatrix_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<boolMatrix.h>
#include<doubleVector.h>

class doubleSubMatrix {
  private:
  // Representation
  doubleHandle
		H;
  Offset	O;
  Extent	N1;
  Stride	S1;
  Extent	N2;
  Stride	S2;
  public:
  // Constructors
		doubleSubMatrix(void);
		doubleSubMatrix(const doubleHandle& h,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
		doubleSubMatrix(const doubleSubMatrix& M);
		~doubleSubMatrix(void);
  // Functions
  const
  doubleHandle&
		handle(void) const;
  Offset	offset(void) const;
  Extent	extent1(void) const;
  Extent	length1(void) const;	// deprecated
  Stride	stride1(void) const;
  Extent	extent2(void) const;
  Extent	length2(void) const;	// deprecated
  Stride	stride2(void) const;

  Extent nRows(void) const 
  { return extent2(); }

  Extent nCols(void) const 
  { return extent1(); }

  bool		 empty(void) const;
  static
  doubleHandle
	      allocate(Extent m, Extent n);
  static
  doubleHandle
	      allocate_(Extent n, Extent m);
  doubleSubMatrix&
		  free(void);
  doubleSubMatrix&
		resize(void);
  doubleSubMatrix&
		resize(const doubleHandle& h,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleSubMatrix&
		resize(const doubleSubMatrix& M);
  doubleSubMatrix&
		resize_(const doubleHandle& h,
      Offset o, Extent n1, Stride s1,
		Extent n2, Stride s2);
  bool	      contains(Offset i, Extent n2, Stride s2) const;
  bool	      contains(Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  bool	      contains_(Offset i, Extent n2, Stride s2) const;
  bool	      contains_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2) const;

  doubleSubMatrix
		   sub(Offset i, Extent n2, Stride s2);
  const
  doubleSubMatrix
		   sub(Offset i, Extent n2, Stride s2) const;
  doubleSubMatrix
		   sub(Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1);
  const
  doubleSubMatrix
		   sub(Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  doubleSubMatrix
		   sub_(Offset i, Extent n2, Stride s2);
  const
  doubleSubMatrix
		   sub_(Offset i, Extent n2, Stride s2) const;
  doubleSubMatrix
		   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2);
  const
  doubleSubMatrix
		   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2) const;
  doubleSubSquare
	     subsquare(Offset i, Extent m, Stride s2,
		       Offset j = (Offset)0, Stride s1 = (Stride)1);
  const
  doubleSubSquare
	     subsquare(Offset i, Extent m, Stride s2,
		       Offset j = (Offset)0, Stride s1 = (Stride)1) const;
  doubleSubSquare
	     subsquare_(Offset i, Extent m, Stride s2);
  const
  doubleSubSquare
	     subsquare_(Offset i, Extent m, Stride s2) const;
  doubleSubSquare
	     subsquare_(Offset j, Stride s1,
			Offset i, Extent m, Stride s2);
  const
  doubleSubSquare
	     subsquare_(Offset j, Stride s1,
			Offset i, Extent m, Stride s2) const;
  const
  doubleSubTensor
	     subtensor(Extent l = 1) const;
  doubleSubMatrix
		     t(void);
  const
  doubleSubMatrix
		     t(void) const;
  doubleSubVector
		  diag(void);
  const
  doubleSubVector
		  diag(void) const;

  doubleSubMatrix
		     r1(void);
  const
  doubleSubMatrix
		     r1(void) const;
  doubleSubMatrix&
	       reverse1(void);
  doubleSubMatrix
		     r2(void);
  const
  doubleSubMatrix
		     r2(void) const;
  doubleSubMatrix&
	       reverse2(void);
  doubleSubMatrix
		     r(void);
  const
  doubleSubMatrix
		     r(void) const;
  doubleSubMatrix&
	       reverse(void);
  doubleSubMatrix
		  even(void);
  const
  doubleSubMatrix
		  even(void) const;
  doubleSubMatrix
		   odd(void);
  const
  doubleSubMatrix
		   odd(void) const;
  doubleSubMatrix
		  even_(void);
  const
  doubleSubMatrix
		  even_(void) const;
  doubleSubMatrix
		   odd_(void);
  const
  doubleSubMatrix
		   odd_(void) const;

  doubleSubMatrix&
		  rdft(int sign = -1);	// complex to real    dft
  doubleSubMatrix&
		  cdft(int sign = -1);	// real    to complex dft
  doubleSubMatrix&
		  ramp(void);
  doubleSubMatrix&
		  swap(Offset i, Offset k);
  doubleSubMatrix&
		  swap_(Offset i, Offset k);
  doubleSubMatrix&
		  swap(doubleSubMatrix& N);
  doubleSubMatrix&
		rotate(Stride n);
  doubleSubMatrix&
		 shift(Stride n,const double& s
		  = (double)0);

  const
  doubleVector
		   sum(void) const;
  const
  doubleMatrix
		   dot(const doubleSubVector& v) const;
  const
  doubleMatrix
		   dot(const doubleSubMatrix& N) const;
  const
  doubleComplexMatrix
		   dot(const doubleComplexSubVector& v) const;
  const
  doubleComplexMatrix
		   dot(const doubleComplexSubMatrix& N) const;
  const
  doubleVector
		   dot_(const doubleSubVector& v) const;
  const
  doubleMatrix
		   dot_(const doubleSubMatrix& N) const;
  const
  doubleComplexVector
		   dot_(const doubleComplexSubVector& v) const;
  const
  doubleComplexMatrix
		   dot_(const doubleComplexSubMatrix& N) const;
  const
  doubleSquare
		   dot(void) const;
  const
  doubleSquare
		square(void) const;
  doubleSubMatrix&
		  pack(const double& s = (double)0);
  doubleSubMatrix&
		  pack(const doubleSubMatrix& N);

  const
  doubleMatrix
		   svd(void);
  const
  offsetVector	   lud(void);
  const
  offsetVector	   qrd(void);
private:
  doubleSubMatrix&
		   hht(const doubleSubVector& v);
public:
  const
  doubleMatrix
	   permutation(const offsetSubVector& p) const;
  const
  doubleMatrix
		    pl(const offsetSubVector& p,
		       const doubleSubMatrix& L) const;
  const
  doubleMatrix
		     l(const doubleSubMatrix& L) const;
  const
  doubleMatrix
		   pld(const offsetSubVector& p,
		       const doubleSubMatrix& LD) const;
  const
  doubleMatrix
		    ld(const doubleSubMatrix& LD) const;
  const
  doubleMatrix
		     u(const doubleSubMatrix& U) const;
  const
  doubleMatrix
		    up(const doubleSubMatrix& U,
		       const offsetSubVector& p) const;
  const
  doubleMatrix
		    du(const doubleSubMatrix& DU) const;
  const
  doubleMatrix
		   dup(const doubleSubMatrix& DU,
		       const offsetSubVector& p) const;
  const
  doubleMatrix
		    pl_(const offsetSubVector& p,
		       const doubleSubMatrix& L) const;
  const
  doubleMatrix
		     l_(const doubleSubMatrix& L) const;
  const
  doubleMatrix
		   pld_(const offsetSubVector& p,
		       const doubleSubMatrix& LD) const;
  const
  doubleMatrix
		    ld_(const doubleSubMatrix& LD) const;
  const
  doubleMatrix
		     u_(const doubleSubMatrix& U) const;
  const
  doubleMatrix
		    up_(const doubleSubMatrix& U,
		       const offsetSubVector& p) const;
  const
  doubleMatrix
		    du_(const doubleSubMatrix& DU) const;
  const
  doubleMatrix
		   dup_(const doubleSubMatrix& DU,
		       const offsetSubVector& p) const;
  const
  doubleMatrix
		    pq(const offsetSubVector& p,
		       const doubleSubMatrix& L) const;
  const
  doubleMatrix
		     q(const doubleSubMatrix& L) const;

  const
  boolMatrix
		    lt(const double& s) const;
  const
  boolMatrix
		    le(const double& s) const;
  const
  boolMatrix
		    gt(const double& s) const;
  const
  boolMatrix
		    ge(const double& s) const;
  const
  boolMatrix
		    lt(const doubleSubMatrix& N) const;
  const
  boolMatrix
		    le(const doubleSubMatrix& N) const;
  const
  boolMatrix
		    gt(const doubleSubMatrix& N) const;
  const
  boolMatrix
		    ge(const doubleSubMatrix& N) const;
  const
  boolMatrix
		    eq(const double& s) const;
  const
  boolMatrix
		    ne(const double& s) const;
  const
  boolMatrix
		    eq(const doubleComplexSubMatrix& N) const;
  const
  boolMatrix
		    ne(const doubleComplexSubMatrix& N) const;
  const
  boolMatrix
		    eq(const doubleSubMatrix& N) const;
  const
  boolMatrix
		    ne(const doubleSubMatrix& N) const;

  Extent	 zeros(void) const;
  const
  offsetVector
		 index(void) const;
  const
  doubleVector
		gather(const offsetSubVector& x) const;
  doubleSubMatrix&
	       scatter(const offsetSubVector& x,
		       const doubleSubVector& t);
  const
  doubleMatrix
		 aside(const doubleSubMatrix& N) const;
  const
  doubleMatrix
		 above(const doubleSubMatrix& N) const;
  const
  doubleMatrix
		 above(const doubleSubVector& v) const;
  const
  doubleTensor
		 afore(const doubleSubMatrix& N) const;
  const
  doubleTensor
		 afore(const doubleSubTensor& T) const;
  const
  doubleMatrix
		 above_(const doubleSubMatrix& N) const;
  const
  doubleMatrix
		 aside_(const doubleSubMatrix& N) const;
  const
  doubleMatrix
		 aside_(const doubleSubVector& v) const;
  const
  doubleMatrix
		  kron(const doubleSubVector& v) const;
  const
  doubleMatrix
		  kron(const doubleSubMatrix& N) const;
  const
  doubleTensor
		  kron(const doubleSubTensor& T) const;
  const
  doubleComplexMatrix
		  kron(const doubleComplexSubVector& v) const;
  const
  doubleComplexMatrix
		  kron(const doubleComplexSubMatrix& N) const;
  const
  doubleComplexTensor
		  kron(const doubleComplexSubTensor& T) const;

  const
  doubleMatrix
		 apply(const double (*f)(const double&)) const;
  const
  doubleMatrix
		 apply(      double (*f)(const double&)) const;
  const
  doubleMatrix
		 apply(      double (*f)(      double )) const;

  // Operators
  doubleSubVector
		operator [](Offset i);
  const
  doubleSubVector
		operator [](Offset i) const;
  doubleSubVector
		operator ()(Offset i);
  const
  doubleSubVector
		operator ()(Offset i) const;
  doubleSubScalar
		operator ()(Offset j, Offset i);
  const
  doubleSubScalar
		operator ()(Offset j, Offset i) const;
  const
  doubleMatrix
		operator -(void) const;
//  const
//  doubleSubMatrix
//		operator +(void) const;
  const
  doubleMatrix
		operator *(const double& s) const;
  const
  doubleMatrix
		operator /(const double& s) const;
  const
  doubleMatrix
		operator +(const double& s) const;
  const
  doubleMatrix
		operator -(const double& s) const;

  const
  doubleComplexMatrix
		operator *(const doubleComplex& s) const;
  const
  doubleComplexMatrix
		operator /(const doubleComplex& s) const;
  const
  doubleComplexMatrix
		operator +(const doubleComplex& s) const;
  const
  doubleComplexMatrix
		operator -(const doubleComplex& s) const;
  const
  doubleComplexMatrix
		operator *(const doubleComplexSubMatrix& N) const;
  const
  doubleComplexMatrix
		operator /(const doubleComplexSubMatrix& N) const;
  const
  doubleComplexMatrix
		operator +(const doubleComplexSubMatrix& N) const;
  const
  doubleComplexMatrix
		operator -(const doubleComplexSubMatrix& N) const;

  const
  doubleMatrix
		operator *(const doubleSubMatrix& N) const;
  const
  doubleMatrix
		operator /(const doubleSubMatrix& N) const;
  const
  doubleMatrix
		operator +(const doubleSubMatrix& N) const;
  const
  doubleMatrix
		operator -(const doubleSubMatrix& N) const;
  bool		operator < (const double& s) const;
  bool		operator <=(const double& s) const;
  bool		operator > (const double& s) const;
  bool		operator >=(const double& s) const;
  bool		operator < (const doubleSubMatrix& N) const;
  bool		operator <=(const doubleSubMatrix& N) const;
  bool		operator > (const doubleSubMatrix& N) const;
  bool		operator >=(const doubleSubMatrix& N) const;
  bool		operator ==(const double& s) const;
  bool		operator !=(const double& s) const;
  bool		operator ==(const doubleComplex& s) const;
  bool		operator !=(const doubleComplex& s) const;
  bool		operator ==(const doubleComplexSubMatrix& N) const;
  bool		operator !=(const doubleComplexSubMatrix& N) const;
  bool		operator ==(const doubleSubMatrix& N) const;
  bool		operator !=(const doubleSubMatrix& N) const;

  doubleSubMatrix&
		operator  =(const double& s);
  doubleSubMatrix&
		operator *=(const double& s);
  doubleSubMatrix&
		operator /=(const double& s);
  doubleSubMatrix&
		operator +=(const double& s);
  doubleSubMatrix&
		operator -=(const double& s);

  doubleSubMatrix&
		operator  =(const doubleSubMatrix& N);
  doubleSubMatrix&
		operator *=(const doubleSubMatrix& N);
  doubleSubMatrix&
		operator /=(const doubleSubMatrix& N);
  doubleSubMatrix&
		operator +=(const doubleSubMatrix& N);
  doubleSubMatrix&
		operator -=(const doubleSubMatrix& N);
  friend class doubleMatrix;
  };

// Constructors
inline		doubleSubMatrix::doubleSubMatrix(void):
  H((double*)0), O((Offset)0), N1((Extent)0), S1((Stride)0),
    N2((Extent)0), S2((Stride)0) { }
inline		doubleSubMatrix::doubleSubMatrix(
    const doubleHandle& h, Offset o,
    Extent n2, Stride s2,
    Extent n1, Stride s1):
  H(h), O(o), N1(n1), S1(s1), N2(n2), S2(s2) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment(
    "doubleSubMatrix::doubleSubMatrix(\n"
    "const doubleHandle&, Offset, Extent, Stride, Extent, Stride)",
    o, n2, s2, n1, s1);
#endif // SVMT_DEBUG_MODE
  }
inline		doubleSubMatrix::doubleSubMatrix(
    const doubleSubMatrix& M): H(M.H), O(M.O),
    N1(M.N1), S1(M.S1), N2(M.N2), S2(M.S2) { }
inline		doubleSubMatrix::~doubleSubMatrix(void) { }

// Functions
inline
const
doubleHandle&
		doubleSubMatrix::handle(void) const { return H; }
inline
Offset		doubleSubMatrix::offset(void) const { return O; }
inline
Extent		doubleSubMatrix::extent1(void) const { return N1; }
inline
Extent		doubleSubMatrix::length1(void) const { return N1; }
inline
Stride		doubleSubMatrix::stride1(void) const { return S1; }
inline
Extent		doubleSubMatrix::extent2(void) const { return N2; }
inline
Extent		doubleSubMatrix::length2(void) const { return N2; }
inline
Stride		doubleSubMatrix::stride2(void) const { return S2; }
inline
bool		doubleSubMatrix::empty(void) const {
  return handle().empty()||0 == extent1()||0 == extent2(); }
inline
doubleHandle
		doubleSubMatrix::allocate(Extent m, Extent n) {
  return doubleHandle(new double[m*n]); }
inline
doubleHandle
		doubleSubMatrix::allocate_(Extent n, Extent m) {
  return allocate(m, n); }
inline
doubleSubMatrix&
		doubleSubMatrix::free(void) {
  delete [] (double*)H; return *this; }

inline
doubleSubMatrix&
		doubleSubMatrix::resize(void) {
  H = doubleHandle((double*)0); O = (Offset)0;
  N1 = (Extent)0; S1 = (Stride)0;
  N2 = (Extent)0; S2 = (Stride)0; return *this; }
inline
doubleSubMatrix&
		doubleSubMatrix::resize(const doubleHandle& h,
    Offset o, Extent n2, Stride s2,
	      Extent n1, Stride s1) {
  H = h; O = o; N1 = n1; S1 = s1; N2 = n2; S2 = s2;
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment(
    "doubleSubMatrix::resize(const doubleHandle&,\n"
    "Offset, Extent, Stride, Extent, Stride)",
    o, n2, s2, n1, s1);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
doubleSubMatrix&
		doubleSubMatrix::resize(
    const doubleSubMatrix& M) { return resize(M.handle(),
    M.offset(), M.extent2(), M.stride2(),
		M.extent1(), M.stride1()); }
inline
doubleSubMatrix&
		doubleSubMatrix::resize_(const doubleHandle& h,
    Offset o, Extent n1, Stride s1,
	      Extent n2, Stride s2) { return resize(h, o, n2, s2, n1, s1); }
inline
bool		doubleSubMatrix::contains(
    Offset i, Extent n2, Stride s2) const {
  return boolSubVector::contains(
    i, n2, s2, extent2()); }
inline
bool		doubleSubMatrix::contains(
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) const {
  return boolSubMatrix::contains(
    i, n2, s2, extent2(),
    j, n1, s1, extent1()); }
inline
bool		doubleSubMatrix::contains_(
    Offset i, Extent n2, Stride s2) const {
  return contains(i-1, n2, s2); }
inline
bool		doubleSubMatrix::contains_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2) const {
  return contains(i-1, n2, s2, j-1, n1, s1); }

inline
doubleSubMatrix
		doubleSubMatrix::sub(
    Offset i, Extent n2, Stride s2) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleSubMatrix::sub(Offset, Extent, Stride)",
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleSubMatrix(handle(), offset() + (Stride)i*stride2(),
    n2, s2*stride2(), extent1(), stride1()); }
inline
const
doubleSubMatrix
		doubleSubMatrix::sub(
    Offset i, Extent n2, Stride s2) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleSubMatrix::sub(Offset, Extent, Stride) const",
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleSubMatrix(handle(), offset() + (Stride)i*stride2(),
    n2, s2*stride2(), extent1(), stride1()); }
inline
doubleSubMatrix
		doubleSubMatrix::sub(
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleSubMatrix::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride)",
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleSubMatrix(handle(),
    offset() + (Stride)i*stride2() + (Stride)j*stride1(),
    n2, s2*stride2(), n1, s1*stride1()); }
inline
const
doubleSubMatrix
		doubleSubMatrix::sub(
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleSubMatrix::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride) const",
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleSubMatrix(handle(),
    offset() + (Stride)i*stride2() + (Stride)j*stride1(),
    n2, s2*stride2(), n1, s1*stride1()); }

inline
doubleSubMatrix
		doubleSubMatrix::sub_(
    Offset i, Extent n2, Stride s2) { return sub(i-1, n2, s2); }
inline
const
doubleSubMatrix
		doubleSubMatrix::sub_(
    Offset i, Extent n2, Stride s2) const { return sub(i-1, n2, s2); }
inline
doubleSubMatrix
		doubleSubMatrix::sub_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2) {
  return sub(i-1, n2, s2, j-1, n1, s1); }
inline
const
doubleSubMatrix
		doubleSubMatrix::sub_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2) const {
  return sub(i-1, n2, s2, j-1, n1, s1); }
inline
const
doubleSubMatrix
		doubleSubScalar::submatrix(Extent n, Extent m) const {
  return doubleSubMatrix(handle(), offset(),
    m, (Stride)0, n, (Stride)0); }
inline
const
doubleSubMatrix
		doubleSubVector::submatrix(Extent m) const {
  return doubleSubMatrix(handle(), offset(),
    m, (Stride)0, extent(), stride()); }
inline
doubleSubMatrix
		doubleSubMatrix::t(void) {
  return doubleSubMatrix(handle(), offset(),
    extent1(), stride1(), extent2(), stride2()); }
inline
const
doubleSubMatrix
		doubleSubMatrix::t(void) const {
  return doubleSubMatrix(handle(), offset(),
    extent1(), stride1(), extent2(), stride2()); }
inline
doubleSubVector
		doubleSubMatrix::diag(void) {
  return doubleSubVector(handle(), offset(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
const
doubleSubVector
		doubleSubMatrix::diag(void) const {
  return doubleSubVector(handle(), offset(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }

inline
doubleSubMatrix
		doubleSubMatrix::r1(void) {
  return doubleSubMatrix(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
const
doubleSubMatrix
		doubleSubMatrix::r1(void) const {
  return doubleSubMatrix(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
doubleSubMatrix
		doubleSubMatrix::r2(void) {
  return doubleSubMatrix(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
const
doubleSubMatrix
		doubleSubMatrix::r2(void) const {
  return doubleSubMatrix(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
doubleSubMatrix
		doubleSubMatrix::r(void) {
  return doubleSubMatrix(handle(),
    offset() + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
const
doubleSubMatrix
		doubleSubMatrix::r(void) const {
  return doubleSubMatrix(handle(),
    offset() + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
doubleSubMatrix&
		doubleSubMatrix::reverse(void) {
  return reverse1().reverse2(); }

inline
doubleSubMatrix
		doubleSubMatrix::even(void) {
  return doubleSubMatrix(handle(), offset(),
    extent2(), stride2(), (extent1() + 1) >> 1, stride1() << 1); }
inline
const
doubleSubMatrix
		doubleSubMatrix::even(void) const {
  return doubleSubMatrix(handle(), offset(),
    extent2(), stride2(), (extent1() + 1) >> 1, stride1() << 1); }
inline
doubleSubMatrix
		doubleSubMatrix::odd(void) {
  return doubleSubMatrix(handle(), offset() + stride1(),
    extent2(), stride2(), extent1() >> 1, stride1() << 1); }
inline
const
doubleSubMatrix
		doubleSubMatrix::odd(void) const {
  return doubleSubMatrix(handle(), offset() + stride1(),
    extent2(), stride2(), extent1() >> 1, stride1() << 1); }
inline
doubleSubMatrix
		doubleSubMatrix::even_(void) { return odd(); }
inline
const
doubleSubMatrix
		doubleSubMatrix::even_(void) const { return odd(); }
inline
doubleSubMatrix
		doubleSubMatrix::odd_(void) { return even(); }
inline
const
doubleSubMatrix
		doubleSubMatrix::odd_(void) const { return even(); }
const
doubleVector
		   min(const doubleSubMatrix& M);
const
doubleVector
		   max(const doubleSubMatrix& M);
const
doubleMatrix
		   min(const doubleSubMatrix& M,
		       const doubleSubMatrix& N);
const
doubleMatrix
		   max(const doubleSubMatrix& M,
		       const doubleSubMatrix& N);
const
doubleMatrix
		   sgn(const doubleSubMatrix& M);
const
doubleMatrix
		   abs(const doubleSubMatrix& M);
const
doubleMatrix
		 floor(const doubleSubMatrix& M);
const
doubleMatrix
		  ceil(const doubleSubMatrix& M);
const
doubleMatrix
		 hypot(const doubleSubMatrix& M,
		       const doubleSubMatrix& N);
const
doubleMatrix
		 atan2(const doubleSubMatrix& N,
		       const doubleSubMatrix& M);

const
doubleMatrix
		   log(const doubleSubMatrix& M);
const
doubleMatrix
		   exp(const doubleSubMatrix& M);
const
doubleMatrix
		  sqrt(const doubleSubMatrix& M);
const
doubleMatrix
		   cos(const doubleSubMatrix& M);
const
doubleMatrix
		   sin(const doubleSubMatrix& M);
const
doubleMatrix
		   tan(const doubleSubMatrix& M);
const
doubleMatrix
		  acos(const doubleSubMatrix& M);
const
doubleMatrix
		  asin(const doubleSubMatrix& M);
const
doubleMatrix
		  atan(const doubleSubMatrix& M);
const
doubleMatrix
		  cosh(const doubleSubMatrix& M);
const
doubleMatrix
		  sinh(const doubleSubMatrix& M);
const
doubleMatrix
		  tanh(const doubleSubMatrix& M);
const
doubleMatrix
		 acosh(const doubleSubMatrix& M);
const
doubleMatrix
		 asinh(const doubleSubMatrix& M);
const
doubleMatrix
		 atanh(const doubleSubMatrix& M);

// Operators
inline
doubleSubVector
		doubleSubMatrix::operator [](Offset i) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleSubMatrix::operator [](Offset)", i, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleSubVector(handle(), offset() + (Stride)i*stride2(),
    extent1(), stride1()); }
inline
const
doubleSubVector
		doubleSubMatrix::operator [](Offset i) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleSubMatrix::operator [](Offset) const", i, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleSubVector(handle(), offset() + (Stride)i*stride2(),
    extent1(), stride1()); }
inline
doubleSubVector
		doubleSubMatrix::operator ()(Offset i) {
  return operator [](i-1); }
inline
const
doubleSubVector
		doubleSubMatrix::operator ()(Offset i) const {
  return operator [](i-1); }
inline
doubleSubScalar
		doubleSubMatrix::operator ()(Offset j, Offset i) {
  doubleSubMatrix&	M = *this; return M[i-1][j-1]; }
inline
const
doubleSubScalar
		doubleSubMatrix::operator ()(Offset j, Offset i) const {
  const
  doubleSubMatrix&	M = *this; return M[i-1][j-1]; }
inline
doubleSubMatrix&
		doubleSubMatrix::swap(Offset i, Offset k) {
  doubleSubMatrix	M = *this;
  doubleSubVector	v = M[i];
  doubleSubVector	w = M[k];
  v.swap(w); return *this; }
inline
doubleSubMatrix&
		doubleSubMatrix::swap_(Offset i, Offset k) {
  return swap(i-1, k-1); }
//inline
//const
//doubleSubMatrix
//		doubleSubMatrix::operator +(void) const {
//  return *this; }
std::istream&	operator >>(std::istream& s,       doubleSubMatrix& M);
std::ostream&	operator <<(std::ostream& s, const doubleSubMatrix& M);
inline
bool		doubleSubMatrix::operator > (
    const doubleSubMatrix& N) const { return N <  *this; }
inline
bool		doubleSubMatrix::operator >=(
    const doubleSubMatrix& N) const { return N <= *this; }

class doubleSubArray2: public doubleSubMatrix {
  public:
  // Constructors
		doubleSubArray2(void);
		doubleSubArray2(double* p,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
		doubleSubArray2(const doubleSubArray2& M);
		~doubleSubArray2(void);
  // Functions
  doubleSubArray2&
		resize(void);
  doubleSubArray2&		// resize from pointer
		resize(double* p,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleSubArray2&		// resize from SubArray2
		resize(const doubleSubArray2& M);
  doubleSubArray2&		// resize from pointer
		resize_(double* p,
      Offset o, Extent n1, Stride s1,
		Extent n2, Stride s2);
  private:
  doubleSubArray2&		// prevent resize from Handle
		resize(const doubleHandle& h,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleSubArray2&		// prevent resize from SubMatrix
		resize(const doubleSubMatrix& M);
  public:
  doubleSubArray2
		   sub(Offset i, Extent n2, Stride s2);
  const
  doubleSubArray2
		   sub(Offset i, Extent n2, Stride s2) const;
  doubleSubArray2
		   sub(Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1);
  const
  doubleSubArray2
		   sub(Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  doubleSubArray2
		   sub_(Offset i, Extent n2, Stride s2);
  const
  doubleSubArray2
		   sub_(Offset i, Extent n2, Stride s2) const;
  doubleSubArray2
		   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2);
  const
  doubleSubArray2
		   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2) const;
  const
  doubleSubArray3
	     subtensor(Extent l = 1) const;
  doubleSubArray2
		     t(void);
  const
  doubleSubArray2
		     t(void) const;

  doubleSubArray1
		  diag(void);
  const
  doubleSubArray1
		  diag(void) const;
  doubleSubArray2
		     r1(void);
  const
  doubleSubArray2
		     r1(void) const;
  doubleSubArray2&
	       reverse1(void);
  doubleSubArray2
		     r2(void);
  const
  doubleSubArray2
		     r2(void) const;
  doubleSubArray2&
	       reverse2(void);
  doubleSubArray2
		     r(void);
  const
  doubleSubArray2
		     r(void) const;
  doubleSubArray2&
	       reverse(void);
  doubleSubArray2
		  even(void);
  const
  doubleSubArray2
		  even(void) const;
  doubleSubArray2
		   odd(void);
  const
  doubleSubArray2
		   odd(void) const;
  doubleSubArray2
		  even_(void);
  const
  doubleSubArray2
		  even_(void) const;
  doubleSubArray2
		   odd_(void);
  const
  doubleSubArray2
		   odd_(void) const;

  doubleSubArray2&
		  rdft(int sign = -1);	// complex to real    dft
  doubleSubArray2&
		  cdft(int sign = -1);	// real    to complex dft
  doubleSubArray2&
		  ramp(void);
  doubleSubArray2&
		  swap(Offset i, Offset k);
  doubleSubArray2&
		  swap_(Offset i, Offset k);
  doubleSubArray2&
		  swap(doubleSubMatrix& N);
  doubleSubArray2&
		rotate(Stride n);
  doubleSubArray2&
		 shift(Stride n,const double& s
		  = (double)0);
  doubleSubArray2&
		  pack(const double& s = (double)0);
  doubleSubArray2&
		  pack(const doubleSubMatrix& N);
  doubleSubArray2&
	       scatter(const offsetSubVector& x,
		       const doubleSubVector& t);

  // Operators
  doubleSubArray1
		operator [](Offset i);
  const
  doubleSubArray1
		operator [](Offset i) const;
  doubleSubArray1
		operator ()(Offset i);
  const
  doubleSubArray1
		operator ()(Offset i) const;
  doubleSubArray0
		operator ()(Offset j, Offset i);
  const
  doubleSubArray0
		operator ()(Offset j, Offset i) const;

//  const
//  doubleSubArray2
//		operator +(void) const;
  doubleSubArray2&
		operator  =(const double& s);
  doubleSubArray2&
		operator *=(const double& s);
  doubleSubArray2&
		operator /=(const double& s);
  doubleSubArray2&
		operator +=(const double& s);
  doubleSubArray2&
		operator -=(const double& s);
  doubleSubArray2&
		operator  =(const doubleSubMatrix& N);
  doubleSubArray2&
		operator *=(const doubleSubMatrix& N);
  doubleSubArray2&
		operator /=(const doubleSubMatrix& N);
  doubleSubArray2&
		operator +=(const doubleSubMatrix& N);
  doubleSubArray2&
		operator -=(const doubleSubMatrix& N);
  };

// Constructors
inline		doubleSubArray2::doubleSubArray2(void):
  doubleSubMatrix() { }
inline		doubleSubArray2::doubleSubArray2(double* p,
    Offset o, Extent n2, Stride s2, Extent n1, Stride s1):
  doubleSubMatrix(doubleHandle(p), o, n2, s2, n1, s1) { }
inline		doubleSubArray2::doubleSubArray2(
    const doubleSubArray2& M): doubleSubMatrix(M) { }
inline		doubleSubArray2::~doubleSubArray2(void) { }

// Functions
inline
doubleSubArray2&
		doubleSubArray2::resize(void) {
  doubleSubMatrix::resize(); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::resize(double* p,
    Offset o, Extent n2, Stride s2,
	      Extent n1, Stride s1) {
  doubleSubMatrix::resize(doubleHandle(p),
    o, n2, s2, n1, s1); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::resize(
    const doubleSubArray2& M) {
  doubleSubMatrix::resize(M); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::resize_(double* p,
    Offset o, Extent n1, Stride s1,
	      Extent n2, Stride s2) { return resize(p, o, n2, s2, n1, s1); }

inline
doubleSubArray2
		doubleSubArray2::sub(
    Offset i, Extent n2, Stride s2) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleSubArray2::sub(Offset, Extent, Stride)",
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset() + (Stride)i*stride2(), n2, s2*stride2(), extent1(), stride1()); }
inline
const
doubleSubArray2
		doubleSubArray2::sub(
    Offset i, Extent n2, Stride s2) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "doubleSubArray2::sub(Offset, Extent, Stride) const",
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset() + (Stride)i*stride2(), n2, s2*stride2(), extent1(), stride1()); }
inline
doubleSubArray2
		doubleSubArray2::sub(
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleSubArray2::sub(\n"
    "Offset, Extent, Stride , Offset, Extent, Stride)",
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset() + (Stride)i*stride2() + (Stride)j*stride1(),
    n2, s2*stride2(), n1, s1*stride1()); }
inline
const
doubleSubArray2
		doubleSubArray2::sub(
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("doubleSubArray2::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride) const",
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset() + (Stride)i*stride2() + (Stride)j*stride1(),
    n2, s2*stride2(), n1, s1*stride1()); }

inline
doubleSubArray2
		doubleSubArray2::sub_(
    Offset i, Extent n2, Stride s2) { return sub(i-1, n2, s2); }
inline
const
doubleSubArray2
		doubleSubArray2::sub_(
    Offset i, Extent n2, Stride s2) const { return sub(i-1, n2, s2); }
inline
doubleSubArray2
		doubleSubArray2::sub_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2) {
  return sub(i-1, n2, s2, j-1, n1, s1); }
inline
const
doubleSubArray2
		doubleSubArray2::sub_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2) const {
  return sub(i-1, n2, s2, j-1, n1, s1); }
inline
const
doubleSubArray2
		doubleSubArray0::submatrix(Extent n, Extent m) const {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset(), m, (Stride)0, n, (Stride)0); }
inline
const
doubleSubArray2
		doubleSubArray1::submatrix(Extent m) const {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset(), m, (Stride)0, extent(), stride()); }
inline
doubleSubArray2
		doubleSubArray2::t(void) {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset(), extent1(), stride1(), extent2(), stride2()); }
inline
const
doubleSubArray2
		doubleSubArray2::t(void) const {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset(), extent1(), stride1(), extent2(), stride2()); }
inline
doubleSubArray1
		doubleSubArray2::diag(void) {
  return doubleSubArray1((double*)(doubleHandle&)handle(),
    offset(), (extent1() < extent2())? extent1(): extent2(),
    stride2() + stride1()); }
inline
const
doubleSubArray1
		doubleSubArray2::diag(void) const {
  return doubleSubArray1((double*)(doubleHandle&)handle(),
    offset(), (extent1() < extent2())? extent1(): extent2(),
    stride2() + stride1()
); }

inline
doubleSubArray2
		doubleSubArray2::r1(void) {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
const
doubleSubArray2
		doubleSubArray2::r1(void) const {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
     extent2(), +stride2(),
     extent1(), -stride1()) ; }
inline
doubleSubArray2&
		doubleSubArray2::reverse1(void) {
  doubleSubMatrix::reverse1(); return *this; }
inline
doubleSubArray2
		doubleSubArray2::r2(void) {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
const
doubleSubArray2
		doubleSubArray2::r2(void) const {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(),
    extent1(), +stride1()) ; }
inline
doubleSubArray2&
		doubleSubArray2::reverse2(void) {
  doubleSubMatrix::reverse2(); return *this; }
inline
doubleSubArray2
		doubleSubArray2::r(void) {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset() + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
const
doubleSubArray2
		doubleSubArray2::r(void) const {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset() + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
     extent2(), -stride2(),
     extent1(), -stride1()) ; }
inline
doubleSubArray2&
		doubleSubArray2::reverse(void) {
  doubleSubMatrix::reverse(); return *this; }

inline
doubleSubArray2
		doubleSubArray2::even(void) {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset(), extent2(), stride2(), (extent1() + 1) >> 1, stride1() << 1); }
inline
const
doubleSubArray2
		doubleSubArray2::even(void) const {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
    offset(), extent2(), stride2(), (extent1() + 1) >> 1, stride1() << 1); }
inline
doubleSubArray2
		doubleSubArray2::odd(void) {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
  offset() + stride1(), extent2(), stride2(), extent1() >> 1, stride1() << 1); }
inline
const
doubleSubArray2
		doubleSubArray2::odd(void) const {
  return doubleSubArray2((double*)(doubleHandle&)handle(),
  offset() + stride1(), extent2(), stride2(), extent1() >> 1, stride1() << 1); }
inline
doubleSubArray2
		doubleSubArray2::even_(void) { return odd(); }
inline
const
doubleSubArray2
		doubleSubArray2::even_(void) const { return odd(); }
inline
doubleSubArray2
		doubleSubArray2::odd_(void) { return even(); }
inline
const
doubleSubArray2
		doubleSubArray2::odd_(void) const { return even(); }
inline
doubleSubArray2&
		doubleSubArray2::rdft(int sign) {
  doubleSubMatrix::rdft(sign); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::cdft(int sign) {
  doubleSubMatrix::cdft(sign); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::ramp(void) {
  doubleSubMatrix::ramp(); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::swap(Offset i, Offset k) {
  doubleSubMatrix::swap(i, k); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::swap(doubleSubMatrix& N) {
  doubleSubMatrix::swap(N); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::swap_(Offset i, Offset k) {
  return swap(i-1, k-1); }
inline
doubleSubArray2&
		doubleSubArray2::rotate(Stride n) {
  doubleSubMatrix::rotate(n); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::shift(Stride n,const double& s) {
  doubleSubMatrix::shift(n,s); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::pack(const double& s) {
  doubleSubMatrix::pack(s); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::pack(
    const doubleSubMatrix& N) {
  doubleSubMatrix::pack(N); return *this; }

// doubleSubMatrix function definitions
// which use doubleSubArray2
inline
const
boolMatrix
		doubleSubMatrix::lt(const double& s) const {
  return lt(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolMatrix
		doubleSubMatrix::le(const double& s) const {
  return le(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolMatrix
		doubleSubMatrix::gt(const double& s) const {
  return doubleSubArray2((double*)&s, (Offset)0,
    extent2(), (Stride)0, extent1(), (Stride)0).lt(*this); }
inline
const
boolMatrix
		doubleSubMatrix::ge(const double& s) const {
  return doubleSubArray2((double*)&s, (Offset)0,
    extent2(), (Stride)0, extent1(), (Stride)0).le(*this); }
inline
const
boolMatrix
		doubleSubMatrix::gt(
    const doubleSubMatrix& N) const { return N.lt(*this); }
inline
const
boolMatrix
		doubleSubMatrix::ge(
    const doubleSubMatrix& N) const { return N.le(*this); }

inline
const
boolMatrix
		doubleSubMatrix::eq(const double& s) const {
  return eq(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolMatrix
		doubleSubMatrix::ne(const double& s) const {
  return ne(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleSubArray2&
	       doubleSubArray2::scatter(
    const offsetSubVector& x, const doubleSubVector& t) {
  doubleSubMatrix::scatter(x, t); return *this; }

// Operators
inline
doubleSubArray1
		doubleSubArray2::operator [](Offset i) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleSubArray2::operator [](Offset)", i, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray1((double*)(doubleHandle&)handle(),
    offset() + (Stride)i*stride2(), extent1(), stride1()); }
inline
const
doubleSubArray1
		doubleSubArray2::operator [](Offset i) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "doubleSubArray2::operator [](Offset) const", i, extent2());
#endif // SVMT_DEBUG_MODE
  return doubleSubArray1((double*)(doubleHandle&)handle(),
    offset() + (Stride)i*stride2(), extent1(), stride1()); }
inline
doubleSubArray1
		doubleSubArray2::operator ()(Offset i) {
  return operator [](i-1); }
inline
const
doubleSubArray1
		doubleSubArray2::operator ()(Offset i) const {
  return operator [](i-1); }
inline
doubleSubArray0
		doubleSubArray2::operator ()(Offset j, Offset i) {
  doubleSubArray2&	M = *this; return M[i-1][j-1]; }
inline
const
doubleSubArray0
		doubleSubArray2::operator ()(Offset j, Offset i) const {
  const
  doubleSubArray2&	M = *this; return M[i-1][j-1]; }

//inline
//const
//doubleSubArray2
//		doubleSubArray2::operator +(void) const {
//  return *this; }
inline
doubleSubArray2&
		doubleSubArray2::operator  =(const double& s) {
  doubleSubMatrix::operator  =(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::operator *=(const double& s) {
  doubleSubMatrix::operator *=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::operator /=(const double& s) {
  doubleSubMatrix::operator /=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::operator +=(const double& s) {
  doubleSubMatrix::operator +=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::operator -=(const double& s) {
  doubleSubMatrix::operator -=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); return *this; }


inline
doubleSubArray2&
		doubleSubArray2::operator  =(
    const doubleSubMatrix& N) {
  doubleSubMatrix::operator  =(N); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::operator *=(
    const doubleSubMatrix& N) {
  doubleSubMatrix::operator *=(N); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::operator /=(
    const doubleSubMatrix& N) {
  doubleSubMatrix::operator /=(N); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::operator +=(
    const doubleSubMatrix& N) {
  doubleSubMatrix::operator +=(N); return *this; }
inline
doubleSubArray2&
		doubleSubArray2::operator -=(
    const doubleSubMatrix& N) {
  doubleSubMatrix::operator -=(N); return *this; }

// doubleSubMatrix operator definitions
// which use doubleSubArray2
inline
bool		doubleSubMatrix::operator < (const double& s) const {
  return operator < (doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		doubleSubMatrix::operator <=(const double& s) const {
  return operator <=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		doubleSubMatrix::operator > (const double& s) const {
  return operator > (doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		doubleSubMatrix::operator >=(const double& s) const {
  return operator >=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		operator < (
    const double& s, const doubleSubMatrix& M) { return M >  s; }
inline
bool		operator <=(
    const double& s, const doubleSubMatrix& M) { return M >= s; }
inline
bool		operator > (
    const double& s, const doubleSubMatrix& M) { return M <  s; }
inline
bool		operator >=(
    const double& s, const doubleSubMatrix& M) { return M <= s; }

inline
bool		doubleSubMatrix::operator ==(const double& s) const {
  return operator ==(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		doubleSubMatrix::operator !=(const double& s) const {
  return operator !=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		operator ==(
    const double& s, const doubleSubMatrix& M) { return M == s; }
inline
bool		operator !=(
    const double& s, const doubleSubMatrix& M) { return M != s; }

inline
doubleSubMatrix&
		doubleSubMatrix::operator  =(const double& s) {
  return operator  =(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleSubMatrix&
		doubleSubMatrix::operator *=(const double& s) {
  return operator *=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleSubMatrix&
		doubleSubMatrix::operator /=(const double& s) {
  return operator /=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleSubMatrix&
		doubleSubMatrix::operator +=(const double& s) {
  return operator +=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
doubleSubMatrix&
		doubleSubMatrix::operator -=(const double& s) {
  return operator -=(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }


class doubleMatrix: public doubleSubMatrix {
  public:
  // Constructors
		doubleMatrix(void);
  explicit	doubleMatrix(Extent m, Extent n = 1);
		doubleMatrix(Extent m, Extent n,
      const double& s);
		doubleMatrix(Extent m, Extent n,
      const double& s, const double& t);
		doubleMatrix(const doubleSubMatrix& M);
		doubleMatrix(const doubleMatrix& M);
		~doubleMatrix(void);
  // Functions
  doubleMatrix&
		resize(void);
  doubleMatrix&
		resize(Extent m, Extent n = 1);
  doubleMatrix&
		resize_(Extent n, Extent m);
  doubleMatrix&
		resize(Extent m, Extent n,
      const double& s);
  doubleMatrix&
		resize_(Extent n, Extent m,
      const double& s);
  doubleMatrix&
		resize(Extent m, Extent n,
      const double& s, const double& t);
  doubleMatrix&
		resize_(Extent m, Extent n,
      const double& s, const double& t);
  doubleMatrix&
		resize(const doubleSubMatrix& M);
private:
  doubleMatrix&
		resize(const doubleHandle& h,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
  doubleMatrix&
		free(void) { doubleSubMatrix::free(); return *this; }
public:
  // Operators
  doubleMatrix&
		operator  =(const double& s);
  doubleMatrix&
		operator  =(const doubleSubMatrix& N);
  doubleMatrix&
		operator  =(const doubleMatrix& N);
  };

// Constructors
inline		doubleMatrix::doubleMatrix(void):
  doubleSubMatrix() { }
inline		doubleMatrix::doubleMatrix(
    Extent m, Extent n): doubleSubMatrix(
    allocate(m, n), (Offset)0, m, (Stride)n, n,  (Stride)1) {
#ifdef SVMT_DEBUG_MODE
  doubleSubMatrix::operator =(doubleBad);
#endif // SVMT_DEBUG_MODE
  }
inline		doubleMatrix::doubleMatrix(
    Extent m, Extent n, const double& s): doubleSubMatrix(
    allocate(m, n), (Offset)0, m, (Stride)n, n, (Stride)1) {
  doubleSubMatrix::operator  =(s); }
inline		doubleMatrix::doubleMatrix(
    Extent m, Extent n, const double& s, const double& t): doubleSubMatrix(
    allocate(m, n), (Offset)0, m, (Stride)n, n, (Stride)1) {
  ramp();
  doubleSubMatrix::operator *=(t);
  doubleSubMatrix::operator +=(s); }
inline		doubleMatrix::doubleMatrix(
    const doubleSubMatrix& M):
  doubleSubMatrix(allocate(M.extent2(), M.extent1()), (Offset)0,
    M.extent2(), (Stride)M.extent1(), M.extent1(), (Stride)1) {
  doubleSubMatrix::operator  =(M); }
inline		doubleMatrix::doubleMatrix(
    const doubleMatrix& M):
  doubleSubMatrix(allocate(M.extent2(), M.extent1()), (Offset)0,
    M.extent2(), (Stride)M.extent1(), M.extent1(), (Stride)1) {
  doubleSubMatrix::operator  =(M); }
inline		doubleMatrix::~doubleMatrix(void) { free(); }

// Assignment Operators
inline
doubleMatrix&	doubleMatrix::operator  =(const double& s) {
  doubleSubMatrix::operator =(s); return *this; }
inline
doubleMatrix&	doubleMatrix::operator  =(
    const doubleSubMatrix& N) {
  doubleSubMatrix::operator =(N); return *this; }
inline
doubleMatrix&	doubleMatrix::operator  =(
    const doubleMatrix& N) {
  doubleSubMatrix::operator =(N); return *this; }

// Functions
inline
doubleMatrix&
		doubleMatrix::resize(void) { free();
  doubleSubMatrix::resize(); return *this; }
inline
doubleMatrix&
		doubleMatrix::resize(Extent m, Extent n) { free();
  doubleSubMatrix::resize(
    allocate(m, n), (Offset)0, m, (Stride)n, n, (Stride)1);
#ifdef SVMT_DEBUG_MODE
  *this = doubleBad;
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
doubleMatrix&
		doubleMatrix::resize_(Extent n, Extent m) {
  return resize(m, n); }
inline
doubleMatrix&
		doubleMatrix::resize(Extent m, Extent n,
    const double& s) { return resize(m, n) = s; }
inline
doubleMatrix&
		doubleMatrix::resize_(Extent n, Extent m,
    const double& s) { return resize(m, n, s); }
inline
doubleMatrix&
		doubleMatrix::resize(Extent m, Extent n,
    const double& s, const double& t) {
  resize(m, n); ramp();
  doubleSubMatrix::operator *=(t);
  doubleSubMatrix::operator +=(s);
  return *this;
  }
inline
doubleMatrix&
		doubleMatrix::resize_(Extent n, Extent m,
    const double& s, const double& t) { return resize(m, n, s, t); }
inline
doubleMatrix&
		doubleMatrix::resize(
    const doubleSubMatrix& M) {
  return resize(M.extent2(), M.extent1()) = M; }

inline
const
doubleMatrix
		doubleSubVector::dot_(
    const doubleSubMatrix& M) const { return M.dot(*this); }
inline
const
doubleMatrix
		doubleSubMatrix::dot_(
    const doubleSubMatrix& M) const { return M.dot(*this); }
inline
const
doubleVector
		doubleSubMatrix::dot_(
    const doubleSubVector& v) const { return v.dot(*this); }

inline
const
doubleMatrix	doubleSubMatrix::pl_(
    const offsetSubVector& p, const doubleSubMatrix& L) const {
  return up(L, p); }
inline
const
doubleMatrix	doubleSubMatrix::l_(
    const doubleSubMatrix& L) const { return u(L); }
inline
const
doubleMatrix	doubleSubMatrix::pld_(
    const offsetSubVector& p, const doubleSubMatrix& LD) const {
  return dup(LD, p); }
inline
const
doubleMatrix	doubleSubMatrix::ld_(
    const doubleSubMatrix& LD) const { return du(LD); }
inline
const
doubleMatrix	doubleSubMatrix::u_(
    const doubleSubMatrix& U) const { return l(U); }
inline
const
doubleMatrix	doubleSubMatrix::up_(
    const doubleSubMatrix& U, const offsetSubVector& p) const {
  return pl(p, U); }
inline
const
doubleMatrix	doubleSubMatrix::du_(
    const doubleSubMatrix& DU) const { return ld(DU); }
inline
const
doubleMatrix	doubleSubMatrix::dup_(
    const doubleSubMatrix& DU, const offsetSubVector& p) const {
  return pld(p, DU); }

inline
const
doubleMatrix
		doubleSubVector::above(
    const doubleSubVector& w) const {
  return submatrix().above(w.submatrix()); }
inline
const
doubleMatrix
		doubleSubVector::above(
    const doubleSubMatrix& M) const { return submatrix().above(M); }
inline
const
doubleMatrix
		doubleSubMatrix::above(
    const doubleSubVector& v) const { return above(v.submatrix()); }
inline
const
doubleMatrix
		doubleSubVector::aside_(
    const doubleSubVector& w) const { return above(w); }
inline
const
doubleMatrix
		doubleSubVector::aside_(
    const doubleSubMatrix& M) const { return above(M); }
inline
const
doubleMatrix
		doubleSubMatrix::above_(
    const doubleSubMatrix& M) const { return aside(M); }
inline
const
doubleMatrix
		doubleSubMatrix::aside_(
    const doubleSubMatrix& M) const { return above(M); }
inline
const
doubleMatrix
		doubleSubMatrix::aside_(
    const doubleSubVector& v) const { return above(v); }

inline
const
doubleMatrix
		doubleSubMatrix::operator *(const double& s) const {
  return operator *(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleMatrix
		doubleSubMatrix::operator /(const double& s) const {
  return operator /(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleMatrix
		doubleSubMatrix::operator +(const double& s) const {
  return operator +(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleMatrix
		doubleSubMatrix::operator -(const double& s) const {
  return operator -(doubleSubArray2((double*)&s, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
doubleMatrix
		operator *(const double& s, const doubleSubMatrix& M) {
  return doubleSubArray2((double*)&s, (Offset)0,
    M.extent2(), (Stride)0, M.extent1(), (Stride)0)*M; }
inline
const
doubleMatrix
		operator /(const double& s, const doubleSubMatrix& M) {
  return doubleSubArray2((double*)&s, (Offset)0,
    M.extent2(), (Stride)0, M.extent1(), (Stride)0)/M; }
inline
const
doubleMatrix
		operator +(const double& s, const doubleSubMatrix& M) {
  return M + doubleSubArray2((double*)&s, (Offset)0,
    M.extent2(), (Stride)0, M.extent1(), (Stride)0); }
inline
const
doubleMatrix
		operator -(const double& s, const doubleSubMatrix& M) {
  return doubleSubArray2((double*)&s, (Offset)0,
    M.extent2(), (Stride)0, M.extent1(), (Stride)0) - M; }



#endif /* _doubleMatrix_h */

