#ifndef doubleComplexArray3_h
#define doubleComplexArray3_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleComplexTensor.h>


template<Extent l, Extent m = 1, Extent n = 1>
class doubleComplexArray3: public doubleComplexSubArray3 {
private:
  // Representation
  double	A[l*m*n*2];
  // Functions			// prevent doubleComplexSubArray3 resize
  doubleComplexArray3<l, m, n>&
		resize(void);
  doubleComplexArray3<l, m, n>&
		resize(double*, Offset, Extent, Stride, Extent, Stride, Extent, Stride);
  doubleComplexArray3<l, m, n>&
		resize_(double*, Offset, Extent, Stride, Extent, Stride, Extent, Stride);
  doubleComplexArray3<l, m, n>&
		resize(const doubleComplexSubArray3&);
public:
  // Constructors
		doubleComplexArray3(void):
    doubleComplexSubArray3(A, 0, l, m*n, m, n, n, 1) { }
		doubleComplexArray3(const doubleComplex& s):
    doubleComplexSubArray3(A, 0, l, m*n, m, n, n, 1) {
    doubleComplexSubArray3::operator =(s); }

		doubleComplexArray3(const doubleComplexArray3<l, m, n>& T):
    doubleComplexSubArray3(A, 0, l, m*n, m, n, n, 1) {
    doubleComplexSubArray3::operator =(T); }
	       ~doubleComplexArray3(void) { }
  // Operators
  doubleComplexArray3<l, m, n>&
  operator =(const doubleComplexArray3<l, m, n>& T) {
    doubleComplexSubArray3::operator =(T);
    return *this; }
  };

#endif /* doubleComplexArray3_h */

