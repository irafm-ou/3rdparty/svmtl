#ifndef _doubleMirror_h
#define _doubleMirror_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 2000 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleHandle.h>

class doubleMirror: doubleHandle {
  // Constructors
  private:
		doubleMirror(double* p);
  public:
		doubleMirror(const doubleMirror& h);
		~doubleMirror(void);
  private:
  doubleMirror&
		operator  =(const doubleMirror& h);
  public:
  // Functions
  static
  int	     places(void);
  double		get(Offset o) const;
  doubleMirror&
		put(Offset o, const double& s);
  friend class doubleSubVector;	friend class doubleVector;
  friend class doubleSubMatrix;	friend class doubleMatrix;
  friend class doubleSubTensor;	friend class doubleTensor;
  };

// Functions
inline
static
int		doubleMirror::places(void) { return 16; }
// Constructors
inline		doubleMirror::doubleMirror(double* p):
  doubleHandle(p) { }
inline		doubleMirror::doubleMirror(
    const doubleMirror& m): doubleHandle(m) { }
inline		doubleMirror::~doubleMirror(void) { }
// Operators
inline
doubleMirror&
		doubleMirror::operator  =(
  const doubleMirror& m) {
  doubleHandle::operator  =(m); return *this; }

// Functions
inline
double		doubleMirror::get(Offset o) const {
  doubleHandle&	h = *this;
  Offset	i = o >> places();
  Offset	j = o - (i << places());
  if (i < j) {			// swap i and j
    Offset	t = i; i = j; j = t; }
  return double<Handle>::get((i*(1 + i) >> 1) + j); }

inline
doubleMirror&
		doubleMirror::put(Offset o, const double& s) {
  doubleHandle&	h = *this;
  Offset	i = o >> places();
  Offset	j = o - (i << places());
  if (i < j) {			// swap i and j
    Offset	t = i; i = j; j = t; }
  double<Handle>::put((i*(1 + i) >> 1) + j, s);
  return *this; }

#endif /* _doubleMirror_h */

