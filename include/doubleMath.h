#ifndef doubleMath_h
#define doubleMath_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<iostream>
#include<iomanip>
#include<cmath>
#ifndef __USE_MISC
extern "C" {
  double	hypot(double, double);
  double	acosh(double);
  double	asinh(double);
  double	atanh(double);
  double	   j0(double);
  double	   j1(double);
  double	   jn(int, double);
  double	   y0(double);
  double	   y1(double);
  double	   yn(int, double);
  };
#endif
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
// inline
// double	abs(double x) { return (x < (double)0)? -x:  x; }
inline
double	sgn(double x) { return (x < (double)0)? -(double)1:
			       ((double)0 < x)? +(double)1: (double)0; }

#endif /* doubleMath_h */

