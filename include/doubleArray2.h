#ifndef _doubleArray2_h
#define _doubleArray2_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<doubleMatrix.h>


template<Extent m, Extent n = 1>
class doubleArray2: public doubleSubArray2 {
private:
  // Representation
  double	A[m*n];
  // Functions			// prevent doubleSubArray2 resize
  doubleArray2<m, n>&
		resize(void);
  doubleArray2<m, n>&
		resize(double*, Offset, Extent, Stride, Extent, Stride);
  doubleArray2<m, n>&
		resize_(double*, Offset, Extent, Stride, Extent, Stride);
  doubleArray2<m, n>&
		resize(const doubleSubArray2&);
public:
  // Constructors
		doubleArray2(void):
    doubleSubArray2(A, 0, m, n, n, 1) { }
		doubleArray2(const double& s):
    doubleSubArray2(A, 0, m, n, n, 1) {
    doubleSubArray2::operator =(s); }
		doubleArray2(const double& s, const double& t):
    doubleSubArray2(A, 0, m, n, n, 1) {
    ramp();
    doubleSubArray2::operator*=(t);
    doubleSubArray2::operator+=(s); }
		doubleArray2(const doubleArray2<m, n>& T):
    doubleSubArray2(A, 0, m, n, n, 1) {
    doubleSubArray2::operator =(T); }
	       ~doubleArray2(void) { }
  // Operators
  doubleArray2<m, n>&
  operator =(const doubleArray2<m, n>& T) {
    doubleSubArray2::operator =(T);
    return *this; }
  };

#endif /* _doubleArray2_h */

