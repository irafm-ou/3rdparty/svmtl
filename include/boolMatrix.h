#ifndef _boolMatrix_h
#define _boolMatrix_h 1
/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<boolVector.h>

class boolSubMatrix {
  private:
  // Representation
  boolHandle
		H;
  Offset	O;
  Extent	N1;
  Stride	S1;
  Extent	N2;
  Stride	S2;
  public:
  // Constructors
		boolSubMatrix(void);
		boolSubMatrix(const boolHandle& h,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
		boolSubMatrix(const boolSubMatrix& M);
		~boolSubMatrix(void);
  // Functions
  const
  boolHandle&	handle(void) const;
  Offset	offset(void) const;
  Extent	extent1(void) const;
  Extent	length1(void) const;
  Stride	stride1(void) const;
  Extent	extent2(void) const;
  Extent	length2(void) const;
  Stride	stride2(void) const;
  bool		 empty(void) const;
  static
  boolHandle allocate(Extent m, Extent n);
  static
  boolHandle allocate_(Extent n, Extent m);
  boolSubMatrix&
		  free(void);
  boolSubMatrix&
		resize(void);
  boolSubMatrix&
		resize(const boolHandle& h,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
  boolSubMatrix&
		resize(const boolSubMatrix& N);
  boolSubMatrix&
		resize_(const boolHandle& h,
      Offset o, Extent n1, Stride s1,
		Extent n2, Stride s2);
  static
  bool	      contains(Offset i, Extent n2, Stride s2, Extent l2,
		       Offset j, Extent n1, Stride s1, Extent l1);
  bool	      contains(Offset i, Extent n2, Stride s2) const;
  bool	      contains(Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  static
  bool	      contains_(Offset j, Extent n1, Stride s1, Extent l1,
			Offset i, Extent n2, Stride s2, Extent l2);
  bool	      contains_(Offset i, Extent n2, Stride s2) const;
  bool	      contains_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2) const;

  boolSubMatrix	   sub(Offset i, Extent n2, Stride s2);
  const
  boolSubMatrix	   sub(Offset i, Extent n2, Stride s2) const;
  boolSubMatrix	   sub(Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1);
  const
  boolSubMatrix	   sub(Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  boolSubMatrix	   sub_(Offset i, Extent n2, Stride s2);
  const
  boolSubMatrix	   sub_(Offset i, Extent n2, Stride s2) const;
  boolSubMatrix	   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2);
  const
  boolSubMatrix	   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2) const;
  const
  boolSubTensor
	     subtensor(Extent l = 1) const;
  boolSubMatrix	     r1(void);
  const
  boolSubMatrix	     r1(void) const;
  boolSubMatrix&
	       reverse1(void);
  boolSubMatrix	     r2(void);
  const
  boolSubMatrix	     r2(void) const;
  boolSubMatrix&
	       reverse2(void);
  boolSubMatrix	     r(void);
  const
  boolSubMatrix	     r(void) const;
  boolSubMatrix&
	       reverse(void);
  boolSubMatrix	  even(void);
  const
  boolSubMatrix	  even(void) const;
  boolSubMatrix	   odd(void);
  const
  boolSubMatrix	   odd(void) const;
  boolSubMatrix	  even_(void);
  const
  boolSubMatrix	  even_(void) const;
  boolSubMatrix	   odd_(void);
  const
  boolSubMatrix	   odd_(void) const;
  boolSubMatrix	     t(void);
  const
  boolSubMatrix	     t(void) const;

  boolSubVector	  diag(void);
  const
  boolSubVector	  diag(void) const;
  boolSubMatrix&  swap(Offset i, Offset k);
  boolSubMatrix&  swap(boolSubMatrix& N);
  boolSubMatrix&  swap_(Offset i, Offset k);
  boolSubMatrix&
		rotate(Stride n);
  boolSubMatrix&
		 shift(Stride n, bool b = false);
  boolSubMatrix&
		  pack(bool b);
  boolSubMatrix&
		  pack(const boolSubMatrix& N);
  const
  boolMatrix	    eq(bool b) const;
  const
  boolMatrix	    ne(bool b) const;
  const
  boolMatrix	    eq(const boolSubMatrix& N) const;
  const
  boolMatrix	    ne(const boolSubMatrix& N) const;
  Extent	 zeros(void) const;
  const
  offsetVector	 index(void) const;
  const
  boolMatrix	 aside(const boolSubMatrix& N) const;
  const
  boolMatrix	 above(const boolSubMatrix& N) const;
  const
  boolMatrix	 above(const boolSubVector& v) const;
  const
  boolTensor	 afore(const boolSubTensor& T) const;
  const
  boolMatrix	 above_(const boolSubMatrix& N) const;
  const
  boolMatrix	 aside_(const boolSubMatrix& N) const;
  const
  boolMatrix	 aside_(const boolSubVector& v) const;
  const
  boolMatrix	 apply(const bool (*f)(const bool&)) const;
  const
  boolMatrix	 apply(      bool (*f)(const bool&)) const;
  const
  boolMatrix	 apply(      bool (*f)(      bool )) const;

  // Operators
  boolSubVector	operator [](Offset i);
  const
  boolSubVector	operator [](Offset i) const;
  boolSubVector	operator ()(Offset i);
  const
  boolSubVector	operator ()(Offset i) const;
  boolSubScalar	operator ()(Offset j, Offset i);
  const
  boolSubScalar	operator ()(Offset j, Offset i) const;
  const
  boolMatrix	operator !(void) const;
  bool		operator ==(bool b) const;
  bool		operator !=(bool b) const;
  bool		operator ==(const boolSubMatrix& N) const;
  bool		operator !=(const boolSubMatrix& N) const;
  const
  boolMatrix	operator &&(bool b) const;
  const
  boolMatrix	operator ||(bool b) const;
  const
  boolMatrix	operator &&(const boolSubMatrix& N) const;
  const
  boolMatrix	operator ||(const boolSubMatrix& N) const;
  boolSubMatrix&
		operator  =(bool b);
  boolSubMatrix&
		operator  =(const boolSubMatrix& N);
  friend class boolMatrix;
  };

// Constructors
inline		boolSubMatrix::boolSubMatrix(void):
  H((bool*)0), O((Offset)0),
  N1((Extent)0), S1((Stride)0),
  N2((Extent)0), S2((Stride)0) { }
inline		boolSubMatrix::boolSubMatrix(const boolHandle& h, Offset o,
    Extent n2, Stride s2, Extent n1, Stride s1):
  H(h), O(o), N1(n1), S1(s1), N2(n2), S2(s2) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("boolSubMatrix::boolSubMatrix(\n"
    "const boolHandle&, Offset, Extent, Stride, Extent, Stride)",
    o, n2, s2, n1, s1);
#endif // SVMT_DEBUG_MODE
  }
inline		boolSubMatrix::boolSubMatrix(const boolSubMatrix& M):
  H(M.H), O(M.O), N1(M.N1), S1(M.S1), N2(M.N2), S2(M.S2) { }
inline		boolSubMatrix::~boolSubMatrix(void) { }

// Functions
inline
const
boolHandle&	boolSubMatrix::handle(void) const { return H; }
inline
Offset		boolSubMatrix::offset(void) const { return O; }
inline
Extent		boolSubMatrix::extent1(void) const { return N1; }
inline
Extent		boolSubMatrix::length1(void) const { return N1; }
inline
Stride		boolSubMatrix::stride1(void) const { return S1; }
inline
Extent		boolSubMatrix::extent2(void) const { return N2; }
inline
Extent		boolSubMatrix::length2(void) const { return N2; }
inline
Stride		boolSubMatrix::stride2(void) const { return S2; }
inline
bool		boolSubMatrix::empty(void) const {
  return handle().empty()||0 == extent1()||0 == extent2(); }
inline
boolHandle	boolSubMatrix::allocate(Extent m, Extent n) {
  return boolHandle(new bool[m*n]); }
inline
boolHandle	boolSubMatrix::allocate_(Extent n, Extent m) {
  return allocate(m, n); }
inline
boolSubMatrix&	boolSubMatrix::free(void) {
  delete [] (bool*)H; return *this; }
inline
boolSubMatrix&	boolSubMatrix::resize(void) {
  H = boolHandle((bool*)0); O = (Offset)0;
  N1 = (Extent)0; S1 = (Stride)0;
  N2 = (Extent)0; S2 = (Stride)0; return *this; }
inline
boolSubMatrix&	boolSubMatrix::resize(const boolHandle& h, Offset o,
    Extent n2, Stride s2, Extent n1 , Stride s1) {
  H = h; O = o; N1 = n1; S1 = s1; N2 = n2; S2 = s2;
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("boolSubMatrix::resize(\n"
    "const boolHandle&, Offset, Extent, Stride, Extent, Stride)",
    o, n2, s2, n1, s1);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
boolSubMatrix&	boolSubMatrix::resize(const boolSubMatrix& N) {
  return resize(N.handle(), N.offset(),
    N.extent2(), N.stride2(),
    N.extent1(), N.stride1()); }
inline
boolSubMatrix&	boolSubMatrix::resize_(const boolHandle& h, Offset o,
    Extent n1, Stride s1, Extent n2 , Stride s2) {
  return resize(h, o, n2, s2, n1, s1); }

inline
bool		boolSubMatrix::contains(
    Offset i, Extent n2, Stride s2, Extent l2,
    Offset j, Extent n1, Stride s1, Extent l1) {
  return boolSubVector::contains(i, n2, s2, l2)
      && boolSubVector::contains(j, n1, s1, l1); }
inline
bool		boolSubMatrix::contains(
    Offset i, Extent n2, Stride s2) const {
  return boolSubVector::contains(i, n2, s2, extent2()); }
inline
bool		boolSubMatrix::contains(
    Offset i, Extent n2, Stride s2,
    Offset j, Extent n1, Stride s1) const {
  return boolSubMatrix::contains(i, n2, s2, extent2(),
				 j, n1, s1, extent1()); }
inline
bool		boolSubMatrix::contains_(
    Offset j, Extent n1, Stride s1, Extent l1,
    Offset i, Extent n2, Stride s2, Extent l2) {
  return contains(i-1, n2, s2, l2, j-1, n1, s1, l1); }
inline
bool		boolSubMatrix::contains_(
    Offset i, Extent n2, Stride s2) const {
  return contains(i-1, n2, s2); }
inline
bool		boolSubMatrix::contains_(
    Offset j, Extent n1, Stride s1,
    Offset i, Extent n2, Stride s2) const {
  return contains(i-1, n2, s2, j-1, n1, s1); }

inline
boolSubMatrix	boolSubMatrix::sub(Offset i, Extent n2, Stride s2) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "boolSubMatrix::sub(Offset, Extent, Stride)",
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return boolSubMatrix(handle(),
    offset() + (Stride)i*stride2(),
    n2, s2*stride2(), extent1(), stride1()); }
inline
const
boolSubMatrix	boolSubMatrix::sub(Offset i, Extent n2, Stride s2) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "boolSubMatrix::sub(Offset, Extent, Stride) const",
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return boolSubMatrix(handle(),
    offset() + (Stride)i*stride2(),
    n2, s2*stride2(), extent1(), stride1()); }
inline
boolSubMatrix	boolSubMatrix::sub(Offset i, Extent n2, Stride s2,
				   Offset j, Extent n1, Stride s1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("boolSubMatrix::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride)",
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return boolSubMatrix(handle(),
    offset() + (Stride)i*stride2() + (Stride)j*stride1(),
    n2, s2*stride2(), n1, s1*stride1()); }
inline
const
boolSubMatrix	boolSubMatrix::sub(Offset i, Extent n2, Stride s2,
				   Offset j, Extent n1, Stride s1) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("boolSubMatrix::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride) const",
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return boolSubMatrix(handle(),
    offset() + (Stride)i*stride2() + (Stride)j*stride1(),
    n2, s2*stride2(), n1, s1*stride1()); }
inline
boolSubMatrix	boolSubMatrix::sub_(Offset i, Extent n2, Stride s2) {
  return sub(i-1, n2, s2); }
inline
const
boolSubMatrix	boolSubMatrix::sub_(Offset i, Extent n2, Stride s2) const {
  return sub(i-1, n2, s2); }
inline
boolSubMatrix	boolSubMatrix::sub_(Offset j, Extent n1, Stride s1,
				    Offset i, Extent n2, Stride s2) {
  return sub(i-1, n2, s2, j-1, n1, s1); }
inline
const
boolSubMatrix	boolSubMatrix::sub_(Offset j, Extent n1, Stride s1,
				    Offset i, Extent n2, Stride s2) const {
  return sub(i-1, n2, s2, j-1, n1, s1); }

inline
const
boolSubMatrix	boolSubScalar::submatrix(Extent n, Extent m) const {
  return boolSubMatrix(handle(), offset(), m, (Stride)0, n, (Stride)0); }
inline
const
boolSubMatrix	boolSubVector::submatrix(Extent m) const {
  return boolSubMatrix(handle(), offset(), m, (Stride)0, extent(), stride()); }
inline
boolSubMatrix	boolSubMatrix::r1(void) {
  return boolSubMatrix(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
const
boolSubMatrix	boolSubMatrix::r1(void) const {
  return boolSubMatrix(handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
boolSubMatrix	boolSubMatrix::r2(void) {
  return boolSubMatrix(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
const
boolSubMatrix	boolSubMatrix::r2(void) const {
  return boolSubMatrix(handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
boolSubMatrix	boolSubMatrix::r(void) {
  return boolSubMatrix(handle(),
    offset() + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
const
boolSubMatrix	boolSubMatrix::r(void) const {
  return boolSubMatrix(handle(),
    offset() + (Stride)(extent2() - 1)*stride2()
	     + (Stride)(extent1() - 1)*stride1(),
    extent2(), -stride2(),
    extent1(), -stride1()); }
inline
boolSubMatrix&	boolSubMatrix::reverse(void) {
  return reverse1().reverse2(); }

inline
boolSubMatrix	boolSubMatrix::even(void) {
  return boolSubMatrix(handle(), offset(),
    extent2(), stride2(), (extent1() + 1) >> 1, stride1() << 1); }
inline
const
boolSubMatrix	boolSubMatrix::even(void) const {
  return boolSubMatrix(handle(), offset(),
    extent2(), stride2(), (extent1() + 1) >> 1, stride1() << 1); }
inline
boolSubMatrix	boolSubMatrix::odd(void) {
  return boolSubMatrix(handle(), offset() + stride1(),
    extent2(), stride2(), extent1() >> 1, stride1() << 1); }
inline
const
boolSubMatrix	boolSubMatrix::odd(void) const {
  return boolSubMatrix(handle(), offset() + stride1(),
    extent2(), stride2(), extent1() >> 1, stride1() << 1); }
inline
boolSubMatrix	boolSubMatrix::even_(void) { return odd(); }
inline
const
boolSubMatrix	boolSubMatrix::even_(void) const { return odd(); }
inline
boolSubMatrix	boolSubMatrix::odd_(void) { return even(); }
inline
const
boolSubMatrix	boolSubMatrix::odd_(void) const { return even(); }
inline
boolSubMatrix	boolSubMatrix::t(void) {
  return boolSubMatrix(handle(), offset(),
    extent1(), stride1(), extent2(), stride2()); }
inline
const
boolSubMatrix	boolSubMatrix::t(void) const {
  return boolSubMatrix(handle(), offset(),
    extent1(), stride1(), extent2(), stride2()); }
inline
boolSubVector	boolSubMatrix::diag(void) {
  return boolSubVector(handle(), offset(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
const
boolSubVector	boolSubMatrix::diag(void) const {
  return boolSubVector(handle(), offset(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }

// Operators
inline
boolSubVector	boolSubMatrix::operator [](Offset i) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "boolSubMatrix::operator [](Offset)", i, extent2());
#endif // SVMT_DEBUG_MODE
  return boolSubVector(handle(), offset() + (Stride)i*stride2(),
    extent1(), stride1()); }
inline
const
boolSubVector	boolSubMatrix::operator [](Offset i) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_index_range(
    "boolSubMatrix::operator [](Offset) const", i, extent2());
#endif // SVMT_DEBUG_MODE
  return boolSubVector(handle(), offset() + (Stride)i*stride2(),
    extent1(), stride1()); }
inline
boolSubVector	boolSubMatrix::operator ()(Offset i) {
  return operator [](i-1); }
inline
const
boolSubVector	boolSubMatrix::operator ()(Offset i) const {
  return operator [](i-1); }
inline
boolSubScalar	boolSubMatrix::operator ()(Offset j, Offset i) {
  boolSubMatrix&	M = *this; return M[i-1][j-1]; }
inline
const
boolSubScalar	boolSubMatrix::operator ()(Offset j, Offset i) const {
  const
  boolSubMatrix&	M = *this; return M[i-1][j-1]; }

std::istream&	operator >>(std::istream& s,       boolSubMatrix& M);
std::ostream&	operator <<(std::ostream& s, const boolSubMatrix& M);

inline
boolSubMatrix&	boolSubMatrix::swap(Offset i, Offset k) {
  boolSubMatrix		M = *this;
  boolSubVector		v = M[i];
  boolSubVector		w = M[k];
  v.swap(w); return *this; }
inline
boolSubMatrix&	boolSubMatrix::swap_(Offset i, Offset k) {
  return swap(i-1, k-1); }

class boolSubArray2: public boolSubMatrix {
  public:
  // Constructors
		boolSubArray2(void);
		boolSubArray2(bool* p,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
		boolSubArray2(const boolSubArray2& M);
		~boolSubArray2(void);
  // Functions
  boolSubArray2&
		resize(void);
  boolSubArray2&			// resize from pointer
		resize(bool* p,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
  boolSubArray2&			// resize from SubArray2
		resize(const boolSubArray2& M);
  boolSubArray2&			// resize from pointer
		resize_(bool* p,
      Offset o, Extent n1, Stride s1,
		Extent n2, Stride s2);
  private:
  boolSubArray2&			// prevent resize from Handle
		resize(const boolHandle& h,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
  boolSubArray2&			// prevent resize from SubMatrix
		resize(const boolSubMatrix& N);
  public:
  boolSubArray2	   sub(Offset i, Extent n2, Stride s2);
  const
  boolSubArray2	   sub(Offset i, Extent n2, Stride s2) const;
  boolSubArray2	   sub(Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1);
  const
  boolSubArray2	   sub(Offset i, Extent n2, Stride s2,
		       Offset j, Extent n1, Stride s1) const;
  boolSubArray2	   sub_(Offset i, Extent n2, Stride s2);
  const
  boolSubArray2	   sub_(Offset i, Extent n2, Stride s2) const;
  boolSubArray2	   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2);
  const
  boolSubArray2	   sub_(Offset j, Extent n1, Stride s1,
			Offset i, Extent n2, Stride s2) const;

  const
  boolSubArray3
	     subtensor(Extent l = 1) const;
  boolSubArray2	     r1(void);
  const
  boolSubArray2	     r1(void) const;
  boolSubArray2&
	       reverse1(void);
  boolSubArray2	     r2(void);
  const
  boolSubArray2	     r2(void) const;
  boolSubArray2&
	       reverse2(void);
  boolSubArray2	     r(void);
  const
  boolSubArray2	     r(void) const;
  boolSubArray2&
	       reverse(void);
  boolSubArray2	  even(void);
  const
  boolSubArray2	  even(void) const;
  boolSubArray2	   odd(void);
  const
  boolSubArray2	   odd(void) const;
  boolSubArray2	  even_(void);
  const
  boolSubArray2	  even_(void) const;
  boolSubArray2	   odd_(void);
  const
  boolSubArray2	   odd_(void) const;
  boolSubArray2	     t(void);
  const
  boolSubArray2	     t(void) const;
  boolSubArray1	  diag(void);
  const
  boolSubArray1	  diag(void) const;
  boolSubArray2&  swap(Offset i, Offset k);
  boolSubArray2&  swap(boolSubMatrix& N);
  boolSubArray2&  swap_(Offset i, Offset k);
  boolSubArray2&
		rotate(Stride n);
  boolSubArray2&
		 shift(Stride n, bool b = false);
  boolSubArray2&
		  pack(bool b);
  boolSubArray2&
		  pack(const boolSubMatrix& N);
  // Operators
  boolSubArray2&
		operator  =(bool b);
  boolSubArray2&
		operator  =(const boolSubMatrix& N);
  };

// Constructors
inline		boolSubArray2::boolSubArray2(void): boolSubMatrix() { }
inline		boolSubArray2::boolSubArray2(bool* p,
    Offset o, Extent n2, Stride s2,
	      Extent n1, Stride s1):
  boolSubMatrix(boolHandle(p), o, n2, s2, n1, s1) { }
inline		boolSubArray2::boolSubArray2(const boolSubArray2& M):
  boolSubMatrix(M) { }
inline		boolSubArray2::~boolSubArray2(void) { }

// Functions
inline
boolSubArray2&	boolSubArray2::resize(void) {
  boolSubMatrix::resize(); return *this; }
inline
boolSubArray2&	boolSubArray2::resize(bool* p,
    Offset o, Extent n2, Stride s2,
	      Extent n1, Stride s1) {
  boolSubMatrix::resize(boolHandle(p), o, n2, s2, n1, s1); return *this; }
inline
boolSubArray2&	boolSubArray2::resize(const boolSubArray2& N) {
  boolSubMatrix::resize(N); return *this; }
inline
boolSubArray2&	boolSubArray2::resize_(bool* p,
    Offset o, Extent n1, Stride s1,
	      Extent n2, Stride s2) { return resize(p, o, n2, s2, n1, s1); }

inline
boolSubArray2	boolSubArray2::sub(Offset i, Extent n2, Stride s2) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "boolSubArray2::sub(Offset, Extent, Stride)",
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return boolSubArray2((bool*)(boolHandle&)handle(),
    offset() + (Stride)i*stride2(),
    n2, s2*stride2(), extent1(), stride1()); }
inline
const
boolSubArray2	boolSubArray2::sub(Offset i, Extent n2, Stride s2) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_vector_containment(
    "boolSubArray2::sub(Offset, Extent, Stride) const",
    i, n2, s2, extent2());
#endif // SVMT_DEBUG_MODE
  return boolSubArray2((bool*)(boolHandle&)handle(),
    offset() + (Stride)i*stride2(),
    n2, s2*stride2(), extent1(), stride1()); }
inline
boolSubArray2	boolSubArray2::sub(Offset i, Extent n2, Stride s2,
				   Offset j, Extent n1, Stride s1) {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("boolSubArray2::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride)",
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return boolSubArray2((bool*)(boolHandle&)handle(),
    offset() + (Stride)i*stride2() + (Stride)j*stride1(),
    n2, s2*stride2(), n1, s1*stride1()); }
inline
const
boolSubArray2	boolSubArray2::sub(Offset i, Extent n2, Stride s2,
				   Offset j, Extent n1, Stride s1) const {
#ifdef SVMT_DEBUG_MODE
  svmt_check_matrix_containment("boolSubArray2::sub(\n"
    "Offset, Extent, Stride, Offset, Extent, Stride) const",
    i, n2, s2, extent2(),
    j, n1, s1, extent1());
#endif // SVMT_DEBUG_MODE
  return boolSubArray2((bool*)(boolHandle&)handle(),
    offset() + (Stride)i*stride2() + (Stride)j*stride1(),
    n2, s2*stride2(), n1, s1*stride1()); }
inline
boolSubArray2	boolSubArray2::sub_(Offset i, Extent n2, Stride s2) {
  return sub(i-1, n2, s2); }
inline
const
boolSubArray2	boolSubArray2::sub_(Offset i, Extent n2, Stride s2) const {
  return sub(i-1, n2, s2); }
inline
boolSubArray2	boolSubArray2::sub_(Offset j, Extent n1, Stride s1,
				    Offset i, Extent n2, Stride s2) {
  return sub(i-1, n2, s2, j-1, n1, s1); }
inline
const
boolSubArray2	boolSubArray2::sub_(Offset j, Extent n1, Stride s1,
				    Offset i, Extent n2, Stride s2) const {
  return sub(i-1, n2, s2, j-1, n1, s1); }

inline
const
boolSubArray2	boolSubArray0::submatrix(Extent n, Extent m) const {
  return boolSubArray2((bool*)(boolHandle&)handle(), offset(),
    m, (Stride)0, n, (Stride)0); }
inline
const
boolSubArray2	boolSubArray1::submatrix(Extent m) const {
  return boolSubArray2((bool*)(boolHandle&)handle(), offset(),
    m, (Stride)0, extent(), stride()); }
inline
boolSubArray2	boolSubArray2::r1(void) {
  return boolSubArray2((bool*)(boolHandle&)handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
const
boolSubArray2	boolSubArray2::r1(void) const {
  return boolSubArray2((bool*)(boolHandle&)handle(),
    offset() + (Stride)(extent1() - 1)*stride1(),
    extent2(), +stride2(),
    extent1(), -stride1()); }
inline
boolSubArray2&	boolSubArray2::reverse1(void) {
  boolSubMatrix::reverse1(); return *this; }
inline
boolSubArray2	boolSubArray2::r2(void) {
  return boolSubArray2((bool*)(boolHandle&)handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
const
boolSubArray2	boolSubArray2::r2(void) const{
  return boolSubArray2((bool*)(boolHandle&)handle(),
    offset() + (Stride)(extent2() - 1)*stride2(),
    extent2(), -stride2(),
    extent1(), +stride1()); }
inline
boolSubArray2&	boolSubArray2::reverse2(void) {
  boolSubMatrix::reverse2(); return *this; }
inline
boolSubArray2	boolSubArray2::r(void) {
  return boolSubArray2((bool*)(boolHandle&)handle(),
    offset() + (Stride)(extent1() - 1)*stride1()
	     + (Stride)(extent2() - 1)*stride2(),
    extent1(), -stride2(),
    extent1(), -stride1()); }
inline
const
boolSubArray2	boolSubArray2::r(void) const {
  return boolSubArray2((bool*)(boolHandle&)handle(),
    offset() + (Stride)(extent1() - 1)*stride1()
	     + (Stride)(extent2() - 1)*stride2(),
    extent1(), -stride2(),
    extent1(), -stride1()); }
inline
boolSubArray2&	boolSubArray2::reverse(void) {
  boolSubMatrix::reverse(); return *this; }

inline
boolSubArray2	boolSubArray2::even(void) {
  return boolSubArray2((bool*)(boolHandle&)handle(), offset(),
    extent2(), stride2(), (extent1() + 1) >> 1, stride1() << 1); }
inline
const
boolSubArray2	boolSubArray2::even(void) const {
  return boolSubArray2((bool*)(boolHandle&)handle(), offset(),
    extent2(), stride2(), (extent1() + 1) >> 1, stride1() << 1); }
inline
boolSubArray2	boolSubArray2::odd(void) {
  return boolSubArray2((bool*)(boolHandle&)handle(), offset() + stride1(),
    extent2(), stride2(), extent1() >> 1, stride1() << 1); }
inline
const
boolSubArray2	boolSubArray2::odd(void) const {
  return boolSubArray2((bool*)(boolHandle&)handle(), offset() + stride1(),
    extent2(), stride2(), extent1() >> 1, stride1() << 1); }
inline
boolSubArray2	boolSubArray2::even_(void) { return odd(); }
inline
const
boolSubArray2	boolSubArray2::even_(void) const { return odd(); }
inline
boolSubArray2	boolSubArray2::odd_(void) { return even(); }
inline
const
boolSubArray2	boolSubArray2::odd_(void) const { return even(); }
inline
boolSubArray2	boolSubArray2::t(void) {
  return boolSubArray2((bool*)(boolHandle&)handle(), offset(),
    extent1(), stride1(), extent2(), stride2()); }
inline
const
boolSubArray2	boolSubArray2::t(void) const {
  return boolSubArray2((bool*)(boolHandle&)handle(), offset(),
    extent1(), stride1(), extent2(), stride2()); }
inline
boolSubArray1	boolSubArray2::diag(void) {
  return boolSubArray1((bool*)(boolHandle&)handle(), offset(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
const
boolSubArray1	boolSubArray2::diag(void) const {
  return boolSubArray1((bool*)(boolHandle&)handle(), offset(),
    (extent1() < extent2())? extent1(): extent2(), stride2() + stride1()); }
inline
boolSubArray2&	boolSubArray2::swap(Offset i, Offset k) {
  boolSubMatrix::swap(i, k); return *this; }
inline
boolSubArray2&	boolSubArray2::swap(boolSubMatrix& N) {
  boolSubMatrix::swap(N); return *this; }
inline
boolSubArray2&	boolSubArray2::swap_(Offset i, Offset k) {
  return swap(i-1, k-1); }
inline
boolSubArray2&  boolSubArray2::rotate(Stride n) {
  boolSubMatrix::rotate(n); return *this; }
inline
boolSubArray2&  boolSubArray2::shift(Stride n, bool b) {
  boolSubMatrix::shift(n, b); return *this; }
inline
boolSubArray2&  boolSubArray2::pack(bool b) {
  boolSubMatrix::pack(b); return *this; }
inline
boolSubArray2&  boolSubArray2::pack(const boolSubMatrix& N) {
  boolSubMatrix::pack(N); return *this; }

// Operators
inline
boolSubArray2&	boolSubArray2::operator  =(bool b) {
  boolSubMatrix::operator  =(boolSubArray2((bool*)&b, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); return *this; }
inline
boolSubArray2&	boolSubArray2::operator  =(const boolSubMatrix& N) {
  boolSubMatrix::operator  =(N); return *this; }

// boolSubMatrix operator definitions which use boolSubArray2
inline
bool		boolSubMatrix::operator ==(bool b) const {
  return operator ==(boolSubArray2((bool*)&b, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		boolSubMatrix::operator !=(bool b) const {
  return operator !=(boolSubArray2((bool*)&b, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
bool		operator ==(bool b, const boolSubMatrix& M) { return M == b; }
inline
bool		operator !=(bool b, const boolSubMatrix& M) { return M != b; }
boolVector	any(const boolSubMatrix& M);
boolVector	all(const boolSubMatrix& M);
inline
boolSubMatrix&	boolSubMatrix::operator  =(bool b) {
  return operator  =(boolSubArray2((bool*)&b, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }

class boolMatrix: public boolSubMatrix {
  public:
  // Constructors
  		boolMatrix(void);
  explicit	boolMatrix(Extent m, Extent n = 1);
		boolMatrix(Extent m, Extent n, bool b);
		boolMatrix(const boolSubMatrix& M);
		boolMatrix(const boolMatrix& M);
		~boolMatrix(void);
  // Functions
  boolMatrix&	resize(void);
  boolMatrix&	resize(Extent m, Extent n = 1);
  boolMatrix&	resize(Extent m, Extent n, bool b);
  boolMatrix&	resize(const boolSubMatrix& N);
  boolMatrix&	resize_(Extent n, Extent m);
  boolMatrix&	resize_(Extent n, Extent m, bool b);
private:
  boolMatrix&	resize(const boolHandle& h,
      Offset o, Extent n2, Stride s2,
		Extent n1, Stride s1);
  boolMatrix&	free(void) { boolSubMatrix::free(); return *this; }
public:
  // Operators
  boolMatrix&	operator  =(bool b);
  boolMatrix&	operator  =(const boolSubMatrix& N);
  boolMatrix&	operator  =(const boolMatrix& N);
  };

// Constructors
inline		boolMatrix::boolMatrix(void): boolSubMatrix() { }
inline		boolMatrix::boolMatrix(Extent m, Extent n):
  boolSubMatrix(allocate(m, n), (Offset)0,
      m, (Stride)n, n, (Stride)1) {
#ifdef SVMT_DEBUG_MODE
  boolSubMatrix::operator =(true);
#endif // SVMT_DEBUG_MODE
  }
inline		boolMatrix::boolMatrix(Extent m, Extent n, bool b):
  boolSubMatrix(allocate(m, n), (Offset)0,
      m, (Stride)n, n, (Stride)1) { boolSubMatrix::operator =(b); }
inline		boolMatrix::boolMatrix(const boolSubMatrix& M):
  boolSubMatrix(allocate(M.extent2(), M.extent1()), (Offset)0,
      M.extent2(), (Stride)M.extent1(), M.extent1(), (Stride)1) {
  boolSubMatrix::operator =(M); }
inline		boolMatrix::boolMatrix(const boolMatrix& M):
  boolSubMatrix(allocate(M.extent2(), M.extent1()), (Offset)0,
      M.extent2(), (Stride)M.extent1(), M.extent1(), (Stride)1) {
  boolSubMatrix::operator =(M); }
inline		boolMatrix::~boolMatrix(void) { free(); }

// Assignment Operators
inline
boolMatrix&	boolMatrix::operator  =(bool b) {
  boolSubMatrix::operator =(b); return *this; }
inline
boolMatrix&	boolMatrix::operator  =(const boolSubMatrix& N) {
  boolSubMatrix::operator =(N); return *this; }
inline
boolMatrix&	boolMatrix::operator  =(const boolMatrix& N) {
  boolSubMatrix::operator =(N); return *this; }

// Functions
inline
boolMatrix&	boolMatrix::resize(void) { free();
  boolSubMatrix::resize(); return *this; }
inline
boolMatrix&	boolMatrix::resize(Extent m, Extent n) { free();
  boolSubMatrix::resize(allocate(m, n), (Offset)0,
    m, (Stride)n, n, (Stride)1);
#ifdef SVMT_DEBUG_MODE
  boolSubMatrix::operator =(true);
#endif // SVMT_DEBUG_MODE
  return *this; }
inline
boolMatrix&	boolMatrix::resize(Extent m, Extent n, bool b) {
  return resize(m, n) = b; }
inline
boolMatrix&	boolMatrix::resize(const boolSubMatrix& N) {
  return resize(N.extent2(), N.extent1()) = N; }
inline
boolMatrix&	boolMatrix::resize_(Extent n, Extent m) {
  return resize(m, n); }
inline
boolMatrix&	boolMatrix::resize_(Extent n, Extent m, bool b) {
  return resize(m, n, b); }

// boolSubMatrix function definitions which return boolMatrix
inline
const
boolMatrix	boolSubMatrix::eq(bool b) const {
  return eq(boolSubArray2(&b, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolMatrix	boolSubMatrix::ne(bool b) const {
  return ne(boolSubArray2(&b, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolMatrix	boolSubVector::above(const boolSubMatrix& M) const {
  return submatrix().above(M); }
inline
const
boolMatrix	boolSubMatrix::above(const boolSubVector& v) const {
  return above(v.submatrix()); }
inline
const
boolMatrix	boolSubVector::aside_(const boolSubMatrix& M) const {
  return above(M); }
inline
const
boolMatrix	boolSubMatrix::above_(const boolSubMatrix& M) const {
  return aside(M); }
inline
const
boolMatrix	boolSubMatrix::aside_(const boolSubMatrix& M) const {
  return above(M); }
inline
const
boolMatrix	boolSubMatrix::aside_(const boolSubVector& v) const {
  return above(v); }

// boolSubMatrix operator definitions which return boolMatrix
inline
const
boolMatrix	boolSubMatrix::operator &&(bool b) const {
  return operator &&(boolSubArray2((bool*)&b, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolMatrix	boolSubMatrix::operator ||(bool b) const {
  return operator ||(boolSubArray2((bool*)&b, (Offset)0,
      extent2(), (Stride)0, extent1(), (Stride)0)); }
inline
const
boolMatrix	operator &&(bool b, const boolSubMatrix& M) { return M && b; }
inline
const
boolMatrix	operator ||(bool b, const boolSubMatrix& M) { return M || b; }

#endif /* _boolMatrix_h */

