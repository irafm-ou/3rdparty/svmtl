/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<svmt.h>
#include<doubleComplexSignal.h>

int
main(int argc, char* argv[]) {
  using std::ios; using std::cout;

// Output as column vectors for gnuplot.

//cout << setw(12, 1) << doubleBlackman(100, 40.0);
//cout << setw(12, 1) << doubleHamming(100, 40.0);
//cout << setw(12, 1) << doubleHanning(100, 40.0);
  cout << setw(12, 1) << doubleChebyshev(100, 40.0);
//cout << setw(12, 1) << doubleKaiser(100, 4.0);
  return 0;
  }

