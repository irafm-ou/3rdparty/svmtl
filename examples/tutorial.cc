/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<svmt.h>

int
main(int argc, char* argv[]) {
  using std::cout; using std::ios; using std::endl;

  // The svmt.h header file defines integral types Offset, Extent
  // and Stride which can be used to define the number of 

  const Extent		m = 6;		// rows and
  const Extent		n = 7;		// columns

  // used to create an uninitialized m by n double precision real matrix

  doubleMatrix		M(m, n);

  // which can be initialized in a nested for loop

  for (Offset i = 0; i < m; i++) {
    for (Offset j = 0; j < n; j++) {
      M[i][j] = 10*i + j;
      }
    }

  // using the same subscripting operator [] used to reference
  // the elements of an ordinary two dimensional array.
  // The svmt.h header file includes the iomanip.h header file
  // which includes, in turn, the iostream.h header file
  // which defines the standard output stream cout,
  // the standard error stream cerr and the standard input stream cin.
  // The iomanip.h header file also defines function setw(int width)
  // to set the output field width and setw(int width) is used, in turn,
  // to define function setw(int width, int columns)
  // which also specifies the number of columns to display on each line.
  // The simple expression

  cout << "M =\n" << setw(2, n) << M;

  // produces the following display
					/*
M =
 0  1  2  3  4  5  6
10 11 12 13 14 15 16
20 21 22 23 24 25 26
30 31 32 33 34 35 36
40 41 42 43 44 45 46
50 51 52 53 54 55 56
					*/
  // on standard output (usually the display monitor.)
  // The output operator << always inserts a space
  // between each element in a row and always appends
  // a line feed to the end of each row.

  // When the subscripting operator is applied to a matrix

  cout << "M[3] =\n" << setw(2, n) << M[3];
					/*
M[3] =
30 31 32 33 34 35 36
					*/
  // it returns a reference to a row of the matrix.

  // When member function t(void) is applied to a matrix

  cout << "M.t() =\n" << setw(2, n) << M.t();
					/*
M.t() =
 0 10 20 30 40 50
 1 11 21 31 41 51
 2 12 22 32 42 52
 3 13 23 33 43 53
 4 14 24 34 44 54
 5 15 25 35 45 55
 6 16 26 36 46 56
					*/
  // it returns a reference to the transpose of the matrix.

  // When the subscripting operator is applied to the transpose

  cout << "M.t()[3] =\n" << setw(2, n) << M.t()[3];
					/*
M.t()[3] =
 3 13 23 33 43 53
					*/
  // it returns a reference to a column of the matrix.

  // When member function diag(void) is applied to a matrix

  cout << "M.diag() =\n" << setw(2, n) << M.diag();
					/*
M.diag() =
 0 11 22 33 44 55
					*/
  // it returns a reference to the diagonal elements of the matrix.

  // An n by n double precision real matrix

  doubleMatrix	I(n, n, 0.0);

  // may be created with all the elements initialized by appending
  // an optional double precision real scalar value to the argument list.

  // When member function diag(void) is applied to the operand
  // on the left hand side of an assignment operation

  I.diag() = 1.0;
  cout << "I =\n" << setw(2, n) << I;
					/*
I =
 1  0  0  0  0  0  0
 0  1  0  0  0  0  0
 0  0  1  0  0  0  0
 0  0  0  1  0  0  0
 0  0  0  0  1  0  0
 0  0  0  0  0  1  0
 0  0  0  0  0  0  1
					*/
  // only the diagonal elements are assigned a new value.

  // When member function r(void) is applied to a matrix

  cout << "M.r() =\n" << setw(2, n) << M.r();
					/*
M.r() =
56 55 54 53 52 51 50
46 45 44 43 42 41 40
36 35 34 33 32 31 30
26 25 24 23 22 21 20
16 15 14 13 12 11 10
 6  5  4  3  2  1  0
					*/
  // it returns a reference to the rows and columns in reverse order.

  // A new m by n double precision real matrix

  doubleMatrix		W = M.r2();

  cout << "M.r2() = W =\n" << setw(2, n) << W;
					/*
M.r2() = W =
50 51 52 53 54 55 56
40 41 42 43 44 45 46
30 31 32 33 34 35 36
20 21 22 23 24 25 26
10 11 12 13 14 15 16
 0  1  2  3  4  5  6
					*/
  // may be created and initialized from another m by n matrix
  // which is, in this case, a reference to the rows
  // of the original matrix in reverse order.

  // Two or more m by n double precision real matrix objects
  // may appear in almost any expression

  cout << "(M - W)/10 =\n" << setw(3, n) << (M - W)/10;
					/*
(M - W)/10 =
 -5  -5  -5  -5  -5  -5  -5
 -3  -3  -3  -3  -3  -3  -3
 -1  -1  -1  -1  -1  -1  -1
  1   1   1   1   1   1   1
  3   3   3   3   3   3   3
  5   5   5   5   5   5   5
					*/
  // where a double precision real scalar might appear.
  // Unary and binary operations and functions are almost always
  // applied element-by-element to double precision real matrix objects.
  // Either operand of almost all binary operations may be a scalar.

  // When member function
  // sub(Offset i, Extent n2, Stride s2,
  //	 Offset j, Extent n1, Stride s1) is applied to a matrix

  cout << "M.sub(2, 4, 1, 1, 3, 2) =\n" << setw(2, n)
       <<  M.sub(2, 4, 1, 1, 3, 2);
					/*
M.sub(2, 4, 1, 1, 3, 2) =
21 23 25
31 33 35
41 43 45
51 53 55
					*/
  // it returns a reference to a submatrix of the matrix.

  // An uninitialized double precision real vector

  doubleVector		R(2*n-1);

  // of length 2*n-1 may be created then initialized using

  R.ramp() -= n-1;
  cout << "R = " << setw(2, 2*n-1) << R;
					/*
R = -6 -5 -4 -3 -2 -1  0  1  2  3  4  5  6
					*/
  // which uses member function ramp(void) to initialize
  // the elements of the vector from 0 to 2*(n-1)
  // and returns a reference to the vector
  // so that n-1 can be subtracted from each element.

  // The member function handle(void) returns a reference
  // to the one dimensional array of numbers
  // created for an existing double precision real vector
  // and the double precision real submatrix constructor
  // doubleSubMatrix(const doubleHandle& h, Offset o, Extent n2, Stride s2,
  //						      Extent n1, Stride s1)
  // can be used to view that one dimensional array of numbers
  // as a double precision real matrix.

  const
  doubleSubMatrix	T(R.handle(), n-1, n, -1, n, 1);
  cout << "T =\n" << setw(2, n) << T;
					/*
T =
 0  1  2  3  4  5  6
-1  0  1  2  3  4  5
-2 -1  0  1  2  3  4
-3 -2 -1  0  1  2  3
-4 -3 -2 -1  0  1  2
-5 -4 -3 -2 -1  0  1
-6 -5 -4 -3 -2 -1  0
					*/
  // Strides may be negative or zero as well as positive
  // and the the submatrix should be declared const
  // when the rows overlap as in the above example
  // because the result of any subsequent assignment operation
  // would be undefined if the submatrix appeared on the left hand side.

  // There are also member functions which return the offset

  cout << "T.offset() = " << T.offset() << endl;
					/*
T.offset() = 6
					*/
  // as well as an extent and stride for each dimension.

  cout << "T.extent1() = " << T.extent1() << endl;
  cout << "T.stride1() = " << T.stride1() << endl;
  cout << "T.extent2() = " << T.extent2() << endl;
  cout << "T.stride2() = " << T.stride2() << endl;
					/*
T.extent1() = 7
T.stride1() = 1
T.extent2() = 7
T.stride2() = -1
					*/

  // The double precision real submatrix constructor
  // doubleSubArray2(double* p, Offset o, Extent n2, Stride s2,
  //					  Extent n1, Stride s1)
  // may be used to view an ordinary one dimensional array

  double a[] = { 1.0,  2.0,  3.0,  3.0,
		-5.0,  2.0,  1.0,  1.0,
		 3.0,  4.0,  7.0,  2.0,
		 1.0,  3.0,  5.0,  4.0};

  // of double precision real numbers as a double precision real matrix.

  doubleMatrix		A = doubleSubArray2(a, 0, 4, 4, 4, 1);
  cout << "A =\n" << setw(3, 4) << A;
					/*
A =
  1   2   3   3
 -5   2   1   1
  3   4   7   2
  1   3   5   4
					*/

  // An uninitialized double precision real vector

  doubleVector		v(4);

  // may be initialized using the same operator [] used to initialize
  // an ordinary one dimensional array of double precision real numbers.

  v[0] = +1.0; v[1] = -2.0; v[2] = -1.0; v[3] = +2.0;
  cout << "v = " << setw(3, 4) << v;
					/*
v =   1  -2  -1   2
					*/

  // Member function doubleSubVector::dot(const doubleSubMatrix&)
  // returns a double precision real vector

  doubleVector		b = v.dot(A);	// b = vA^{T}
  cout << "b = " << setw(3, 4) << b;
					/*
b =   0  -8  -8  -2
					*/
  // which is the dot product of a double precision real row vector
  // with each row of a double precision real matrix.

  // Member function doubleSubMatrix::lud(void)
  // returns an offset vector

  offsetVector		p = A.lud();
  cout << "p = " << setw(3, 4) << p;
					/*
p =   1   2   0   3
					*/
  // which represents a permutation of the rows of the matrix.
  // It decomposes the permuted matrix into a lower triangular matrix
  // and an upper triangular matrix in place with the diagonal elements
  // of the lower triangular matrix omitted because they are all ones.

  cout.precision(2);
  cout.setf(ios::fixed);
  cout << "LU =\n" << setw(6, 4) << A;
					/*
LU =
 -5.00   2.00   1.00   1.00
 -0.60   5.20   7.60   2.60
 -0.20   0.46  -0.31   2.00
 -0.20   0.65  -0.75   4.00
					*/

  // The (permuted) LU decomposition can be used to solve b = xA^{T} for x.
  // First, solve bP^{T} = yL^{T} for
  
  doubleVector		y = b.pl(p, A);
  cout << "y = " << setw(6, 4) << y;
					/*
y =  -8.00 -12.80   4.31   8.00
					*/
  // where P is the permutation matrix represented by offset vector p.
  // Then, solve y = xU^{T} for

  doubleVector		x = y.du(A);
  cout << "x = " << setw(5, 4) << x;
					/*
x =  1.00 -2.00 -1.00  2.00
					*/

  // A double precision complex vector

  doubleComplexVector
	z(30, doubleComplex(+1.0, +1.0));

  // may be created with all of the elements initialized by appending
  // an optional double precision complex scalar value to the argument list.

  // Member functions real(void) and imag(void) return a reference
  // to the real and imaginary parts of a double precision complex vector
  // respectively.  Member functions even(void) and odd(void) return
  // a reference to the respective even and odd elements
  // of a double precision real or complex vector.

  z.real().even() = -1.0;
  z.imag().odd()  = -1.0;

  cout << "z =\n" << setw(5, 5) << z;
					/*
z =
(-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00)
( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00)
(-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00)
( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00)
(-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00)
( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00)
					*/

  // Member function dft(int) effects a fast Discrete Fourier Transform
  // on a double precision complex vector of any length in place

  cout << "z.dft(-1)/30.0 =\n" << setw(5, 5) << z.dft(-1)/30.0;
					/*
z.dft(-1)/30.0 =
( 0.00,  0.00) ( 0.00,  0.00) ( 0.00, -0.00) ( 0.00, -0.00) ( 0.00, -0.00)
( 0.00,  0.00) (-0.00,  0.00) ( 0.00,  0.00) ( 0.00,  0.00) ( 0.00, -0.00)
( 0.00,  0.00) (-0.00,  0.00) ( 0.00,  0.00) ( 0.00,  0.00) ( 0.00, -0.00)
(-1.00,  1.00) ( 0.00, -0.00) (-0.00, -0.00) (-0.00, -0.00) (-0.00, -0.00)
( 0.00, -0.00) ( 0.00,  0.00) ( 0.00, -0.00) ( 0.00, -0.00) (-0.00, -0.00)
( 0.00, -0.00) ( 0.00,  0.00) ( 0.00, -0.00) ( 0.00, -0.00) (-0.00, -0.00)
					*/
  // and returns a reference to the original vector.

  cout << "z.dft(+1)/30.0 =\n" << setw(5, 5) << z.dft(+1)/30.0;
					/*
z.dft(+1)/30.0 =
(-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00)
( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00)
(-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00)
( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00)
(-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00)
( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00) (-1.00,  1.00) ( 1.00, -1.00)
					*/
  return 0;
  }

