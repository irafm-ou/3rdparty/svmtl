/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/
/*
	This little program demonstrates how
	an object oriented Fortran programmer
	might use the C++ SVMT class libraries.
	The Fortran programmer views two dimensional arrays
	as collections of column vectors so they naturally
	think of one dimensional arrays as column vectors.
	The C++ SVMT class libraries provide special
	member operators and functions to support this point of view
	but object oriented Fortran programmers are obliged
	to implement their own pseudo constructors
	and input/output operators for matrices.
	This program also shows how the comma operator ,
	might be overloaded to reference both vector v and matrix M
	through an single object (v, M) which may appear
	on either the left or right hand side of an assignment.
*/
#include<svmt.h>

inline
std::ostream&        operator <<(std::ostream& os, const doubleSubMatrix& M) {
  // Column vectors appear as row vectors on output
  // but we output the columns of submatrix E = M^T
  // instead of the columns of submatrix M
  // so that M appears on output instead of M^T.
  const
  doubleSubMatrix&	E = M.t();
  int                   width = os.width();
  for (Offset j = 1; j <= E.extent2(); j++) {
    os << std::setw(width) << E(j);
    }
  return os;
  }

class doubleVasideM {		// adjoin vector V and matrix M
private:
  doubleSubVector&	V;
  doubleSubMatrix&	M;
public:
  doubleVasideM(doubleSubVector& v, doubleSubMatrix& m): V(v), M(m) { }
  doubleVasideM&	operator  =(const doubleSubMatrix& A) {
    V = A(1); M = A.sub_(2, A.extent2()-1, 1);
    return *this; }
  			operator doubleMatrix (void) { return V.aside_(M); }
  };

inline				// vector-matrix adjoin operator ,
doubleVasideM	operator ,(doubleSubVector& v, doubleSubMatrix& M) {
  return doubleVasideM(v, M); }

inline
const				// pseudo constructor
doubleMatrix doubleMatrix_(Extent n, Extent m) {
  return doubleMatrix(m, n); }

int
main(int argc, char* argv[]) {
  using std::cout;
  Extent m = 4;			// the number of rows and
  Extent n = 5;			// the number of columns
  doubleMatrix	A = doubleMatrix_(m, n);
  for (Offset i = 1; i <= m; i++)
    for (Offset j = 1; j <= n; j++)
      A(i, j) = 10*i + j;
  cout << "A =\n" << setw(2, 5) << A;
				/*
A =
11 12 13 14 15
21 22 23 24 25
31 32 33 34 35
41 42 43 44 45
				*/
  doubleVector	v(m);
  doubleMatrix	M = doubleMatrix_(m, n-1);
  (v, M) = A;
  cout << "v^T =\n" << setw(2, 4) << v;
				/*
v^T =
11 21 31 41
				*/
  cout << "M =\n" << setw(2, 4) << M;
				/*
M =
12 13 14 15
22 23 24 25
32 33 34 35
42 43 44 45
				*/
  doubleMatrix	P = (v, M);
  cout << "P = (v, M) =\n" << setw(2, 5) << P;
				/*
P = (v, M) =
11 12 13 14 15
21 22 23 24 25
31 32 33 34 35
41 42 43 44 45
				*/
  return 0;
  }

