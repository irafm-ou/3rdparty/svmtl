/*

This little program fits a polynomial

	y = a_0 + a_1*x + a_2*x^2 + ... + a_m*x^m

of order m to a set of n data pairs {x_j, y_j}
from standard input, prints the coefficients to standard error
and prints the best fit to standard output.

	$ make poly
	$ poly 2 15 < data > fit2
	a =
	-3.44668 1.01307 0.499563
	$ gnuplot
	gnuplot> plot "data", "fit2" with lines
*/

#include<doubleSquare.h>

inline
doubleVector
        solve(const doubleSubVector& b, doubleSubSquare& A) {
  // solve b = xA^T assuming A is a positive definite symmetric matrix
  offsetVector	p = A.lld();	// Cholesky decomposition PAP^T = (LD)(LD)^T
  return b.pld(p, A).dup(A.t(), p);
  // triangular solvers b = y(P^T(LD))^T and y = x((DU)P)^T
 }

int
main(int argc, char* argv[]) {
  using std::cin; using std::cout; using std::cerr;

  Extent m = (1 < argc)? atoi(argv[1]): 1;	// polynomial order
  Extent n = (2 < argc)? atoi(argv[2]): 1;	// number of data points

  doubleMatrix		data(n, 2);		// I/O data pairs
  doubleSubVector	x = data.t()[0];	//  input data
  doubleSubVector	y = data.t()[1];	// output data
  cin >> data;

  doubleMatrix		X(1+m, n);
  X[0] = 1.0;
  for (Offset i = 0; i < m; i++)
    X[1+i] = x*X[i];				// X_ij = (x_j)^i

  doubleVector		a = solve(y.dot(X), X.dot());
  cerr << "a =\n" << a;

  cout << x.above(a.dot(X.t())).t();		// I/O best fit
  return 0;
  }

/*
#include<doubleMatrix.h>

inline
double	f(double x) {
  return ((x + 1.0)*(x + 1.0) - 8.0)/2.0 + 0.125*drand48();
  }

int
main(int argc, char* argv[]) {
  const
  Extent	n = 15;			// number of data points
  doubleVector	x(n);
  (x.ramp() /= 2.0) -= 4.0;		// (-4.0, -3.5, ..., +3.0)
  doubleVector	y = x.apply(f);
  //   (+1/2,   -7/8, -2, -2 7/8, -3 1/2, -3 7/8, -4, -3 7/8,
  //  -3 1/2, -2 7/8, -2,   -7/8,   +1/2, +2 1/8, +4)
  cout << n << endl << x.above(y).t();
  return 0;
  }
*/
