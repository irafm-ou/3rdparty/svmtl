/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/
/*

This little program fits a hyper plane

	y = a_0 + a_1*x_1 + a_2*x_2 + ... + a_{n-1}*x_{n-1}

of dimension n-1 to a set of data {x_i1, x_i2, ..., x_i{n-1}, y_i}
from standard input and prints the coefficients to standard output.

        $ make hyperplane
	$ hyperplane 2 < data
	a =
	-1.24028 0.513509
	$ gnuplot
	gnuplot> y(x) = -1.24028 + 0.513509*x
	gnuplot> plot "data", y(x)

*/

#include<doubleSquare.h>

inline
doubleVector
        solve(const doubleSubVector& b, doubleSubSquare& A) {
  // solve b = xA^T assuming that A is a positive definite symmetric matrix
  offsetVector	p = A.lld();	// Cholesky decomposition PAP^T = (LD)(LD)^T
  return b.pld(p, A).dup(A.t(), p);
  // triangular system solvers b = y(P^T(LD))^T and y = x((DU)P)^T
 }

int
main(int argc, char* argv[]) {

  // The algorithm:

  // For an overdetermined set of equations
  //
  //	Y = aX
  //
  // where
  //
  //	Y = [y_1, y_2, ..., y_i, ..., y_m],
  //
  //	X = [(w_1)^T, (w_2)^T, ..., (w_i)^T, ..., (w_m)^T].
  //
  // and
  //
  //	w_i = [1, x_i1, x_i2, ..., x_ij, ..., x_i{n-1}]
  //
  // find the set of coefficients
  //
  //	a = [a_0, a_1, ..., a_j, ..., a_{n-1}]
  //
  // which represents a least squares best fit
  // of a polynomial of order n to the data pairs {x_i, y_i}.
  // First, post multiply both sides of the equation by X^T
  //
  //	z = YX^T = aXX^T = aS
  // 
  // then solve z = aS for a.
  //
  // The square symmetric matrix
  //
  //	S = S_1 + S_2 + ... + S_i + ... + S_m
  //
  // where the outer products
  //
  //	S_i = ((w_i)^T)w_i
  //
  // and
  //
  //	z = y_1*w_1 + y_2*w_2 + ... + y_i*w_i + ... + y_m*w_m.
  
  // The implementation:

  using std::cin; using std::cout;

  Extent n = (1 < argc)? atoi(argv[1]): 1;	// hyper space dimension

  doubleSquare		S(n, 0.0);		// symmetric matrix
  doubleVector		w(n, 1.0);
  doubleVector		z(n, 0.0);
  doubleSubVector	x = w.sub(1, n-1, 1);
  double		y;
  while (cin >> x && cin >> y) {		// {x_i, y_i}
    S += w.submatrix().t().dot();		// S += ((w_i)^T)w_i
    z += y*w;					// z += y_i*w_i
    }

  doubleVector		a = solve(z, S);	// solve z = aS for a
  cout << "a =\n" << a;

  return 0;
  }
