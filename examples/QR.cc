/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<svmt.h>

int
main(int argc, char* argv[]) {
  using std::ios; using std::cout;

  double a[] = { 1.0,  1.0, -1.0,  4.0,
		-1.0, -1.0,  2.0,  1.0,
		10.0,  3.0,  0.0,  1.0,
		 0.0,  1.0,  0.0, -1.0};
  doubleMatrix		A = doubleSubArray2(a, 0, 4, 4, 4, 1);
  cout << "A =\n" << setw(5, 4) << A;
				/*
A =
    1     1    -1     4
   -1    -1     2     1
   10     3     0     1
    0     1     0    -1
				*/
  doubleVector		b(4);
  b[0] = 10.0; b[1] =  0.0; b[2] = 29.0; b[3] = -3.0; 
  cout << "b =\n" << setw(5, 4) << b;
				/*
b =
   10     0    29    -3
				*/
  offsetVector		p = A.qrd();
  cout << "p = " << p;
				/*
p = 2 3 1 0
				*/
  cout.precision(7);
  cout.setf(ios::fixed);
  cout << "QR =\n" << setw(11, 4) << A;
				/*
QR =
-10.0995049  -3.1684721   0.2970443  -1.2871918
  0.0000000  -1.4002801   1.4702941  -0.6581316
 -0.0497525  -0.2887593  -1.6583124   0.3919647
  0.0497525   0.2887593  -0.1741736  -4.0934538
				*/
  doubleVector		y = b.pq(p, A);
  cout << "y =\n" << setw(11, 4) << y;
				/*
y =
-29.7044263   0.0840168   0.7839295  -8.1869075
				*/
  doubleVector		x = y.du(A);
  cout << "x =\n" << setw(11, 4) << x;
				/*
x =
  3.0000000  -1.0000000   0.0000000   2.0000000
				*/
  doubleMatrix		I(4, 4, 0.0);
  I.diag() = 1.0;

  doubleMatrix		Q = I.pq(p, A);
  cout << "Q =\n" << setw(11, 4) << Q;
				/*
Q =
 -0.0990148  -0.4900980   0.1507557  -0.8528029
  0.0990148   0.4900980  -0.7537784  -0.4264014
 -0.9901475   0.0980196  -0.0904534   0.0426401
  0.0000000  -0.7141428  -0.6331738   0.2984810
				*/
  cout << "QQ^T =\n" << setw(11, 4) << Q.dot(Q);
				/*
QQ^T =
  1.0000000   0.0000000   0.0000000  -0.0000000
  0.0000000   1.0000000  -0.0000000  -0.0000000
  0.0000000  -0.0000000   1.0000000  -0.0000000
 -0.0000000  -0.0000000  -0.0000000   1.0000000
				*/
  return 0;
  }

