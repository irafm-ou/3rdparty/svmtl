// Cartesian complex number class template
template<typename T>
class Complex {
private:
  // Representation
  double	R[2];
public:
  // Constructors
  explicit	Complex(const T& r, const T& i = 0) {
    R[0] = r; R[1] = i; }
  // Functions
  const
  T&		real(void) const { return R[0]; }
  T&		real(void)       { return R[0]; }
  const
  T&		imag(void) const { return R[1]; }
  T&		imag(void)       { return R[1]; }
  // ...
  // Operators
		operator T* (void) { return R; }
  // ...
  };

template<typename T>
  Complex<T>	iconj(const Complex<T>& c) {
    return(c.imag(), c.real()); }

// standard synonyms
typedef Complex<float>		floatComplex;
typedef Complex<double>		doubleComplex;
typedef Complex<long double>	long_doubleComplex;

