/*

This little program reads n values {x_j} from standard input
then prints the average and standard deviation to standard output.

	$ make average
	$ average 15 < values
	1 = average
	4.47214 = standard deviation

*/

#include<doubleVector.h>

int
main(int argc, char* argv[]) {
  using std::cin; using std::cout; using std::cerr; using std::endl;

  Extent n = (1 < argc)? atoi(argv[1]): 0;	// number of values
  if (n < 1) {
    cerr << "Sorry, "
    "the average requires at least one value." << endl;
    return 2;
    }

  doubleVector	x(n);			// input values
  cin >> x;

  double	a = x.sum()/n;
  cout << a << " = average" << endl;

  if (n < 2) {
    cerr << "Sorry, "
    "the standard deviation requires at least two values." << endl;
    return 1;
    }

  double	d = sqrt((x.dot() - n*a*a)/(n - 1));
  cout << d << " = standard deviation" << endl;

  return 0;
  }
