/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/
/*
	The portable reference library contains definitions for
	three non-standard class templates

	  <Type><System>Array1<n>,
	  <Type><System>Array2<m, n> and
	  <Type><System>Array3<l, m, n>

	derived from

	  <Type><System>SubArray1,
	  <Type><System>SubArray2 and
	  <Type><System>SubArray3

	respectively which allocate automatic storage
	(from the program stack) for the underlying
	one dimensional array.  Template parameters l, m and n
	must be constant positive integral expressions
	that can be evaluated at compile time.
	These objects cannot be resized.

	It probably isn't a very good idea to allocate storage
	for large one, two or three dimensional arrays
	from the program stack.  These Array classes should
	be reserved for small objects with fixed sizes
	such as the vector and matrix objects required
	for 3D graphics for example.
*/

#include<doubleArray1.h>
#include<doubleArray2.h>
#include<doubleArray3.h>

int
main(int argc, char* argv[]) {
  const Extent	l = 5;
  const Extent	m = 6;
  const Extent	n = 7;
  doubleArray1<n>	v(17.0);
  std::cout << "v =\n" << setw(2, n) << v;
  doubleArray2<m, n>	M(18.0);
  std::cout << "M =\n" << setw(2, n) << M;
  doubleArray3<l, m, n>	T(19.0);
  std::cout << "T =\n" << setw(2, n) << T;
  return 0;
  }
