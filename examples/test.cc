/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<svmt.h>

class doubleVasideM {
private:
  doubleSubVector&	V;
  doubleSubMatrix&	M;
public:
  doubleVasideM(doubleSubVector& v, doubleSubMatrix& m): V(v), M(m) { }
  doubleVasideM&	operator  =(const doubleSubMatrix& A) {
    V = A(1); M = A.sub_(2, A.extent2()-1, 1);
    return *this; }
  			operator doubleMatrix (void) { return V.aside_(M); }
  };

inline
doubleVasideM	operator ,(doubleSubVector& v, doubleSubMatrix& M) {
  return doubleVasideM(v, M); }
inline
doubleMatrix	operator %(const doubleSubMatrix& L,
			   const doubleSubMatrix& M) { return L.dot(M.t()); }
inline
doubleVector	operator %(const doubleSubVector& u,
			   const doubleSubMatrix& M) { return u.dot(M.t()); }
inline
double		operator |(const doubleSubVector& u,
			   const doubleSubVector& v) { return u.dot(v); }
inline
double		     inner(const doubleSubVector& u,
			   const doubleSubVector& v) { return u.dot(v); }
inline
doubleMatrix	     outer(const doubleSubVector& u,
			   const doubleSubVector& v) {
  return u.submatrix().t().dot(v.submatrix().t()); }

inline
const				// pseudo constructor
doubleMatrix doubleMatrix_(Extent n, Extent m) {
  return doubleMatrix(m, n); }

int
main(int argc, char* argv[]) {
  using std::cout;

  const
  Extent	n = 5;
  doubleVector	v(n, 1.0);
  cout << (v|v) << " = <v|v>\n";
				/*
5 = <v|v>
				*/
  return 0;
  }

