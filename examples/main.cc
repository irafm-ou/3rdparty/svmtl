	// main.cc
	#include"main.h"
	
	int
	main(int argc, char *argv[]) {
	  using std::cerr; using std::endl;
	
	  doubleSquare A(2);
	  A[0][0] = 1.0;  A[0][1] = 0.5;  
	  A[1][0] = 0.5;  A[1][1] = 2.0;
	  cerr << "IN ******************" << endl;
	  cerr << "square symmetric matrix A:\n";
	  cerr << A << endl;
	
	  // compute principal axis matrix P
	  // and rotation to principal axes system
	  doubleSquare P = A;
	  doubleSquare Rot = rot_principal2x2(P, (double)1e-05);
	  cerr << "OUT *****************" << endl;
	  cerr << "matrix in coord. of principal axes:" << endl; 
	  cerr << "P:\n";
	  cerr << P << endl;
	  cerr << "rotation matrix to principal axes:" << endl;
	  cerr << "Rot:\n" << Rot;
	
	  double rad = atan(1.0)*4.0/180.0;
	  double angle = atan(Rot[1][0]/Rot[0][0]);
	  cerr << "with rotation angle: " << angle/rad << " degree" << endl;
	  cerr << endl;
	
	  doubleVector EX(2), EY(2);
	  EX[0] = 1.0; EY[0] = 0.0;
	  EX[1] = 0.0; EY[1] = 1.0;
	  cerr << "axes of principal coord. system:" << endl;
	  cerr << "x-axis: " << EX.dot(Rot);
	  cerr << "y-axis: " << EY.dot(Rot);
	
	  // check result
	  cerr << "check result: Rot*P*Rot^T should be A" << endl;
	  doubleSquare C = Rot.dot(P).dot(Rot);
	  cerr << "(Rot*P*Rot^T) - A:\n" << C - A;
	
	  return 0;
	  }
