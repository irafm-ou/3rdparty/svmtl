/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<svmt.h>
#include<doubleComplexSignal.h>

int
main(int argc, char* argv[]) {
  using std::ios; using std::cout;

  cout.precision(2);
  cout.setf(ios::fixed);

  Extent		p = 6;
  Extent		q = 5;
  Extent		n = p*q;
  cout << n << " = n\n";
  doubleVector		v(n, 1.0);
  v.odd() = -1.0;
  cout << "v =\n" << setw(5, q) << v;
  double1DCRIDFT	idft(n);
  double1DRCDDFT	ddft(n);
  cout.setf(ios::scientific);
  cout.precision(7);
  cout << "ddft(v)/n =\n" << setw(13, q) << ddft(v)/n;
  ddft(v);
  cout << "ddft(v)/n =\n" << setw(13, q) << v;
  cout << "idft(v)/n =\n" << setw(13, q) << idft(v)/n;
  return 0;
  }

