/*
The C++ Scalar, Vector, Matrix and Tensor classes.
Copyright (C) 1998 E. Robert Tisdale

This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
This library is free software which you can redistribute and/or modify
under the terms of the GNU Library General Public License
as published by the Free Software Foundation;
either version 2, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Library General Public License
along with this library.  If not, write to the Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Written by E. Robert Tisdale
*/

#include<svmt.h>

int
main(int argc, char* argv[]) {
  using std::ios; using std::cout;

  double a[] = { 1,  0, -1,  0,
		 1,  5, -3,  3,
		 2,  4,  1,  7,
		 3,  2,  0,  5};
  doubleMatrix		A = doubleSubArray2(a, 0, 4, 4, 4, 1);
  cout << "A =\n" << setw(3, 4) << A;
				/*
A =
  1   0  -1   0
  1   5  -3   3
  2   4   1   7
  3   2   0   5
				*/
  doubleVector		b(4, 0.0);
  cout << "b = " << setw(3, 4) << b;
				/*
b =   0   0   0   0
				*/
  offsetVector		p = A.lud();
  cout << "p = " << setw(3, 4) << p;
				/*
p =   3   1   2   0
				*/
  cout.precision(2);
  cout.setf(ios::fixed);
  cout << "LU =\n" << setw(6, 4) << A;
				/*
LU =
  3.00   2.00   0.00   5.00
  0.33   4.33  -3.00   1.33
  0.67   0.62   2.85   2.85
  0.33  -0.15  -0.51   0.00
				*/
  doubleVector		y = b.pl(p, A);
  cout << "y = " << setw(6, 4) << y;
				/*
y =   0.00   0.00   0.00   0.00
				*/
  doubleVector		x = y.du(A);
  cout << "x = " << setw(5, 4) << x;
				/*
x =  0.00  0.00  0.00  0.00
				*/
  return 0;
  }

