// The C++ Scalar, Vector, Matrix and Tensor classes.
// Copyright (C) 1998 E. Robert Tisdale
// 
// This file is part of The C++ Scalar, Vector, Matrix and Tensor classes.
// This library is free software which you can redistribute and/or modify
// under the terms of the GNU Library General Public License
// as published by the Free Software Foundation;
// either version 2, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU Library General Public License
// along with this library.  If not, write to the Free Software Foundation,
// Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
// 
// Written by E. Robert Tisdale

#include <svmt.h>

doubleComplexVector
	XCS(const doubleSubVector& v, const double s, const double t = 0) {
  doubleComplexVector	result(v.length());
  result.imag().ramp() *= s;		// s*[0: v.length() - 1]
  result.real()  = cos(result.imag());
  result.imag()  = sin(result.imag());
  return v*result; }

inline
double	mean(const doubleVector& v) { return v.sum()/v.length(); }

inline
doubleComplexVector			// solve b = xA^T
	solve(const doubleComplexVector& b, doubleComplexMatrix& A) {
  return b.pl(A.lud(), A).du(A); }

// The adaptive beamformer is part of the MountainTop code
// written in MATLAB by Yaron Seliktar at Georgia Tech. 
// The original MATLAB code appears in C++ comments
// immediately after the equivalent SVMT code.

doubleComplexVector adaptive(
  const doubleComplexSubMatrix& X,	// x[MN][L]
  const double& d,			// Dn
  const double& f,			// Fn
  const doubleSubVector& sw,		//  spatial_window[N]
  const doubleSubVector& tw) {		// temporal_window[M]
  if (X.extent2() != tw.length()*sw.length()) {
					// Handle the error.
    }
  doubleComplexVector	  t = XCS(tw, 2.0*M_PI*f);
		// temporal = temporal_window .* exp(j*2*pi*Fn*[0: M-1]')
  doubleComplexVector	  s = XCS(sw, 2.0*M_PI*d);
		//  spatial =  spatial_window .* exp(j*2*pi*Dn*[0: N-1]')
  doubleComplexVector	  v = t.kron(s);// steer = kron(temporal, spatial)
  doubleComplexMatrix	  R = X.dot(conj(X))/X.extent1();
					// R = x*x'/L
  R.diag() += 1.0e-12*max(abs(R.diag().real()));
					// R = R + noisef*eye(size(R))
  return solve(v/mean(abs(v)), R);	// weight = R\(steer/mean(abs(steer)))
  }

