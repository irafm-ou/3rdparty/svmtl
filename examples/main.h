	// main.h
	#include<doubleSquare.h>
	
	doubleSquare rot_principal2x2(doubleSubSquare& A,
	  const double& epsilon = (double)1e-06) {
	
	  doubleSquare	D(2, 2);// rotation matrix to principal axes
	
	  double	delta = (A[0][0] - A[1][1])/2.0;
	  // transform A to principal axes
	  if (abs(A[1][0]) > epsilon*abs(delta)) {
	    double root = sqrt(delta*delta + A[1][0]*A[1][0]);
	    double average = (A[0][0] + A[1][1])/2.0;
	    double a = atan((root - delta)/A[0][1]);
	    
	    A[0][0] = average + root;
	    A[0][1] = A[1][0] = 0.0;
	    A[1][1] = average - root;
	    D[0][0] = +(D[1][1] = cos(a));
	    D[0][1] = -(D[1][0] = sin(a));
	    }
	  else {
	    A[0][1] = A[1][0] = 0.0;
	    D[0][0] = D[1][1] = 1.0;
	    D[0][1] = D[1][0] = 0.0;
	    }
	
	  return D;		// rotation matrix to principal axes
	  }
